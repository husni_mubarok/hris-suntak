-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2017 at 11:36 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `msul`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'read', 'read', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'create', 'create', 0, NULL, NULL, NULL, NULL, NULL),
(3, 'update', 'update', 0, NULL, NULL, NULL, NULL, NULL),
(4, 'delete', 'delete', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_invoice`
--

CREATE TABLE `ap_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `ap_reference` varchar(50) DEFAULT NULL,
  `ap_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_invoice`
--

INSERT INTO `ap_invoice` (`id`, `ap_reference`, `ap_date`, `supplier_id`, `paytype_id`, `company_id`, `paydue`, `currency`, `top`, `remark`, `status`, `attachment`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PRCX-24091700001', '2017-09-24', 1, 0, 1, 0, '', 0, '', 0, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, NULL, '2017-09-23 17:00:00', '2017-10-12 13:33:51', NULL),
(2, 'PRCX-24091700002', '2017-09-24', 0, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', NULL, NULL),
(3, 'APCX-24091700003', '2017-09-24', 0, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', NULL, NULL),
(4, 'APCX-24091700004', '2017-09-24', 0, 0, NULL, 0, '', 0, '', 0, '24-09-2017 01-13-0954eb9e632b7c7_-_peruvian-lily-pink-yellow-xl.jpg', 1, 1, NULL, '2017-09-23 17:00:00', '2017-09-24 06:13:09', NULL),
(5, 'APCX-24091700005', '2017-09-24', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', '2017-09-24 06:13:25', NULL),
(6, 'APCX-26091700006', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:26:14', NULL),
(7, 'APCX-26091700007', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:26:32', NULL),
(8, 'APCX-26091700008', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:26:54', NULL),
(9, 'APCX-26091700009', '2017-09-26', 0, 0, NULL, 0, '', 0, '', 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:27:28', NULL),
(10, 'APCX-01101700010', '2017-10-13', 2, 0, NULL, 0, '', 0, '', 0, NULL, 1, 1, NULL, '2017-09-30 17:00:00', '2017-09-30 16:14:43', NULL),
(11, 'APCX-08101700011', '2017-10-08', 2, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 17:21:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_invoice_detail`
--

CREATE TABLE `ap_invoice_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `ap_id` int(10) UNSIGNED DEFAULT NULL,
  `prec_d_id` int(10) UNSIGNED DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `cp_amount` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_invoice_detail`
--

INSERT INTO `ap_invoice_detail` (`id`, `ap_id`, `prec_d_id`, `amount`, `cp_amount`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 4, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-24 05:09:28', '2017-09-24 05:09:28', NULL),
(4, 4, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-24 05:09:28', '2017-09-24 05:09:28', NULL),
(5, 5, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-24 06:13:25', '2017-09-24 06:13:25', NULL),
(6, 5, 1, 5, 26, NULL, NULL, NULL, '2017-09-24 06:13:25', '2017-09-24 06:13:25', NULL),
(7, 6, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-25 23:26:15', '2017-09-25 23:26:15', NULL),
(8, 7, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-25 23:26:32', '2017-09-25 23:26:32', NULL),
(9, 7, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-25 23:26:32', '2017-09-25 23:26:32', NULL),
(10, 8, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-25 23:26:54', '2017-09-25 23:26:54', NULL),
(11, 9, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-25 23:27:17', '2017-09-25 23:27:17', NULL),
(12, 10, 7, 150000, 170000, NULL, NULL, NULL, '2017-09-30 16:14:37', '2017-09-30 16:14:37', NULL),
(13, 11, 13, 1, 1, NULL, NULL, NULL, '2017-10-07 17:21:52', '2017-10-07 17:21:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_payment_detail`
--

CREATE TABLE `ap_payment_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `cp_id` int(10) UNSIGNED DEFAULT NULL,
  `ap_d_id` int(10) UNSIGNED DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_payment_detail`
--

INSERT INTO `ap_payment_detail` (`id`, `cp_id`, `ap_d_id`, `account_id`, `amount`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 4, 2, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 05:09:28', '2017-09-24 05:09:28', NULL),
(4, 4, 1, NULL, 108000, NULL, NULL, NULL, '2017-09-24 05:09:28', '2017-09-24 05:09:28', NULL),
(5, 5, 2, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 06:13:25', '2017-09-24 06:13:25', NULL),
(6, 5, 1, NULL, 5, NULL, NULL, NULL, '2017-09-24 06:13:25', '2017-09-25 02:26:22', '2017-09-25 02:26:22'),
(7, 13, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 11:08:46', '2017-09-24 11:08:46', NULL),
(8, 13, 6, NULL, 5, NULL, NULL, NULL, '2017-09-24 11:08:46', '2017-09-25 02:28:55', '2017-09-25 02:28:55'),
(9, 1, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 11:08:02', '2017-09-25 11:08:02', NULL),
(10, 1, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 11:08:02', '2017-09-25 11:14:06', '2017-09-25 11:14:06'),
(11, 1, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 11:13:54', '2017-09-25 11:14:10', '2017-09-25 11:14:10'),
(12, 1, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 11:13:55', '2017-09-25 11:14:02', '2017-09-25 11:14:02'),
(13, 2, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 11:17:34', '2017-09-25 11:17:34', NULL),
(14, 2, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 11:17:34', '2017-09-25 11:17:34', NULL),
(15, 3, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 11:17:50', '2017-09-25 11:17:50', NULL),
(16, 5, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 23:33:05', '2017-09-25 23:33:05', NULL),
(17, 6, 7, NULL, 108000, NULL, NULL, NULL, '2017-09-27 23:03:59', '2017-09-27 23:03:59', NULL),
(18, 8, 12, NULL, 150000, NULL, NULL, NULL, '2017-09-30 16:15:12', '2017-09-30 16:15:12', NULL),
(19, 9, 12, NULL, 20000, NULL, NULL, NULL, '2017-10-07 17:44:34', '2017-10-07 17:44:34', NULL),
(20, 9, 13, NULL, 1, NULL, NULL, NULL, '2017-10-07 17:44:34', '2017-10-07 17:44:34', NULL),
(21, 10, 6, NULL, 1, NULL, NULL, NULL, '2017-10-07 17:45:26', '2017-10-07 17:45:26', NULL);

--
-- Triggers `ap_payment_detail`
--
DELIMITER $$
CREATE TRIGGER `delete_app` AFTER DELETE ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (OLD.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (OLD.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (OLD.ap_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_app` AFTER INSERT ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (NEW.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (NEW.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (NEW.ap_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_app` AFTER UPDATE ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (NEW.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (NEW.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (NEW.ap_d_id);

END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ar_receive_detail`
--

CREATE TABLE `ar_receive_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `cr_id` int(10) UNSIGNED DEFAULT NULL,
  `ar_id` int(10) UNSIGNED DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_receive_detail`
--

INSERT INTO `ar_receive_detail` (`id`, `cr_id`, `ar_id`, `account_id`, `amount`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 17, 2, NULL, 100, NULL, NULL, NULL, '2017-10-07 19:04:42', '2017-10-07 19:04:42', NULL),
(2, 18, 2, NULL, 900, NULL, NULL, NULL, '2017-10-07 19:09:33', '2017-10-07 19:09:33', NULL);

--
-- Triggers `ar_receive_detail`
--
DELIMITER $$
CREATE TRIGGER `delete_arr` AFTER DELETE ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (OLD.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (OLD.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (OLD.ar_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_arr` AFTER INSERT ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (NEW.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (NEW.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (NEW.ar_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_arr` AFTER UPDATE ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (NEW.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (NEW.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (NEW.ar_id);

END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cash_payment`
--

CREATE TABLE `cash_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `cp_reference` varchar(50) DEFAULT NULL,
  `cp_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_payment`
--

INSERT INTO `cash_payment` (`id`, `cp_reference`, `cp_date`, `supplier_id`, `paytype_id`, `so_id`, `account_id`, `currency`, `remark`, `company_id`, `status`, `attachment`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CPCX-26091700001', '2017-09-26', 0, 3, 5, 8, '', '', 1, 0, NULL, 1, 1, NULL, '2017-09-25 17:00:00', '2017-11-04 02:17:36', NULL),
(2, 'CPCX-26091700002', '2017-09-26', 1, 0, NULL, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 11:17:34', NULL),
(3, 'CPCX-26091700003', '2017-09-26', 3, 0, NULL, 14, '', '', NULL, 0, '25-09-2017 10-13-35fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 15:13:54', NULL),
(4, 'CPPY-26091700004', '2017-09-26', 0, 4, NULL, 12, '', '', NULL, 0, '26-09-2017 12-45-26fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 17:45:26', NULL),
(5, 'CPCX-26091700005', '2017-09-26', 1, 1, NULL, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:33:10', NULL),
(6, 'CPCX-28091700006', '2017-09-28', 1, 1, NULL, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-09-27 17:00:00', '2017-09-27 23:04:04', NULL),
(7, 'CPPY-28091700007', '2017-09-28', 0, 0, NULL, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-27 17:00:00', NULL, NULL),
(8, 'CPCX-01101700008', '2017-10-01', 2, 3, NULL, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 16:15:21', NULL),
(9, 'CPCX-08101700009', '2017-10-08', 2, 0, NULL, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 17:44:34', NULL),
(10, 'CPCX-08101700010', '2017-10-08', 1, 0, NULL, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 17:45:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cash_payment_detail`
--

CREATE TABLE `cash_payment_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `cp_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `debt` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_payment_detail`
--

INSERT INTO `cash_payment_detail` (`id`, `cp_id`, `description`, `account_id`, `debt`, `credit`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Test', 0, 1000000, 0, NULL, NULL, NULL, '2017-09-25 14:45:16', '2017-09-25 14:55:29', '2017-09-25 14:55:29'),
(2, 3, 'DASDASD', 0, 23123123, 12, NULL, NULL, NULL, '2017-09-25 14:48:05', '2017-09-25 14:55:24', '2017-09-25 14:55:24'),
(3, 3, 'ASSaS', 13, 23133, 213, NULL, NULL, NULL, '2017-09-25 14:49:28', '2017-09-25 14:55:12', '2017-09-25 14:55:12'),
(4, 3, 'sadad', 8, 342342, 23, NULL, NULL, NULL, '2017-09-25 14:52:08', '2017-09-25 14:55:09', '2017-09-25 14:55:09'),
(5, 3, 'sasASSas', 8, 20000, 0, NULL, NULL, NULL, '2017-09-25 14:53:10', '2017-09-25 14:55:27', '2017-09-25 14:55:27'),
(6, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 14:55:02', '2017-09-25 14:55:19', '2017-09-25 14:55:19'),
(7, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 14:55:05', '2017-09-25 14:55:22', '2017-09-25 14:55:22'),
(8, 3, 'Test transaction', 14, 500000, 222222, NULL, NULL, NULL, '2017-09-25 14:56:53', '2017-09-25 15:13:24', '2017-09-25 15:13:24'),
(9, 3, 'Test', 11, 500000, 2000000, NULL, NULL, NULL, '2017-09-25 15:10:29', '2017-09-25 15:21:32', NULL),
(10, 3, 'Test', 7, 300000, 500000, NULL, NULL, NULL, '2017-09-25 15:11:16', '2017-09-25 15:11:16', NULL),
(11, 4, 'Test', 6, 500000, 0, NULL, NULL, NULL, '2017-09-25 17:45:08', '2017-09-25 17:45:08', NULL),
(12, 4, 'Test 1', 14, 0, 3000000, NULL, NULL, NULL, '2017-09-25 17:45:22', '2017-09-25 17:45:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cash_receive`
--

CREATE TABLE `cash_receive` (
  `id` int(10) UNSIGNED NOT NULL,
  `cr_reference` varchar(50) DEFAULT NULL,
  `cr_date` date DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_receive`
--

INSERT INTO `cash_receive` (`id`, `cr_reference`, `cr_date`, `customer_id`, `paytype_id`, `account_id`, `currency`, `company_id`, `remark`, `status`, `attachment`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CPCX-26091700001', '2017-09-26', 0, 3, 6, '', 1, '', 0, NULL, 1, 1, NULL, '2017-09-25 17:00:00', '2017-10-07 17:57:54', NULL),
(2, 'CPCX-26091700002', '2017-09-26', 1, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 11:17:34', NULL),
(3, 'CPCX-26091700003', '2017-09-26', 3, 0, 14, '', NULL, '', 0, '25-09-2017 10-13-35fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 15:13:54', NULL),
(4, 'CPPY-26091700004', '2017-09-26', 0, 4, 12, '', NULL, '', 0, '26-09-2017 12-45-26fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 17:45:26', NULL),
(5, 'CRPY-26091700005', '2017-09-26', 0, 4, 7, '', NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 18:13:43', NULL),
(6, 'CRPY-28091700006', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 9, NULL, 1, 1, NULL, '2017-09-27 17:00:00', '2017-10-07 18:58:08', NULL),
(7, 'CRPY-28091700007', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-27 17:00:00', NULL, NULL),
(8, 'CRPY-28091700008', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-27 17:00:00', NULL, NULL),
(9, 'CPCX-08101700009', '2017-10-08', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(10, 'CPCX-08101700010', '2017-10-08', 2, 1, NULL, '', 1, '', 0, NULL, 1, 1, NULL, '2017-10-07 17:00:00', '2017-10-07 18:50:55', NULL),
(11, 'CPCX-08101700011', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 18:36:14', NULL),
(12, 'CPCX-08101700012', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 18:37:34', NULL),
(13, 'CPCX-08101700013', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 18:39:04', NULL),
(14, 'CPCX-08101700014', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 18:49:15', NULL),
(15, 'CPCX-08101700015', '2017-10-08', 2, 1, NULL, '', NULL, '', 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 18:49:44', NULL),
(16, 'CPCX-08101700016', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 19:03:49', NULL),
(17, 'CPCX-08101700017', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 19:04:42', NULL),
(18, 'CPCX-08101700018', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 19:09:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cash_receive_detail`
--

CREATE TABLE `cash_receive_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `cr_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `debt` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_receive_detail`
--

INSERT INTO `cash_receive_detail` (`id`, `cr_id`, `description`, `account_id`, `debt`, `credit`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Test', 0, 1000000, 0, NULL, NULL, NULL, '2017-09-25 14:45:16', '2017-09-25 14:55:29', '2017-09-25 14:55:29'),
(2, 3, 'DASDASD', 0, 23123123, 12, NULL, NULL, NULL, '2017-09-25 14:48:05', '2017-09-25 14:55:24', '2017-09-25 14:55:24'),
(3, 3, 'ASSaS', 13, 23133, 213, NULL, NULL, NULL, '2017-09-25 14:49:28', '2017-09-25 14:55:12', '2017-09-25 14:55:12'),
(4, 3, 'sadad', 8, 342342, 23, NULL, NULL, NULL, '2017-09-25 14:52:08', '2017-09-25 14:55:09', '2017-09-25 14:55:09'),
(5, 3, 'sasASSas', 8, 20000, 0, NULL, NULL, NULL, '2017-09-25 14:53:10', '2017-09-25 14:55:27', '2017-09-25 14:55:27'),
(6, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 14:55:02', '2017-09-25 14:55:19', '2017-09-25 14:55:19'),
(7, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 14:55:05', '2017-09-25 14:55:22', '2017-09-25 14:55:22'),
(8, 3, 'Test transaction', 14, 500000, 222222, NULL, NULL, NULL, '2017-09-25 14:56:53', '2017-09-25 15:13:24', '2017-09-25 15:13:24'),
(9, 3, 'Test', 11, 500000, 2000000, NULL, NULL, NULL, '2017-09-25 15:10:29', '2017-09-25 15:21:32', NULL),
(10, 3, 'Test', 7, 300000, 500000, NULL, NULL, NULL, '2017-09-25 15:11:16', '2017-09-25 15:11:16', NULL),
(11, 4, 'Test', 6, 500000, 0, NULL, NULL, NULL, '2017-09-25 17:45:08', '2017-09-25 17:45:08', NULL),
(12, 4, 'Test 1', 14, 0, 3000000, NULL, NULL, NULL, '2017-09-25 17:45:22', '2017-09-25 17:45:22', NULL),
(13, 5, 'Test', 10, 50, 900000, NULL, NULL, NULL, '2017-09-25 18:13:33', '2017-09-25 18:13:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cash_transfer`
--

CREATE TABLE `cash_transfer` (
  `id` int(10) UNSIGNED NOT NULL,
  `ct_reference` varchar(50) DEFAULT NULL,
  `ct_date` date DEFAULT NULL,
  `account_from_id` int(11) DEFAULT NULL,
  `account_to_id` int(11) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_transfer`
--

INSERT INTO `cash_transfer` (`id`, `ct_reference`, `ct_date`, `account_from_id`, `account_to_id`, `currency`, `amount`, `remark`, `status`, `company_id`, `attachment`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PRCX-24091700001', '2017-09-24', 5, 5, NULL, 34234, '', 0, 1, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, NULL, '2017-09-23 17:00:00', '2017-10-07 17:59:05', NULL),
(2, 'PRCX-24091700002', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', NULL, NULL),
(3, 'APCX-24091700003', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', NULL, NULL),
(4, 'APCX-24091700004', '2017-09-24', NULL, NULL, NULL, NULL, '', 0, NULL, '24-09-2017 01-13-0954eb9e632b7c7_-_peruvian-lily-pink-yellow-xl.jpg', 1, 1, NULL, '2017-09-23 17:00:00', '2017-09-24 06:13:09', NULL),
(5, 'APCX-24091700005', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', '2017-09-24 06:13:25', NULL),
(6, 'CTTR-26091700006', '2017-09-26', 12, 10, NULL, 50000000, 'qweqwqeqw', 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 19:07:18', NULL),
(7, 'CTTR-26091700007', '2017-09-26', 6, 8, NULL, 350000, '', 0, NULL, '26-09-2017 05-59-10fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 22:59:10', NULL),
(8, 'CTTR-26091700008', '2017-09-26', 5, 10, NULL, 5000000, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 19:24:59', NULL),
(9, 'CTTR-26091700009', '2017-09-26', 5, 8, NULL, 450000, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 21:05:12', NULL),
(10, 'CTTR-28091700010', '2017-09-28', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-27 17:00:00', NULL, NULL),
(11, 'CTTR-01101700011', '2017-10-01', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-30 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE `delivery_order` (
  `do_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `do_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `do_date` date DEFAULT NULL,
  `do_delivery_fee` decimal(25,10) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `do_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `approved_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_order`
--

INSERT INTO `delivery_order` (`do_id`, `so_id`, `do_number`, `so_reference`, `customer_id`, `warehouse_id`, `do_date`, `do_delivery_fee`, `notes`, `do_status`, `company_id`, `created_by`, `approved_by`, `updated_by`, `deleted_by`, `created_at`, `approved_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'DO/MSU/201710/0001', NULL, 2, 1, '0000-00-00', '222.0000000000', '', 'DRAFT', NULL, 1, NULL, NULL, NULL, '2017-09-16 16:08:32', NULL, '2017-09-16 16:08:32', '2017-09-16 23:09:17'),
(2, 1, 'DO/MSU/201710/0002', NULL, 2, 1, '0000-00-00', '255.0000000000', '', '', NULL, 1, NULL, NULL, NULL, '2017-09-16 16:10:33', NULL, '2017-10-07 09:32:45', NULL),
(3, 1, 'DO/MSU/201710/0003', NULL, 2, 1, '0000-00-00', '2000.0000000000', '', '', NULL, 1, NULL, NULL, NULL, '2017-09-16 16:32:34', NULL, '2017-10-07 09:33:00', NULL),
(4, 2, 'DO/MSU/201710/0004', NULL, 5, 1, '1970-01-01', '100000.0000000000', '', 'COMPLETED', NULL, 1, 1, 1, NULL, '2017-09-26 00:03:42', '2017-10-06 17:00:00', '2017-10-07 09:47:01', NULL),
(5, 2, 'DO/MSU/201710/0005', NULL, 5, 1, '1970-01-01', '100000.0000000000', '', 'DRAFT', NULL, 1, NULL, 1, NULL, '2017-09-26 00:07:55', NULL, '2017-09-26 00:09:28', NULL),
(6, 1, 'DO/MSU/201710/0001', NULL, 2, 1, '0000-00-00', '50000.0000000000', '', 'APPROVAL', NULL, 1, NULL, NULL, NULL, '2017-10-05 01:05:38', NULL, '2017-10-07 09:16:48', NULL),
(7, 4, 'DO/MSU/201710/0001', NULL, 3, 1, '2017-10-25', '0.0000000000', '', 'DRAFT', 1, 1, NULL, NULL, NULL, '2017-10-21 23:17:14', NULL, '2017-10-21 23:17:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order_detail`
--

CREATE TABLE `delivery_order_detail` (
  `do_d_id` int(10) UNSIGNED NOT NULL,
  `do_id` int(11) DEFAULT NULL,
  `so_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `delivery_quantity` decimal(25,10) DEFAULT NULL,
  `do_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_order_detail`
--

INSERT INTO `delivery_order_detail` (`do_d_id`, `do_id`, `so_d_id`, `item_id`, `delivery_quantity`, `do_status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 4, 1, '1.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:10:33', '2017-09-16 16:10:33', NULL),
(2, 2, 5, 3, '1.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:10:33', '2017-09-16 16:10:33', NULL),
(3, 2, 6, 2, '1.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:10:33', '2017-09-16 16:10:33', NULL),
(4, 3, 4, 1, '5.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:32:34', '2017-09-16 16:32:34', NULL),
(5, 3, 5, 3, '77.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:32:34', '2017-09-16 16:32:34', NULL),
(6, 3, 6, 2, '69.0000000000', NULL, 1, NULL, NULL, '2017-09-16 16:32:34', '2017-09-16 16:32:34', NULL),
(7, 4, NULL, 1, '500.0000000000', NULL, 1, NULL, 1, '2017-09-26 00:03:42', '2017-09-26 00:05:18', '2017-09-26 00:05:18'),
(8, 4, NULL, 1, '200.0000000000', NULL, 1, NULL, 1, '2017-09-26 00:05:18', '2017-09-26 00:05:44', '2017-09-26 00:05:44'),
(9, 4, NULL, 1, '500.0000000000', NULL, 1, NULL, NULL, '2017-09-26 00:05:44', '2017-09-26 00:05:44', NULL),
(10, 5, NULL, 1, '10000.0000000000', NULL, 1, NULL, 1, '2017-09-26 00:07:56', '2017-09-26 00:09:28', '2017-09-26 00:09:28'),
(11, 5, NULL, 1, '500.0000000000', NULL, 1, NULL, NULL, '2017-09-26 00:09:28', '2017-09-26 00:09:28', NULL),
(12, 6, NULL, 1, '2.0000000000', NULL, 1, NULL, NULL, '2017-10-05 01:05:38', '2017-10-05 01:05:38', NULL),
(13, 6, NULL, 3, '5.0000000000', NULL, 1, NULL, NULL, '2017-10-05 01:05:38', '2017-10-05 01:05:38', NULL),
(14, 6, NULL, 2, '6.0000000000', NULL, 1, NULL, NULL, '2017-10-05 01:05:38', '2017-10-05 01:05:38', NULL),
(15, 7, NULL, 2, '1.0000000000', NULL, 1, NULL, NULL, '2017-10-21 23:17:14', '2017-10-21 23:17:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_user_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_15_100000_create_module_table', 1),
(4, '2017_03_15_100001_create_action_table', 1),
(5, '2017_03_15_100002_create_privilege_table', 1),
(6, '2017_07_09_100123_create_mst_company_table', 1),
(7, '2017_07_09_100134_create_mst_account_table', 1),
(8, '2017_07_09_100136_create_mst_customer_table', 1),
(9, '2017_07_09_100137_create_mst_supplier_table', 1),
(10, '2017_07_09_100139_create_mst_department_table', 1),
(11, '2017_07_09_100142_create_mst_item_table', 1),
(12, '2017_07_09_100144_create_mst_term_table', 1),
(13, '2017_07_09_100147_create_mst_warehouse_table', 1),
(14, '2017_07_09_100150_create_mst_bom_table', 2),
(15, '2017_07_09_100152_create_mst_bom_detail_table', 2),
(16, '2017_07_09_100155_create_sales_order_table', 3),
(17, '2017_07_09_100158_create_sales_order_detail_table', 3),
(18, '2017_09_16_151017_create_stock_movement_table', 3),
(19, '2017_09_16_151049_create_stock_inventory_table', 3),
(20, '2017_09_16_201922_create_delivery_order_table', 4),
(21, '2017_09_16_201938_create_delivery_order_detail_table', 4),
(22, '2017_09_18_145256_create_sales_invoice_table', 5),
(23, '2017_09_19_153704_create_refund_order_table', 6),
(24, '2017_09_19_153721_create_refund_order_detail_table', 6),
(25, '2017_09_23_095508_create_module_group_table', 7),
(26, '2017_09_23_150907_create_production_table', 7),
(27, '2017_09_23_150924_create_production_detail_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `module_group`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'user', 'user', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'admin', 'module', 'module', 0, NULL, NULL, NULL, NULL, NULL),
(3, 'admin', 'action', 'action', 0, NULL, NULL, NULL, NULL, NULL),
(4, 'admin', 'privilege', 'privilege', 0, NULL, NULL, NULL, NULL, NULL),
(5, 'admin', 'mst_company', 'Master Company', 0, 1, NULL, NULL, '2017-10-07 10:15:31', NULL),
(6, NULL, 'mst_account', 'Master Account', 0, 1, NULL, NULL, '2017-10-02 07:25:26', NULL),
(7, NULL, 'mst_bom', 'Bill of Material', 0, 1, NULL, NULL, '2017-10-02 07:25:40', NULL),
(8, NULL, 'mst_customer', 'Master Customer', 0, 1, NULL, NULL, '2017-10-02 07:25:53', NULL),
(9, NULL, 'mst_supplier', 'Master Supplier', 0, 1, NULL, NULL, '2017-10-02 07:26:05', NULL),
(10, NULL, 'mst_department', 'Master Department', 0, 1, NULL, NULL, '2017-10-02 07:26:16', NULL),
(11, NULL, 'mst_warehouse', 'Master Warehouse', 0, 1, NULL, NULL, '2017-10-02 07:26:31', NULL),
(12, NULL, 'mst_item', 'Master Item', 0, 1, NULL, NULL, '2017-10-02 07:26:42', NULL),
(13, NULL, 'mst_term', 'Payment Type', 0, 1, NULL, NULL, '2017-10-02 07:27:26', NULL),
(14, NULL, 'purchase_order', 'PO', 0, 1, NULL, NULL, '2017-10-02 07:27:39', NULL),
(15, NULL, 'receive_order', 'Receiving', 0, 1, NULL, NULL, '2017-10-02 07:27:50', NULL),
(16, NULL, 'purchase_invoice', 'Purchase Invoice', 0, 1, NULL, NULL, '2017-10-02 07:28:03', NULL),
(17, NULL, 'return_order', 'Return Order', 0, 1, NULL, NULL, '2017-10-02 07:28:18', NULL),
(18, NULL, 'sales_order', 'SO', 0, 1, NULL, NULL, '2017-10-02 07:28:28', NULL),
(19, NULL, 'delivery_order', 'Shipping', 0, 1, NULL, NULL, '2017-10-02 07:28:39', NULL),
(20, NULL, 'sales_invoice', 'Sales Invoice', 0, 1, NULL, NULL, '2017-10-02 07:28:50', NULL),
(21, NULL, 'refund_order', 'Refund Order', 0, 1, NULL, NULL, '2017-10-02 07:29:02', NULL),
(22, NULL, 'production', 'Work Order', 0, 1, NULL, NULL, '2017-10-02 07:29:16', NULL),
(23, NULL, 'production_qc', 'Convert Work Order', 0, 1, NULL, NULL, '2017-10-02 07:29:29', NULL),
(24, NULL, 'production_reject', 'Reject', 0, 1, NULL, NULL, '2017-10-02 07:29:38', NULL),
(25, NULL, 'inventory', 'inventory', 0, NULL, NULL, NULL, NULL, NULL),
(26, NULL, 'stock_opname', 'Stock Opname', 0, 1, NULL, NULL, '2017-10-03 09:27:12', NULL),
(27, NULL, 'inventory_mutation', 'Transfer Stock', 0, 1, NULL, NULL, '2017-10-02 07:30:14', NULL),
(28, NULL, 'stock_adjustment', 'Stock Adjustment', 0, 1, NULL, NULL, '2017-10-03 07:19:07', NULL),
(29, NULL, 'report', 'Report', 0, 1, NULL, NULL, '2017-10-02 07:30:28', NULL),
(30, NULL, 'purchase_advance', 'purchase_advance', 1, NULL, NULL, '2017-09-30 16:07:55', '2017-09-30 16:07:55', NULL),
(31, NULL, 'purchase_request', 'purchase_request', 1, NULL, NULL, '2017-09-30 16:08:05', '2017-09-30 16:08:05', NULL),
(32, NULL, 'purchase_return', 'purchase_return', 1, NULL, NULL, '2017-09-30 16:08:15', '2017-09-30 16:08:15', NULL),
(33, NULL, 'purchase_receive', 'purchase_receive', 1, NULL, NULL, '2017-09-30 16:08:25', '2017-09-30 16:08:25', NULL),
(34, NULL, 'ap_invoice', 'ap_invoice', 1, NULL, NULL, '2017-09-30 16:09:14', '2017-09-30 16:09:14', NULL),
(35, NULL, 'ar_receive', 'ar_receive', 1, NULL, NULL, '2017-09-30 16:09:28', '2017-09-30 16:09:28', NULL),
(36, NULL, 'ap_payment', 'ap_payment', 1, NULL, NULL, '2017-09-30 16:09:38', '2017-09-30 16:09:38', NULL),
(37, NULL, 'cash_receive', 'cash_receive', 1, NULL, NULL, '2017-09-30 16:09:47', '2017-09-30 16:09:47', NULL),
(38, NULL, 'cash_payment', 'cash_payment', 1, NULL, NULL, '2017-09-30 16:10:05', '2017-09-30 16:10:05', NULL),
(39, NULL, 'cash_transfer', 'cash_transfer', 1, NULL, NULL, '2017-09-30 16:10:23', '2017-09-30 16:10:23', NULL),
(40, NULL, 'stock_mutation', 'Stock Mutation', 1, NULL, NULL, '2017-10-02 08:01:47', '2017-10-02 08:01:47', NULL),
(41, '', 'purchase_order_app', 'purchase_order_app', 1, NULL, NULL, '2017-10-28 19:07:37', '2017-10-28 19:07:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_group`
--

CREATE TABLE `module_group` (
  `mg_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_account`
--

CREATE TABLE `mst_account` (
  `account_id` int(10) UNSIGNED NOT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_parent_id` int(11) DEFAULT NULL,
  `account_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_account`
--

INSERT INTO `mst_account` (`account_id`, `account_name`, `account_code`, `account_parent_id`, `account_type`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Aktiva', '1-0000', NULL, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Fixed Assets', '1-1000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Gedung', '1-1100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Mobil', '1-1200', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Current Asset', '1-2000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Kas dan Bank', '1-2100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Kas', '1-2110', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Bank', '1-2120', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Inventori', '1-3000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Inventory - Mesin', '1-3100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Hutang', '2-0000', NULL, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Pinjaman Bank', '2-1000', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Pajak', '2-2000', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Pajak Masukan', '2-2100', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Pajak Dibayar', '2-2200', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Modal', '3-0000', NULL, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Laba', '3-1000', 3, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Laba Ditahan', '3-2000', 3, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Pendapatan', '4-0000', NULL, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Pendapatan Penjualan', '4-1000', 4, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Bunga Bank', '4-2000', 4, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Harga Pokok Penjualan', '5-0000', NULL, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Harga Pokok Bahan Baku', '5-1000', 5, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Beban Tenaga Kerja', '5-2000', 5, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Biaya Operasional', '6-0000', NULL, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Biaya Penjualan', '6-1000', 6, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Biaya Lain', '6-2000', 6, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_bom`
--

CREATE TABLE `mst_bom` (
  `bom_id` int(10) UNSIGNED NOT NULL,
  `bom_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bom_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bom_quantity` decimal(25,10) DEFAULT NULL,
  `bom_production_time` decimal(25,10) DEFAULT NULL,
  `bom_cost` decimal(25,10) DEFAULT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_bom`
--

INSERT INTO `mst_bom` (`bom_id`, `bom_name`, `bom_description`, `bom_quantity`, `bom_production_time`, `bom_cost`, `item_id`, `department_id`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sugus 1 pcs', NULL, '1.0000000000', '2.0000000000', '2000.0000000000', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Sugus 20 pcs', '', '25.0000000000', '2.0000000000', '5000.0000000000', 2, 1, 1, 1, NULL, NULL, '2017-09-16 04:47:15', '2017-09-16 04:47:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_bom_detail`
--

CREATE TABLE `mst_bom_detail` (
  `bom_d_id` int(10) UNSIGNED NOT NULL,
  `bom_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `bom_detail_quantity` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_bom_detail`
--

INSERT INTO `mst_bom_detail` (`bom_d_id`, `bom_id`, `item_id`, `bom_detail_quantity`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, '1.0000000000', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 3, '1.0000000000', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 2, '20.0000000000', 1, NULL, 1, '2017-09-16 04:51:14', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(4, 2, 3, '10.0000000000', 1, NULL, 1, '2017-09-16 04:51:14', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(5, 2, 2, '20.0000000000', 1, NULL, 1, '2017-09-16 04:58:28', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(6, 2, 3, '10.0000000000', 1, NULL, 1, '2017-09-16 04:58:28', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(7, 2, 2, '20.0000000000', 1, NULL, 1, '2017-09-16 04:58:57', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(8, 2, 3, '10.0000000000', 1, NULL, 1, '2017-09-16 04:58:57', '2017-09-16 04:59:58', '2017-09-16 04:59:58'),
(9, 2, 2, '20.0000000000', 1, NULL, NULL, '2017-09-16 04:59:58', '2017-09-16 04:59:58', NULL),
(10, 2, 3, '10.0000000000', 1, NULL, NULL, '2017-09-16 04:59:59', '2017-09-16 04:59:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_company`
--

CREATE TABLE `mst_company` (
  `company_id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_npwp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_po_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_receive_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_receive_invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_jo_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_so_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_do_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_do_invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_format_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_company`
--

INSERT INTO `mst_company` (`company_id`, `company_name`, `company_address`, `company_alias`, `company_email`, `company_phone`, `company_npwp`, `company_po_no`, `company_receive_no`, `company_receive_invoice_no`, `company_jo_no`, `company_so_no`, `company_do_no`, `company_do_invoice_no`, `company_format_date`, `company_status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MS UNION', 'Gunung Putri - Bogor', 'MSU', 'info@msu.com', '123456', '12.234.5123.123.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-01 10:07:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_currency`
--

CREATE TABLE `mst_currency` (
  `currency_id` int(10) UNSIGNED NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_currency`
--

INSERT INTO `mst_currency` (`currency_id`, `currency_code`, `currency_name`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Rp', 'IDR', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '$', 'USD', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_customer`
--

CREATE TABLE `mst_customer` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_credit_limit` decimal(15,2) DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_customer`
--

INSERT INTO `mst_customer` (`customer_id`, `customer_name`, `customer_phone`, `customer_email`, `customer_pic`, `customer_address`, `customer_credit_limit`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Potato', '123456789', 'abc@def.com', 'Kentang', 'Jl. Pohon', '2000.00', 1, 1, 1, 1, '2017-09-04 10:32:52', '2017-09-04 10:33:52', NULL),
(2, 'Yohannes', '', '', '', '', '0.00', 1, 1, 1, NULL, '2017-09-04 10:34:00', '2017-09-04 10:46:30', NULL),
(3, 'Kuncoro MK II', '0851990998', 'Kuncoro@KentangBakar.com', 'Kuncoro', 'Jl. Pemakan Kentang 10', '5000000.00', 1, 1, 1, NULL, '2017-09-25 23:47:57', '2017-09-25 23:48:52', NULL),
(4, 'Ta', '02135444', 'ta.xo.eo', 'PON', 'LAKSD', '1000.00', 1, 1, NULL, 1, '2017-09-25 23:49:25', '2017-09-25 23:49:31', '2017-09-25 23:49:31'),
(5, 'PT. Pendekar Sakti', '022-93952221', 'info@pendekarsakti.co.id', 'Cahyono', 'Jl. Kebakaran', '1000000.00', 1, 1, NULL, NULL, '2017-09-25 23:51:36', '2017-09-25 23:51:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_department`
--

CREATE TABLE `mst_department` (
  `department_id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_worker_amount` int(11) DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_department`
--

INSERT INTO `mst_department` (`department_id`, `department_name`, `department_worker_amount`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Finance', 8, 1, 1, 1, NULL, '2017-09-05 09:27:46', '2017-09-05 09:30:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_item`
--

CREATE TABLE `mst_item` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_last_price` decimal(15,2) DEFAULT '0.00',
  `item_min_stock` decimal(15,2) DEFAULT NULL,
  `item_max_stock` decimal(15,2) DEFAULT NULL,
  `item_cos_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_income_inventory_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_asset_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_item`
--

INSERT INTO `mst_item` (`item_id`, `item_name`, `item_description`, `item_group`, `item_last_price`, `item_min_stock`, `item_max_stock`, `item_cos_account_code`, `item_income_inventory_account_code`, `item_asset_account_code`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sugus', 'Permen manis', 'Konsumsi', NULL, '1.00', '100.00', '0001', '0002', '0003', 1, 1, 1, NULL, '2017-09-05 10:46:22', '2017-09-05 10:48:24', NULL),
(2, 'Gula', 'Pemanis', 'RAW', NULL, '0.00', '100.00', '', '', '', 1, 1, NULL, NULL, '2017-09-15 23:48:17', '2017-09-15 23:48:17', NULL),
(3, 'Paper Wrap', 'Bungkus Kertas', 'MATERIAL', NULL, '0.00', '10000.00', '', '', '', 1, 1, 1, NULL, '2017-09-15 23:48:35', '2017-09-15 23:48:47', NULL),
(4, 'Box Sugus', 'Box Pengemas Permen Sugus', 'Sugus', NULL, '10.00', '5000.00', 'Inventori', 'Inventori', 'Inventoris', 1, 1, 1, NULL, '2017-09-26 00:18:45', '2017-09-26 00:19:13', NULL),
(5, 'a', 'aa', 'a', NULL, '11.00', '1.00', 'a', 'a', 'a', 1, 1, 1, 1, '2017-09-26 00:19:31', '2017-09-26 00:19:48', '2017-09-26 00:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `mst_paytype`
--

CREATE TABLE `mst_paytype` (
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `paytype_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_paytype`
--

INSERT INTO `mst_paytype` (`paytype_id`, `paytype_name`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Transfer', 1, 1, NULL, '2017-09-05 10:25:56', '2017-09-05 10:26:22', NULL),
(2, 'Cash', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Giro', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Cek', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_supplier`
--

CREATE TABLE `mst_supplier` (
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_credit_limit` decimal(15,2) DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_supplier`
--

INSERT INTO `mst_supplier` (`supplier_id`, `supplier_name`, `supplier_phone`, `supplier_email`, `supplier_pic`, `supplier_address`, `supplier_credit_limit`, `company_id`, `warehouse_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'abbbbbbb', '', '', '', '', '0.00', 1, NULL, 1, 1, NULL, '2017-09-04 11:02:28', '2017-10-27 19:01:07', '2017-10-27 19:01:07'),
(2, 'Supplier biasa aja q d', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-09-15 23:24:28', '2017-10-27 19:09:10', NULL),
(3, 'Test Supplier baru BB', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-09-15 23:24:40', '2017-10-27 19:07:23', NULL),
(4, 'PT. Pembuat Kosmetik Jaya Abadi Mk II', '021-22501050', 'info@pkja.com', 'Cahyono', 'Jl. Cileungsi Raya no 20B', '100000000.00', 1, NULL, 1, 1, 1, '2017-09-25 22:51:33', '2017-09-25 22:52:28', '2017-09-25 22:52:28'),
(5, 'asddddddddd', 'asd', 'asd', 'asd', 'asd', '23.00', 1, 2, 1, NULL, NULL, '2017-10-27 18:15:21', '2017-10-27 19:53:34', NULL),
(6, 'ddddd', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-10-27 18:15:57', '2017-10-27 18:15:57', NULL),
(7, 'errrrrr abv', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-10-27 18:17:27', '2017-10-27 18:58:57', NULL),
(8, 'sssss', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-10-27 18:18:59', '2017-10-27 18:18:59', NULL),
(9, 'www', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-10-27 18:21:57', '2017-10-27 19:07:16', '2017-10-27 19:07:16'),
(10, 'mmmmmm', '', '', '', '', '0.00', 1, NULL, 1, NULL, NULL, '2017-10-27 18:23:15', '2017-10-27 19:01:27', '2017-10-27 19:01:27'),
(11, 'abc', '08777', 'hu@jjj', 'Husni', 'Test', '100000.00', 1, 2, 1, NULL, NULL, '2017-10-27 19:10:06', '2017-10-27 19:11:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_term`
--

CREATE TABLE `mst_term` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term_interval` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_term`
--

INSERT INTO `mst_term` (`term_id`, `term_name`, `term_interval`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kerjasama 3 hari', 5, 1, 1, NULL, '2017-09-05 10:25:56', '2017-09-05 10:26:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_unit`
--

CREATE TABLE `mst_unit` (
  `unit_id` int(10) UNSIGNED NOT NULL,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_unit`
--

INSERT INTO `mst_unit` (`unit_id`, `unit_name`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(28, 'UNIT', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'BK', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'BKS', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'BOX', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'BTG', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'CRT', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'DOS', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'DZ', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_warehouse`
--

CREATE TABLE `mst_warehouse` (
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `warehouse_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_warehouse`
--

INSERT INTO `mst_warehouse` (`warehouse_id`, `warehouse_name`, `warehouse_description`, `warehouse_address`, `warehouse_pic`, `warehouse_phone`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gudang Cileungsi', 'Penyimpanan bahan baku', 'Jl. Keramat Jati 6', 'Achmad Susanto', '021-66685125', 1, 1, 1, NULL, '2017-09-05 09:45:16', '2017-09-05 09:47:28', NULL),
(2, 'Gudang Cikarang', '', 'Jl. Raya Cikarang', 'Jono', '65421233', 1, 1, NULL, NULL, '2017-10-02 21:53:06', '2017-10-02 21:53:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

CREATE TABLE `privilege` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `action_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `privilege`
--

INSERT INTO `privilege` (`id`, `user_id`, `module_id`, `action_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(2, 1, 1, 2, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(3, 1, 1, 3, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(4, 1, 1, 4, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(5, 1, 2, 1, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(6, 1, 2, 2, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(7, 1, 2, 3, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(8, 1, 2, 4, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(9, 1, 3, 1, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(10, 1, 3, 2, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(11, 1, 3, 3, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(12, 1, 3, 4, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(13, 1, 4, 1, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(14, 1, 4, 2, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(15, 1, 4, 3, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(16, 1, 4, 4, NULL, '2017-09-01 10:19:22', '2017-09-01 10:19:22'),
(17, 1, 1, 1, '2017-09-01 10:19:22', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(18, 1, 1, 2, '2017-09-01 10:19:22', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(19, 1, 1, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(20, 1, 1, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(21, 1, 2, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(22, 1, 2, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(23, 1, 2, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(24, 1, 2, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(25, 1, 3, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(26, 1, 3, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(27, 1, 3, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(28, 1, 3, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(29, 1, 4, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(30, 1, 4, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(31, 1, 4, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(32, 1, 4, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(33, 1, 5, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(34, 1, 5, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(35, 1, 5, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(36, 1, 5, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(37, 1, 6, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(38, 1, 6, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(39, 1, 6, 3, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(40, 1, 6, 4, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(41, 1, 7, 1, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(42, 1, 7, 2, '2017-09-01 10:19:23', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(43, 1, 7, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(44, 1, 7, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(45, 1, 8, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(46, 1, 8, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(47, 1, 8, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(48, 1, 8, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(49, 1, 9, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(50, 1, 9, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(51, 1, 9, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(52, 1, 9, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(53, 1, 10, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(54, 1, 10, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(55, 1, 10, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(56, 1, 10, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(57, 1, 11, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(58, 1, 11, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(59, 1, 11, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(60, 1, 11, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(61, 1, 12, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(62, 1, 12, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(63, 1, 12, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(64, 1, 12, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(65, 1, 13, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(66, 1, 13, 2, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(67, 1, 13, 3, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(68, 1, 13, 4, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(69, 1, 14, 1, '2017-09-01 10:19:24', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(70, 1, 14, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(71, 1, 14, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(72, 1, 14, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(73, 1, 15, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(74, 1, 15, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(75, 1, 15, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(76, 1, 15, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(77, 1, 16, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(78, 1, 16, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(79, 1, 16, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(80, 1, 16, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(81, 1, 17, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(82, 1, 17, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(83, 1, 17, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(84, 1, 17, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(85, 1, 18, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(86, 1, 18, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(87, 1, 18, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(88, 1, 18, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(89, 1, 19, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(90, 1, 19, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(91, 1, 19, 3, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(92, 1, 19, 4, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(93, 1, 20, 1, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(94, 1, 20, 2, '2017-09-01 10:19:25', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(95, 1, 20, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(96, 1, 20, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(97, 1, 21, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(98, 1, 21, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(99, 1, 21, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(100, 1, 21, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(101, 1, 22, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(102, 1, 22, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(103, 1, 22, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(104, 1, 22, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(105, 1, 23, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(106, 1, 23, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(107, 1, 23, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(108, 1, 23, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(109, 1, 24, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(110, 1, 24, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(111, 1, 24, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(112, 1, 24, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(113, 1, 25, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(114, 1, 25, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(115, 1, 25, 3, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(116, 1, 25, 4, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(117, 1, 26, 1, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(118, 1, 26, 2, '2017-09-01 10:19:26', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(119, 1, 26, 3, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(120, 1, 26, 4, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(121, 1, 27, 1, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(122, 1, 27, 2, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(123, 1, 27, 3, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(124, 1, 27, 4, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(125, 1, 28, 1, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(126, 1, 28, 2, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(127, 1, 28, 3, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(128, 1, 28, 4, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(129, 1, 29, 1, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(130, 1, 29, 2, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(131, 1, 29, 3, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(132, 1, 29, 4, '2017-09-01 10:19:27', '2017-09-25 19:19:28', '2017-09-25 19:19:28'),
(133, 1, 1, 1, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(134, 1, 1, 2, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(135, 1, 1, 3, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(136, 1, 1, 4, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(137, 1, 2, 1, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(138, 1, 2, 2, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(139, 1, 2, 3, '2017-09-25 19:19:28', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(140, 1, 2, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(141, 1, 3, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(142, 1, 3, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(143, 1, 3, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(144, 1, 3, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(145, 1, 4, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(146, 1, 4, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(147, 1, 4, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(148, 1, 4, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(149, 1, 5, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(150, 1, 5, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(151, 1, 5, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(152, 1, 5, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(153, 1, 6, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(154, 1, 6, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(155, 1, 6, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(156, 1, 6, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(157, 1, 7, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(158, 1, 7, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(159, 1, 7, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(160, 1, 7, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(161, 1, 8, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(162, 1, 8, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(163, 1, 8, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(164, 1, 8, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(165, 1, 9, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(166, 1, 9, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(167, 1, 9, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(168, 1, 9, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(169, 1, 10, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(170, 1, 10, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(171, 1, 10, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(172, 1, 10, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(173, 1, 11, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(174, 1, 11, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(175, 1, 11, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(176, 1, 11, 4, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(177, 1, 12, 1, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(178, 1, 12, 2, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(179, 1, 12, 3, '2017-09-25 19:19:29', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(180, 1, 12, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(181, 1, 13, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(182, 1, 13, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(183, 1, 13, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(184, 1, 13, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(185, 1, 14, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(186, 1, 14, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(187, 1, 14, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(188, 1, 14, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(189, 1, 15, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(190, 1, 15, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(191, 1, 15, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(192, 1, 15, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(193, 1, 16, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(194, 1, 16, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(195, 1, 16, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(196, 1, 16, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(197, 1, 17, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(198, 1, 17, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(199, 1, 17, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(200, 1, 17, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(201, 1, 18, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(202, 1, 18, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(203, 1, 18, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(204, 1, 18, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(205, 1, 19, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(206, 1, 19, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(207, 1, 19, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(208, 1, 19, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(209, 1, 20, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(210, 1, 20, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(211, 1, 20, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(212, 1, 20, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(213, 1, 21, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(214, 1, 21, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(215, 1, 21, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(216, 1, 21, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(217, 1, 22, 1, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(218, 1, 22, 2, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(219, 1, 22, 3, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(220, 1, 22, 4, '2017-09-25 19:19:30', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(221, 1, 23, 1, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(222, 1, 23, 2, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(223, 1, 23, 3, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(224, 1, 23, 4, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(225, 1, 24, 1, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(226, 1, 24, 2, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(227, 1, 24, 3, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(228, 1, 24, 4, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(229, 1, 25, 1, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(230, 1, 25, 2, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(231, 1, 25, 3, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(232, 1, 25, 4, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(233, 1, 26, 1, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(234, 1, 26, 2, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(235, 1, 26, 3, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(236, 1, 26, 4, '2017-09-25 19:19:31', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(237, 1, 27, 1, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(238, 1, 27, 2, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(239, 1, 27, 3, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(240, 1, 27, 4, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(241, 1, 28, 1, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(242, 1, 28, 2, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(243, 1, 28, 3, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(244, 1, 28, 4, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(245, 1, 29, 1, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(246, 1, 29, 2, '2017-09-25 19:19:32', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(247, 1, 29, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(248, 1, 29, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(249, 1, 30, 1, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(250, 1, 30, 2, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(251, 1, 30, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(252, 1, 30, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(253, 1, 31, 1, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(254, 1, 31, 2, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(255, 1, 31, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(256, 1, 31, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(257, 1, 32, 1, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(258, 1, 32, 2, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(259, 1, 32, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(260, 1, 32, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(261, 1, 33, 1, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(262, 1, 33, 2, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(263, 1, 33, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(264, 1, 33, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(265, 1, 34, 1, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(266, 1, 34, 2, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(267, 1, 34, 3, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(268, 1, 34, 4, '2017-09-25 19:19:33', '2017-09-25 19:22:26', '2017-09-25 19:22:26'),
(269, 1, 1, 1, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(270, 1, 1, 2, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(271, 1, 1, 3, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(272, 1, 1, 4, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(273, 1, 2, 1, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(274, 1, 2, 2, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(275, 1, 2, 3, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(276, 1, 2, 4, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(277, 1, 3, 1, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(278, 1, 3, 2, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(279, 1, 3, 3, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(280, 1, 3, 4, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(281, 1, 4, 1, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(282, 1, 4, 2, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(283, 1, 4, 3, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(284, 1, 4, 4, '2017-09-25 19:22:26', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(285, 1, 5, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(286, 1, 5, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(287, 1, 5, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(288, 1, 5, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(289, 1, 6, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(290, 1, 6, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(291, 1, 6, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(292, 1, 6, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(293, 1, 7, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(294, 1, 7, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(295, 1, 7, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(296, 1, 7, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(297, 1, 8, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(298, 1, 8, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(299, 1, 8, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(300, 1, 8, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(301, 1, 9, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(302, 1, 9, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(303, 1, 9, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(304, 1, 9, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(305, 1, 10, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(306, 1, 10, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(307, 1, 10, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(308, 1, 10, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(309, 1, 11, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(310, 1, 11, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(311, 1, 11, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(312, 1, 11, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(313, 1, 12, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(314, 1, 12, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(315, 1, 12, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(316, 1, 12, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(317, 1, 13, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(318, 1, 13, 2, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(319, 1, 13, 3, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(320, 1, 13, 4, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(321, 1, 14, 1, '2017-09-25 19:22:27', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(322, 1, 14, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(323, 1, 14, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(324, 1, 14, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(325, 1, 15, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(326, 1, 15, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(327, 1, 15, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(328, 1, 15, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(329, 1, 16, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(330, 1, 16, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(331, 1, 16, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(332, 1, 16, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(333, 1, 17, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(334, 1, 17, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(335, 1, 17, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(336, 1, 17, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(337, 1, 18, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(338, 1, 18, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(339, 1, 18, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(340, 1, 18, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(341, 1, 19, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(342, 1, 19, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(343, 1, 19, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(344, 1, 19, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(345, 1, 20, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(346, 1, 20, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(347, 1, 20, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(348, 1, 20, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(349, 1, 21, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(350, 1, 21, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(351, 1, 21, 3, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(352, 1, 21, 4, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(353, 1, 22, 1, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(354, 1, 22, 2, '2017-09-25 19:22:28', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(355, 1, 22, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(356, 1, 22, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(357, 1, 23, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(358, 1, 23, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(359, 1, 23, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(360, 1, 23, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(361, 1, 24, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(362, 1, 24, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(363, 1, 24, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(364, 1, 24, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(365, 1, 25, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(366, 1, 25, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(367, 1, 25, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(368, 1, 25, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(369, 1, 26, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(370, 1, 26, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(371, 1, 26, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(372, 1, 26, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(373, 1, 27, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(374, 1, 27, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(375, 1, 27, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(376, 1, 27, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(377, 1, 28, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(378, 1, 28, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(379, 1, 28, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(380, 1, 28, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(381, 1, 29, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(382, 1, 29, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(383, 1, 29, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(384, 1, 29, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(385, 1, 30, 1, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(386, 1, 30, 2, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(387, 1, 30, 3, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(388, 1, 30, 4, '2017-09-25 19:22:29', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(389, 1, 31, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(390, 1, 31, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(391, 1, 31, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(392, 1, 31, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(393, 1, 32, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(394, 1, 32, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(395, 1, 32, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(396, 1, 32, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(397, 1, 33, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(398, 1, 33, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(399, 1, 33, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(400, 1, 33, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(401, 1, 34, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(402, 1, 34, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(403, 1, 34, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(404, 1, 34, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(405, 1, 35, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(406, 1, 35, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(407, 1, 35, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(408, 1, 35, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(409, 1, 36, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(410, 1, 36, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(411, 1, 36, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(412, 1, 36, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(413, 1, 37, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(414, 1, 37, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(415, 1, 37, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(416, 1, 37, 4, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(417, 1, 38, 1, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(418, 1, 38, 2, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(419, 1, 38, 3, '2017-09-25 19:22:30', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(420, 1, 38, 4, '2017-09-25 19:22:31', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(421, 1, 39, 1, '2017-09-25 19:22:31', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(422, 1, 39, 2, '2017-09-25 19:22:31', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(423, 1, 39, 3, '2017-09-25 19:22:31', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(424, 1, 39, 4, '2017-09-25 19:22:31', '2017-10-02 21:52:06', '2017-10-02 21:52:06'),
(425, 1, 5, 1, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(426, 1, 5, 2, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(427, 1, 5, 3, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(428, 1, 5, 4, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(429, 1, 6, 1, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(430, 1, 6, 2, '2017-10-02 21:52:06', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(431, 1, 6, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(432, 1, 6, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(433, 1, 7, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(434, 1, 7, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(435, 1, 7, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(436, 1, 7, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(437, 1, 8, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(438, 1, 8, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(439, 1, 8, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(440, 1, 8, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(441, 1, 9, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(442, 1, 9, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(443, 1, 9, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(444, 1, 9, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(445, 1, 10, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(446, 1, 10, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(447, 1, 10, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(448, 1, 10, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(449, 1, 11, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(450, 1, 11, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(451, 1, 11, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(452, 1, 11, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(453, 1, 12, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(454, 1, 12, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(455, 1, 12, 3, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(456, 1, 12, 4, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(457, 1, 13, 1, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(458, 1, 13, 2, '2017-10-02 21:52:07', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(459, 1, 13, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(460, 1, 13, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(461, 1, 14, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(462, 1, 14, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(463, 1, 14, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(464, 1, 14, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(465, 1, 15, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(466, 1, 15, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(467, 1, 15, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(468, 1, 15, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(469, 1, 16, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(470, 1, 16, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(471, 1, 16, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(472, 1, 16, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(473, 1, 17, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(474, 1, 17, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(475, 1, 17, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(476, 1, 17, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(477, 1, 18, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(478, 1, 18, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(479, 1, 18, 3, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(480, 1, 18, 4, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(481, 1, 19, 1, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(482, 1, 19, 2, '2017-10-02 21:52:08', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(483, 1, 19, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(484, 1, 19, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(485, 1, 20, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(486, 1, 20, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(487, 1, 20, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(488, 1, 20, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(489, 1, 21, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(490, 1, 21, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(491, 1, 21, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(492, 1, 21, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(493, 1, 22, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(494, 1, 22, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(495, 1, 22, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(496, 1, 22, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(497, 1, 23, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(498, 1, 23, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(499, 1, 23, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(500, 1, 23, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(501, 1, 24, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(502, 1, 24, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(503, 1, 24, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(504, 1, 24, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(505, 1, 25, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(506, 1, 25, 2, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(507, 1, 25, 3, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(508, 1, 25, 4, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(509, 1, 26, 1, '2017-10-02 21:52:09', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(510, 1, 26, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(511, 1, 26, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(512, 1, 26, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(513, 1, 27, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(514, 1, 27, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(515, 1, 27, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(516, 1, 27, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(517, 1, 28, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(518, 1, 28, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(519, 1, 28, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(520, 1, 28, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(521, 1, 29, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(522, 1, 29, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(523, 1, 29, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(524, 1, 29, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(525, 1, 30, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(526, 1, 30, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(527, 1, 30, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(528, 1, 30, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(529, 1, 31, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(530, 1, 31, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(531, 1, 31, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(532, 1, 31, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(533, 1, 32, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(534, 1, 32, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(535, 1, 32, 3, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(536, 1, 32, 4, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(537, 1, 33, 1, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(538, 1, 33, 2, '2017-10-02 21:52:10', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(539, 1, 33, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(540, 1, 33, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(541, 1, 34, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(542, 1, 34, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(543, 1, 34, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(544, 1, 34, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(545, 1, 35, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(546, 1, 35, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(547, 1, 35, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(548, 1, 35, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(549, 1, 36, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(550, 1, 36, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(551, 1, 36, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(552, 1, 36, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(553, 1, 37, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(554, 1, 37, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(555, 1, 37, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(556, 1, 37, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(557, 1, 38, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(558, 1, 38, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(559, 1, 38, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(560, 1, 38, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(561, 1, 39, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(562, 1, 39, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(563, 1, 39, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(564, 1, 39, 4, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(565, 1, 40, 1, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(566, 1, 40, 2, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(567, 1, 40, 3, '2017-10-02 21:52:11', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(568, 1, 40, 4, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(569, 1, 1, 1, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(570, 1, 1, 2, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(571, 1, 1, 3, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(572, 1, 1, 4, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(573, 1, 2, 1, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(574, 1, 2, 2, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(575, 1, 2, 3, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(576, 1, 2, 4, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(577, 1, 3, 1, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(578, 1, 3, 2, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(579, 1, 3, 3, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(580, 1, 3, 4, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(581, 1, 4, 1, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(582, 1, 4, 2, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(583, 1, 4, 3, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(584, 1, 4, 4, '2017-10-02 21:52:12', '2017-10-28 19:08:02', '2017-10-28 19:08:02'),
(585, 1, 6, 1, '2017-10-28 19:08:02', '2017-10-28 19:08:02', NULL),
(586, 1, 6, 2, '2017-10-28 19:08:02', '2017-10-28 19:08:02', NULL),
(587, 1, 6, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(588, 1, 6, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(589, 1, 7, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(590, 1, 7, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(591, 1, 7, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(592, 1, 7, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(593, 1, 8, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(594, 1, 8, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(595, 1, 8, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(596, 1, 8, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(597, 1, 9, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(598, 1, 9, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(599, 1, 9, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(600, 1, 9, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(601, 1, 10, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(602, 1, 10, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(603, 1, 10, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(604, 1, 10, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL);
INSERT INTO `privilege` (`id`, `user_id`, `module_id`, `action_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(605, 1, 11, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(606, 1, 11, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(607, 1, 11, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(608, 1, 11, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(609, 1, 12, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(610, 1, 12, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(611, 1, 12, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(612, 1, 12, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(613, 1, 13, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(614, 1, 13, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(615, 1, 13, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(616, 1, 13, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(617, 1, 14, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(618, 1, 14, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(619, 1, 14, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(620, 1, 14, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(621, 1, 15, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(622, 1, 15, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(623, 1, 15, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(624, 1, 15, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(625, 1, 16, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(626, 1, 16, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(627, 1, 16, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(628, 1, 16, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(629, 1, 17, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(630, 1, 17, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(631, 1, 17, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(632, 1, 17, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(633, 1, 18, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(634, 1, 18, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(635, 1, 18, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(636, 1, 18, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(637, 1, 19, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(638, 1, 19, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(639, 1, 19, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(640, 1, 19, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(641, 1, 20, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(642, 1, 20, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(643, 1, 20, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(644, 1, 20, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(645, 1, 21, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(646, 1, 21, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(647, 1, 21, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(648, 1, 21, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(649, 1, 22, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(650, 1, 22, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(651, 1, 22, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(652, 1, 22, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(653, 1, 23, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(654, 1, 23, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(655, 1, 23, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(656, 1, 23, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(657, 1, 24, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(658, 1, 24, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(659, 1, 24, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(660, 1, 24, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(661, 1, 25, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(662, 1, 25, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(663, 1, 25, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(664, 1, 25, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(665, 1, 26, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(666, 1, 26, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(667, 1, 26, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(668, 1, 26, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(669, 1, 27, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(670, 1, 27, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(671, 1, 27, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(672, 1, 27, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(673, 1, 28, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(674, 1, 28, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(675, 1, 28, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(676, 1, 28, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(677, 1, 29, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(678, 1, 29, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(679, 1, 29, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(680, 1, 29, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(681, 1, 30, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(682, 1, 30, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(683, 1, 30, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(684, 1, 30, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(685, 1, 31, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(686, 1, 31, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(687, 1, 31, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(688, 1, 31, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(689, 1, 32, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(690, 1, 32, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(691, 1, 32, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(692, 1, 32, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(693, 1, 33, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(694, 1, 33, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(695, 1, 33, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(696, 1, 33, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(697, 1, 34, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(698, 1, 34, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(699, 1, 34, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(700, 1, 34, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(701, 1, 35, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(702, 1, 35, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(703, 1, 35, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(704, 1, 35, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(705, 1, 36, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(706, 1, 36, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(707, 1, 36, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(708, 1, 36, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(709, 1, 37, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(710, 1, 37, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(711, 1, 37, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(712, 1, 37, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(713, 1, 38, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(714, 1, 38, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(715, 1, 38, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(716, 1, 38, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(717, 1, 39, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(718, 1, 39, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(719, 1, 39, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(720, 1, 39, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(721, 1, 40, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(722, 1, 40, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(723, 1, 40, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(724, 1, 40, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(725, 1, 41, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(726, 1, 41, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(727, 1, 41, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(728, 1, 41, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(729, 1, 1, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(730, 1, 1, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(731, 1, 1, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(732, 1, 1, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(733, 1, 2, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(734, 1, 2, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(735, 1, 2, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(736, 1, 2, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(737, 1, 3, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(738, 1, 3, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(739, 1, 3, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(740, 1, 3, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(741, 1, 4, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(742, 1, 4, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(743, 1, 4, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(744, 1, 4, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(745, 1, 5, 1, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(746, 1, 5, 2, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(747, 1, 5, 3, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL),
(748, 1, 5, 4, '2017-10-28 19:08:03', '2017-10-28 19:08:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `privilege_group`
--

CREATE TABLE `privilege_group` (
  `pg_id` int(10) UNSIGNED NOT NULL,
  `pg_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pg_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `privilege_group`
--

INSERT INTO `privilege_group` (`pg_id`, `pg_name`, `pg_description`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Administrator', 'Bisa edit user dan permission', 1, 0, 1, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `privilege_group_detail`
--

CREATE TABLE `privilege_group_detail` (
  `pg_d_id` int(10) UNSIGNED NOT NULL,
  `pg_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `privilege_group_detail`
--

INSERT INTO `privilege_group_detail` (`pg_d_id`, `pg_id`, `company_id`, `module_id`, `action_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 0, 1, 1, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(2, 2, 0, 1, 2, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(3, 2, 0, 1, 3, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(4, 2, 0, 1, 4, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(5, 2, 0, 2, 1, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(6, 2, 0, 2, 2, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(7, 2, 0, 2, 3, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(8, 2, 0, 2, 4, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(9, 2, 0, 3, 1, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(10, 2, 0, 3, 2, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(11, 2, 0, 3, 3, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(12, 2, 0, 3, 4, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(13, 2, 0, 4, 1, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(14, 2, 0, 4, 2, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(15, 2, 0, 4, 3, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(16, 2, 0, 4, 4, 0, NULL, NULL, '2017-10-02 09:52:53', '2017-10-02 10:09:41', '2017-10-02 10:09:41'),
(17, 2, 1, 1, 1, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(18, 2, 1, 1, 2, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(19, 2, 1, 1, 3, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(20, 2, 1, 1, 4, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(21, 2, 1, 2, 1, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(22, 2, 1, 2, 2, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(23, 2, 1, 2, 3, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(24, 2, 1, 2, 4, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(25, 2, 1, 3, 1, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(26, 2, 1, 3, 2, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(27, 2, 1, 3, 3, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(28, 2, 1, 3, 4, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(29, 2, 1, 4, 1, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(30, 2, 1, 4, 2, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(31, 2, 1, 4, 3, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL),
(32, 2, 1, 4, 4, 1, NULL, NULL, '2017-10-02 10:09:53', '2017-10-02 10:09:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `production_id` int(10) UNSIGNED NOT NULL,
  `bom_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `production_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `multiplier` decimal(25,10) DEFAULT NULL,
  `estimated_result` decimal(25,10) DEFAULT NULL,
  `actual_result` decimal(25,10) DEFAULT NULL,
  `estimated_time` decimal(25,10) DEFAULT NULL,
  `actual_time` decimal(25,10) DEFAULT NULL,
  `worker_amount` decimal(25,10) DEFAULT NULL,
  `production_cost` decimal(25,10) DEFAULT NULL,
  `total_production_cost` decimal(25,10) DEFAULT NULL,
  `production_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`production_id`, `bom_id`, `item_id`, `warehouse_id`, `department_id`, `production_number`, `notes`, `multiplier`, `estimated_result`, `actual_result`, `estimated_time`, `actual_time`, `worker_amount`, `production_cost`, `total_production_cost`, `production_status`, `approved_by`, `approved_at`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 'WO/MSU/2017/10/0001', '1', '1.0000000000', '1.0000000000', NULL, '1.0000000000', NULL, '2.0000000000', '1.0000000000', NULL, 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-09-25 17:21:18', '2017-09-25 17:21:18', '2017-09-26 00:22:28'),
(2, 1, 1, 1, 1, 'WO/MSU/2017/10/0002', '1', '1.0000000000', '2000.0000000000', NULL, '1.0000000000', NULL, '1.0000000000', '5.0000000000', NULL, 'ACTIVE', 1, '2017-10-01', 1, 1, NULL, NULL, '2017-09-25 17:25:59', '2017-09-30 20:29:28', NULL),
(3, 1, 1, 1, 1, 'WO/MSU/2017/10/0003', '1', '1.0000000000', '2000.0000000000', '2000.0000000000', '2.0000000000', '2.0000000000', '1.0000000000', '5.0000000000', '5.0000000000', 'COMPLETED', 1, '2017-09-30', 1, 1, NULL, NULL, '2017-09-25 17:29:30', '2017-09-30 02:05:49', NULL),
(4, 1, 1, 1, 1, 'WO/MSU/2017/10/0004', '1', '2.0000000000', '2.0000000000', NULL, '4.0000000000', NULL, '1.0000000000', '5.0000000000', '10.0000000000', 'COMPLETED', 1, '2017-09-30', 1, 1, 1, NULL, '2017-09-25 17:33:36', '2017-09-29 23:36:04', NULL),
(5, 2, 2, 1, 1, 'WO/MSU/2017/10/0005', '', '5.0000000000', '125.0000000000', '100.0000000000', '10.0000000000', '12.0000000000', '10.0000000000', '0.0000000000', '0.0000000000', 'COMPLETED', 1, '2017-10-01', 1, 1, NULL, NULL, '2017-09-30 20:31:16', '2017-09-30 20:32:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `production_detail`
--

CREATE TABLE `production_detail` (
  `production_d_id` int(10) UNSIGNED NOT NULL,
  `production_id` int(11) DEFAULT NULL,
  `bom_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `estimated_material` decimal(25,10) DEFAULT NULL,
  `multiplied_estimated_material` decimal(25,10) DEFAULT NULL,
  `actual_material` decimal(25,10) DEFAULT NULL,
  `multiplied_actual_material` decimal(25,10) DEFAULT NULL,
  `cost` decimal(25,10) DEFAULT NULL,
  `multiplied_cost` decimal(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `production_detail`
--

INSERT INTO `production_detail` (`production_d_id`, `production_id`, `bom_d_id`, `item_id`, `estimated_material`, `multiplied_estimated_material`, `actual_material`, `multiplied_actual_material`, `cost`, `multiplied_cost`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-25 17:25:59', '2017-09-25 17:25:59', '2017-09-30 09:26:01'),
(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-25 17:25:59', '2017-09-25 17:25:59', '2017-09-30 09:26:01'),
(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-25 17:29:30', '2017-09-25 17:29:30', '2017-09-30 09:26:00'),
(4, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-25 17:29:30', '2017-09-25 17:29:30', '2017-09-30 09:25:59'),
(5, 4, 1, 2, '1.0000000000', '1.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, 1, '2017-09-25 17:33:36', '2017-09-25 17:55:36', '2017-09-25 17:55:36'),
(6, 4, 2, 3, '1.0000000000', '1.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, 1, '2017-09-25 17:33:36', '2017-09-25 17:55:37', '2017-09-25 17:55:37'),
(7, 4, 1, 2, '2.0000000000', '4.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-09-25 17:55:37', '2017-09-25 17:55:37', NULL),
(8, 4, 2, 3, '3.0000000000', '6.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-09-25 17:55:37', '2017-09-25 17:55:37', NULL),
(9, 5, 9, 2, '20.0000000000', '100.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-09-30 20:31:16', '2017-09-30 20:31:16', NULL),
(10, 5, 10, 3, '10.0000000000', '50.0000000000', NULL, NULL, '0.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-09-30 20:31:16', '2017-09-30 20:31:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_advance`
--

CREATE TABLE `purchase_advance` (
  `id` int(10) UNSIGNED NOT NULL,
  `pa_reference` varchar(50) DEFAULT NULL,
  `pa_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `amount` double DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `po_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_advance`
--

INSERT INTO `purchase_advance` (`id`, `pa_reference`, `pa_date`, `supplier_id`, `paytype_id`, `account_id`, `currency`, `amount`, `attachment`, `remark`, `status`, `company_id`, `warehouse_id`, `po_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PAAD-26091700001', '2017-09-26', 1, 0, 9, 'IDR', 1000000000, NULL, NULL, 9, NULL, NULL, NULL, 1, 1, NULL, '2017-09-25 17:00:00', '2017-09-25 23:25:40', NULL),
(2, 'PAAD-01101700002', '2017-10-01', 2, 0, 8, 'IDR', 1500000, NULL, NULL, 0, 1, NULL, 7, 1, 1, NULL, '2017-09-30 17:00:00', '2017-11-04 02:27:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `po_reference` varchar(50) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `packing_list` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `resi_tracking` varchar(255) DEFAULT NULL,
  `need_app` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_reference`, `po_date`, `supplier_id`, `paytype_id`, `company_id`, `paydue`, `currency`, `top`, `tracking_number`, `packing_list`, `amount`, `remark`, `status`, `attachment`, `resi_tracking`, `need_app`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'POTX-24091700001', '2017-09-24', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, '24-09-2017 05-49-22Untitled-1.jpg', NULL, 0, 1, NULL, NULL, '2017-09-23 17:00:00', '2017-09-23 22:49:22', NULL),
(2, 'POTX-26091700002', '2017-09-26', 1, 0, 1, 0, 'USD', 0, NULL, NULL, NULL, '', 1, NULL, NULL, 0, 1, 1, NULL, '2017-09-25 17:00:00', '2017-10-07 17:49:08', NULL),
(3, 'PONT-26091700003', '2017-09-26', 1, 0, NULL, 0, 'IDR', 14, NULL, NULL, NULL, 'Barang diambil sendiri di gudang supplier', 0, '26-09-2017 06-06-33Quotation Test.docx', NULL, 0, 1, 1, NULL, '2017-09-25 17:00:00', '2017-09-25 23:10:02', NULL),
(4, 'PONT-26091700004', '2017-09-26', 3, 0, NULL, 0, 'USD', 30, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:08:20', NULL),
(5, 'POTX-26091700005', '2017-09-26', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', NULL, NULL),
(6, 'POTX-26091700006', '2017-09-26', 1, 0, NULL, 0, 'IDR', 15, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:11:29', NULL),
(7, 'POTX-26091700007', '2017-09-26', 1, 0, NULL, 0, 'USD', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:14:13', NULL),
(8, 'POTX-26091700008', '2017-09-26', 2, 0, NULL, 0, 'IDR', 30, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:15:05', NULL),
(9, 'POTX-26091700009', '2017-09-26', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:17:33', NULL),
(10, 'POTX-26091700010', '2017-09-26', 1, 0, 1, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-10-07 17:48:21', NULL),
(11, 'POTX-01101700011', '2017-10-01', 2, 0, NULL, 0, 'IDR', 50, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 16:12:54', NULL),
(12, 'POTX-01101700012', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 20:40:04', NULL),
(13, 'POTX-01101700013', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 20:43:04', NULL),
(14, 'POTX-01101700014', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 20:57:18', NULL),
(15, 'POTX-04101700015', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-03 17:00:00', NULL, NULL),
(16, 'POTX-04101700016', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-03 17:00:00', NULL, NULL),
(17, 'POTX-04101700017', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-03 17:00:00', NULL, NULL),
(18, 'POTX-08101700018', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(19, 'POTX-08101700019', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(20, 'POTX-08101700020', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(21, 'POTX-22101700021', '2017-10-22', 2, 0, 1, 0, '', 0, 'asd', 'asdas', NULL, '', 0, NULL, '22-10-2017 01-32-52amelia.docx', 0, 1, 1, NULL, '2017-10-21 17:00:00', '2017-10-28 20:05:36', NULL),
(22, 'POTX-22101700022', '2017-10-22', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-21 17:00:00', NULL, NULL),
(23, 'POTX-28101700023', '2017-10-28', 5, 0, 1, 0, 'IDR', 90, '001', '002', 10300000, '', 0, NULL, NULL, 0, 1, 1, NULL, '2017-10-27 17:00:00', '2017-10-28 20:55:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_detail`
--

CREATE TABLE `purchase_order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `po_id` int(10) UNSIGNED DEFAULT NULL,
  `pr_d_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `unitprice` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `gr_qty` double DEFAULT NULL,
  `gr_remain` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_detail`
--

INSERT INTO `purchase_order_detail` (`id`, `po_id`, `pr_d_id`, `item_id`, `unit`, `unitprice`, `discount`, `quantity`, `amount`, `gr_qty`, `gr_remain`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 2, 'CRT', 40000, 10, 50, 1800000, 103, NULL, NULL, NULL, NULL, '2017-09-23 22:48:47', '2017-09-23 22:49:09', NULL),
(2, 1, 4, 14, 'BKS', 50000, 10, 70, 3150000, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:48:47', '2017-09-23 22:49:01', NULL),
(3, 3, NULL, 2, 'BOX', 150000, 10, 10, 1350000, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:07:17', '2017-09-25 23:07:17', NULL),
(4, 4, NULL, 3, 'BOX', 50, 10, 20, 900, 1, NULL, NULL, NULL, NULL, '2017-09-25 23:07:57', '2017-09-25 23:08:13', NULL),
(5, 6, 1, 2, 'BOX', NULL, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:11:03', '2017-09-25 23:11:03', NULL),
(6, 7, 3, 3, 'DOS', 100, 0, 5, 50, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:11:53', '2017-09-25 23:13:53', NULL),
(7, 8, 7, 1, 'BOX', 50, 0, 10, 50, 1, NULL, NULL, NULL, NULL, '2017-09-25 23:14:40', '2017-09-25 23:14:57', NULL),
(8, 9, 10, 2, 'BOX', 5000, 0, 100, 50000, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:17:06', '2017-09-25 23:17:24', NULL),
(9, 11, 13, 4, 'BOX', 50000, 0, 5, 250000, 5, NULL, NULL, NULL, NULL, '2017-09-30 16:12:06', '2017-09-30 16:12:38', NULL),
(10, 12, 14, 3, 'BOX', 500, 10, 800, 360000, 750, NULL, NULL, NULL, NULL, '2017-09-30 20:39:15', '2017-09-30 20:40:45', NULL),
(11, 13, 14, 3, 'BOX', 500, 10, 300, 135000, 200, NULL, NULL, NULL, NULL, '2017-09-30 20:42:16', '2017-09-30 20:42:59', NULL),
(12, 14, 10, 2, 'BOX', 500, 0, 196, 9800, NULL, NULL, NULL, NULL, NULL, '2017-09-30 20:56:05', '2017-09-30 20:57:08', NULL),
(13, 16, NULL, 1, '', 200, 5, 1, 190, NULL, NULL, NULL, NULL, NULL, '2017-10-04 08:57:19', '2017-10-04 08:57:19', NULL),
(14, 18, 1, 2, 'BOX', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 16:33:07', '2017-10-07 16:33:07', NULL),
(15, 20, 3, 3, 'DOS', 500, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 16:47:08', '2017-10-07 16:47:08', NULL),
(16, 23, NULL, 1, 'BKS', 1010000, 0, 10, 10100000, 5, NULL, NULL, NULL, NULL, '2017-10-27 19:51:40', '2017-10-28 20:51:40', NULL),
(17, 21, NULL, 1, 'BOX', 100000, 0, 10, 1000000, NULL, NULL, NULL, NULL, NULL, '2017-10-28 19:56:15', '2017-10-28 19:56:15', NULL),
(18, 23, NULL, 3, 'BOX', 10000, 0, 20, 200000, NULL, NULL, NULL, NULL, NULL, '2017-10-28 20:51:53', '2017-10-28 20:51:53', NULL);

--
-- Triggers `purchase_order_detail`
--
DELIMITER $$
CREATE TRIGGER `delete_pr` AFTER DELETE ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (OLD.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (OLD.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (OLD.pr_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_pr` AFTER INSERT ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (NEW.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (NEW.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (NEW.pr_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_pr` AFTER UPDATE ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (NEW.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (NEW.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (NEW.pr_d_id);

END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_receive`
--

CREATE TABLE `purchase_receive` (
  `id` int(10) UNSIGNED NOT NULL,
  `prec_reference` varchar(50) DEFAULT NULL,
  `prec_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `paytype_id` int(10) UNSIGNED NOT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_receive`
--

INSERT INTO `purchase_receive` (`id`, `prec_reference`, `prec_date`, `supplier_id`, `paytype_id`, `paydue`, `currency`, `top`, `remark`, `company_id`, `status`, `attachment`, `warehouse_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PRCX-24091700001', '2017-09-24', 0, 0, 0, '', 0, '', 1, 0, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, 1, NULL, '2017-09-23 17:00:00', '2017-10-07 17:50:36', NULL),
(2, 'PRCX-24091700002', '2017-09-24', 0, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-23 17:00:00', NULL, NULL),
(3, 'PRCX-26091700003', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:18:30', NULL),
(4, 'PRCX-26091700004', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:19:04', NULL),
(5, 'PRCX-26091700005', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:20:28', NULL),
(6, 'PRCX-26091700006', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:20:45', NULL),
(7, 'PRCX-01101700007', '2017-10-21', 2, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, 1, NULL, '2017-09-30 17:00:00', '2017-09-30 16:14:13', NULL),
(8, 'PRCX-01101700008', '2017-10-01', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-30 17:00:00', NULL, NULL),
(9, 'PRCX-01101700009', '2017-10-01', 1, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 20:47:05', NULL),
(10, 'PRCX-01101700010', '2017-10-01', 1, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 20:59:17', NULL),
(11, 'PRCX-07101700011', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-06 17:00:00', NULL, NULL),
(12, 'PRCT-07101700012', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-06 17:00:00', NULL, NULL),
(13, 'PRCX-07101700013', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-06 17:00:00', NULL, NULL),
(14, 'PRCX-08101700014', '2017-10-08', 3, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(15, 'PRCX-08101700015', '2017-10-08', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(16, 'PRCX-08101700016', '2017-10-08', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', NULL, NULL),
(17, 'PRCX-28101700017', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 5, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(18, 'PRCX-28101700018', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 5, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(19, 'PRCX-28101700019', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 2, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_receive_detail`
--

CREATE TABLE `purchase_receive_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `prec_id` int(10) UNSIGNED DEFAULT NULL,
  `po_d_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `unitprice` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `gr_qty` double DEFAULT NULL,
  `gr_remain` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_receive_detail`
--

INSERT INTO `purchase_receive_detail` (`id`, `prec_id`, `po_d_id`, `item_id`, `unit`, `unitprice`, `discount`, `quantity`, `amount`, `gr_qty`, `gr_remain`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 14, 'BKS', 50000, NULL, 40, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:49:42', '2017-09-23 22:49:50', NULL),
(2, 2, 1, 2, 'CRT', 40000, 10, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:53:00', '2017-09-23 22:53:00', NULL),
(3, 3, 8, 2, 'BOX', NULL, NULL, 55, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:18:10', '2017-09-25 23:18:29', NULL),
(4, 4, 5, 2, 'BOX', NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:18:55', '2017-09-25 23:19:01', NULL),
(5, 5, 5, 2, 'BOX', 0, 0, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:20:22', '2017-09-25 23:20:22', NULL),
(6, 6, 5, 2, 'BOX', 0, 0, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:20:42', '2017-09-25 23:20:42', NULL),
(7, 7, 9, 4, 'BOX', 50000, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 16:14:04', '2017-09-30 16:14:04', NULL),
(8, 8, 9, 4, 'BOX', 50000, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 17:48:06', '2017-09-30 17:48:06', NULL),
(9, 9, 10, 3, 'BOX', NULL, NULL, 750, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 20:46:46', '2017-09-30 20:48:02', NULL),
(10, 9, 11, 3, 'BOX', NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 20:46:46', '2017-09-30 20:47:21', NULL),
(11, 10, 1, 2, 'CRT', 40000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 20:58:44', '2017-09-30 20:58:44', NULL),
(12, 14, 4, 3, 'BOX', 50, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 11:08:20', '2017-10-07 11:08:20', NULL),
(13, 16, 7, 1, 'BOX', 50, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 16:57:18', '2017-10-07 16:57:18', NULL),
(14, 17, 16, 1, 'BKS', 200000, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 19:55:37', '2017-10-27 19:55:37', NULL),
(15, 18, 16, 1, 'BKS', 200000, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 19:56:03', '2017-10-27 19:56:03', NULL),
(16, 19, 16, 1, 'BKS', 200000, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 20:00:36', '2017-10-27 20:00:36', NULL);

--
-- Triggers `purchase_receive_detail`
--
DELIMITER $$
CREATE TRIGGER `delete_po` AFTER DELETE ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (OLD.po_d_id)) THEN

UPDATE purchase_order_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (OLD.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (OLD.po_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_po` AFTER INSERT ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (NEW.po_d_id)) THEN

UPDATE purchase_order_detail SET gr_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (NEW.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (NEW.po_d_id);

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_po` AFTER UPDATE ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (NEW.po_d_id)) THEN

UPDATE purchase_order_detail SET gr_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (NEW.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (NEW.po_d_id);

END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request`
--

CREATE TABLE `purchase_request` (
  `id` int(10) UNSIGNED NOT NULL,
  `pr_reference` varchar(50) DEFAULT NULL,
  `pr_date` date DEFAULT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_request`
--

INSERT INTO `purchase_request` (`id`, `pr_reference`, `pr_date`, `warehouse_id`, `remark`, `status`, `attachment`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PRRQ-24091700001', '2017-09-24', 1, '', 0, '24-09-2017 05-48-07Untitled-1.jpg', 1, 1, 1, NULL, '2017-09-23 17:00:00', '2017-10-07 15:43:51', NULL),
(2, 'PRRQ-24091700002', '2017-09-24', 1, '', 9, NULL, NULL, 1, 1, NULL, '2017-09-23 17:00:00', '2017-09-23 22:48:15', NULL),
(3, 'PRRQ-25091700003', '2017-09-25', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-24 17:00:00', '2017-09-24 20:46:55', NULL),
(4, 'PRRQ-26091700004', '2017-11-20', 1, '', 9, '26-09-2017 05-56-05Quotation Test.docx', NULL, 1, 1, NULL, '2017-09-25 17:00:00', '2017-09-25 22:57:05', NULL),
(5, 'PRRQ-26091700005', '2017-09-26', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', NULL, NULL),
(6, 'PRRQ-26091700006', '2017-09-26', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', '2017-09-25 23:16:49', NULL),
(7, 'PRRQ-27091700007', '2017-09-27', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 17:00:00', NULL, NULL),
(8, 'PRRQ-30091700008', '2017-09-30', 1, '', 0, NULL, NULL, 1, 1, NULL, '2017-09-29 17:00:00', '2017-09-30 02:05:15', NULL),
(9, 'PRRQ-01101700009', '2017-10-27', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-30 17:00:00', '2017-09-30 16:11:48', NULL),
(10, 'PRRQ-01101700010', '2017-10-01', 1, '', 0, NULL, NULL, 1, 1, NULL, '2017-09-30 17:00:00', '2017-09-30 20:38:40', NULL),
(11, 'PRRQ-04101700011', '2017-10-04', 0, NULL, 1, NULL, NULL, 1, 1, NULL, '2017-10-03 17:00:00', '2017-10-04 01:47:58', NULL),
(12, 'PRRQ-04101700012', '2017-10-04', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-03 17:00:00', NULL, NULL),
(13, 'PRRQ-08101700013', '2017-10-08', 2, '', 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 17:00:00', '2017-10-07 15:41:36', NULL),
(14, 'PRRQ-22101700014', '2017-10-22', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-21 17:00:00', NULL, NULL),
(15, 'PRTX-23101700015', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-22 17:00:00', NULL, NULL),
(16, 'PRTX-23101700016', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-22 17:00:00', NULL, NULL),
(17, 'PRTX-23101700017', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-22 17:00:00', NULL, NULL),
(18, 'PRTX-28101700018', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(19, 'PRTX-28101700019', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(20, 'PRTX-28101700020', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(21, 'PRTX-28101700021', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(22, 'PRTX-28101700022', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(23, 'PRTX-28101700023', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(24, 'PRTX-28101700024', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(25, 'PRTX-28101700025', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(26, 'PRTX-28101700026', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(27, 'PRTX-28101700027', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(28, 'PRTX-28101700028', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(29, 'PRTX-28101700029', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(30, 'PRTX-28101700030', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(31, 'PRTX-28101700031', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(32, 'PRTX-28101700032', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(33, 'PRTX-28101700033', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(34, 'PRTX-28101700034', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(35, 'PRTX-28101700035', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(36, 'PRTX-28101700036', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(37, 'PRTX-28101700037', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL),
(38, 'PRTX-28101700038', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-27 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request_detail`
--

CREATE TABLE `purchase_request_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `pr_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `po_qty` double DEFAULT NULL,
  `so_d_id` int(10) DEFAULT NULL,
  `estimate_price` double DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_request_detail`
--

INSERT INTO `purchase_request_detail` (`id`, `pr_id`, `item_id`, `unit`, `quantity`, `po_qty`, `so_d_id`, `estimate_price`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 'BOX', 300, 51, NULL, 10000, NULL, NULL, NULL, '2017-09-23 22:46:42', '2017-10-21 18:42:39', NULL),
(2, 1, 2, 'CRT', 10, 50, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:47:00', '2017-09-23 22:47:00', NULL),
(3, 1, 3, 'DOS', 8, 6, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:47:10', '2017-10-22 06:14:01', '2017-10-22 06:14:01'),
(4, 2, 14, 'BKS', 60, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:47:26', '2017-09-23 22:47:26', NULL),
(5, 2, 18, 'CRT', 6, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-23 22:47:35', '2017-09-24 02:13:33', NULL),
(6, 2, 1, 'BKS', 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 02:13:26', '2017-09-24 02:13:26', NULL),
(7, 3, 1, 'BOX', 20, 10, NULL, NULL, NULL, NULL, NULL, '2017-09-24 20:46:32', '2017-09-24 20:46:41', NULL),
(8, 4, 2, 'BOX', 5, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 22:58:59', '2017-09-25 22:58:59', NULL),
(9, 4, 3, 'BOX', 10, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 22:59:24', '2017-09-25 22:59:24', NULL),
(10, 6, 2, 'BOX', 196, 296, NULL, NULL, NULL, NULL, NULL, '2017-09-25 23:16:47', '2017-09-25 23:36:44', NULL),
(11, 8, 1, 'BK', 5, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 02:05:28', '2017-09-30 02:05:28', NULL),
(12, 8, 1, 'BK', 4, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 02:05:50', '2017-09-30 02:05:50', NULL),
(13, 9, 4, 'BOX', 20, 5, NULL, NULL, NULL, NULL, NULL, '2017-09-30 16:11:44', '2017-09-30 16:11:44', NULL),
(14, 10, 3, 'BOX', 1000, 1100, NULL, NULL, NULL, NULL, NULL, '2017-09-30 20:38:05', '2017-09-30 20:38:05', NULL),
(15, 11, 1, 'UNIT', 25, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-04 01:47:43', '2017-10-04 01:47:43', NULL),
(16, 13, 1, 'BK', 111, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 15:41:31', '2017-10-07 15:41:31', NULL),
(17, 1, 2, 'BKS', 2, NULL, NULL, 111111, NULL, NULL, NULL, '2017-10-22 04:15:44', '2017-10-22 04:15:49', '2017-10-22 04:15:49'),
(18, 21, 2, 'BOX', 1, NULL, 4, 100000, NULL, NULL, NULL, '2017-10-27 19:20:28', '2017-10-27 19:20:38', NULL),
(19, 21, 3, 'CRT', 200, NULL, 1, 300000, NULL, NULL, NULL, '2017-10-27 19:20:28', '2017-10-27 19:20:47', NULL),
(20, 22, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:21:11', '2017-10-27 19:21:11', NULL),
(21, 23, 3, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:21:55', '2017-10-27 19:21:55', NULL),
(22, 23, 1, NULL, 1000, NULL, 2, NULL, NULL, NULL, NULL, '2017-10-27 19:21:55', '2017-10-27 19:21:55', NULL),
(23, 23, 1, NULL, 1000, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-27 19:21:56', '2017-10-27 19:21:56', NULL),
(24, 23, 2, NULL, 1, NULL, 4, NULL, NULL, NULL, NULL, '2017-10-27 19:21:56', '2017-10-27 19:21:56', NULL),
(25, 23, 3, NULL, 10, NULL, 5, NULL, NULL, NULL, NULL, '2017-10-27 19:21:56', '2017-10-27 19:21:56', NULL),
(26, 24, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:22:42', '2017-10-27 19:22:42', NULL),
(27, 24, 1, NULL, 1000, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-27 19:22:42', '2017-10-27 19:22:42', NULL),
(28, 25, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:27:41', '2017-10-27 19:27:41', NULL),
(29, 26, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:28:17', '2017-10-27 19:28:17', NULL),
(30, 27, 2, NULL, 500, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:31:58', '2017-10-27 19:31:58', NULL),
(31, 28, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:32:20', '2017-10-27 19:32:20', NULL),
(32, 28, 1, NULL, 1000, NULL, 2, NULL, NULL, NULL, NULL, '2017-10-27 19:32:20', '2017-10-27 19:32:20', NULL),
(33, 29, 1, NULL, 1000, NULL, 8, NULL, NULL, NULL, NULL, '2017-10-27 19:33:42', '2017-10-27 19:33:42', NULL),
(34, 29, 2, NULL, 500, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-27 19:33:42', '2017-10-27 19:33:42', NULL),
(35, 34, 1, NULL, 200, NULL, 4, NULL, NULL, NULL, NULL, '2017-10-27 19:38:02', '2017-10-27 19:38:02', NULL),
(36, 35, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:41:02', '2017-10-27 19:41:02', NULL),
(37, 36, 1, NULL, 1000, NULL, 7, NULL, NULL, NULL, NULL, '2017-10-27 19:41:23', '2017-10-27 19:41:23', NULL),
(38, 37, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:41:33', '2017-10-27 19:41:33', NULL),
(39, 38, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-27 19:43:22', '2017-10-27 19:43:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

CREATE TABLE `purchase_return` (
  `id` int(11) NOT NULL,
  `pr_reference` varchar(50) NOT NULL,
  `pr_date` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_return`
--

INSERT INTO `purchase_return` (`id`, `pr_reference`, `pr_date`, `supplier_id`, `attachment`, `warehouse_id`, `currency`, `remark`, `status`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'RTRN-24091700001', '2017-09-24', 1, NULL, 2, 'IDR', '', 0, 1, 1, 1, NULL, '2017-09-23 17:00:00', '2017-10-07 17:53:46', NULL),
(2, 'RTRN-26091700002', '2017-09-26', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', NULL, NULL),
(3, 'RTRN-26091700003', '2017-09-26', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-25 17:00:00', NULL, NULL),
(4, 'RTRN-30091700004', '2017-09-30', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-29 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_detail`
--

CREATE TABLE `purchase_return_detail` (
  `pr_d_id` int(11) NOT NULL,
  `pr_id` int(11) NOT NULL,
  `prec_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `unitprice` double NOT NULL,
  `discount` int(11) NOT NULL,
  `amount` double NOT NULL,
  `quantity` double NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_return_detail`
--

INSERT INTO `purchase_return_detail` (`pr_d_id`, `pr_id`, `prec_d_id`, `item_id`, `unit`, `unitprice`, `discount`, `amount`, `quantity`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 5, 2, 'BOX', 0, 0, 0, 50, NULL, NULL, NULL, '2017-09-25 23:22:14', '2017-09-25 23:23:23', '2017-09-25 23:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `refund_order`
--

CREATE TABLE `refund_order` (
  `ro_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `ro_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `ro_date` date DEFAULT NULL,
  `refund_total` decimal(25,10) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `ro_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `refund_order`
--

INSERT INTO `refund_order` (`ro_id`, `so_id`, `ro_number`, `customer_id`, `warehouse_id`, `ro_date`, `refund_total`, `payment_method`, `notes`, `ro_status`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'RO/MSU/201710/0001', 2, 1, '0000-00-00', '13255.0000000000', 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-20 21:40:57', '2017-09-20 21:40:57', '2017-09-23 09:16:22'),
(2, 1, 'RO/MSU/201710/0002', 2, 1, '0000-00-00', '4675.0000000000', 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 02:07:40', '2017-09-23 02:07:40', '2017-09-23 09:16:24'),
(3, 1, 'RO/MSU/201710/0003', 2, 1, '0000-00-00', '21285.0000000000', 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 02:08:48', '2017-09-23 02:08:48', '2017-09-23 09:16:26'),
(4, 1, 'RO/MSU/201710/0004', 2, 1, '0000-00-00', '7370.0000000000', 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 02:10:14', '2017-09-23 02:10:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `refund_order_detail`
--

CREATE TABLE `refund_order_detail` (
  `ro_d_id` int(10) UNSIGNED NOT NULL,
  `ro_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `so_d_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `refund_amount` decimal(25,10) DEFAULT NULL,
  `total_refund_amount` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `refund_order_detail`
--

INSERT INTO `refund_order_detail` (`ro_d_id`, `ro_id`, `item_id`, `so_d_id`, `quantity`, `refund_amount`, `total_refund_amount`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-20 21:40:57', '2017-09-20 21:40:57', '2017-09-22 17:00:00'),
(2, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-20 21:40:57', '2017-09-20 21:40:57', '2017-09-23 09:15:37'),
(3, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-20 21:40:57', '2017-09-20 21:40:57', '2017-09-23 09:15:51'),
(4, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:07:40', '2017-09-23 02:07:40', '2017-09-23 09:15:53'),
(5, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:07:40', '2017-09-23 02:07:40', '2017-09-23 09:15:55'),
(6, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:07:40', '2017-09-23 02:07:40', '2017-09-22 17:00:00'),
(7, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:08:48', '2017-09-23 02:08:48', '2017-09-23 09:16:00'),
(8, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:08:48', '2017-09-23 02:08:48', '2017-09-23 09:16:01'),
(9, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 02:08:48', '2017-09-23 02:08:48', '2017-09-23 09:16:04'),
(10, 4, 1, 4, '1.0000000000', '1980.0000000000', '1980.0000000000', 1, NULL, NULL, '2017-09-23 02:10:14', '2017-09-23 02:10:14', NULL),
(11, 4, 3, 5, '2.0000000000', '1650.0000000000', '3300.0000000000', 1, NULL, NULL, '2017-09-23 02:10:14', '2017-09-23 02:10:14', NULL),
(12, 4, 2, 6, '2.0000000000', '1045.0000000000', '2090.0000000000', 1, NULL, NULL, '2017-09-23 02:10:14', '2017-09-23 02:10:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_invoice`
--

CREATE TABLE `sales_invoice` (
  `si_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `si_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `si_issue_date` date DEFAULT NULL,
  `si_due_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_by_detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_amount` double(25,10) DEFAULT NULL,
  `rec_amount` double(25,0) DEFAULT NULL,
  `si_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_invoice`
--

INSERT INTO `sales_invoice` (`si_id`, `so_id`, `si_number`, `si_issue_date`, `si_due_date`, `customer_id`, `payment_method`, `payment_by`, `payment_by_detail`, `payment_date`, `payment_amount`, `rec_amount`, `si_status`, `notes`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'INV/MSU/201710/0001', '1970-01-01', '1970-01-01', 2, 'abc', NULL, NULL, NULL, 10000.0000000000, NULL, 'DRAFT', 'Sekali bayar harus lunas', 1, 1, NULL, '2017-09-18 10:39:42', '2017-09-18 10:53:17', NULL),
(2, 1, 'INV/MSU/201710/0002', '1970-01-01', '1970-01-01', 2, 'CASH', NULL, NULL, NULL, 200000.0000000000, 1000, 'DRAFT', '', 1, NULL, NULL, '2017-09-20 21:18:37', '2017-09-20 21:18:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `so_id` int(10) UNSIGNED NOT NULL,
  `so_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_quotation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `tax_rate` decimal(25,10) DEFAULT NULL,
  `tax_value` decimal(25,10) DEFAULT NULL,
  `total_line_item` decimal(25,10) DEFAULT NULL,
  `grand_total` decimal(25,10) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_date` date DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `so_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `need_pr` int(10) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`so_id`, `so_number`, `so_quotation`, `customer_id`, `term_id`, `tax_rate`, `tax_value`, `total_line_item`, `grand_total`, `payment_method`, `so_date`, `notes`, `so_status`, `company_id`, `need_pr`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SO/MSU/201710/0001', NULL, 2, 1, '10.0000000000', '113500.0000000000', '1135000.0000000000', '1248500.0000000000', 'CASH', '2017-09-11', '', '0', 1, NULL, 1, NULL, NULL, '2017-09-16 10:12:36', '2017-09-16 11:04:55', NULL),
(2, 'SO/MSU/201710/0002', NULL, 5, 1, '10.0000000000', '47500.0000000000', '475000.0000000000', '522500.0000000000', 'Cash', '0000-00-00', '', 'DRAFT', 1, NULL, 1, NULL, NULL, '2017-09-26 00:01:50', '2017-09-26 00:01:50', NULL),
(3, 'SO/MSU/201710/0003', NULL, 3, 1, '10.0000000000', '42750.0000000000', '427500.0000000000', '470250.0000000000', 'Transfer', '0000-00-00', '', 'DRAFT', 1, NULL, 1, NULL, NULL, '2017-09-26 00:02:29', '2017-09-26 00:02:29', NULL),
(4, 'SO/MSU/201710/0004', NULL, 2, 1, '10.0000000000', '0.5000000000', '5.0000000000', '5.5000000000', 'CASH', '0000-00-00', '', 'APPROVAL', 1, NULL, 1, NULL, NULL, '2017-09-30 21:53:13', '2017-10-12 13:33:09', NULL),
(5, 'SO/MSU/201710/0001', NULL, 5, 1, '10.0000000000', '2000.0000000000', '20000.0000000000', '22000.0000000000', 'Cash', '0000-00-00', '', 'COMPLETED', 1, NULL, 1, NULL, NULL, '2017-10-02 20:42:31', '2017-10-07 03:25:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_detail`
--

CREATE TABLE `sales_order_detail` (
  `so_d_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `discount_rate` decimal(25,10) DEFAULT NULL,
  `discount_value` decimal(25,10) DEFAULT NULL,
  `total_discount_value` decimal(25,10) DEFAULT NULL,
  `price_before_discount` decimal(25,10) DEFAULT NULL,
  `price_after_discount` decimal(25,10) DEFAULT NULL,
  `total_price_before_discount` decimal(25,10) DEFAULT NULL,
  `total_price_after_discount` decimal(25,10) DEFAULT NULL,
  `discount_price_before_tax` decimal(25,10) DEFAULT NULL,
  `discount_price_after_tax` decimal(25,10) DEFAULT NULL,
  `total_discount_price_before_tax` decimal(25,10) DEFAULT NULL,
  `total_discount_price_after_tax` decimal(25,10) DEFAULT NULL,
  `pr_qty` double DEFAULT NULL,
  `created_pr` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_order_detail`
--

INSERT INTO `sales_order_detail` (`so_d_id`, `so_id`, `item_id`, `quantity`, `discount_rate`, `discount_value`, `total_discount_value`, `price_before_discount`, `price_after_discount`, `total_price_before_discount`, `total_price_after_discount`, `discount_price_before_tax`, `discount_price_after_tax`, `total_discount_price_before_tax`, `total_discount_price_after_tax`, `pr_qty`, `created_pr`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 200, '10.0000000000', '200.0000000000', '40000.0000000000', '2000.0000000000', '1800.0000000000', '400000.0000000000', '360000.0000000000', '1800.0000000000', '1620.0000000000', '360000.0000000000', '324000.0000000000', NULL, NULL, NULL, NULL, 1, '2017-09-16 10:12:36', '2017-09-16 11:04:55', '2017-09-16 11:04:55'),
(2, 1, 3, 200, '0.0000000000', '0.0000000000', '0.0000000000', '1500.0000000000', '1500.0000000000', '300000.0000000000', '300000.0000000000', '1500.0000000000', '1350.0000000000', '300000.0000000000', '270000.0000000000', NULL, NULL, NULL, NULL, 1, '2017-09-16 10:12:36', '2017-09-16 11:04:55', '2017-09-16 11:04:55'),
(3, 1, 2, 500, '5.0000000000', '50.0000000000', '25000.0000000000', '1000.0000000000', '950.0000000000', '500000.0000000000', '475000.0000000000', '950.0000000000', '855.0000000000', '475000.0000000000', '427500.0000000000', NULL, NULL, NULL, NULL, 1, '2017-09-16 10:12:36', '2017-09-16 11:04:55', '2017-09-16 11:04:55'),
(4, 1, 1, 200, '10.0000000000', '200.0000000000', '40000.0000000000', '2000.0000000000', '1800.0000000000', '400000.0000000000', '360000.0000000000', '1800.0000000000', '1980.0000000000', '360000.0000000000', '396000.0000000001', NULL, 1, NULL, NULL, NULL, '2017-09-16 11:04:56', '2017-10-27 19:38:02', NULL),
(5, 1, 3, 200, '0.0000000000', '0.0000000000', '0.0000000000', '1500.0000000000', '1500.0000000000', '300000.0000000000', '300000.0000000000', '1500.0000000000', '1650.0000000000', '300000.0000000000', '330000.0000000001', NULL, NULL, NULL, NULL, NULL, '2017-09-16 11:04:56', '2017-09-16 11:04:56', NULL),
(6, 1, 2, 500, '5.0000000000', '50.0000000000', '25000.0000000000', '1000.0000000000', '950.0000000000', '500000.0000000000', '475000.0000000000', '950.0000000000', '1045.0000000000', '475000.0000000000', '522500.0000000000', NULL, NULL, NULL, NULL, NULL, '2017-09-16 11:04:56', '2017-09-16 11:04:56', NULL),
(7, 2, 1, 1000, '5.0000000000', '25.0000000000', '25000.0000000000', '500.0000000000', '475.0000000000', '500000.0000000000', '475000.0000000000', '475.0000000000', '522.5000000000', '475000.0000000000', '522500.0000000000', NULL, 1, NULL, NULL, NULL, '2017-09-26 00:01:50', '2017-10-27 19:41:23', NULL),
(8, 3, 1, 1000, '5.0000000000', '22.5000000000', '22500.0000000000', '450.0000000000', '427.5000000000', '450000.0000000000', '427500.0000000000', '427.5000000000', '470.2500000000', '427500.0000000000', '470250.0000000001', NULL, 1, NULL, NULL, NULL, '2017-09-26 00:02:29', '2017-10-27 19:33:42', NULL),
(9, 4, 2, 1, '0.0000000000', '0.0000000000', '0.0000000000', '5.0000000000', '5.0000000000', '5.0000000000', '5.0000000000', '5.0000000000', '5.5000000000', '5.0000000000', '5.5000000000', NULL, NULL, NULL, NULL, NULL, '2017-09-30 21:53:13', '2017-09-30 21:53:13', NULL),
(10, 5, 3, 10, '0.0000000000', '0.0000000000', '0.0000000000', '2000.0000000000', '2000.0000000000', '20000.0000000000', '20000.0000000000', '2000.0000000000', '2200.0000000000', '20000.0000000000', '22000.0000000000', NULL, NULL, NULL, NULL, NULL, '2017-10-02 20:42:31', '2017-10-02 20:42:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustment`
--

CREATE TABLE `stock_adjustment` (
  `sa_id` int(10) UNSIGNED NOT NULL,
  `sa_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sa_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `so_id` int(11) DEFAULT NULL,
  `sa_date` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_adjustment`
--

INSERT INTO `stock_adjustment` (`sa_id`, `sa_number`, `sa_status`, `warehouse_id`, `so_id`, `sa_date`, `approved_by`, `approved_at`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SADJ/MSU/201710/0001', 'DRAFT', 1, NULL, '2017-10-10', NULL, NULL, 1, 1, NULL, NULL, '2017-10-03 08:28:33', '2017-10-03 08:28:33', '2017-10-03 15:29:31'),
(2, 'SADJ/MSU/201710/0001', 'APPROVED', 1, NULL, '2017-10-10', 1, '2017-10-03', 1, 1, NULL, NULL, '2017-10-03 08:29:33', '2017-10-03 08:43:52', NULL),
(3, 'SADJ/MSU/201710/0002', 'APPROVED', 1, NULL, '2010-06-01', 1, '2017-10-04', 1, 1, 1, NULL, '2017-10-03 20:18:01', '2017-10-03 20:47:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustment_detail`
--

CREATE TABLE `stock_adjustment_detail` (
  `sa_d_id` int(10) UNSIGNED NOT NULL,
  `sa_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `db_quantity` double(25,10) DEFAULT NULL,
  `actual_quantity` double(25,10) DEFAULT NULL,
  `quantity_difference` double(25,10) DEFAULT NULL,
  `price` double(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_adjustment_detail`
--

INSERT INTO `stock_adjustment_detail` (`sa_d_id`, `sa_id`, `item_id`, `notes`, `db_quantity`, `actual_quantity`, `quantity_difference`, `price`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 4, '', -15.0000000000, 0.0000000000, 15.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 08:29:33', '2017-10-03 08:29:33', NULL),
(2, 2, 2, '', -10.0000000000, 0.0000000000, 10.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 08:29:33', '2017-10-03 08:29:33', NULL),
(3, 2, 3, '', -60.0000000000, 0.0000000000, 60.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 08:29:34', '2017-10-03 08:29:34', NULL),
(4, 2, 1, '', -10.0000000000, 0.0000000000, 10.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 08:29:34', '2017-10-03 08:29:34', NULL),
(5, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:18:01', '2017-10-03 20:28:12', '2017-10-03 20:28:12'),
(6, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:18:01', '2017-10-03 20:28:12', '2017-10-03 20:28:12'),
(7, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:18:01', '2017-10-03 20:28:12', '2017-10-03 20:28:12'),
(8, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:18:01', '2017-10-03 20:28:12', '2017-10-03 20:28:12'),
(9, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:46:57', '2017-10-03 20:47:08', '2017-10-03 20:47:08'),
(10, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:46:57', '2017-10-03 20:47:08', '2017-10-03 20:47:08'),
(11, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:46:57', '2017-10-03 20:47:08', '2017-10-03 20:47:08'),
(12, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:46:57', '2017-10-03 20:47:08', '2017-10-03 20:47:08'),
(13, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:47:08', '2017-10-03 20:47:08', NULL),
(14, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:47:08', '2017-10-03 20:47:08', NULL),
(15, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:47:08', '2017-10-03 20:47:08', NULL),
(16, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 20:47:08', '2017-10-03 20:47:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_inventory`
--

CREATE TABLE `stock_inventory` (
  `si_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `last_price` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_inventory`
--

INSERT INTO `stock_inventory` (`si_id`, `warehouse_id`, `item_id`, `quantity`, `last_price`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 4, '2.0000000000', NULL, NULL, NULL, NULL, '2017-09-30 17:48:06', '2017-09-30 17:48:06', '2017-10-02 16:29:55'),
(2, 1, 2, '0.0000000000', NULL, NULL, NULL, NULL, '2017-09-30 20:31:32', '2017-10-03 08:43:52', NULL),
(3, 1, 3, '5.0000000000', NULL, NULL, NULL, NULL, '2017-09-30 20:31:32', '2017-10-03 08:43:52', NULL),
(4, 1, 4, '0.0000000000', NULL, NULL, NULL, NULL, '2017-10-02 09:30:50', '2017-10-03 08:43:52', NULL),
(5, 1, 1, '2000.0000000000', '500.0000000000', NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-07 09:46:45', NULL),
(6, NULL, 3, '1.0000000000', NULL, NULL, NULL, NULL, '2017-10-07 11:08:20', '2017-10-07 11:08:20', NULL),
(7, NULL, 1, '1.0000000000', NULL, NULL, NULL, NULL, '2017-10-07 16:57:18', '2017-10-07 16:57:18', NULL),
(8, NULL, 1, '3.0000000000', NULL, NULL, NULL, NULL, '2017-10-27 19:55:37', '2017-10-27 19:55:37', NULL),
(9, NULL, 1, '1.0000000000', NULL, NULL, NULL, NULL, '2017-10-27 19:56:03', '2017-10-27 19:56:03', NULL),
(10, NULL, 1, '1.0000000000', NULL, NULL, NULL, NULL, '2017-10-27 20:00:36', '2017-10-27 20:00:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_movement`
--

CREATE TABLE `stock_movement` (
  `sm_id` int(10) UNSIGNED NOT NULL,
  `si_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `price` decimal(25,10) DEFAULT NULL,
  `total_price` decimal(25,10) DEFAULT NULL,
  `trigger_table` text COLLATE utf8_unicode_ci,
  `trigger_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_movement`
--

INSERT INTO `stock_movement` (`sm_id`, `si_id`, `item_id`, `warehouse_id`, `quantity`, `price`, `total_price`, `trigger_table`, `trigger_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, 4, NULL, '2.0000000000', '50000.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 17:48:06', '2017-09-30 17:48:06', NULL),
(3, 2, 2, 1, '100.0000000000', NULL, NULL, 'production_detail', NULL, NULL, NULL, NULL, '2017-09-30 20:31:32', '2017-09-30 20:31:32', NULL),
(4, 3, 3, 1, '50.0000000000', NULL, NULL, 'production_detail', NULL, NULL, NULL, NULL, '2017-09-30 20:31:32', '2017-09-30 20:31:32', NULL),
(5, 2, 2, 1, '100.0000000000', '0.0000000000', '0.0000000000', 'production', NULL, NULL, NULL, NULL, '2017-09-30 20:32:37', '2017-09-30 20:32:37', NULL),
(6, 4, 4, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:30:50', '2017-10-02 09:30:50', NULL),
(7, 4, 4, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(8, 4, 4, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(9, 2, 2, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(10, 2, 2, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(11, 3, 3, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(12, 3, 3, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(13, 5, 1, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(14, 5, 1, 1, '5.0000000000', NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 09:31:02', '2017-10-02 09:31:02', NULL),
(15, 4, 4, 1, '15.0000000000', NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 08:43:52', '2017-10-03 08:43:52', NULL),
(16, 2, 2, 1, '10.0000000000', NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 08:43:52', '2017-10-03 08:43:52', NULL),
(17, 3, 3, 1, '60.0000000000', NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 08:43:52', '2017-10-03 08:43:52', NULL),
(18, 5, 1, 1, '10.0000000000', NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 08:43:52', '2017-10-03 08:43:52', NULL),
(19, 5, 1, 1, '2000.0000000000', NULL, NULL, 'stock_adjustment', 3, NULL, NULL, NULL, '2017-10-03 20:47:15', '2017-10-03 20:47:15', NULL),
(20, 5, 1, 1, '-500.0000000000', '500.0000000000', '500000.0000000000', 'delivery_order', NULL, NULL, NULL, NULL, '2017-10-07 09:46:45', '2017-10-07 09:46:45', NULL),
(21, NULL, 3, NULL, '1.0000000000', '50.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 11:08:20', '2017-10-07 11:08:20', NULL),
(22, NULL, 1, NULL, '1.0000000000', '50.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 16:57:18', '2017-10-07 16:57:18', NULL),
(23, NULL, 1, NULL, '3.0000000000', '200000.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 19:55:37', '2017-10-27 19:55:37', NULL),
(24, NULL, 1, NULL, '1.0000000000', '200000.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 19:56:03', '2017-10-27 19:56:03', NULL),
(25, NULL, 1, NULL, '1.0000000000', '200000.0000000000', NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 20:00:36', '2017-10-27 20:00:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_mutation`
--

CREATE TABLE `stock_mutation` (
  `sm_id` int(10) UNSIGNED NOT NULL,
  `sm_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warehouse_id_from` int(11) DEFAULT NULL,
  `warehouse_id_to` int(11) DEFAULT NULL,
  `sm_date` date DEFAULT NULL,
  `sm_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_mutation`
--

INSERT INTO `stock_mutation` (`sm_id`, `sm_number`, `warehouse_id_from`, `warehouse_id_to`, `sm_date`, `sm_status`, `approved_by`, `approved_at`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SMUT/MSU/201710/0001', 1, 1, '2017-10-18', 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-10-02 09:14:58', '2017-10-02 09:14:58', '2017-10-02 16:16:12'),
(2, 'SMUT/MSU/201710/0001', 1, 1, '2017-10-18', 'APPROVED', 1, '2017-10-02', 1, 1, NULL, NULL, '2017-10-02 09:16:14', '2017-10-02 09:31:02', NULL),
(3, 'SMUT/MSU/201710/0002', 1, 2, '2017-10-10', 'REJECTED', NULL, NULL, 1, 1, NULL, NULL, '2017-10-02 21:53:54', '2017-10-02 21:54:03', NULL),
(4, 'SMUT/MSU/201710/0003', 1, 2, '2017-10-06', 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-10-03 20:12:06', '2017-10-03 20:12:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_mutation_detail`
--

CREATE TABLE `stock_mutation_detail` (
  `sm_d_id` int(10) UNSIGNED NOT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `quantity` decimal(25,10) DEFAULT NULL,
  `quantity_from_before` decimal(25,10) DEFAULT NULL,
  `quantity_to_before` decimal(25,10) DEFAULT NULL,
  `quantity_from_after` decimal(25,10) DEFAULT NULL,
  `quantity_to_after` decimal(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_mutation_detail`
--

INSERT INTO `stock_mutation_detail` (`sm_d_id`, `sm_id`, `item_id`, `notes`, `quantity`, `quantity_from_before`, `quantity_to_before`, `quantity_from_after`, `quantity_to_after`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 4, '', '5.0000000000', '0.0000000000', '0.0000000000', '-5.0000000000', '5.0000000000', 1, 1, NULL, NULL, '2017-10-02 09:16:15', '2017-10-02 09:16:15', NULL),
(2, 2, 2, '', '5.0000000000', '0.0000000000', '0.0000000000', '-5.0000000000', '5.0000000000', 1, 1, NULL, NULL, '2017-10-02 09:16:15', '2017-10-02 09:16:15', NULL),
(3, 2, 3, '', '5.0000000000', '-50.0000000000', '-50.0000000000', '-55.0000000000', '-45.0000000000', 1, 1, NULL, NULL, '2017-10-02 09:16:15', '2017-10-02 09:16:15', NULL),
(4, 2, 1, '', '5.0000000000', '0.0000000000', '0.0000000000', '-5.0000000000', '5.0000000000', 1, 1, NULL, NULL, '2017-10-02 09:16:15', '2017-10-02 09:16:15', NULL),
(5, 3, 1, '', '0.0000000000', '-10.0000000000', '0.0000000000', '-10.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-10-02 21:53:54', '2017-10-02 21:53:54', NULL),
(6, 3, 2, '', '5.0000000000', '-10.0000000000', '0.0000000000', '-15.0000000000', '5.0000000000', 1, 1, NULL, NULL, '2017-10-02 21:53:54', '2017-10-02 21:53:54', NULL),
(7, 3, 3, '', '0.0000000000', '-60.0000000000', '0.0000000000', '-60.0000000000', '0.0000000000', 1, 1, NULL, NULL, '2017-10-02 21:53:54', '2017-10-02 21:53:54', NULL),
(8, 3, 4, '', '10.0000000000', '-15.0000000000', '0.0000000000', '-25.0000000000', '10.0000000000', 1, 1, NULL, NULL, '2017-10-02 21:53:54', '2017-10-02 21:53:54', NULL),
(9, 4, 4, '', '10.0000000000', '0.0000000000', '0.0000000000', '-10.0000000000', '10.0000000000', 1, 1, NULL, NULL, '2017-10-03 20:12:06', '2017-10-03 20:12:06', NULL),
(10, 4, 2, '', '10.0000000000', '0.0000000000', '0.0000000000', '-10.0000000000', '10.0000000000', 1, 1, NULL, NULL, '2017-10-03 20:12:06', '2017-10-03 20:12:06', NULL),
(11, 4, 3, '', '10.0000000000', '5.0000000000', '0.0000000000', '-5.0000000000', '10.0000000000', 1, 1, NULL, NULL, '2017-10-03 20:12:06', '2017-10-03 20:12:06', NULL),
(12, 4, 1, '', '10.0000000000', '0.0000000000', '0.0000000000', '-10.0000000000', '10.0000000000', 1, 1, NULL, NULL, '2017-10-03 20:12:06', '2017-10-03 20:12:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_opname`
--

CREATE TABLE `stock_opname` (
  `so_id` int(10) UNSIGNED NOT NULL,
  `so_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `so_date` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_opname`
--

INSERT INTO `stock_opname` (`so_id`, `so_number`, `so_status`, `warehouse_id`, `so_date`, `approved_by`, `approved_at`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'STOPN/MSU/201710/0001', 'APPROVED', 1, '2017-10-26', 1, '2017-09-30', 1, 1, 1, NULL, '2017-09-30 11:23:16', '2017-09-30 12:16:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_opname_detail`
--

CREATE TABLE `stock_opname_detail` (
  `so_d_id` int(10) UNSIGNED NOT NULL,
  `so_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `db_quantity` double(25,10) DEFAULT NULL,
  `actual_quantity` double(25,10) DEFAULT NULL,
  `quantity_difference` double(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_opname_detail`
--

INSERT INTO `stock_opname_detail` (`so_d_id`, `so_id`, `item_id`, `notes`, `db_quantity`, `actual_quantity`, `quantity_difference`, `company_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:23:17', '2017-09-30 11:48:30', '2017-09-30 11:48:30'),
(2, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:23:17', '2017-09-30 11:48:30', '2017-09-30 11:48:30'),
(3, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:23:17', '2017-09-30 11:48:30', '2017-09-30 11:48:30'),
(4, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:48:47', '2017-09-30 11:49:23', '2017-09-30 11:49:23'),
(5, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:48:47', '2017-09-30 11:49:23', '2017-09-30 11:49:23'),
(6, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 11:48:47', '2017-09-30 11:49:23', '2017-09-30 11:49:23'),
(7, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:50:08', '2017-09-30 11:58:33', '2017-09-30 11:58:33'),
(8, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:50:08', '2017-09-30 11:58:33', '2017-09-30 11:58:33'),
(9, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:50:08', '2017-09-30 11:58:33', '2017-09-30 11:58:33'),
(10, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:58:33', '2017-09-30 12:00:47', '2017-09-30 12:00:47'),
(11, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:58:33', '2017-09-30 12:00:47', '2017-09-30 12:00:47'),
(12, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 11:58:33', '2017-09-30 12:00:47', '2017-09-30 12:00:47'),
(13, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:00:47', '2017-09-30 12:02:13', '2017-09-30 12:02:13'),
(14, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:00:47', '2017-09-30 12:02:13', '2017-09-30 12:02:13'),
(15, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:00:47', '2017-09-30 12:02:13', '2017-09-30 12:02:13'),
(16, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:02:13', '2017-09-30 12:02:13', NULL),
(17, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:02:13', '2017-09-30 12:02:13', NULL),
(18, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 12:02:13', '2017-09-30 12:02:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `note` varchar(0) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `maxpoamount` double DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `real_name`, `email`, `phone`, `company_id`, `date_from`, `date_to`, `note`, `amount`, `maxpoamount`, `status`, `remember_token`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '$2y$10$7tSXd2W7XunXGV.CJq.WhO5SE/p0cLSx4Yz8WIIxI.uwPLQiLtsWS', 'Admin', 'admin@jafelmia.com', '0000000', 1, '2017-09-27', '2017-10-13', '', NULL, 200000, '0', 'ap0V57gvbVp9eJ9GGOKkMqbvXURKhvtTRFwojh0IuAwYFJQam6cV5dghGqRo', 0, 1, NULL, NULL, '2017-10-22 04:14:55', NULL),
(2, 'Sales', '$2y$10$NwRH9aKCFWgkE6VRrEkkw.JAsD5vOmveSVr.ThFv0vCA9zdT9mOiK', 'Sales 1', 'sales@sales.sales', '123456', 0, NULL, NULL, NULL, NULL, NULL, '0', NULL, 1, NULL, NULL, '2017-10-02 20:51:28', '2017-10-02 20:51:28', NULL),
(3, 'husni', '$2y$10$v4mJsTgXKgf.DTCrRUG4oeiSZLQo.DFZ5DXcj8Sxy1gn99HmCiC.K', 'husni mubarok', 'husni.mubarok@outlook.com', '08717161617', 1, NULL, NULL, NULL, NULL, 10000000, '0', NULL, 1, NULL, NULL, '2017-10-28 19:02:58', '2017-10-28 19:02:58', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `action_name_unique` (`name`);

--
-- Indexes for table `ap_invoice`
--
ALTER TABLE `ap_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `ap_invoice_detail`
--
ALTER TABLE `ap_invoice_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`ap_id`);

--
-- Indexes for table `ap_payment_detail`
--
ALTER TABLE `ap_payment_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`cp_id`);

--
-- Indexes for table `ar_receive_detail`
--
ALTER TABLE `ar_receive_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`cr_id`);

--
-- Indexes for table `cash_payment`
--
ALTER TABLE `cash_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `cash_payment_detail`
--
ALTER TABLE `cash_payment_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`cp_id`);

--
-- Indexes for table `cash_receive`
--
ALTER TABLE `cash_receive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`customer_id`);

--
-- Indexes for table `cash_receive_detail`
--
ALTER TABLE `cash_receive_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`cr_id`);

--
-- Indexes for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD PRIMARY KEY (`do_id`);

--
-- Indexes for table `delivery_order_detail`
--
ALTER TABLE `delivery_order_detail`
  ADD PRIMARY KEY (`do_d_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `module_name_unique` (`name`);

--
-- Indexes for table `module_group`
--
ALTER TABLE `module_group`
  ADD PRIMARY KEY (`mg_id`),
  ADD UNIQUE KEY `module_group_name_unique` (`name`);

--
-- Indexes for table `mst_account`
--
ALTER TABLE `mst_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `mst_bom`
--
ALTER TABLE `mst_bom`
  ADD PRIMARY KEY (`bom_id`);

--
-- Indexes for table `mst_bom_detail`
--
ALTER TABLE `mst_bom_detail`
  ADD PRIMARY KEY (`bom_d_id`);

--
-- Indexes for table `mst_company`
--
ALTER TABLE `mst_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `mst_currency`
--
ALTER TABLE `mst_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `mst_customer`
--
ALTER TABLE `mst_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `mst_department`
--
ALTER TABLE `mst_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `mst_item`
--
ALTER TABLE `mst_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `mst_paytype`
--
ALTER TABLE `mst_paytype`
  ADD PRIMARY KEY (`paytype_id`);

--
-- Indexes for table `mst_supplier`
--
ALTER TABLE `mst_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `mst_term`
--
ALTER TABLE `mst_term`
  ADD PRIMARY KEY (`term_id`);

--
-- Indexes for table `mst_unit`
--
ALTER TABLE `mst_unit`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `mst_warehouse`
--
ALTER TABLE `mst_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id`),
  ADD KEY `privilege_user_id_foreign` (`user_id`),
  ADD KEY `privilege_module_id_foreign` (`module_id`),
  ADD KEY `privilege_action_id_foreign` (`action_id`);

--
-- Indexes for table `privilege_group`
--
ALTER TABLE `privilege_group`
  ADD PRIMARY KEY (`pg_id`),
  ADD UNIQUE KEY `privilege_group_pg_name_unique` (`pg_name`);

--
-- Indexes for table `privilege_group_detail`
--
ALTER TABLE `privilege_group_detail`
  ADD PRIMARY KEY (`pg_d_id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`production_id`);

--
-- Indexes for table `production_detail`
--
ALTER TABLE `production_detail`
  ADD PRIMARY KEY (`production_d_id`);

--
-- Indexes for table `purchase_advance`
--
ALTER TABLE `purchase_advance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`po_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `purchase_receive`
--
ALTER TABLE `purchase_receive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `purchase_receive_detail`
--
ALTER TABLE `purchase_receive_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`prec_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `purchase_request`
--
ALTER TABLE `purchase_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`warehouse_id`);

--
-- Indexes for table `purchase_request_detail`
--
ALTER TABLE `purchase_request_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_id` (`pr_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_return_detail`
--
ALTER TABLE `purchase_return_detail`
  ADD PRIMARY KEY (`pr_d_id`);

--
-- Indexes for table `refund_order`
--
ALTER TABLE `refund_order`
  ADD PRIMARY KEY (`ro_id`);

--
-- Indexes for table `refund_order_detail`
--
ALTER TABLE `refund_order_detail`
  ADD PRIMARY KEY (`ro_d_id`);

--
-- Indexes for table `sales_invoice`
--
ALTER TABLE `sales_invoice`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`so_id`);

--
-- Indexes for table `sales_order_detail`
--
ALTER TABLE `sales_order_detail`
  ADD PRIMARY KEY (`so_d_id`);

--
-- Indexes for table `stock_adjustment`
--
ALTER TABLE `stock_adjustment`
  ADD PRIMARY KEY (`sa_id`);

--
-- Indexes for table `stock_adjustment_detail`
--
ALTER TABLE `stock_adjustment_detail`
  ADD PRIMARY KEY (`sa_d_id`);

--
-- Indexes for table `stock_inventory`
--
ALTER TABLE `stock_inventory`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `stock_movement`
--
ALTER TABLE `stock_movement`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `stock_mutation`
--
ALTER TABLE `stock_mutation`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `stock_mutation_detail`
--
ALTER TABLE `stock_mutation_detail`
  ADD PRIMARY KEY (`sm_d_id`);

--
-- Indexes for table `stock_opname`
--
ALTER TABLE `stock_opname`
  ADD PRIMARY KEY (`so_id`);

--
-- Indexes for table `stock_opname_detail`
--
ALTER TABLE `stock_opname_detail`
  ADD PRIMARY KEY (`so_d_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ap_invoice`
--
ALTER TABLE `ap_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ap_invoice_detail`
--
ALTER TABLE `ap_invoice_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ap_payment_detail`
--
ALTER TABLE `ap_payment_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ar_receive_detail`
--
ALTER TABLE `ar_receive_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cash_payment`
--
ALTER TABLE `cash_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cash_payment_detail`
--
ALTER TABLE `cash_payment_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `cash_receive`
--
ALTER TABLE `cash_receive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `cash_receive_detail`
--
ALTER TABLE `cash_receive_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cash_transfer`
--
ALTER TABLE `cash_transfer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `delivery_order`
--
ALTER TABLE `delivery_order`
  MODIFY `do_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `delivery_order_detail`
--
ALTER TABLE `delivery_order_detail`
  MODIFY `do_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `module_group`
--
ALTER TABLE `module_group`
  MODIFY `mg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_account`
--
ALTER TABLE `mst_account`
  MODIFY `account_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `mst_bom`
--
ALTER TABLE `mst_bom`
  MODIFY `bom_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_bom_detail`
--
ALTER TABLE `mst_bom_detail`
  MODIFY `bom_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mst_company`
--
ALTER TABLE `mst_company`
  MODIFY `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_currency`
--
ALTER TABLE `mst_currency`
  MODIFY `currency_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_customer`
--
ALTER TABLE `mst_customer`
  MODIFY `customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mst_department`
--
ALTER TABLE `mst_department`
  MODIFY `department_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_item`
--
ALTER TABLE `mst_item`
  MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mst_paytype`
--
ALTER TABLE `mst_paytype`
  MODIFY `paytype_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mst_supplier`
--
ALTER TABLE `mst_supplier`
  MODIFY `supplier_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mst_term`
--
ALTER TABLE `mst_term`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_unit`
--
ALTER TABLE `mst_unit`
  MODIFY `unit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `mst_warehouse`
--
ALTER TABLE `mst_warehouse`
  MODIFY `warehouse_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=749;
--
-- AUTO_INCREMENT for table `privilege_group`
--
ALTER TABLE `privilege_group`
  MODIFY `pg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `privilege_group_detail`
--
ALTER TABLE `privilege_group_detail`
  MODIFY `pg_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `production_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `production_detail`
--
ALTER TABLE `production_detail`
  MODIFY `production_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `purchase_advance`
--
ALTER TABLE `purchase_advance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `purchase_receive`
--
ALTER TABLE `purchase_receive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `purchase_receive_detail`
--
ALTER TABLE `purchase_receive_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `purchase_request`
--
ALTER TABLE `purchase_request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `purchase_request_detail`
--
ALTER TABLE `purchase_request_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `purchase_return`
--
ALTER TABLE `purchase_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `purchase_return_detail`
--
ALTER TABLE `purchase_return_detail`
  MODIFY `pr_d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `refund_order`
--
ALTER TABLE `refund_order`
  MODIFY `ro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `refund_order_detail`
--
ALTER TABLE `refund_order_detail`
  MODIFY `ro_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sales_invoice`
--
ALTER TABLE `sales_invoice`
  MODIFY `si_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `so_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sales_order_detail`
--
ALTER TABLE `sales_order_detail`
  MODIFY `so_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `stock_adjustment`
--
ALTER TABLE `stock_adjustment`
  MODIFY `sa_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stock_adjustment_detail`
--
ALTER TABLE `stock_adjustment_detail`
  MODIFY `sa_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `stock_inventory`
--
ALTER TABLE `stock_inventory`
  MODIFY `si_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `stock_movement`
--
ALTER TABLE `stock_movement`
  MODIFY `sm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `stock_mutation`
--
ALTER TABLE `stock_mutation`
  MODIFY `sm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stock_mutation_detail`
--
ALTER TABLE `stock_mutation_detail`
  MODIFY `sm_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `stock_opname`
--
ALTER TABLE `stock_opname`
  MODIFY `so_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stock_opname_detail`
--
ALTER TABLE `stock_opname_detail`
  MODIFY `so_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `privilege`
--
ALTER TABLE `privilege`
  ADD CONSTRAINT `privilege_action_id_foreign` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `privilege_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `privilege_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
