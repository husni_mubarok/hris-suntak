/*
 Navicat Premium Data Transfer

 Source Server         : MySql
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : msul

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 04/11/2017 03:21:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for action
-- ----------------------------
DROP TABLE IF EXISTS `action`;
CREATE TABLE `action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of action
-- ----------------------------
BEGIN;
INSERT INTO `action` VALUES (1, 'read', 'read', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `action` VALUES (2, 'create', 'create', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `action` VALUES (3, 'update', 'update', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `action` VALUES (4, 'delete', 'delete', 0, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for ap_invoice
-- ----------------------------
DROP TABLE IF EXISTS `ap_invoice`;
CREATE TABLE `ap_invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ap_reference` varchar(50) DEFAULT NULL,
  `ap_date` date DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ap_invoice
-- ----------------------------
BEGIN;
INSERT INTO `ap_invoice` VALUES (1, 'PRCX-24091700001', '2017-09-24', 1, 0, 1, 0, '', 0, '', 0, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, NULL, '2017-09-24 00:00:00', '2017-10-12 20:33:51', NULL);
INSERT INTO `ap_invoice` VALUES (2, 'PRCX-24091700002', '2017-09-24', 0, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', NULL, NULL);
INSERT INTO `ap_invoice` VALUES (3, 'APCX-24091700003', '2017-09-24', 0, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', NULL, NULL);
INSERT INTO `ap_invoice` VALUES (4, 'APCX-24091700004', '2017-09-24', 0, 0, NULL, 0, '', 0, '', 0, '24-09-2017 01-13-0954eb9e632b7c7_-_peruvian-lily-pink-yellow-xl.jpg', 1, 1, NULL, '2017-09-24 00:00:00', '2017-09-24 13:13:09', NULL);
INSERT INTO `ap_invoice` VALUES (5, 'APCX-24091700005', '2017-09-24', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', '2017-09-24 13:13:25', NULL);
INSERT INTO `ap_invoice` VALUES (6, 'APCX-26091700006', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:26:14', NULL);
INSERT INTO `ap_invoice` VALUES (7, 'APCX-26091700007', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:26:32', NULL);
INSERT INTO `ap_invoice` VALUES (8, 'APCX-26091700008', '2017-09-26', 1, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:26:54', NULL);
INSERT INTO `ap_invoice` VALUES (9, 'APCX-26091700009', '2017-09-26', 0, 0, NULL, 0, '', 0, '', 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:27:28', NULL);
INSERT INTO `ap_invoice` VALUES (10, 'APCX-01101700010', '2017-10-13', 2, 0, NULL, 0, '', 0, '', 0, NULL, 1, 1, NULL, '2017-10-01 00:00:00', '2017-09-30 23:14:43', NULL);
INSERT INTO `ap_invoice` VALUES (11, 'APCX-08101700011', '2017-10-08', 2, 0, NULL, 0, '', 0, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 00:21:52', NULL);
COMMIT;

-- ----------------------------
-- Table structure for ap_invoice_detail
-- ----------------------------
DROP TABLE IF EXISTS `ap_invoice_detail`;
CREATE TABLE `ap_invoice_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ap_id` int(10) unsigned DEFAULT NULL,
  `prec_d_id` int(10) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `cp_amount` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`ap_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ap_invoice_detail
-- ----------------------------
BEGIN;
INSERT INTO `ap_invoice_detail` VALUES (3, 4, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-24 12:09:28', '2017-09-24 12:09:28', NULL);
INSERT INTO `ap_invoice_detail` VALUES (4, 4, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-24 12:09:28', '2017-09-24 12:09:28', NULL);
INSERT INTO `ap_invoice_detail` VALUES (5, 5, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-24 13:13:25', '2017-09-24 13:13:25', NULL);
INSERT INTO `ap_invoice_detail` VALUES (6, 5, 1, 5, 26, NULL, NULL, NULL, '2017-09-24 13:13:25', '2017-09-24 13:13:25', NULL);
INSERT INTO `ap_invoice_detail` VALUES (7, 6, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-26 06:26:15', '2017-09-26 06:26:15', NULL);
INSERT INTO `ap_invoice_detail` VALUES (8, 7, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-26 06:26:32', '2017-09-26 06:26:32', NULL);
INSERT INTO `ap_invoice_detail` VALUES (9, 7, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-26 06:26:32', '2017-09-26 06:26:32', NULL);
INSERT INTO `ap_invoice_detail` VALUES (10, 8, 1, 108000, NULL, NULL, NULL, NULL, '2017-09-26 06:26:54', '2017-09-26 06:26:54', NULL);
INSERT INTO `ap_invoice_detail` VALUES (11, 9, 2, 2000000, NULL, NULL, NULL, NULL, '2017-09-26 06:27:17', '2017-09-26 06:27:17', NULL);
INSERT INTO `ap_invoice_detail` VALUES (12, 10, 7, 150000, 170000, NULL, NULL, NULL, '2017-09-30 23:14:37', '2017-09-30 23:14:37', NULL);
INSERT INTO `ap_invoice_detail` VALUES (13, 11, 13, 1, 1, NULL, NULL, NULL, '2017-10-08 00:21:52', '2017-10-08 00:21:52', NULL);
COMMIT;

-- ----------------------------
-- Table structure for ap_payment_detail
-- ----------------------------
DROP TABLE IF EXISTS `ap_payment_detail`;
CREATE TABLE `ap_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(10) unsigned DEFAULT NULL,
  `ap_d_id` int(10) unsigned DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`cp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ap_payment_detail
-- ----------------------------
BEGIN;
INSERT INTO `ap_payment_detail` VALUES (3, 4, 2, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 12:09:28', '2017-09-24 12:09:28', NULL);
INSERT INTO `ap_payment_detail` VALUES (4, 4, 1, NULL, 108000, NULL, NULL, NULL, '2017-09-24 12:09:28', '2017-09-24 12:09:28', NULL);
INSERT INTO `ap_payment_detail` VALUES (5, 5, 2, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 13:13:25', '2017-09-24 13:13:25', NULL);
INSERT INTO `ap_payment_detail` VALUES (6, 5, 1, NULL, 5, NULL, NULL, NULL, '2017-09-24 13:13:25', '2017-09-25 09:26:22', '2017-09-25 09:26:22');
INSERT INTO `ap_payment_detail` VALUES (7, 13, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-24 18:08:46', '2017-09-24 18:08:46', NULL);
INSERT INTO `ap_payment_detail` VALUES (8, 13, 6, NULL, 5, NULL, NULL, NULL, '2017-09-24 18:08:46', '2017-09-25 09:28:55', '2017-09-25 09:28:55');
INSERT INTO `ap_payment_detail` VALUES (9, 1, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 18:08:02', '2017-09-25 18:08:02', NULL);
INSERT INTO `ap_payment_detail` VALUES (10, 1, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 18:08:02', '2017-09-25 18:14:06', '2017-09-25 18:14:06');
INSERT INTO `ap_payment_detail` VALUES (11, 1, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 18:13:54', '2017-09-25 18:14:10', '2017-09-25 18:14:10');
INSERT INTO `ap_payment_detail` VALUES (12, 1, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 18:13:55', '2017-09-25 18:14:02', '2017-09-25 18:14:02');
INSERT INTO `ap_payment_detail` VALUES (13, 2, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-25 18:17:34', '2017-09-25 18:17:34', NULL);
INSERT INTO `ap_payment_detail` VALUES (14, 2, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 18:17:34', '2017-09-25 18:17:34', NULL);
INSERT INTO `ap_payment_detail` VALUES (15, 3, 6, NULL, 5, NULL, NULL, NULL, '2017-09-25 18:17:50', '2017-09-25 18:17:50', NULL);
INSERT INTO `ap_payment_detail` VALUES (16, 5, 5, NULL, 2000000, NULL, NULL, NULL, '2017-09-26 06:33:05', '2017-09-26 06:33:05', NULL);
INSERT INTO `ap_payment_detail` VALUES (17, 6, 7, NULL, 108000, NULL, NULL, NULL, '2017-09-28 06:03:59', '2017-09-28 06:03:59', NULL);
INSERT INTO `ap_payment_detail` VALUES (18, 8, 12, NULL, 150000, NULL, NULL, NULL, '2017-09-30 23:15:12', '2017-09-30 23:15:12', NULL);
INSERT INTO `ap_payment_detail` VALUES (19, 9, 12, NULL, 20000, NULL, NULL, NULL, '2017-10-08 00:44:34', '2017-10-08 00:44:34', NULL);
INSERT INTO `ap_payment_detail` VALUES (20, 9, 13, NULL, 1, NULL, NULL, NULL, '2017-10-08 00:44:34', '2017-10-08 00:44:34', NULL);
INSERT INTO `ap_payment_detail` VALUES (21, 10, 6, NULL, 1, NULL, NULL, NULL, '2017-10-08 00:45:26', '2017-10-08 00:45:26', NULL);
COMMIT;

-- ----------------------------
-- Table structure for ar_receive_detail
-- ----------------------------
DROP TABLE IF EXISTS `ar_receive_detail`;
CREATE TABLE `ar_receive_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cr_id` int(10) unsigned DEFAULT NULL,
  `ar_id` int(10) unsigned DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ar_receive_detail
-- ----------------------------
BEGIN;
INSERT INTO `ar_receive_detail` VALUES (1, 17, 2, NULL, 100, NULL, NULL, NULL, '2017-10-08 02:04:42', '2017-10-08 02:04:42', NULL);
INSERT INTO `ar_receive_detail` VALUES (2, 18, 2, NULL, 900, NULL, NULL, NULL, '2017-10-08 02:09:33', '2017-10-08 02:09:33', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cash_payment
-- ----------------------------
DROP TABLE IF EXISTS `cash_payment`;
CREATE TABLE `cash_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_reference` varchar(50) DEFAULT NULL,
  `cp_date` date DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cash_payment
-- ----------------------------
BEGIN;
INSERT INTO `cash_payment` VALUES (1, 'CPCX-26091700001', '2017-09-26', 0, 3, 8, '', '', 1, 0, NULL, 1, 1, NULL, '2017-09-26 00:00:00', '2017-10-08 00:58:04', NULL);
INSERT INTO `cash_payment` VALUES (2, 'CPCX-26091700002', '2017-09-26', 1, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-25 18:17:34', NULL);
INSERT INTO `cash_payment` VALUES (3, 'CPCX-26091700003', '2017-09-26', 3, 0, 14, '', '', NULL, 0, '25-09-2017 10-13-35fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-25 22:13:54', NULL);
INSERT INTO `cash_payment` VALUES (4, 'CPPY-26091700004', '2017-09-26', 0, 4, 12, '', '', NULL, 0, '26-09-2017 12-45-26fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 00:45:26', NULL);
INSERT INTO `cash_payment` VALUES (5, 'CPCX-26091700005', '2017-09-26', 1, 1, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:33:10', NULL);
INSERT INTO `cash_payment` VALUES (6, 'CPCX-28091700006', '2017-09-28', 1, 1, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-09-28 00:00:00', '2017-09-28 06:04:04', NULL);
INSERT INTO `cash_payment` VALUES (7, 'CPPY-28091700007', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-28 00:00:00', NULL, NULL);
INSERT INTO `cash_payment` VALUES (8, 'CPCX-01101700008', '2017-10-01', 2, 3, NULL, '', '', NULL, 0, NULL, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-09-30 23:15:21', NULL);
INSERT INTO `cash_payment` VALUES (9, 'CPCX-08101700009', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 00:44:34', NULL);
INSERT INTO `cash_payment` VALUES (10, 'CPCX-08101700010', '2017-10-08', 1, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 00:45:26', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cash_payment_detail
-- ----------------------------
DROP TABLE IF EXISTS `cash_payment_detail`;
CREATE TABLE `cash_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `debt` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`cp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cash_payment_detail
-- ----------------------------
BEGIN;
INSERT INTO `cash_payment_detail` VALUES (1, 3, 'Test', 0, 1000000, 0, NULL, NULL, NULL, '2017-09-25 21:45:16', '2017-09-25 21:55:29', '2017-09-25 21:55:29');
INSERT INTO `cash_payment_detail` VALUES (2, 3, 'DASDASD', 0, 23123123, 12, NULL, NULL, NULL, '2017-09-25 21:48:05', '2017-09-25 21:55:24', '2017-09-25 21:55:24');
INSERT INTO `cash_payment_detail` VALUES (3, 3, 'ASSaS', 13, 23133, 213, NULL, NULL, NULL, '2017-09-25 21:49:28', '2017-09-25 21:55:12', '2017-09-25 21:55:12');
INSERT INTO `cash_payment_detail` VALUES (4, 3, 'sadad', 8, 342342, 23, NULL, NULL, NULL, '2017-09-25 21:52:08', '2017-09-25 21:55:09', '2017-09-25 21:55:09');
INSERT INTO `cash_payment_detail` VALUES (5, 3, 'sasASSas', 8, 20000, 0, NULL, NULL, NULL, '2017-09-25 21:53:10', '2017-09-25 21:55:27', '2017-09-25 21:55:27');
INSERT INTO `cash_payment_detail` VALUES (6, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 21:55:02', '2017-09-25 21:55:19', '2017-09-25 21:55:19');
INSERT INTO `cash_payment_detail` VALUES (7, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 21:55:05', '2017-09-25 21:55:22', '2017-09-25 21:55:22');
INSERT INTO `cash_payment_detail` VALUES (8, 3, 'Test transaction', 14, 500000, 222222, NULL, NULL, NULL, '2017-09-25 21:56:53', '2017-09-25 22:13:24', '2017-09-25 22:13:24');
INSERT INTO `cash_payment_detail` VALUES (9, 3, 'Test', 11, 500000, 2000000, NULL, NULL, NULL, '2017-09-25 22:10:29', '2017-09-25 22:21:32', NULL);
INSERT INTO `cash_payment_detail` VALUES (10, 3, 'Test', 7, 300000, 500000, NULL, NULL, NULL, '2017-09-25 22:11:16', '2017-09-25 22:11:16', NULL);
INSERT INTO `cash_payment_detail` VALUES (11, 4, 'Test', 6, 500000, 0, NULL, NULL, NULL, '2017-09-26 00:45:08', '2017-09-26 00:45:08', NULL);
INSERT INTO `cash_payment_detail` VALUES (12, 4, 'Test 1', 14, 0, 3000000, NULL, NULL, NULL, '2017-09-26 00:45:22', '2017-09-26 00:45:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cash_receive
-- ----------------------------
DROP TABLE IF EXISTS `cash_receive`;
CREATE TABLE `cash_receive` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cr_reference` varchar(50) DEFAULT NULL,
  `cr_date` date DEFAULT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cash_receive
-- ----------------------------
BEGIN;
INSERT INTO `cash_receive` VALUES (1, 'CPCX-26091700001', '2017-09-26', 0, 3, 6, '', 1, '', 0, NULL, 1, 1, NULL, '2017-09-26 00:00:00', '2017-10-08 00:57:54', NULL);
INSERT INTO `cash_receive` VALUES (2, 'CPCX-26091700002', '2017-09-26', 1, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-25 18:17:34', NULL);
INSERT INTO `cash_receive` VALUES (3, 'CPCX-26091700003', '2017-09-26', 3, 0, 14, '', NULL, '', 0, '25-09-2017 10-13-35fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-25 22:13:54', NULL);
INSERT INTO `cash_receive` VALUES (4, 'CPPY-26091700004', '2017-09-26', 0, 4, 12, '', NULL, '', 0, '26-09-2017 12-45-26fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 00:45:26', NULL);
INSERT INTO `cash_receive` VALUES (5, 'CRPY-26091700005', '2017-09-26', 0, 4, 7, '', NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 01:13:43', NULL);
INSERT INTO `cash_receive` VALUES (6, 'CRPY-28091700006', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 9, NULL, 1, 1, NULL, '2017-09-28 00:00:00', '2017-10-08 01:58:08', NULL);
INSERT INTO `cash_receive` VALUES (7, 'CRPY-28091700007', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-28 00:00:00', NULL, NULL);
INSERT INTO `cash_receive` VALUES (8, 'CRPY-28091700008', '2017-09-28', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-09-28 00:00:00', NULL, NULL);
INSERT INTO `cash_receive` VALUES (9, 'CPCX-08101700009', '2017-10-08', 0, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `cash_receive` VALUES (10, 'CPCX-08101700010', '2017-10-08', 2, 1, NULL, '', 1, '', 0, NULL, 1, 1, NULL, '2017-10-08 00:00:00', '2017-10-08 01:50:55', NULL);
INSERT INTO `cash_receive` VALUES (11, 'CPCX-08101700011', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 01:36:14', NULL);
INSERT INTO `cash_receive` VALUES (12, 'CPCX-08101700012', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 01:37:34', NULL);
INSERT INTO `cash_receive` VALUES (13, 'CPCX-08101700013', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 01:39:04', NULL);
INSERT INTO `cash_receive` VALUES (14, 'CPCX-08101700014', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 01:49:15', NULL);
INSERT INTO `cash_receive` VALUES (15, 'CPCX-08101700015', '2017-10-08', 2, 1, NULL, '', NULL, '', 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 01:49:44', NULL);
INSERT INTO `cash_receive` VALUES (16, 'CPCX-08101700016', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 02:03:49', NULL);
INSERT INTO `cash_receive` VALUES (17, 'CPCX-08101700017', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 02:04:42', NULL);
INSERT INTO `cash_receive` VALUES (18, 'CPCX-08101700018', '2017-10-08', 2, 0, NULL, '', NULL, NULL, 0, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-08 02:09:33', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cash_receive_detail
-- ----------------------------
DROP TABLE IF EXISTS `cash_receive_detail`;
CREATE TABLE `cash_receive_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cr_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `debt` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cash_receive_detail
-- ----------------------------
BEGIN;
INSERT INTO `cash_receive_detail` VALUES (1, 3, 'Test', 0, 1000000, 0, NULL, NULL, NULL, '2017-09-25 21:45:16', '2017-09-25 21:55:29', '2017-09-25 21:55:29');
INSERT INTO `cash_receive_detail` VALUES (2, 3, 'DASDASD', 0, 23123123, 12, NULL, NULL, NULL, '2017-09-25 21:48:05', '2017-09-25 21:55:24', '2017-09-25 21:55:24');
INSERT INTO `cash_receive_detail` VALUES (3, 3, 'ASSaS', 13, 23133, 213, NULL, NULL, NULL, '2017-09-25 21:49:28', '2017-09-25 21:55:12', '2017-09-25 21:55:12');
INSERT INTO `cash_receive_detail` VALUES (4, 3, 'sadad', 8, 342342, 23, NULL, NULL, NULL, '2017-09-25 21:52:08', '2017-09-25 21:55:09', '2017-09-25 21:55:09');
INSERT INTO `cash_receive_detail` VALUES (5, 3, 'sasASSas', 8, 20000, 0, NULL, NULL, NULL, '2017-09-25 21:53:10', '2017-09-25 21:55:27', '2017-09-25 21:55:27');
INSERT INTO `cash_receive_detail` VALUES (6, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 21:55:02', '2017-09-25 21:55:19', '2017-09-25 21:55:19');
INSERT INTO `cash_receive_detail` VALUES (7, 3, 'asdad', 9, 123, 123, NULL, NULL, NULL, '2017-09-25 21:55:05', '2017-09-25 21:55:22', '2017-09-25 21:55:22');
INSERT INTO `cash_receive_detail` VALUES (8, 3, 'Test transaction', 14, 500000, 222222, NULL, NULL, NULL, '2017-09-25 21:56:53', '2017-09-25 22:13:24', '2017-09-25 22:13:24');
INSERT INTO `cash_receive_detail` VALUES (9, 3, 'Test', 11, 500000, 2000000, NULL, NULL, NULL, '2017-09-25 22:10:29', '2017-09-25 22:21:32', NULL);
INSERT INTO `cash_receive_detail` VALUES (10, 3, 'Test', 7, 300000, 500000, NULL, NULL, NULL, '2017-09-25 22:11:16', '2017-09-25 22:11:16', NULL);
INSERT INTO `cash_receive_detail` VALUES (11, 4, 'Test', 6, 500000, 0, NULL, NULL, NULL, '2017-09-26 00:45:08', '2017-09-26 00:45:08', NULL);
INSERT INTO `cash_receive_detail` VALUES (12, 4, 'Test 1', 14, 0, 3000000, NULL, NULL, NULL, '2017-09-26 00:45:22', '2017-09-26 00:45:22', NULL);
INSERT INTO `cash_receive_detail` VALUES (13, 5, 'Test', 10, 50, 900000, NULL, NULL, NULL, '2017-09-26 01:13:33', '2017-09-26 01:13:40', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cash_transfer
-- ----------------------------
DROP TABLE IF EXISTS `cash_transfer`;
CREATE TABLE `cash_transfer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ct_reference` varchar(50) DEFAULT NULL,
  `ct_date` date DEFAULT NULL,
  `account_from_id` int(11) DEFAULT NULL,
  `account_to_id` int(11) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cash_transfer
-- ----------------------------
BEGIN;
INSERT INTO `cash_transfer` VALUES (1, 'PRCX-24091700001', '2017-09-24', 5, 5, NULL, 34234, '', 0, 1, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, NULL, '2017-09-24 00:00:00', '2017-10-08 00:59:05', NULL);
INSERT INTO `cash_transfer` VALUES (2, 'PRCX-24091700002', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', NULL, NULL);
INSERT INTO `cash_transfer` VALUES (3, 'APCX-24091700003', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', NULL, NULL);
INSERT INTO `cash_transfer` VALUES (4, 'APCX-24091700004', '2017-09-24', NULL, NULL, NULL, NULL, '', 0, NULL, '24-09-2017 01-13-0954eb9e632b7c7_-_peruvian-lily-pink-yellow-xl.jpg', 1, 1, NULL, '2017-09-24 00:00:00', '2017-09-24 13:13:09', NULL);
INSERT INTO `cash_transfer` VALUES (5, 'APCX-24091700005', '2017-09-24', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', '2017-09-24 13:13:25', NULL);
INSERT INTO `cash_transfer` VALUES (6, 'CTTR-26091700006', '2017-09-26', 12, 10, NULL, 50000000, 'qweqwqeqw', 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 02:07:18', NULL);
INSERT INTO `cash_transfer` VALUES (7, 'CTTR-26091700007', '2017-09-26', 6, 8, NULL, 350000, '', 0, NULL, '26-09-2017 05-59-10fa393710-cb8d-4059-a9b3-64743abf3e11.jpg', 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 05:59:10', NULL);
INSERT INTO `cash_transfer` VALUES (8, 'CTTR-26091700008', '2017-09-26', 5, 10, NULL, 5000000, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 02:24:59', NULL);
INSERT INTO `cash_transfer` VALUES (9, 'CTTR-26091700009', '2017-09-26', 5, 8, NULL, 450000, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 04:05:12', NULL);
INSERT INTO `cash_transfer` VALUES (10, 'CTTR-28091700010', '2017-09-28', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-28 00:00:00', NULL, NULL);
INSERT INTO `cash_transfer` VALUES (11, 'CTTR-01101700011', '2017-10-01', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-01 00:00:00', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for delivery_order
-- ----------------------------
DROP TABLE IF EXISTS `delivery_order`;
CREATE TABLE `delivery_order` (
  `do_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_id` int(11) DEFAULT NULL,
  `do_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `do_date` date DEFAULT NULL,
  `do_delivery_fee` decimal(25,10) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `do_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `approved_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of delivery_order
-- ----------------------------
BEGIN;
INSERT INTO `delivery_order` VALUES (1, 1, 'DO/MSU/201710/0001', NULL, 2, 1, '0000-00-00', 222.0000000000, '', 'DRAFT', NULL, 1, NULL, NULL, NULL, '2017-09-16 23:08:32', NULL, '2017-09-16 23:08:32', '2017-09-17 06:09:17');
INSERT INTO `delivery_order` VALUES (2, 1, 'DO/MSU/201710/0002', NULL, 2, 1, '0000-00-00', 255.0000000000, '', '', NULL, 1, NULL, NULL, NULL, '2017-09-16 23:10:33', NULL, '2017-10-07 16:32:45', NULL);
INSERT INTO `delivery_order` VALUES (3, 1, 'DO/MSU/201710/0003', NULL, 2, 1, '0000-00-00', 2000.0000000000, '', '', NULL, 1, NULL, NULL, NULL, '2017-09-16 23:32:34', NULL, '2017-10-07 16:33:00', NULL);
INSERT INTO `delivery_order` VALUES (4, 2, 'DO/MSU/201710/0004', NULL, 5, 1, '1970-01-01', 100000.0000000000, '', 'COMPLETED', NULL, 1, 1, 1, NULL, '2017-09-26 07:03:42', '2017-10-07 00:00:00', '2017-10-07 16:47:01', NULL);
INSERT INTO `delivery_order` VALUES (5, 2, 'DO/MSU/201710/0005', NULL, 5, 1, '1970-01-01', 100000.0000000000, '', 'DRAFT', NULL, 1, NULL, 1, NULL, '2017-09-26 07:07:55', NULL, '2017-09-26 07:09:28', NULL);
INSERT INTO `delivery_order` VALUES (6, 1, 'DO/MSU/201710/0001', NULL, 2, 1, '0000-00-00', 50000.0000000000, '', 'APPROVAL', NULL, 1, NULL, NULL, NULL, '2017-10-05 08:05:38', NULL, '2017-10-07 16:16:48', NULL);
INSERT INTO `delivery_order` VALUES (7, 4, 'DO/MSU/201710/0001', NULL, 3, 1, '2017-10-25', 0.0000000000, '', 'DRAFT', 1, 1, NULL, NULL, NULL, '2017-10-22 06:17:14', NULL, '2017-10-22 06:17:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for delivery_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `delivery_order_detail`;
CREATE TABLE `delivery_order_detail` (
  `do_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `do_id` int(11) DEFAULT NULL,
  `so_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `delivery_quantity` decimal(25,10) DEFAULT NULL,
  `do_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`do_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of delivery_order_detail
-- ----------------------------
BEGIN;
INSERT INTO `delivery_order_detail` VALUES (1, 2, 4, 1, 1.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:10:33', '2017-09-16 23:10:33', NULL);
INSERT INTO `delivery_order_detail` VALUES (2, 2, 5, 3, 1.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:10:33', '2017-09-16 23:10:33', NULL);
INSERT INTO `delivery_order_detail` VALUES (3, 2, 6, 2, 1.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:10:33', '2017-09-16 23:10:33', NULL);
INSERT INTO `delivery_order_detail` VALUES (4, 3, 4, 1, 5.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:32:34', '2017-09-16 23:32:34', NULL);
INSERT INTO `delivery_order_detail` VALUES (5, 3, 5, 3, 77.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:32:34', '2017-09-16 23:32:34', NULL);
INSERT INTO `delivery_order_detail` VALUES (6, 3, 6, 2, 69.0000000000, NULL, 1, NULL, NULL, '2017-09-16 23:32:34', '2017-09-16 23:32:34', NULL);
INSERT INTO `delivery_order_detail` VALUES (7, 4, NULL, 1, 500.0000000000, NULL, 1, NULL, 1, '2017-09-26 07:03:42', '2017-09-26 07:05:18', '2017-09-26 07:05:18');
INSERT INTO `delivery_order_detail` VALUES (8, 4, NULL, 1, 200.0000000000, NULL, 1, NULL, 1, '2017-09-26 07:05:18', '2017-09-26 07:05:44', '2017-09-26 07:05:44');
INSERT INTO `delivery_order_detail` VALUES (9, 4, NULL, 1, 500.0000000000, NULL, 1, NULL, NULL, '2017-09-26 07:05:44', '2017-09-26 07:05:44', NULL);
INSERT INTO `delivery_order_detail` VALUES (10, 5, NULL, 1, 10000.0000000000, NULL, 1, NULL, 1, '2017-09-26 07:07:56', '2017-09-26 07:09:28', '2017-09-26 07:09:28');
INSERT INTO `delivery_order_detail` VALUES (11, 5, NULL, 1, 500.0000000000, NULL, 1, NULL, NULL, '2017-09-26 07:09:28', '2017-09-26 07:09:28', NULL);
INSERT INTO `delivery_order_detail` VALUES (12, 6, NULL, 1, 2.0000000000, NULL, 1, NULL, NULL, '2017-10-05 08:05:38', '2017-10-05 08:05:38', NULL);
INSERT INTO `delivery_order_detail` VALUES (13, 6, NULL, 3, 5.0000000000, NULL, 1, NULL, NULL, '2017-10-05 08:05:38', '2017-10-05 08:05:38', NULL);
INSERT INTO `delivery_order_detail` VALUES (14, 6, NULL, 2, 6.0000000000, NULL, 1, NULL, NULL, '2017-10-05 08:05:38', '2017-10-05 08:05:38', NULL);
INSERT INTO `delivery_order_detail` VALUES (15, 7, NULL, 2, 1.0000000000, NULL, 1, NULL, NULL, '2017-10-22 06:17:14', '2017-10-22 06:17:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_user_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_03_15_100000_create_module_table', 1);
INSERT INTO `migrations` VALUES (4, '2017_03_15_100001_create_action_table', 1);
INSERT INTO `migrations` VALUES (5, '2017_03_15_100002_create_privilege_table', 1);
INSERT INTO `migrations` VALUES (6, '2017_07_09_100123_create_mst_company_table', 1);
INSERT INTO `migrations` VALUES (7, '2017_07_09_100134_create_mst_account_table', 1);
INSERT INTO `migrations` VALUES (8, '2017_07_09_100136_create_mst_customer_table', 1);
INSERT INTO `migrations` VALUES (9, '2017_07_09_100137_create_mst_supplier_table', 1);
INSERT INTO `migrations` VALUES (10, '2017_07_09_100139_create_mst_department_table', 1);
INSERT INTO `migrations` VALUES (11, '2017_07_09_100142_create_mst_item_table', 1);
INSERT INTO `migrations` VALUES (12, '2017_07_09_100144_create_mst_term_table', 1);
INSERT INTO `migrations` VALUES (13, '2017_07_09_100147_create_mst_warehouse_table', 1);
INSERT INTO `migrations` VALUES (14, '2017_07_09_100150_create_mst_bom_table', 2);
INSERT INTO `migrations` VALUES (15, '2017_07_09_100152_create_mst_bom_detail_table', 2);
INSERT INTO `migrations` VALUES (16, '2017_07_09_100155_create_sales_order_table', 3);
INSERT INTO `migrations` VALUES (17, '2017_07_09_100158_create_sales_order_detail_table', 3);
INSERT INTO `migrations` VALUES (18, '2017_09_16_151017_create_stock_movement_table', 3);
INSERT INTO `migrations` VALUES (19, '2017_09_16_151049_create_stock_inventory_table', 3);
INSERT INTO `migrations` VALUES (20, '2017_09_16_201922_create_delivery_order_table', 4);
INSERT INTO `migrations` VALUES (21, '2017_09_16_201938_create_delivery_order_detail_table', 4);
INSERT INTO `migrations` VALUES (22, '2017_09_18_145256_create_sales_invoice_table', 5);
INSERT INTO `migrations` VALUES (23, '2017_09_19_153704_create_refund_order_table', 6);
INSERT INTO `migrations` VALUES (24, '2017_09_19_153721_create_refund_order_detail_table', 6);
INSERT INTO `migrations` VALUES (25, '2017_09_23_095508_create_module_group_table', 7);
INSERT INTO `migrations` VALUES (26, '2017_09_23_150907_create_production_table', 7);
INSERT INTO `migrations` VALUES (27, '2017_09_23_150924_create_production_detail_table', 7);
COMMIT;

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of module
-- ----------------------------
BEGIN;
INSERT INTO `module` VALUES (1, 'admin', 'user', 'user', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `module` VALUES (2, 'admin', 'module', 'module', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `module` VALUES (3, 'admin', 'action', 'action', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `module` VALUES (4, 'admin', 'privilege', 'privilege', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `module` VALUES (5, 'admin', 'mst_company', 'Master Company', 0, 1, NULL, NULL, '2017-10-07 17:15:31', NULL);
INSERT INTO `module` VALUES (6, NULL, 'mst_account', 'Master Account', 0, 1, NULL, NULL, '2017-10-02 14:25:26', NULL);
INSERT INTO `module` VALUES (7, NULL, 'mst_bom', 'Bill of Material', 0, 1, NULL, NULL, '2017-10-02 14:25:40', NULL);
INSERT INTO `module` VALUES (8, NULL, 'mst_customer', 'Master Customer', 0, 1, NULL, NULL, '2017-10-02 14:25:53', NULL);
INSERT INTO `module` VALUES (9, NULL, 'mst_supplier', 'Master Supplier', 0, 1, NULL, NULL, '2017-10-02 14:26:05', NULL);
INSERT INTO `module` VALUES (10, NULL, 'mst_department', 'Master Department', 0, 1, NULL, NULL, '2017-10-02 14:26:16', NULL);
INSERT INTO `module` VALUES (11, NULL, 'mst_warehouse', 'Master Warehouse', 0, 1, NULL, NULL, '2017-10-02 14:26:31', NULL);
INSERT INTO `module` VALUES (12, NULL, 'mst_item', 'Master Item', 0, 1, NULL, NULL, '2017-10-02 14:26:42', NULL);
INSERT INTO `module` VALUES (13, NULL, 'mst_term', 'Payment Type', 0, 1, NULL, NULL, '2017-10-02 14:27:26', NULL);
INSERT INTO `module` VALUES (14, NULL, 'purchase_order', 'PO', 0, 1, NULL, NULL, '2017-10-02 14:27:39', NULL);
INSERT INTO `module` VALUES (15, NULL, 'receive_order', 'Receiving', 0, 1, NULL, NULL, '2017-10-02 14:27:50', NULL);
INSERT INTO `module` VALUES (16, NULL, 'purchase_invoice', 'Purchase Invoice', 0, 1, NULL, NULL, '2017-10-02 14:28:03', NULL);
INSERT INTO `module` VALUES (17, NULL, 'return_order', 'Return Order', 0, 1, NULL, NULL, '2017-10-02 14:28:18', NULL);
INSERT INTO `module` VALUES (18, NULL, 'sales_order', 'SO', 0, 1, NULL, NULL, '2017-10-02 14:28:28', NULL);
INSERT INTO `module` VALUES (19, NULL, 'delivery_order', 'Shipping', 0, 1, NULL, NULL, '2017-10-02 14:28:39', NULL);
INSERT INTO `module` VALUES (20, NULL, 'sales_invoice', 'Sales Invoice', 0, 1, NULL, NULL, '2017-10-02 14:28:50', NULL);
INSERT INTO `module` VALUES (21, NULL, 'refund_order', 'Refund Order', 0, 1, NULL, NULL, '2017-10-02 14:29:02', NULL);
INSERT INTO `module` VALUES (22, NULL, 'production', 'Work Order', 0, 1, NULL, NULL, '2017-10-02 14:29:16', NULL);
INSERT INTO `module` VALUES (23, NULL, 'production_qc', 'Convert Work Order', 0, 1, NULL, NULL, '2017-10-02 14:29:29', NULL);
INSERT INTO `module` VALUES (24, NULL, 'production_reject', 'Reject', 0, 1, NULL, NULL, '2017-10-02 14:29:38', NULL);
INSERT INTO `module` VALUES (25, NULL, 'inventory', 'inventory', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `module` VALUES (26, NULL, 'stock_opname', 'Stock Opname', 0, 1, NULL, NULL, '2017-10-03 16:27:12', NULL);
INSERT INTO `module` VALUES (27, NULL, 'inventory_mutation', 'Transfer Stock', 0, 1, NULL, NULL, '2017-10-02 14:30:14', NULL);
INSERT INTO `module` VALUES (28, NULL, 'stock_adjustment', 'Stock Adjustment', 0, 1, NULL, NULL, '2017-10-03 14:19:07', NULL);
INSERT INTO `module` VALUES (29, NULL, 'report', 'Report', 0, 1, NULL, NULL, '2017-10-02 14:30:28', NULL);
INSERT INTO `module` VALUES (30, NULL, 'purchase_advance', 'purchase_advance', 1, NULL, NULL, '2017-09-30 23:07:55', '2017-09-30 23:07:55', NULL);
INSERT INTO `module` VALUES (31, NULL, 'purchase_request', 'purchase_request', 1, NULL, NULL, '2017-09-30 23:08:05', '2017-09-30 23:08:05', NULL);
INSERT INTO `module` VALUES (32, NULL, 'purchase_return', 'purchase_return', 1, NULL, NULL, '2017-09-30 23:08:15', '2017-09-30 23:08:15', NULL);
INSERT INTO `module` VALUES (33, NULL, 'purchase_receive', 'purchase_receive', 1, NULL, NULL, '2017-09-30 23:08:25', '2017-09-30 23:08:25', NULL);
INSERT INTO `module` VALUES (34, NULL, 'ap_invoice', 'ap_invoice', 1, NULL, NULL, '2017-09-30 23:09:14', '2017-09-30 23:09:14', NULL);
INSERT INTO `module` VALUES (35, NULL, 'ar_receive', 'ar_receive', 1, NULL, NULL, '2017-09-30 23:09:28', '2017-09-30 23:09:28', NULL);
INSERT INTO `module` VALUES (36, NULL, 'ap_payment', 'ap_payment', 1, NULL, NULL, '2017-09-30 23:09:38', '2017-09-30 23:09:38', NULL);
INSERT INTO `module` VALUES (37, NULL, 'cash_receive', 'cash_receive', 1, NULL, NULL, '2017-09-30 23:09:47', '2017-09-30 23:09:47', NULL);
INSERT INTO `module` VALUES (38, NULL, 'cash_payment', 'cash_payment', 1, NULL, NULL, '2017-09-30 23:10:05', '2017-09-30 23:10:05', NULL);
INSERT INTO `module` VALUES (39, NULL, 'cash_transfer', 'cash_transfer', 1, NULL, NULL, '2017-09-30 23:10:23', '2017-09-30 23:10:23', NULL);
INSERT INTO `module` VALUES (40, NULL, 'stock_mutation', 'Stock Mutation', 1, NULL, NULL, '2017-10-02 15:01:47', '2017-10-02 15:01:47', NULL);
INSERT INTO `module` VALUES (41, '', 'purchase_order_app', 'purchase_order_app', 1, NULL, NULL, '2017-10-29 02:07:37', '2017-10-29 02:07:37', NULL);
COMMIT;

-- ----------------------------
-- Table structure for module_group
-- ----------------------------
DROP TABLE IF EXISTS `module_group`;
CREATE TABLE `module_group` (
  `mg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mg_id`),
  UNIQUE KEY `module_group_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mst_account
-- ----------------------------
DROP TABLE IF EXISTS `mst_account`;
CREATE TABLE `mst_account` (
  `account_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_parent_id` int(11) DEFAULT NULL,
  `account_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_account
-- ----------------------------
BEGIN;
INSERT INTO `mst_account` VALUES (1, 'Aktiva', '1-0000', NULL, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (2, 'Fixed Assets', '1-1000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (3, 'Gedung', '1-1100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (4, 'Mobil', '1-1200', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (5, 'Current Asset', '1-2000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (6, 'Kas dan Bank', '1-2100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (7, 'Kas', '1-2110', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (8, 'Bank', '1-2120', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (9, 'Inventori', '1-3000', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (10, 'Inventory - Mesin', '1-3100', 1, 'asset', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (11, 'Hutang', '2-0000', NULL, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (12, 'Pinjaman Bank', '2-1000', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (13, 'Pajak', '2-2000', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (14, 'Pajak Masukan', '2-2100', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (15, 'Pajak Dibayar', '2-2200', 2, 'liability', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (16, 'Modal', '3-0000', NULL, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (17, 'Laba', '3-1000', 3, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (18, 'Laba Ditahan', '3-2000', 3, 'equity', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (19, 'Pendapatan', '4-0000', NULL, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (20, 'Pendapatan Penjualan', '4-1000', 4, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (21, 'Bunga Bank', '4-2000', 4, 'income', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (22, 'Harga Pokok Penjualan', '5-0000', NULL, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (23, 'Harga Pokok Bahan Baku', '5-1000', 5, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (24, 'Beban Tenaga Kerja', '5-2000', 5, 'cogs', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (25, 'Biaya Operasional', '6-0000', NULL, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (26, 'Biaya Penjualan', '6-1000', 6, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_account` VALUES (27, 'Biaya Lain', '6-2000', 6, 'expense', 1, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_bom
-- ----------------------------
DROP TABLE IF EXISTS `mst_bom`;
CREATE TABLE `mst_bom` (
  `bom_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bom_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bom_quantity` decimal(25,10) DEFAULT NULL,
  `bom_production_time` decimal(25,10) DEFAULT NULL,
  `bom_cost` decimal(25,10) DEFAULT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_bom
-- ----------------------------
BEGIN;
INSERT INTO `mst_bom` VALUES (1, 'Sugus 1 pcs', NULL, 1.0000000000, 2.0000000000, 2000.0000000000, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_bom` VALUES (2, 'Sugus 20 pcs', '', 25.0000000000, 2.0000000000, 5000.0000000000, 2, 1, 1, 1, NULL, NULL, '2017-09-16 11:47:15', '2017-09-16 11:47:15', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_bom_detail
-- ----------------------------
DROP TABLE IF EXISTS `mst_bom_detail`;
CREATE TABLE `mst_bom_detail` (
  `bom_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bom_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `bom_detail_quantity` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bom_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_bom_detail
-- ----------------------------
BEGIN;
INSERT INTO `mst_bom_detail` VALUES (1, 1, 2, 1.0000000000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_bom_detail` VALUES (2, 1, 3, 1.0000000000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_bom_detail` VALUES (3, 2, 2, 20.0000000000, 1, NULL, 1, '2017-09-16 11:51:14', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (4, 2, 3, 10.0000000000, 1, NULL, 1, '2017-09-16 11:51:14', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (5, 2, 2, 20.0000000000, 1, NULL, 1, '2017-09-16 11:58:28', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (6, 2, 3, 10.0000000000, 1, NULL, 1, '2017-09-16 11:58:28', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (7, 2, 2, 20.0000000000, 1, NULL, 1, '2017-09-16 11:58:57', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (8, 2, 3, 10.0000000000, 1, NULL, 1, '2017-09-16 11:58:57', '2017-09-16 11:59:58', '2017-09-16 11:59:58');
INSERT INTO `mst_bom_detail` VALUES (9, 2, 2, 20.0000000000, 1, NULL, NULL, '2017-09-16 11:59:58', '2017-09-16 11:59:58', NULL);
INSERT INTO `mst_bom_detail` VALUES (10, 2, 3, 10.0000000000, 1, NULL, NULL, '2017-09-16 11:59:59', '2017-09-16 11:59:59', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_company
-- ----------------------------
DROP TABLE IF EXISTS `mst_company`;
CREATE TABLE `mst_company` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_npwp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_po_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_receive_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_receive_invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_jo_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_so_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_do_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_do_invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_format_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_company
-- ----------------------------
BEGIN;
INSERT INTO `mst_company` VALUES (1, 'MS UNION', 'Gunung Putri - Bogor', 'MSU', 'info@msu.com', '123456', '12.234.5123.123.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-01 17:07:07', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_currency
-- ----------------------------
DROP TABLE IF EXISTS `mst_currency`;
CREATE TABLE `mst_currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_currency
-- ----------------------------
BEGIN;
INSERT INTO `mst_currency` VALUES (1, 'Rp', 'IDR', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_currency` VALUES (2, '$', 'USD', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_customer
-- ----------------------------
DROP TABLE IF EXISTS `mst_customer`;
CREATE TABLE `mst_customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_credit_limit` decimal(15,2) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_customer
-- ----------------------------
BEGIN;
INSERT INTO `mst_customer` VALUES (1, 'Potato', '123456789', 'abc@def.com', 'Kentang', 'Jl. Pohon', 2000.00, 1, 1, 1, 1, '2017-09-04 17:32:52', '2017-09-04 17:33:52', NULL);
INSERT INTO `mst_customer` VALUES (2, 'Yohannes', '', '', '', '', 0.00, 1, 1, 1, NULL, '2017-09-04 17:34:00', '2017-09-04 17:46:30', NULL);
INSERT INTO `mst_customer` VALUES (3, 'Kuncoro MK II', '0851990998', 'Kuncoro@KentangBakar.com', 'Kuncoro', 'Jl. Pemakan Kentang 10', 5000000.00, 1, 1, 1, NULL, '2017-09-26 06:47:57', '2017-09-26 06:48:52', NULL);
INSERT INTO `mst_customer` VALUES (4, 'Ta', '02135444', 'ta.xo.eo', 'PON', 'LAKSD', 1000.00, 1, 1, NULL, 1, '2017-09-26 06:49:25', '2017-09-26 06:49:31', '2017-09-26 06:49:31');
INSERT INTO `mst_customer` VALUES (5, 'PT. Pendekar Sakti', '022-93952221', 'info@pendekarsakti.co.id', 'Cahyono', 'Jl. Kebakaran', 1000000.00, 1, 1, NULL, NULL, '2017-09-26 06:51:36', '2017-09-26 06:51:36', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_department
-- ----------------------------
DROP TABLE IF EXISTS `mst_department`;
CREATE TABLE `mst_department` (
  `department_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_worker_amount` int(11) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_department
-- ----------------------------
BEGIN;
INSERT INTO `mst_department` VALUES (1, 'Finance', 8, 1, 1, 1, NULL, '2017-09-05 16:27:46', '2017-09-05 16:30:25', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_item
-- ----------------------------
DROP TABLE IF EXISTS `mst_item`;
CREATE TABLE `mst_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_last_price` decimal(15,2) DEFAULT '0.00',
  `item_min_stock` decimal(15,2) DEFAULT NULL,
  `item_max_stock` decimal(15,2) DEFAULT NULL,
  `item_cos_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_income_inventory_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_asset_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_item
-- ----------------------------
BEGIN;
INSERT INTO `mst_item` VALUES (1, 'Sugus', 'Permen manis', 'Konsumsi', NULL, 1.00, 100.00, '0001', '0002', '0003', 1, 1, 1, NULL, '2017-09-05 17:46:22', '2017-09-05 17:48:24', NULL);
INSERT INTO `mst_item` VALUES (2, 'Gula', 'Pemanis', 'RAW', NULL, 0.00, 100.00, '', '', '', 1, 1, NULL, NULL, '2017-09-16 06:48:17', '2017-09-16 06:48:17', NULL);
INSERT INTO `mst_item` VALUES (3, 'Paper Wrap', 'Bungkus Kertas', 'MATERIAL', NULL, 0.00, 10000.00, '', '', '', 1, 1, 1, NULL, '2017-09-16 06:48:35', '2017-09-16 06:48:47', NULL);
INSERT INTO `mst_item` VALUES (4, 'Box Sugus', 'Box Pengemas Permen Sugus', 'Sugus', NULL, 10.00, 5000.00, 'Inventori', 'Inventori', 'Inventoris', 1, 1, 1, NULL, '2017-09-26 07:18:45', '2017-09-26 07:19:13', NULL);
INSERT INTO `mst_item` VALUES (5, 'a', 'aa', 'a', NULL, 11.00, 1.00, 'a', 'a', 'a', 1, 1, 1, 1, '2017-09-26 07:19:31', '2017-09-26 07:19:48', '2017-09-26 07:19:48');
COMMIT;

-- ----------------------------
-- Table structure for mst_paytype
-- ----------------------------
DROP TABLE IF EXISTS `mst_paytype`;
CREATE TABLE `mst_paytype` (
  `paytype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paytype_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`paytype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_paytype
-- ----------------------------
BEGIN;
INSERT INTO `mst_paytype` VALUES (1, 'Transfer', 1, 1, NULL, '2017-09-05 17:25:56', '2017-09-05 17:26:22', NULL);
INSERT INTO `mst_paytype` VALUES (2, 'Cash', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_paytype` VALUES (3, 'Giro', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_paytype` VALUES (4, 'Cek', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_supplier
-- ----------------------------
DROP TABLE IF EXISTS `mst_supplier`;
CREATE TABLE `mst_supplier` (
  `supplier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_credit_limit` decimal(15,2) DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_supplier
-- ----------------------------
BEGIN;
INSERT INTO `mst_supplier` VALUES (1, 'abbbbbbb', '', '', '', '', 0.00, 1, NULL, 1, 1, NULL, '2017-09-04 18:02:28', '2017-10-28 02:01:07', '2017-10-28 02:01:07');
INSERT INTO `mst_supplier` VALUES (2, 'Supplier biasa aja q d', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-09-16 06:24:28', '2017-10-28 02:09:10', NULL);
INSERT INTO `mst_supplier` VALUES (3, 'Test Supplier baru BB', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-09-16 06:24:40', '2017-10-28 02:07:23', NULL);
INSERT INTO `mst_supplier` VALUES (4, 'PT. Pembuat Kosmetik Jaya Abadi Mk II', '021-22501050', 'info@pkja.com', 'Cahyono', 'Jl. Cileungsi Raya no 20B', 100000000.00, 1, NULL, 1, 1, 1, '2017-09-26 05:51:33', '2017-09-26 05:52:28', '2017-09-26 05:52:28');
INSERT INTO `mst_supplier` VALUES (5, 'asddddddddd', 'asd', 'asd', 'asd', 'asd', 23.00, 1, 2, 1, NULL, NULL, '2017-10-28 01:15:21', '2017-10-28 02:53:34', NULL);
INSERT INTO `mst_supplier` VALUES (6, 'ddddd', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-10-28 01:15:57', '2017-10-28 01:15:57', NULL);
INSERT INTO `mst_supplier` VALUES (7, 'errrrrr abv', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-10-28 01:17:27', '2017-10-28 01:58:57', NULL);
INSERT INTO `mst_supplier` VALUES (8, 'sssss', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-10-28 01:18:59', '2017-10-28 01:18:59', NULL);
INSERT INTO `mst_supplier` VALUES (9, 'www', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-10-28 01:21:57', '2017-10-28 02:07:16', '2017-10-28 02:07:16');
INSERT INTO `mst_supplier` VALUES (10, 'mmmmmm', '', '', '', '', 0.00, 1, NULL, 1, NULL, NULL, '2017-10-28 01:23:15', '2017-10-28 02:01:27', '2017-10-28 02:01:27');
INSERT INTO `mst_supplier` VALUES (11, 'abc', '08777', 'hu@jjj', 'Husni', 'Test', 100000.00, 1, 2, 1, NULL, NULL, '2017-10-28 02:10:06', '2017-10-28 02:11:30', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_term
-- ----------------------------
DROP TABLE IF EXISTS `mst_term`;
CREATE TABLE `mst_term` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term_interval` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_term
-- ----------------------------
BEGIN;
INSERT INTO `mst_term` VALUES (1, 'Kerjasama 3 hari', 5, 1, 1, NULL, '2017-09-05 17:25:56', '2017-09-05 17:26:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_unit
-- ----------------------------
DROP TABLE IF EXISTS `mst_unit`;
CREATE TABLE `mst_unit` (
  `unit_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_unit
-- ----------------------------
BEGIN;
INSERT INTO `mst_unit` VALUES (28, 'UNIT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (29, 'BK', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (30, 'BKS', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (31, 'BOX', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (32, 'BTG', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (33, 'CRT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (34, 'DOS', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_unit` VALUES (35, 'DZ', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for mst_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `mst_warehouse`;
CREATE TABLE `mst_warehouse` (
  `warehouse_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mst_warehouse
-- ----------------------------
BEGIN;
INSERT INTO `mst_warehouse` VALUES (1, 'Gudang Cileungsi', 'Penyimpanan bahan baku', 'Jl. Keramat Jati 6', 'Achmad Susanto', '021-66685125', 1, 1, 1, NULL, '2017-09-05 16:45:16', '2017-09-05 16:47:28', NULL);
INSERT INTO `mst_warehouse` VALUES (2, 'Gudang Cikarang', '', 'Jl. Raya Cikarang', 'Jono', '65421233', 1, 1, NULL, NULL, '2017-10-03 04:53:06', '2017-10-03 04:53:06', NULL);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for privilege
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `privilege_user_id_foreign` (`user_id`),
  KEY `privilege_module_id_foreign` (`module_id`),
  KEY `privilege_action_id_foreign` (`action_id`),
  CONSTRAINT `privilege_action_id_foreign` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `privilege_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `privilege_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=749 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of privilege
-- ----------------------------
BEGIN;
INSERT INTO `privilege` VALUES (1, 1, 1, 1, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (2, 1, 1, 2, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (3, 1, 1, 3, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (4, 1, 1, 4, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (5, 1, 2, 1, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (6, 1, 2, 2, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (7, 1, 2, 3, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (8, 1, 2, 4, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (9, 1, 3, 1, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (10, 1, 3, 2, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (11, 1, 3, 3, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (12, 1, 3, 4, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (13, 1, 4, 1, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (14, 1, 4, 2, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (15, 1, 4, 3, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (16, 1, 4, 4, NULL, '2017-09-01 17:19:22', '2017-09-01 17:19:22');
INSERT INTO `privilege` VALUES (17, 1, 1, 1, '2017-09-01 17:19:22', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (18, 1, 1, 2, '2017-09-01 17:19:22', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (19, 1, 1, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (20, 1, 1, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (21, 1, 2, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (22, 1, 2, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (23, 1, 2, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (24, 1, 2, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (25, 1, 3, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (26, 1, 3, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (27, 1, 3, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (28, 1, 3, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (29, 1, 4, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (30, 1, 4, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (31, 1, 4, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (32, 1, 4, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (33, 1, 5, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (34, 1, 5, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (35, 1, 5, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (36, 1, 5, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (37, 1, 6, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (38, 1, 6, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (39, 1, 6, 3, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (40, 1, 6, 4, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (41, 1, 7, 1, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (42, 1, 7, 2, '2017-09-01 17:19:23', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (43, 1, 7, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (44, 1, 7, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (45, 1, 8, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (46, 1, 8, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (47, 1, 8, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (48, 1, 8, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (49, 1, 9, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (50, 1, 9, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (51, 1, 9, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (52, 1, 9, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (53, 1, 10, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (54, 1, 10, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (55, 1, 10, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (56, 1, 10, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (57, 1, 11, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (58, 1, 11, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (59, 1, 11, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (60, 1, 11, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (61, 1, 12, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (62, 1, 12, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (63, 1, 12, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (64, 1, 12, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (65, 1, 13, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (66, 1, 13, 2, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (67, 1, 13, 3, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (68, 1, 13, 4, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (69, 1, 14, 1, '2017-09-01 17:19:24', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (70, 1, 14, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (71, 1, 14, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (72, 1, 14, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (73, 1, 15, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (74, 1, 15, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (75, 1, 15, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (76, 1, 15, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (77, 1, 16, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (78, 1, 16, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (79, 1, 16, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (80, 1, 16, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (81, 1, 17, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (82, 1, 17, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (83, 1, 17, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (84, 1, 17, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (85, 1, 18, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (86, 1, 18, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (87, 1, 18, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (88, 1, 18, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (89, 1, 19, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (90, 1, 19, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (91, 1, 19, 3, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (92, 1, 19, 4, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (93, 1, 20, 1, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (94, 1, 20, 2, '2017-09-01 17:19:25', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (95, 1, 20, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (96, 1, 20, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (97, 1, 21, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (98, 1, 21, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (99, 1, 21, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (100, 1, 21, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (101, 1, 22, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (102, 1, 22, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (103, 1, 22, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (104, 1, 22, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (105, 1, 23, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (106, 1, 23, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (107, 1, 23, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (108, 1, 23, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (109, 1, 24, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (110, 1, 24, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (111, 1, 24, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (112, 1, 24, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (113, 1, 25, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (114, 1, 25, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (115, 1, 25, 3, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (116, 1, 25, 4, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (117, 1, 26, 1, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (118, 1, 26, 2, '2017-09-01 17:19:26', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (119, 1, 26, 3, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (120, 1, 26, 4, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (121, 1, 27, 1, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (122, 1, 27, 2, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (123, 1, 27, 3, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (124, 1, 27, 4, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (125, 1, 28, 1, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (126, 1, 28, 2, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (127, 1, 28, 3, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (128, 1, 28, 4, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (129, 1, 29, 1, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (130, 1, 29, 2, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (131, 1, 29, 3, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (132, 1, 29, 4, '2017-09-01 17:19:27', '2017-09-26 02:19:28', '2017-09-26 02:19:28');
INSERT INTO `privilege` VALUES (133, 1, 1, 1, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (134, 1, 1, 2, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (135, 1, 1, 3, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (136, 1, 1, 4, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (137, 1, 2, 1, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (138, 1, 2, 2, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (139, 1, 2, 3, '2017-09-26 02:19:28', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (140, 1, 2, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (141, 1, 3, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (142, 1, 3, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (143, 1, 3, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (144, 1, 3, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (145, 1, 4, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (146, 1, 4, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (147, 1, 4, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (148, 1, 4, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (149, 1, 5, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (150, 1, 5, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (151, 1, 5, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (152, 1, 5, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (153, 1, 6, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (154, 1, 6, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (155, 1, 6, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (156, 1, 6, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (157, 1, 7, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (158, 1, 7, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (159, 1, 7, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (160, 1, 7, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (161, 1, 8, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (162, 1, 8, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (163, 1, 8, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (164, 1, 8, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (165, 1, 9, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (166, 1, 9, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (167, 1, 9, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (168, 1, 9, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (169, 1, 10, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (170, 1, 10, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (171, 1, 10, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (172, 1, 10, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (173, 1, 11, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (174, 1, 11, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (175, 1, 11, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (176, 1, 11, 4, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (177, 1, 12, 1, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (178, 1, 12, 2, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (179, 1, 12, 3, '2017-09-26 02:19:29', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (180, 1, 12, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (181, 1, 13, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (182, 1, 13, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (183, 1, 13, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (184, 1, 13, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (185, 1, 14, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (186, 1, 14, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (187, 1, 14, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (188, 1, 14, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (189, 1, 15, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (190, 1, 15, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (191, 1, 15, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (192, 1, 15, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (193, 1, 16, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (194, 1, 16, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (195, 1, 16, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (196, 1, 16, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (197, 1, 17, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (198, 1, 17, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (199, 1, 17, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (200, 1, 17, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (201, 1, 18, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (202, 1, 18, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (203, 1, 18, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (204, 1, 18, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (205, 1, 19, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (206, 1, 19, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (207, 1, 19, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (208, 1, 19, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (209, 1, 20, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (210, 1, 20, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (211, 1, 20, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (212, 1, 20, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (213, 1, 21, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (214, 1, 21, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (215, 1, 21, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (216, 1, 21, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (217, 1, 22, 1, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (218, 1, 22, 2, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (219, 1, 22, 3, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (220, 1, 22, 4, '2017-09-26 02:19:30', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (221, 1, 23, 1, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (222, 1, 23, 2, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (223, 1, 23, 3, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (224, 1, 23, 4, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (225, 1, 24, 1, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (226, 1, 24, 2, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (227, 1, 24, 3, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (228, 1, 24, 4, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (229, 1, 25, 1, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (230, 1, 25, 2, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (231, 1, 25, 3, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (232, 1, 25, 4, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (233, 1, 26, 1, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (234, 1, 26, 2, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (235, 1, 26, 3, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (236, 1, 26, 4, '2017-09-26 02:19:31', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (237, 1, 27, 1, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (238, 1, 27, 2, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (239, 1, 27, 3, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (240, 1, 27, 4, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (241, 1, 28, 1, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (242, 1, 28, 2, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (243, 1, 28, 3, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (244, 1, 28, 4, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (245, 1, 29, 1, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (246, 1, 29, 2, '2017-09-26 02:19:32', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (247, 1, 29, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (248, 1, 29, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (249, 1, 30, 1, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (250, 1, 30, 2, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (251, 1, 30, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (252, 1, 30, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (253, 1, 31, 1, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (254, 1, 31, 2, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (255, 1, 31, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (256, 1, 31, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (257, 1, 32, 1, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (258, 1, 32, 2, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (259, 1, 32, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (260, 1, 32, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (261, 1, 33, 1, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (262, 1, 33, 2, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (263, 1, 33, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (264, 1, 33, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (265, 1, 34, 1, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (266, 1, 34, 2, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (267, 1, 34, 3, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (268, 1, 34, 4, '2017-09-26 02:19:33', '2017-09-26 02:22:26', '2017-09-26 02:22:26');
INSERT INTO `privilege` VALUES (269, 1, 1, 1, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (270, 1, 1, 2, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (271, 1, 1, 3, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (272, 1, 1, 4, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (273, 1, 2, 1, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (274, 1, 2, 2, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (275, 1, 2, 3, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (276, 1, 2, 4, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (277, 1, 3, 1, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (278, 1, 3, 2, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (279, 1, 3, 3, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (280, 1, 3, 4, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (281, 1, 4, 1, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (282, 1, 4, 2, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (283, 1, 4, 3, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (284, 1, 4, 4, '2017-09-26 02:22:26', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (285, 1, 5, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (286, 1, 5, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (287, 1, 5, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (288, 1, 5, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (289, 1, 6, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (290, 1, 6, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (291, 1, 6, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (292, 1, 6, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (293, 1, 7, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (294, 1, 7, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (295, 1, 7, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (296, 1, 7, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (297, 1, 8, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (298, 1, 8, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (299, 1, 8, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (300, 1, 8, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (301, 1, 9, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (302, 1, 9, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (303, 1, 9, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (304, 1, 9, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (305, 1, 10, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (306, 1, 10, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (307, 1, 10, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (308, 1, 10, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (309, 1, 11, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (310, 1, 11, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (311, 1, 11, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (312, 1, 11, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (313, 1, 12, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (314, 1, 12, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (315, 1, 12, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (316, 1, 12, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (317, 1, 13, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (318, 1, 13, 2, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (319, 1, 13, 3, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (320, 1, 13, 4, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (321, 1, 14, 1, '2017-09-26 02:22:27', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (322, 1, 14, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (323, 1, 14, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (324, 1, 14, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (325, 1, 15, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (326, 1, 15, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (327, 1, 15, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (328, 1, 15, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (329, 1, 16, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (330, 1, 16, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (331, 1, 16, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (332, 1, 16, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (333, 1, 17, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (334, 1, 17, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (335, 1, 17, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (336, 1, 17, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (337, 1, 18, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (338, 1, 18, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (339, 1, 18, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (340, 1, 18, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (341, 1, 19, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (342, 1, 19, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (343, 1, 19, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (344, 1, 19, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (345, 1, 20, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (346, 1, 20, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (347, 1, 20, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (348, 1, 20, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (349, 1, 21, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (350, 1, 21, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (351, 1, 21, 3, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (352, 1, 21, 4, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (353, 1, 22, 1, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (354, 1, 22, 2, '2017-09-26 02:22:28', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (355, 1, 22, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (356, 1, 22, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (357, 1, 23, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (358, 1, 23, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (359, 1, 23, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (360, 1, 23, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (361, 1, 24, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (362, 1, 24, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (363, 1, 24, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (364, 1, 24, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (365, 1, 25, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (366, 1, 25, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (367, 1, 25, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (368, 1, 25, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (369, 1, 26, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (370, 1, 26, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (371, 1, 26, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (372, 1, 26, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (373, 1, 27, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (374, 1, 27, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (375, 1, 27, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (376, 1, 27, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (377, 1, 28, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (378, 1, 28, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (379, 1, 28, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (380, 1, 28, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (381, 1, 29, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (382, 1, 29, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (383, 1, 29, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (384, 1, 29, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (385, 1, 30, 1, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (386, 1, 30, 2, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (387, 1, 30, 3, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (388, 1, 30, 4, '2017-09-26 02:22:29', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (389, 1, 31, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (390, 1, 31, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (391, 1, 31, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (392, 1, 31, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (393, 1, 32, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (394, 1, 32, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (395, 1, 32, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (396, 1, 32, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (397, 1, 33, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (398, 1, 33, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (399, 1, 33, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (400, 1, 33, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (401, 1, 34, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (402, 1, 34, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (403, 1, 34, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (404, 1, 34, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (405, 1, 35, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (406, 1, 35, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (407, 1, 35, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (408, 1, 35, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (409, 1, 36, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (410, 1, 36, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (411, 1, 36, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (412, 1, 36, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (413, 1, 37, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (414, 1, 37, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (415, 1, 37, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (416, 1, 37, 4, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (417, 1, 38, 1, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (418, 1, 38, 2, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (419, 1, 38, 3, '2017-09-26 02:22:30', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (420, 1, 38, 4, '2017-09-26 02:22:31', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (421, 1, 39, 1, '2017-09-26 02:22:31', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (422, 1, 39, 2, '2017-09-26 02:22:31', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (423, 1, 39, 3, '2017-09-26 02:22:31', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (424, 1, 39, 4, '2017-09-26 02:22:31', '2017-10-03 04:52:06', '2017-10-03 04:52:06');
INSERT INTO `privilege` VALUES (425, 1, 5, 1, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (426, 1, 5, 2, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (427, 1, 5, 3, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (428, 1, 5, 4, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (429, 1, 6, 1, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (430, 1, 6, 2, '2017-10-03 04:52:06', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (431, 1, 6, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (432, 1, 6, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (433, 1, 7, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (434, 1, 7, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (435, 1, 7, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (436, 1, 7, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (437, 1, 8, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (438, 1, 8, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (439, 1, 8, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (440, 1, 8, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (441, 1, 9, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (442, 1, 9, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (443, 1, 9, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (444, 1, 9, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (445, 1, 10, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (446, 1, 10, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (447, 1, 10, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (448, 1, 10, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (449, 1, 11, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (450, 1, 11, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (451, 1, 11, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (452, 1, 11, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (453, 1, 12, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (454, 1, 12, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (455, 1, 12, 3, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (456, 1, 12, 4, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (457, 1, 13, 1, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (458, 1, 13, 2, '2017-10-03 04:52:07', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (459, 1, 13, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (460, 1, 13, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (461, 1, 14, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (462, 1, 14, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (463, 1, 14, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (464, 1, 14, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (465, 1, 15, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (466, 1, 15, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (467, 1, 15, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (468, 1, 15, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (469, 1, 16, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (470, 1, 16, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (471, 1, 16, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (472, 1, 16, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (473, 1, 17, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (474, 1, 17, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (475, 1, 17, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (476, 1, 17, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (477, 1, 18, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (478, 1, 18, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (479, 1, 18, 3, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (480, 1, 18, 4, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (481, 1, 19, 1, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (482, 1, 19, 2, '2017-10-03 04:52:08', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (483, 1, 19, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (484, 1, 19, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (485, 1, 20, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (486, 1, 20, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (487, 1, 20, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (488, 1, 20, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (489, 1, 21, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (490, 1, 21, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (491, 1, 21, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (492, 1, 21, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (493, 1, 22, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (494, 1, 22, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (495, 1, 22, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (496, 1, 22, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (497, 1, 23, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (498, 1, 23, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (499, 1, 23, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (500, 1, 23, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (501, 1, 24, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (502, 1, 24, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (503, 1, 24, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (504, 1, 24, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (505, 1, 25, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (506, 1, 25, 2, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (507, 1, 25, 3, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (508, 1, 25, 4, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (509, 1, 26, 1, '2017-10-03 04:52:09', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (510, 1, 26, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (511, 1, 26, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (512, 1, 26, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (513, 1, 27, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (514, 1, 27, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (515, 1, 27, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (516, 1, 27, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (517, 1, 28, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (518, 1, 28, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (519, 1, 28, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (520, 1, 28, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (521, 1, 29, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (522, 1, 29, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (523, 1, 29, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (524, 1, 29, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (525, 1, 30, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (526, 1, 30, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (527, 1, 30, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (528, 1, 30, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (529, 1, 31, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (530, 1, 31, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (531, 1, 31, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (532, 1, 31, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (533, 1, 32, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (534, 1, 32, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (535, 1, 32, 3, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (536, 1, 32, 4, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (537, 1, 33, 1, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (538, 1, 33, 2, '2017-10-03 04:52:10', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (539, 1, 33, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (540, 1, 33, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (541, 1, 34, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (542, 1, 34, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (543, 1, 34, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (544, 1, 34, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (545, 1, 35, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (546, 1, 35, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (547, 1, 35, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (548, 1, 35, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (549, 1, 36, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (550, 1, 36, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (551, 1, 36, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (552, 1, 36, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (553, 1, 37, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (554, 1, 37, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (555, 1, 37, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (556, 1, 37, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (557, 1, 38, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (558, 1, 38, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (559, 1, 38, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (560, 1, 38, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (561, 1, 39, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (562, 1, 39, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (563, 1, 39, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (564, 1, 39, 4, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (565, 1, 40, 1, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (566, 1, 40, 2, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (567, 1, 40, 3, '2017-10-03 04:52:11', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (568, 1, 40, 4, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (569, 1, 1, 1, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (570, 1, 1, 2, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (571, 1, 1, 3, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (572, 1, 1, 4, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (573, 1, 2, 1, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (574, 1, 2, 2, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (575, 1, 2, 3, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (576, 1, 2, 4, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (577, 1, 3, 1, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (578, 1, 3, 2, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (579, 1, 3, 3, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (580, 1, 3, 4, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (581, 1, 4, 1, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (582, 1, 4, 2, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (583, 1, 4, 3, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (584, 1, 4, 4, '2017-10-03 04:52:12', '2017-10-29 02:08:02', '2017-10-29 02:08:02');
INSERT INTO `privilege` VALUES (585, 1, 6, 1, '2017-10-29 02:08:02', '2017-10-29 02:08:02', NULL);
INSERT INTO `privilege` VALUES (586, 1, 6, 2, '2017-10-29 02:08:02', '2017-10-29 02:08:02', NULL);
INSERT INTO `privilege` VALUES (587, 1, 6, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (588, 1, 6, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (589, 1, 7, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (590, 1, 7, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (591, 1, 7, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (592, 1, 7, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (593, 1, 8, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (594, 1, 8, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (595, 1, 8, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (596, 1, 8, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (597, 1, 9, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (598, 1, 9, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (599, 1, 9, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (600, 1, 9, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (601, 1, 10, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (602, 1, 10, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (603, 1, 10, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (604, 1, 10, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (605, 1, 11, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (606, 1, 11, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (607, 1, 11, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (608, 1, 11, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (609, 1, 12, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (610, 1, 12, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (611, 1, 12, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (612, 1, 12, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (613, 1, 13, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (614, 1, 13, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (615, 1, 13, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (616, 1, 13, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (617, 1, 14, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (618, 1, 14, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (619, 1, 14, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (620, 1, 14, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (621, 1, 15, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (622, 1, 15, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (623, 1, 15, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (624, 1, 15, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (625, 1, 16, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (626, 1, 16, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (627, 1, 16, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (628, 1, 16, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (629, 1, 17, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (630, 1, 17, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (631, 1, 17, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (632, 1, 17, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (633, 1, 18, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (634, 1, 18, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (635, 1, 18, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (636, 1, 18, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (637, 1, 19, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (638, 1, 19, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (639, 1, 19, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (640, 1, 19, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (641, 1, 20, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (642, 1, 20, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (643, 1, 20, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (644, 1, 20, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (645, 1, 21, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (646, 1, 21, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (647, 1, 21, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (648, 1, 21, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (649, 1, 22, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (650, 1, 22, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (651, 1, 22, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (652, 1, 22, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (653, 1, 23, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (654, 1, 23, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (655, 1, 23, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (656, 1, 23, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (657, 1, 24, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (658, 1, 24, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (659, 1, 24, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (660, 1, 24, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (661, 1, 25, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (662, 1, 25, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (663, 1, 25, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (664, 1, 25, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (665, 1, 26, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (666, 1, 26, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (667, 1, 26, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (668, 1, 26, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (669, 1, 27, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (670, 1, 27, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (671, 1, 27, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (672, 1, 27, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (673, 1, 28, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (674, 1, 28, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (675, 1, 28, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (676, 1, 28, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (677, 1, 29, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (678, 1, 29, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (679, 1, 29, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (680, 1, 29, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (681, 1, 30, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (682, 1, 30, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (683, 1, 30, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (684, 1, 30, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (685, 1, 31, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (686, 1, 31, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (687, 1, 31, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (688, 1, 31, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (689, 1, 32, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (690, 1, 32, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (691, 1, 32, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (692, 1, 32, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (693, 1, 33, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (694, 1, 33, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (695, 1, 33, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (696, 1, 33, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (697, 1, 34, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (698, 1, 34, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (699, 1, 34, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (700, 1, 34, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (701, 1, 35, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (702, 1, 35, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (703, 1, 35, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (704, 1, 35, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (705, 1, 36, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (706, 1, 36, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (707, 1, 36, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (708, 1, 36, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (709, 1, 37, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (710, 1, 37, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (711, 1, 37, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (712, 1, 37, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (713, 1, 38, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (714, 1, 38, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (715, 1, 38, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (716, 1, 38, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (717, 1, 39, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (718, 1, 39, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (719, 1, 39, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (720, 1, 39, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (721, 1, 40, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (722, 1, 40, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (723, 1, 40, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (724, 1, 40, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (725, 1, 41, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (726, 1, 41, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (727, 1, 41, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (728, 1, 41, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (729, 1, 1, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (730, 1, 1, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (731, 1, 1, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (732, 1, 1, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (733, 1, 2, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (734, 1, 2, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (735, 1, 2, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (736, 1, 2, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (737, 1, 3, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (738, 1, 3, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (739, 1, 3, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (740, 1, 3, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (741, 1, 4, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (742, 1, 4, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (743, 1, 4, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (744, 1, 4, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (745, 1, 5, 1, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (746, 1, 5, 2, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (747, 1, 5, 3, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
INSERT INTO `privilege` VALUES (748, 1, 5, 4, '2017-10-29 02:08:03', '2017-10-29 02:08:03', NULL);
COMMIT;

-- ----------------------------
-- Table structure for privilege_group
-- ----------------------------
DROP TABLE IF EXISTS `privilege_group`;
CREATE TABLE `privilege_group` (
  `pg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pg_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pg_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pg_id`),
  UNIQUE KEY `privilege_group_pg_name_unique` (`pg_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of privilege_group
-- ----------------------------
BEGIN;
INSERT INTO `privilege_group` VALUES (2, 'Administrator', 'Bisa edit user dan permission', 1, 0, 1, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:53', NULL);
COMMIT;

-- ----------------------------
-- Table structure for privilege_group_detail
-- ----------------------------
DROP TABLE IF EXISTS `privilege_group_detail`;
CREATE TABLE `privilege_group_detail` (
  `pg_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pg_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pg_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of privilege_group_detail
-- ----------------------------
BEGIN;
INSERT INTO `privilege_group_detail` VALUES (1, 2, 0, 1, 1, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (2, 2, 0, 1, 2, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (3, 2, 0, 1, 3, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (4, 2, 0, 1, 4, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (5, 2, 0, 2, 1, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (6, 2, 0, 2, 2, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (7, 2, 0, 2, 3, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (8, 2, 0, 2, 4, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (9, 2, 0, 3, 1, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (10, 2, 0, 3, 2, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (11, 2, 0, 3, 3, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (12, 2, 0, 3, 4, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (13, 2, 0, 4, 1, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (14, 2, 0, 4, 2, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (15, 2, 0, 4, 3, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (16, 2, 0, 4, 4, 0, NULL, NULL, '2017-10-02 16:52:53', '2017-10-02 17:09:41', '2017-10-02 17:09:41');
INSERT INTO `privilege_group_detail` VALUES (17, 2, 1, 1, 1, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (18, 2, 1, 1, 2, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (19, 2, 1, 1, 3, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (20, 2, 1, 1, 4, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (21, 2, 1, 2, 1, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (22, 2, 1, 2, 2, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (23, 2, 1, 2, 3, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (24, 2, 1, 2, 4, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (25, 2, 1, 3, 1, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (26, 2, 1, 3, 2, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (27, 2, 1, 3, 3, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (28, 2, 1, 3, 4, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (29, 2, 1, 4, 1, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (30, 2, 1, 4, 2, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (31, 2, 1, 4, 3, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
INSERT INTO `privilege_group_detail` VALUES (32, 2, 1, 4, 4, 1, NULL, NULL, '2017-10-02 17:09:53', '2017-10-02 17:09:53', NULL);
COMMIT;

-- ----------------------------
-- Table structure for production
-- ----------------------------
DROP TABLE IF EXISTS `production`;
CREATE TABLE `production` (
  `production_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bom_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `production_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `multiplier` decimal(25,10) DEFAULT NULL,
  `estimated_result` decimal(25,10) DEFAULT NULL,
  `actual_result` decimal(25,10) DEFAULT NULL,
  `estimated_time` decimal(25,10) DEFAULT NULL,
  `actual_time` decimal(25,10) DEFAULT NULL,
  `worker_amount` decimal(25,10) DEFAULT NULL,
  `production_cost` decimal(25,10) DEFAULT NULL,
  `total_production_cost` decimal(25,10) DEFAULT NULL,
  `production_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`production_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of production
-- ----------------------------
BEGIN;
INSERT INTO `production` VALUES (1, 1, 1, 1, 1, 'WO/MSU/2017/10/0001', '1', 1.0000000000, 1.0000000000, NULL, 1.0000000000, NULL, 2.0000000000, 1.0000000000, NULL, 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-09-26 00:21:18', '2017-09-26 00:21:18', '2017-09-26 07:22:28');
INSERT INTO `production` VALUES (2, 1, 1, 1, 1, 'WO/MSU/2017/10/0002', '1', 1.0000000000, 2000.0000000000, NULL, 1.0000000000, NULL, 1.0000000000, 5.0000000000, NULL, 'ACTIVE', 1, '2017-10-01', 1, 1, NULL, NULL, '2017-09-26 00:25:59', '2017-10-01 03:29:28', NULL);
INSERT INTO `production` VALUES (3, 1, 1, 1, 1, 'WO/MSU/2017/10/0003', '1', 1.0000000000, 2000.0000000000, 2000.0000000000, 2.0000000000, 2.0000000000, 1.0000000000, 5.0000000000, 5.0000000000, 'COMPLETED', 1, '2017-09-30', 1, 1, NULL, NULL, '2017-09-26 00:29:30', '2017-09-30 09:05:49', NULL);
INSERT INTO `production` VALUES (4, 1, 1, 1, 1, 'WO/MSU/2017/10/0004', '1', 2.0000000000, 2.0000000000, NULL, 4.0000000000, NULL, 1.0000000000, 5.0000000000, 10.0000000000, 'COMPLETED', 1, '2017-09-30', 1, 1, 1, NULL, '2017-09-26 00:33:36', '2017-09-30 06:36:04', NULL);
INSERT INTO `production` VALUES (5, 2, 2, 1, 1, 'WO/MSU/2017/10/0005', '', 5.0000000000, 125.0000000000, 100.0000000000, 10.0000000000, 12.0000000000, 10.0000000000, 0.0000000000, 0.0000000000, 'COMPLETED', 1, '2017-10-01', 1, 1, NULL, NULL, '2017-10-01 03:31:16', '2017-10-01 03:32:37', NULL);
COMMIT;

-- ----------------------------
-- Table structure for production_detail
-- ----------------------------
DROP TABLE IF EXISTS `production_detail`;
CREATE TABLE `production_detail` (
  `production_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `production_id` int(11) DEFAULT NULL,
  `bom_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `estimated_material` decimal(25,10) DEFAULT NULL,
  `multiplied_estimated_material` decimal(25,10) DEFAULT NULL,
  `actual_material` decimal(25,10) DEFAULT NULL,
  `multiplied_actual_material` decimal(25,10) DEFAULT NULL,
  `cost` decimal(25,10) DEFAULT NULL,
  `multiplied_cost` decimal(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`production_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of production_detail
-- ----------------------------
BEGIN;
INSERT INTO `production_detail` VALUES (1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-26 00:25:59', '2017-09-26 00:25:59', '2017-09-30 16:26:01');
INSERT INTO `production_detail` VALUES (2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-26 00:25:59', '2017-09-26 00:25:59', '2017-09-30 16:26:01');
INSERT INTO `production_detail` VALUES (3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-26 00:29:30', '2017-09-26 00:29:30', '2017-09-30 16:26:00');
INSERT INTO `production_detail` VALUES (4, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-26 00:29:30', '2017-09-26 00:29:30', '2017-09-30 16:25:59');
INSERT INTO `production_detail` VALUES (5, 4, 1, 2, 1.0000000000, 1.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, 1, '2017-09-26 00:33:36', '2017-09-26 00:55:36', '2017-09-26 00:55:36');
INSERT INTO `production_detail` VALUES (6, 4, 2, 3, 1.0000000000, 1.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, 1, '2017-09-26 00:33:36', '2017-09-26 00:55:37', '2017-09-26 00:55:37');
INSERT INTO `production_detail` VALUES (7, 4, 1, 2, 2.0000000000, 4.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-26 00:55:37', '2017-09-26 00:55:37', NULL);
INSERT INTO `production_detail` VALUES (8, 4, 2, 3, 3.0000000000, 6.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-26 00:55:37', '2017-09-26 00:55:37', NULL);
INSERT INTO `production_detail` VALUES (9, 5, 9, 2, 20.0000000000, 100.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-10-01 03:31:16', '2017-10-01 03:31:16', NULL);
INSERT INTO `production_detail` VALUES (10, 5, 10, 3, 10.0000000000, 50.0000000000, NULL, NULL, 0.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-10-01 03:31:16', '2017-10-01 03:31:16', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_advance
-- ----------------------------
DROP TABLE IF EXISTS `purchase_advance`;
CREATE TABLE `purchase_advance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pa_reference` varchar(50) DEFAULT NULL,
  `pa_date` date DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `amount` double DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_advance
-- ----------------------------
BEGIN;
INSERT INTO `purchase_advance` VALUES (1, 'PAAD-26091700001', '2017-09-26', 1, 0, 9, 'IDR', 1000000000, NULL, NULL, 9, NULL, NULL, 1, 1, NULL, '2017-09-26 00:00:00', '2017-09-26 06:25:40', NULL);
INSERT INTO `purchase_advance` VALUES (2, 'PAAD-01101700002', '2017-10-01', 1, 0, 8, 'IDR', 1500000, NULL, NULL, 0, 1, NULL, 1, 1, NULL, '2017-10-01 00:00:00', '2017-10-08 00:54:15', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_reference` varchar(50) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `packing_list` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `resi_tracking` varchar(255) DEFAULT NULL,
  `need_app` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_order
-- ----------------------------
BEGIN;
INSERT INTO `purchase_order` VALUES (1, 'POTX-24091700001', '2017-09-24', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, '24-09-2017 05-49-22Untitled-1.jpg', NULL, 0, 1, NULL, NULL, '2017-09-24 00:00:00', '2017-09-24 05:49:22', NULL);
INSERT INTO `purchase_order` VALUES (2, 'POTX-26091700002', '2017-09-26', 1, 0, 1, 0, 'USD', 0, NULL, NULL, NULL, '', 1, NULL, NULL, 0, 1, 1, NULL, '2017-09-26 00:00:00', '2017-10-08 00:49:08', NULL);
INSERT INTO `purchase_order` VALUES (3, 'PONT-26091700003', '2017-09-26', 1, 0, NULL, 0, 'IDR', 14, NULL, NULL, NULL, 'Barang diambil sendiri di gudang supplier', 0, '26-09-2017 06-06-33Quotation Test.docx', NULL, 0, 1, 1, NULL, '2017-09-26 00:00:00', '2017-09-26 06:10:02', NULL);
INSERT INTO `purchase_order` VALUES (4, 'PONT-26091700004', '2017-09-26', 3, 0, NULL, 0, 'USD', 30, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:08:20', NULL);
INSERT INTO `purchase_order` VALUES (5, 'POTX-26091700005', '2017-09-26', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (6, 'POTX-26091700006', '2017-09-26', 1, 0, NULL, 0, 'IDR', 15, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:11:29', NULL);
INSERT INTO `purchase_order` VALUES (7, 'POTX-26091700007', '2017-09-26', 1, 0, NULL, 0, 'USD', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:14:13', NULL);
INSERT INTO `purchase_order` VALUES (8, 'POTX-26091700008', '2017-09-26', 2, 0, NULL, 0, 'IDR', 30, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:15:05', NULL);
INSERT INTO `purchase_order` VALUES (9, 'POTX-26091700009', '2017-09-26', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:17:33', NULL);
INSERT INTO `purchase_order` VALUES (10, 'POTX-26091700010', '2017-09-26', 1, 0, 1, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-10-08 00:48:21', NULL);
INSERT INTO `purchase_order` VALUES (11, 'POTX-01101700011', '2017-10-01', 2, 0, NULL, 0, 'IDR', 50, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-09-30 23:12:54', NULL);
INSERT INTO `purchase_order` VALUES (12, 'POTX-01101700012', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-10-01 03:40:04', NULL);
INSERT INTO `purchase_order` VALUES (13, 'POTX-01101700013', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-10-01 03:43:04', NULL);
INSERT INTO `purchase_order` VALUES (14, 'POTX-01101700014', '2017-10-01', 1, 0, NULL, 0, 'IDR', 0, NULL, NULL, NULL, '', 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-10-01 03:57:18', NULL);
INSERT INTO `purchase_order` VALUES (15, 'POTX-04101700015', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-04 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (16, 'POTX-04101700016', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-04 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (17, 'POTX-04101700017', '2017-10-04', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-04 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (18, 'POTX-08101700018', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (19, 'POTX-08101700019', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (20, 'POTX-08101700020', '2017-10-08', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (21, 'POTX-22101700021', '2017-10-22', 2, 0, 1, 0, '', 0, 'asd', 'asdas', NULL, '', 0, NULL, '22-10-2017 01-32-52amelia.docx', 0, 1, 1, NULL, '2017-10-22 00:00:00', '2017-10-29 03:05:36', NULL);
INSERT INTO `purchase_order` VALUES (22, 'POTX-22101700022', '2017-10-22', 0, 0, NULL, 0, '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, NULL, NULL, '2017-10-22 00:00:00', NULL, NULL);
INSERT INTO `purchase_order` VALUES (23, 'POTX-28101700023', '2017-10-28', 5, 0, 1, 0, 'IDR', 90, '001', '002', 10300000, '', 0, NULL, NULL, 0, 1, 1, NULL, '2017-10-28 00:00:00', '2017-10-29 03:55:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order_detail`;
CREATE TABLE `purchase_order_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(10) unsigned DEFAULT NULL,
  `pr_d_id` int(10) unsigned DEFAULT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `unitprice` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `gr_qty` double DEFAULT NULL,
  `gr_remain` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`po_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_order_detail
-- ----------------------------
BEGIN;
INSERT INTO `purchase_order_detail` VALUES (1, 1, 2, 2, 'CRT', 40000, 10, 50, 1800000, 103, NULL, NULL, NULL, NULL, '2017-09-24 05:48:47', '2017-09-24 05:49:09', NULL);
INSERT INTO `purchase_order_detail` VALUES (2, 1, 4, 14, 'BKS', 50000, 10, 70, 3150000, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:48:47', '2017-09-24 05:49:01', NULL);
INSERT INTO `purchase_order_detail` VALUES (3, 3, NULL, 2, 'BOX', 150000, 10, 10, 1350000, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:07:17', '2017-09-26 06:07:17', NULL);
INSERT INTO `purchase_order_detail` VALUES (4, 4, NULL, 3, 'BOX', 50, 10, 20, 900, 1, NULL, NULL, NULL, NULL, '2017-09-26 06:07:57', '2017-09-26 06:08:13', NULL);
INSERT INTO `purchase_order_detail` VALUES (5, 6, 1, 2, 'BOX', NULL, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:11:03', '2017-09-26 06:11:03', NULL);
INSERT INTO `purchase_order_detail` VALUES (6, 7, 3, 3, 'DOS', 100, 0, 5, 50, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:11:53', '2017-09-26 06:13:53', NULL);
INSERT INTO `purchase_order_detail` VALUES (7, 8, 7, 1, 'BOX', 50, 0, 10, 50, 1, NULL, NULL, NULL, NULL, '2017-09-26 06:14:40', '2017-09-26 06:14:57', NULL);
INSERT INTO `purchase_order_detail` VALUES (8, 9, 10, 2, 'BOX', 5000, 0, 100, 50000, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:17:06', '2017-09-26 06:17:24', NULL);
INSERT INTO `purchase_order_detail` VALUES (9, 11, 13, 4, 'BOX', 50000, 0, 5, 250000, 5, NULL, NULL, NULL, NULL, '2017-09-30 23:12:06', '2017-09-30 23:12:38', NULL);
INSERT INTO `purchase_order_detail` VALUES (10, 12, 14, 3, 'BOX', 500, 10, 800, 360000, 750, NULL, NULL, NULL, NULL, '2017-10-01 03:39:15', '2017-10-01 03:40:45', NULL);
INSERT INTO `purchase_order_detail` VALUES (11, 13, 14, 3, 'BOX', 500, 10, 300, 135000, 200, NULL, NULL, NULL, NULL, '2017-10-01 03:42:16', '2017-10-01 03:42:59', NULL);
INSERT INTO `purchase_order_detail` VALUES (12, 14, 10, 2, 'BOX', 500, 0, 196, 9800, NULL, NULL, NULL, NULL, NULL, '2017-10-01 03:56:05', '2017-10-01 03:57:08', NULL);
INSERT INTO `purchase_order_detail` VALUES (13, 16, NULL, 1, '', 200, 5, 1, 190, NULL, NULL, NULL, NULL, NULL, '2017-10-04 15:57:19', '2017-10-04 15:57:19', NULL);
INSERT INTO `purchase_order_detail` VALUES (14, 18, 1, 2, 'BOX', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 23:33:07', '2017-10-07 23:33:07', NULL);
INSERT INTO `purchase_order_detail` VALUES (15, 20, 3, 3, 'DOS', 500, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 23:47:08', '2017-10-07 23:47:08', NULL);
INSERT INTO `purchase_order_detail` VALUES (16, 23, NULL, 1, 'BKS', 1010000, 0, 10, 10100000, 5, NULL, NULL, NULL, NULL, '2017-10-28 02:51:40', '2017-10-29 03:51:40', NULL);
INSERT INTO `purchase_order_detail` VALUES (17, 21, NULL, 1, 'BOX', 100000, 0, 10, 1000000, NULL, NULL, NULL, NULL, NULL, '2017-10-29 02:56:15', '2017-10-29 02:56:15', NULL);
INSERT INTO `purchase_order_detail` VALUES (18, 23, NULL, 3, 'BOX', 10000, 0, 20, 200000, NULL, NULL, NULL, NULL, NULL, '2017-10-29 03:51:53', '2017-10-29 03:51:53', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_receive
-- ----------------------------
DROP TABLE IF EXISTS `purchase_receive`;
CREATE TABLE `purchase_receive` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prec_reference` varchar(50) DEFAULT NULL,
  `prec_date` date DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `paytype_id` int(10) unsigned NOT NULL,
  `paydue` double NOT NULL,
  `currency` varchar(10) NOT NULL,
  `top` int(11) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_receive
-- ----------------------------
BEGIN;
INSERT INTO `purchase_receive` VALUES (1, 'PRCX-24091700001', '2017-09-24', 0, 0, 0, '', 0, '', 1, 0, '24-09-2017 05-49-57Untitled-1.jpg', 1, 1, 1, NULL, '2017-09-24 00:00:00', '2017-10-08 00:50:36', NULL);
INSERT INTO `purchase_receive` VALUES (2, 'PRCX-24091700002', '2017-09-24', 0, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-24 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (3, 'PRCX-26091700003', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:18:30', NULL);
INSERT INTO `purchase_receive` VALUES (4, 'PRCX-26091700004', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:19:04', NULL);
INSERT INTO `purchase_receive` VALUES (5, 'PRCX-26091700005', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:20:28', NULL);
INSERT INTO `purchase_receive` VALUES (6, 'PRCX-26091700006', '2017-09-26', 0, 0, 0, '', 0, '', NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:20:45', NULL);
INSERT INTO `purchase_receive` VALUES (7, 'PRCX-01101700007', '2017-10-21', 2, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, 1, NULL, '2017-10-01 00:00:00', '2017-09-30 23:14:13', NULL);
INSERT INTO `purchase_receive` VALUES (8, 'PRCX-01101700008', '2017-10-01', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-01 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (9, 'PRCX-01101700009', '2017-10-01', 1, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-10-01 03:47:05', NULL);
INSERT INTO `purchase_receive` VALUES (10, 'PRCX-01101700010', '2017-10-01', 1, 0, 0, '', 0, '', NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-10-01 03:59:17', NULL);
INSERT INTO `purchase_receive` VALUES (11, 'PRCX-07101700011', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (12, 'PRCT-07101700012', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (13, 'PRCX-07101700013', '2017-10-07', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-07 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (14, 'PRCX-08101700014', '2017-10-08', 3, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (15, 'PRCX-08101700015', '2017-10-08', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (16, 'PRCX-08101700016', '2017-10-08', 2, 0, 0, '', 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (17, 'PRCX-28101700017', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 5, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (18, 'PRCX-28101700018', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 5, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_receive` VALUES (19, 'PRCX-28101700019', '2017-10-28', 5, 0, 0, '', 0, NULL, NULL, 0, NULL, 2, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_receive_detail
-- ----------------------------
DROP TABLE IF EXISTS `purchase_receive_detail`;
CREATE TABLE `purchase_receive_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prec_id` int(10) unsigned DEFAULT NULL,
  `po_d_id` int(10) unsigned DEFAULT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `unitprice` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `gr_qty` double DEFAULT NULL,
  `gr_remain` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`prec_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_receive_detail
-- ----------------------------
BEGIN;
INSERT INTO `purchase_receive_detail` VALUES (1, 1, 2, 14, 'BKS', 50000, NULL, 40, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:49:42', '2017-09-24 05:49:50', NULL);
INSERT INTO `purchase_receive_detail` VALUES (2, 2, 1, 2, 'CRT', 40000, 10, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:53:00', '2017-09-24 05:53:00', NULL);
INSERT INTO `purchase_receive_detail` VALUES (3, 3, 8, 2, 'BOX', NULL, NULL, 55, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:18:10', '2017-09-26 06:18:29', NULL);
INSERT INTO `purchase_receive_detail` VALUES (4, 4, 5, 2, 'BOX', NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:18:55', '2017-09-26 06:19:01', NULL);
INSERT INTO `purchase_receive_detail` VALUES (5, 5, 5, 2, 'BOX', 0, 0, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:20:22', '2017-09-26 06:20:22', NULL);
INSERT INTO `purchase_receive_detail` VALUES (6, 6, 5, 2, 'BOX', 0, 0, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:20:42', '2017-09-26 06:20:42', NULL);
INSERT INTO `purchase_receive_detail` VALUES (7, 7, 9, 4, 'BOX', 50000, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 23:14:04', '2017-09-30 23:14:04', NULL);
INSERT INTO `purchase_receive_detail` VALUES (8, 8, 9, 4, 'BOX', 50000, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-01 00:48:06', '2017-10-01 00:48:06', NULL);
INSERT INTO `purchase_receive_detail` VALUES (9, 9, 10, 3, 'BOX', NULL, NULL, 750, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-01 03:46:46', '2017-10-01 03:48:02', NULL);
INSERT INTO `purchase_receive_detail` VALUES (10, 9, 11, 3, 'BOX', NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-01 03:46:46', '2017-10-01 03:47:21', NULL);
INSERT INTO `purchase_receive_detail` VALUES (11, 10, 1, 2, 'CRT', 40000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-01 03:58:44', '2017-10-01 03:58:44', NULL);
INSERT INTO `purchase_receive_detail` VALUES (12, 14, 4, 3, 'BOX', 50, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 18:08:20', '2017-10-07 18:08:20', NULL);
INSERT INTO `purchase_receive_detail` VALUES (13, 16, 7, 1, 'BOX', 50, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 23:57:18', '2017-10-07 23:57:18', NULL);
INSERT INTO `purchase_receive_detail` VALUES (14, 17, 16, 1, 'BKS', 200000, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 02:55:37', '2017-10-28 02:55:37', NULL);
INSERT INTO `purchase_receive_detail` VALUES (15, 18, 16, 1, 'BKS', 200000, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 02:56:03', '2017-10-28 02:56:03', NULL);
INSERT INTO `purchase_receive_detail` VALUES (16, 19, 16, 1, 'BKS', 200000, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 03:00:36', '2017-10-28 03:00:36', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_request
-- ----------------------------
DROP TABLE IF EXISTS `purchase_request`;
CREATE TABLE `purchase_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_reference` varchar(50) DEFAULT NULL,
  `pr_date` date DEFAULT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_request
-- ----------------------------
BEGIN;
INSERT INTO `purchase_request` VALUES (1, 'PRRQ-24091700001', '2017-09-24', 1, '', 0, '24-09-2017 05-48-07Untitled-1.jpg', 1, 1, 1, NULL, '2017-09-24 00:00:00', '2017-10-07 22:43:51', NULL);
INSERT INTO `purchase_request` VALUES (2, 'PRRQ-24091700002', '2017-09-24', 1, '', 9, NULL, NULL, 1, 1, NULL, '2017-09-24 00:00:00', '2017-09-24 05:48:15', NULL);
INSERT INTO `purchase_request` VALUES (3, 'PRRQ-25091700003', '2017-09-25', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-25 00:00:00', '2017-09-25 03:46:55', NULL);
INSERT INTO `purchase_request` VALUES (4, 'PRRQ-26091700004', '2017-11-20', 1, '', 9, '26-09-2017 05-56-05Quotation Test.docx', NULL, 1, 1, NULL, '2017-09-26 00:00:00', '2017-09-26 05:57:05', NULL);
INSERT INTO `purchase_request` VALUES (5, 'PRRQ-26091700005', '2017-09-26', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (6, 'PRRQ-26091700006', '2017-09-26', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', '2017-09-26 06:16:49', NULL);
INSERT INTO `purchase_request` VALUES (7, 'PRRQ-27091700007', '2017-09-27', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-09-27 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (8, 'PRRQ-30091700008', '2017-09-30', 1, '', 0, NULL, NULL, 1, 1, NULL, '2017-09-30 00:00:00', '2017-09-30 09:05:15', NULL);
INSERT INTO `purchase_request` VALUES (9, 'PRRQ-01101700009', '2017-10-27', 1, '', 0, NULL, NULL, 1, NULL, NULL, '2017-10-01 00:00:00', '2017-09-30 23:11:48', NULL);
INSERT INTO `purchase_request` VALUES (10, 'PRRQ-01101700010', '2017-10-01', 1, '', 0, NULL, NULL, 1, 1, NULL, '2017-10-01 00:00:00', '2017-10-01 03:38:40', NULL);
INSERT INTO `purchase_request` VALUES (11, 'PRRQ-04101700011', '2017-10-04', 0, NULL, 1, NULL, NULL, 1, 1, NULL, '2017-10-04 00:00:00', '2017-10-04 08:47:58', NULL);
INSERT INTO `purchase_request` VALUES (12, 'PRRQ-04101700012', '2017-10-04', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-04 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (13, 'PRRQ-08101700013', '2017-10-08', 2, '', 0, NULL, NULL, 1, NULL, NULL, '2017-10-08 00:00:00', '2017-10-07 22:41:36', NULL);
INSERT INTO `purchase_request` VALUES (14, 'PRRQ-22101700014', '2017-10-22', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-22 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (15, 'PRTX-23101700015', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-23 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (16, 'PRTX-23101700016', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-23 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (17, 'PRTX-23101700017', '2017-10-23', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-23 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (18, 'PRTX-28101700018', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (19, 'PRTX-28101700019', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (20, 'PRTX-28101700020', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (21, 'PRTX-28101700021', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (22, 'PRTX-28101700022', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (23, 'PRTX-28101700023', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (24, 'PRTX-28101700024', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (25, 'PRTX-28101700025', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (26, 'PRTX-28101700026', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (27, 'PRTX-28101700027', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (28, 'PRTX-28101700028', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (29, 'PRTX-28101700029', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (30, 'PRTX-28101700030', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (31, 'PRTX-28101700031', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (32, 'PRTX-28101700032', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (33, 'PRTX-28101700033', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (34, 'PRTX-28101700034', '2017-10-28', 0, NULL, 0, NULL, NULL, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (35, 'PRTX-28101700035', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (36, 'PRTX-28101700036', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (37, 'PRTX-28101700037', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
INSERT INTO `purchase_request` VALUES (38, 'PRTX-28101700038', '2017-10-28', 0, NULL, 0, NULL, 1, 1, NULL, NULL, '2017-10-28 00:00:00', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_request_detail
-- ----------------------------
DROP TABLE IF EXISTS `purchase_request_detail`;
CREATE TABLE `purchase_request_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_id` int(10) unsigned DEFAULT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `po_qty` double DEFAULT NULL,
  `so_d_id` int(10) DEFAULT NULL,
  `estimate_price` double DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`pr_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_request_detail
-- ----------------------------
BEGIN;
INSERT INTO `purchase_request_detail` VALUES (1, 1, 2, 'BOX', 300, 51, NULL, 10000, NULL, NULL, NULL, '2017-09-24 05:46:42', '2017-10-22 01:42:39', NULL);
INSERT INTO `purchase_request_detail` VALUES (2, 1, 2, 'CRT', 10, 50, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:47:00', '2017-09-24 05:47:00', NULL);
INSERT INTO `purchase_request_detail` VALUES (3, 1, 3, 'DOS', 8, 6, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:47:10', '2017-10-22 13:14:01', '2017-10-22 13:14:01');
INSERT INTO `purchase_request_detail` VALUES (4, 2, 14, 'BKS', 60, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:47:26', '2017-09-24 05:47:26', NULL);
INSERT INTO `purchase_request_detail` VALUES (5, 2, 18, 'CRT', 6, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 05:47:35', '2017-09-24 09:13:33', NULL);
INSERT INTO `purchase_request_detail` VALUES (6, 2, 1, 'BKS', 3, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-24 09:13:26', '2017-09-24 09:13:26', NULL);
INSERT INTO `purchase_request_detail` VALUES (7, 3, 1, 'BOX', 20, 10, NULL, NULL, NULL, NULL, NULL, '2017-09-25 03:46:32', '2017-09-25 03:46:41', NULL);
INSERT INTO `purchase_request_detail` VALUES (8, 4, 2, 'BOX', 5, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 05:58:59', '2017-09-26 05:58:59', NULL);
INSERT INTO `purchase_request_detail` VALUES (9, 4, 3, 'BOX', 10, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-26 05:59:24', '2017-09-26 05:59:24', NULL);
INSERT INTO `purchase_request_detail` VALUES (10, 6, 2, 'BOX', 196, 296, NULL, NULL, NULL, NULL, NULL, '2017-09-26 06:16:47', '2017-09-26 06:36:44', NULL);
INSERT INTO `purchase_request_detail` VALUES (11, 8, 1, 'BK', 5, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 09:05:28', '2017-09-30 09:05:28', NULL);
INSERT INTO `purchase_request_detail` VALUES (12, 8, 1, 'BK', 4, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-30 09:05:50', '2017-09-30 09:05:50', NULL);
INSERT INTO `purchase_request_detail` VALUES (13, 9, 4, 'BOX', 20, 5, NULL, NULL, NULL, NULL, NULL, '2017-09-30 23:11:44', '2017-09-30 23:11:44', NULL);
INSERT INTO `purchase_request_detail` VALUES (14, 10, 3, 'BOX', 1000, 1100, NULL, NULL, NULL, NULL, NULL, '2017-10-01 03:38:05', '2017-10-01 03:38:05', NULL);
INSERT INTO `purchase_request_detail` VALUES (15, 11, 1, 'UNIT', 25, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-04 08:47:43', '2017-10-04 08:47:43', NULL);
INSERT INTO `purchase_request_detail` VALUES (16, 13, 1, 'BK', 111, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 22:41:31', '2017-10-07 22:41:31', NULL);
INSERT INTO `purchase_request_detail` VALUES (17, 1, 2, 'BKS', 2, NULL, NULL, 111111, NULL, NULL, NULL, '2017-10-22 11:15:44', '2017-10-22 11:15:49', '2017-10-22 11:15:49');
INSERT INTO `purchase_request_detail` VALUES (18, 21, 2, 'BOX', 1, NULL, 4, 100000, NULL, NULL, NULL, '2017-10-28 02:20:28', '2017-10-28 02:20:38', NULL);
INSERT INTO `purchase_request_detail` VALUES (19, 21, 3, 'CRT', 200, NULL, 1, 300000, NULL, NULL, NULL, '2017-10-28 02:20:28', '2017-10-28 02:20:47', NULL);
INSERT INTO `purchase_request_detail` VALUES (20, 22, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:21:11', '2017-10-28 02:21:11', NULL);
INSERT INTO `purchase_request_detail` VALUES (21, 23, 3, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:21:55', '2017-10-28 02:21:55', NULL);
INSERT INTO `purchase_request_detail` VALUES (22, 23, 1, NULL, 1000, NULL, 2, NULL, NULL, NULL, NULL, '2017-10-28 02:21:55', '2017-10-28 02:21:55', NULL);
INSERT INTO `purchase_request_detail` VALUES (23, 23, 1, NULL, 1000, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-28 02:21:56', '2017-10-28 02:21:56', NULL);
INSERT INTO `purchase_request_detail` VALUES (24, 23, 2, NULL, 1, NULL, 4, NULL, NULL, NULL, NULL, '2017-10-28 02:21:56', '2017-10-28 02:21:56', NULL);
INSERT INTO `purchase_request_detail` VALUES (25, 23, 3, NULL, 10, NULL, 5, NULL, NULL, NULL, NULL, '2017-10-28 02:21:56', '2017-10-28 02:21:56', NULL);
INSERT INTO `purchase_request_detail` VALUES (26, 24, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:22:42', '2017-10-28 02:22:42', NULL);
INSERT INTO `purchase_request_detail` VALUES (27, 24, 1, NULL, 1000, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-28 02:22:42', '2017-10-28 02:22:42', NULL);
INSERT INTO `purchase_request_detail` VALUES (28, 25, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:27:41', '2017-10-28 02:27:41', NULL);
INSERT INTO `purchase_request_detail` VALUES (29, 26, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:28:17', '2017-10-28 02:28:17', NULL);
INSERT INTO `purchase_request_detail` VALUES (30, 27, 2, NULL, 500, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:31:58', '2017-10-28 02:31:58', NULL);
INSERT INTO `purchase_request_detail` VALUES (31, 28, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:32:20', '2017-10-28 02:32:20', NULL);
INSERT INTO `purchase_request_detail` VALUES (32, 28, 1, NULL, 1000, NULL, 2, NULL, NULL, NULL, NULL, '2017-10-28 02:32:20', '2017-10-28 02:32:20', NULL);
INSERT INTO `purchase_request_detail` VALUES (33, 29, 1, NULL, 1000, NULL, 8, NULL, NULL, NULL, NULL, '2017-10-28 02:33:42', '2017-10-28 02:33:42', NULL);
INSERT INTO `purchase_request_detail` VALUES (34, 29, 2, NULL, 500, NULL, 3, NULL, NULL, NULL, NULL, '2017-10-28 02:33:42', '2017-10-28 02:33:42', NULL);
INSERT INTO `purchase_request_detail` VALUES (35, 34, 1, NULL, 200, NULL, 4, NULL, NULL, NULL, NULL, '2017-10-28 02:38:02', '2017-10-28 02:38:02', NULL);
INSERT INTO `purchase_request_detail` VALUES (36, 35, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:41:02', '2017-10-28 02:41:02', NULL);
INSERT INTO `purchase_request_detail` VALUES (37, 36, 1, NULL, 1000, NULL, 7, NULL, NULL, NULL, NULL, '2017-10-28 02:41:23', '2017-10-28 02:41:23', NULL);
INSERT INTO `purchase_request_detail` VALUES (38, 37, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:41:33', '2017-10-28 02:41:33', NULL);
INSERT INTO `purchase_request_detail` VALUES (39, 38, 1, NULL, 200, NULL, 1, NULL, NULL, NULL, NULL, '2017-10-28 02:43:22', '2017-10-28 02:43:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_return
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return`;
CREATE TABLE `purchase_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pr_reference` varchar(50) NOT NULL,
  `pr_date` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_return
-- ----------------------------
BEGIN;
INSERT INTO `purchase_return` VALUES (1, 'RTRN-24091700001', '2017-09-24', 1, NULL, 2, 'IDR', '', 0, 1, 1, 1, NULL, '2017-09-24 00:00:00', '2017-10-08 00:53:46', NULL);
INSERT INTO `purchase_return` VALUES (2, 'RTRN-26091700002', '2017-09-26', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', NULL, NULL);
INSERT INTO `purchase_return` VALUES (3, 'RTRN-26091700003', '2017-09-26', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-26 00:00:00', NULL, NULL);
INSERT INTO `purchase_return` VALUES (4, 'RTRN-30091700004', '2017-09-30', 0, NULL, NULL, NULL, '', 0, NULL, 1, NULL, NULL, '2017-09-30 00:00:00', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for purchase_return_detail
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return_detail`;
CREATE TABLE `purchase_return_detail` (
  `pr_d_id` int(11) NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) NOT NULL,
  `prec_d_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `unitprice` double NOT NULL,
  `discount` int(11) NOT NULL,
  `amount` double NOT NULL,
  `quantity` double NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pr_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_return_detail
-- ----------------------------
BEGIN;
INSERT INTO `purchase_return_detail` VALUES (1, 2, 5, 2, 'BOX', 0, 0, 0, 50, NULL, NULL, NULL, '2017-09-26 06:22:14', '2017-09-26 06:23:23', '2017-09-26 06:23:23');
COMMIT;

-- ----------------------------
-- Table structure for refund_order
-- ----------------------------
DROP TABLE IF EXISTS `refund_order`;
CREATE TABLE `refund_order` (
  `ro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_id` int(11) DEFAULT NULL,
  `ro_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `ro_date` date DEFAULT NULL,
  `refund_total` decimal(25,10) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `ro_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of refund_order
-- ----------------------------
BEGIN;
INSERT INTO `refund_order` VALUES (1, 1, 'RO/MSU/201710/0001', 2, 1, '0000-00-00', 13255.0000000000, 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-21 04:40:57', '2017-09-21 04:40:57', '2017-09-23 16:16:22');
INSERT INTO `refund_order` VALUES (2, 1, 'RO/MSU/201710/0002', 2, 1, '0000-00-00', 4675.0000000000, 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 09:07:40', '2017-09-23 09:07:40', '2017-09-23 16:16:24');
INSERT INTO `refund_order` VALUES (3, 1, 'RO/MSU/201710/0003', 2, 1, '0000-00-00', 21285.0000000000, 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 09:08:48', '2017-09-23 09:08:48', '2017-09-23 16:16:26');
INSERT INTO `refund_order` VALUES (4, 1, 'RO/MSU/201710/0004', 2, 1, '0000-00-00', 7370.0000000000, 'CASH', '', 'DRAFT', NULL, 1, NULL, NULL, '2017-09-23 09:10:14', '2017-09-23 09:10:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for refund_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `refund_order_detail`;
CREATE TABLE `refund_order_detail` (
  `ro_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ro_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `so_d_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `refund_amount` decimal(25,10) DEFAULT NULL,
  `total_refund_amount` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ro_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of refund_order_detail
-- ----------------------------
BEGIN;
INSERT INTO `refund_order_detail` VALUES (1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-21 04:40:57', '2017-09-21 04:40:57', '2017-09-23 00:00:00');
INSERT INTO `refund_order_detail` VALUES (2, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-21 04:40:57', '2017-09-21 04:40:57', '2017-09-23 16:15:37');
INSERT INTO `refund_order_detail` VALUES (3, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-21 04:40:57', '2017-09-21 04:40:57', '2017-09-23 16:15:51');
INSERT INTO `refund_order_detail` VALUES (4, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:07:40', '2017-09-23 09:07:40', '2017-09-23 16:15:53');
INSERT INTO `refund_order_detail` VALUES (5, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:07:40', '2017-09-23 09:07:40', '2017-09-23 16:15:55');
INSERT INTO `refund_order_detail` VALUES (6, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:07:40', '2017-09-23 09:07:40', '2017-09-23 00:00:00');
INSERT INTO `refund_order_detail` VALUES (7, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:08:48', '2017-09-23 09:08:48', '2017-09-23 16:16:00');
INSERT INTO `refund_order_detail` VALUES (8, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:08:48', '2017-09-23 09:08:48', '2017-09-23 16:16:01');
INSERT INTO `refund_order_detail` VALUES (9, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-23 09:08:48', '2017-09-23 09:08:48', '2017-09-23 16:16:04');
INSERT INTO `refund_order_detail` VALUES (10, 4, 1, 4, 1.0000000000, 1980.0000000000, 1980.0000000000, 1, NULL, NULL, '2017-09-23 09:10:14', '2017-09-23 09:10:14', NULL);
INSERT INTO `refund_order_detail` VALUES (11, 4, 3, 5, 2.0000000000, 1650.0000000000, 3300.0000000000, 1, NULL, NULL, '2017-09-23 09:10:14', '2017-09-23 09:10:14', NULL);
INSERT INTO `refund_order_detail` VALUES (12, 4, 2, 6, 2.0000000000, 1045.0000000000, 2090.0000000000, 1, NULL, NULL, '2017-09-23 09:10:14', '2017-09-23 09:10:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sales_invoice
-- ----------------------------
DROP TABLE IF EXISTS `sales_invoice`;
CREATE TABLE `sales_invoice` (
  `si_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_id` int(11) DEFAULT NULL,
  `si_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `si_issue_date` date DEFAULT NULL,
  `si_due_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_by_detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_amount` double(25,10) DEFAULT NULL,
  `rec_amount` double(25,0) DEFAULT NULL,
  `si_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`si_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_invoice
-- ----------------------------
BEGIN;
INSERT INTO `sales_invoice` VALUES (1, 1, 'INV/MSU/201710/0001', '1970-01-01', '1970-01-01', 2, 'abc', NULL, NULL, NULL, 10000.0000000000, NULL, 'DRAFT', 'Sekali bayar harus lunas', 1, 1, NULL, '2017-09-18 17:39:42', '2017-09-18 17:53:17', NULL);
INSERT INTO `sales_invoice` VALUES (2, 1, 'INV/MSU/201710/0002', '1970-01-01', '1970-01-01', 2, 'CASH', NULL, NULL, NULL, 200000.0000000000, 1000, 'DRAFT', '', 1, NULL, NULL, '2017-09-21 04:18:37', '2017-09-21 04:18:37', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sales_order
-- ----------------------------
DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE `sales_order` (
  `so_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_quotation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `tax_rate` decimal(25,10) DEFAULT NULL,
  `tax_value` decimal(25,10) DEFAULT NULL,
  `total_line_item` decimal(25,10) DEFAULT NULL,
  `grand_total` decimal(25,10) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_date` date DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `so_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `need_pr` int(10) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`so_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_order
-- ----------------------------
BEGIN;
INSERT INTO `sales_order` VALUES (1, 'SO/MSU/201710/0001', NULL, 2, 1, 10.0000000000, 113500.0000000000, 1135000.0000000000, 1248500.0000000000, 'CASH', '2017-09-11', '', '0', 1, NULL, 1, NULL, NULL, '2017-09-16 17:12:36', '2017-09-16 18:04:55', NULL);
INSERT INTO `sales_order` VALUES (2, 'SO/MSU/201710/0002', NULL, 5, 1, 10.0000000000, 47500.0000000000, 475000.0000000000, 522500.0000000000, 'Cash', '0000-00-00', '', 'DRAFT', 1, NULL, 1, NULL, NULL, '2017-09-26 07:01:50', '2017-09-26 07:01:50', NULL);
INSERT INTO `sales_order` VALUES (3, 'SO/MSU/201710/0003', NULL, 3, 1, 10.0000000000, 42750.0000000000, 427500.0000000000, 470250.0000000000, 'Transfer', '0000-00-00', '', 'DRAFT', 1, NULL, 1, NULL, NULL, '2017-09-26 07:02:29', '2017-09-26 07:02:29', NULL);
INSERT INTO `sales_order` VALUES (4, 'SO/MSU/201710/0004', NULL, 2, 1, 10.0000000000, 0.5000000000, 5.0000000000, 5.5000000000, 'CASH', '0000-00-00', '', 'APPROVAL', 1, NULL, 1, NULL, NULL, '2017-10-01 04:53:13', '2017-10-12 20:33:09', NULL);
INSERT INTO `sales_order` VALUES (5, 'SO/MSU/201710/0001', NULL, 5, 1, 10.0000000000, 2000.0000000000, 20000.0000000000, 22000.0000000000, 'Cash', '0000-00-00', '', 'COMPLETED', 1, NULL, 1, NULL, NULL, '2017-10-03 03:42:31', '2017-10-07 10:25:15', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sales_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `sales_order_detail`;
CREATE TABLE `sales_order_detail` (
  `so_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `discount_rate` decimal(25,10) DEFAULT NULL,
  `discount_value` decimal(25,10) DEFAULT NULL,
  `total_discount_value` decimal(25,10) DEFAULT NULL,
  `price_before_discount` decimal(25,10) DEFAULT NULL,
  `price_after_discount` decimal(25,10) DEFAULT NULL,
  `total_price_before_discount` decimal(25,10) DEFAULT NULL,
  `total_price_after_discount` decimal(25,10) DEFAULT NULL,
  `discount_price_before_tax` decimal(25,10) DEFAULT NULL,
  `discount_price_after_tax` decimal(25,10) DEFAULT NULL,
  `total_discount_price_before_tax` decimal(25,10) DEFAULT NULL,
  `total_discount_price_after_tax` decimal(25,10) DEFAULT NULL,
  `pr_qty` double DEFAULT NULL,
  `created_pr` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`so_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_order_detail
-- ----------------------------
BEGIN;
INSERT INTO `sales_order_detail` VALUES (1, 1, 1, 200, 10.0000000000, 200.0000000000, 40000.0000000000, 2000.0000000000, 1800.0000000000, 400000.0000000000, 360000.0000000000, 1800.0000000000, 1620.0000000000, 360000.0000000000, 324000.0000000000, NULL, NULL, NULL, NULL, 1, '2017-09-16 17:12:36', '2017-09-16 18:04:55', '2017-09-16 18:04:55');
INSERT INTO `sales_order_detail` VALUES (2, 1, 3, 200, 0.0000000000, 0.0000000000, 0.0000000000, 1500.0000000000, 1500.0000000000, 300000.0000000000, 300000.0000000000, 1500.0000000000, 1350.0000000000, 300000.0000000000, 270000.0000000000, NULL, NULL, NULL, NULL, 1, '2017-09-16 17:12:36', '2017-09-16 18:04:55', '2017-09-16 18:04:55');
INSERT INTO `sales_order_detail` VALUES (3, 1, 2, 500, 5.0000000000, 50.0000000000, 25000.0000000000, 1000.0000000000, 950.0000000000, 500000.0000000000, 475000.0000000000, 950.0000000000, 855.0000000000, 475000.0000000000, 427500.0000000000, NULL, NULL, NULL, NULL, 1, '2017-09-16 17:12:36', '2017-09-16 18:04:55', '2017-09-16 18:04:55');
INSERT INTO `sales_order_detail` VALUES (4, 1, 1, 200, 10.0000000000, 200.0000000000, 40000.0000000000, 2000.0000000000, 1800.0000000000, 400000.0000000000, 360000.0000000000, 1800.0000000000, 1980.0000000000, 360000.0000000000, 396000.0000000001, NULL, 1, NULL, NULL, NULL, '2017-09-16 18:04:56', '2017-10-28 02:38:02', NULL);
INSERT INTO `sales_order_detail` VALUES (5, 1, 3, 200, 0.0000000000, 0.0000000000, 0.0000000000, 1500.0000000000, 1500.0000000000, 300000.0000000000, 300000.0000000000, 1500.0000000000, 1650.0000000000, 300000.0000000000, 330000.0000000001, NULL, NULL, NULL, NULL, NULL, '2017-09-16 18:04:56', '2017-09-16 18:04:56', NULL);
INSERT INTO `sales_order_detail` VALUES (6, 1, 2, 500, 5.0000000000, 50.0000000000, 25000.0000000000, 1000.0000000000, 950.0000000000, 500000.0000000000, 475000.0000000000, 950.0000000000, 1045.0000000000, 475000.0000000000, 522500.0000000000, NULL, NULL, NULL, NULL, NULL, '2017-09-16 18:04:56', '2017-09-16 18:04:56', NULL);
INSERT INTO `sales_order_detail` VALUES (7, 2, 1, 1000, 5.0000000000, 25.0000000000, 25000.0000000000, 500.0000000000, 475.0000000000, 500000.0000000000, 475000.0000000000, 475.0000000000, 522.5000000000, 475000.0000000000, 522500.0000000000, NULL, 1, NULL, NULL, NULL, '2017-09-26 07:01:50', '2017-10-28 02:41:23', NULL);
INSERT INTO `sales_order_detail` VALUES (8, 3, 1, 1000, 5.0000000000, 22.5000000000, 22500.0000000000, 450.0000000000, 427.5000000000, 450000.0000000000, 427500.0000000000, 427.5000000000, 470.2500000000, 427500.0000000000, 470250.0000000001, NULL, 1, NULL, NULL, NULL, '2017-09-26 07:02:29', '2017-10-28 02:33:42', NULL);
INSERT INTO `sales_order_detail` VALUES (9, 4, 2, 1, 0.0000000000, 0.0000000000, 0.0000000000, 5.0000000000, 5.0000000000, 5.0000000000, 5.0000000000, 5.0000000000, 5.5000000000, 5.0000000000, 5.5000000000, NULL, NULL, NULL, NULL, NULL, '2017-10-01 04:53:13', '2017-10-01 04:53:13', NULL);
INSERT INTO `sales_order_detail` VALUES (10, 5, 3, 10, 0.0000000000, 0.0000000000, 0.0000000000, 2000.0000000000, 2000.0000000000, 20000.0000000000, 20000.0000000000, 2000.0000000000, 2200.0000000000, 20000.0000000000, 22000.0000000000, NULL, NULL, NULL, NULL, NULL, '2017-10-03 03:42:31', '2017-10-03 03:42:31', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_adjustment
-- ----------------------------
DROP TABLE IF EXISTS `stock_adjustment`;
CREATE TABLE `stock_adjustment` (
  `sa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sa_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sa_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `so_id` int(11) DEFAULT NULL,
  `sa_date` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_adjustment
-- ----------------------------
BEGIN;
INSERT INTO `stock_adjustment` VALUES (1, 'SADJ/MSU/201710/0001', 'DRAFT', 1, NULL, '2017-10-10', NULL, NULL, 1, 1, NULL, NULL, '2017-10-03 15:28:33', '2017-10-03 15:28:33', '2017-10-03 22:29:31');
INSERT INTO `stock_adjustment` VALUES (2, 'SADJ/MSU/201710/0001', 'APPROVED', 1, NULL, '2017-10-10', 1, '2017-10-03', 1, 1, NULL, NULL, '2017-10-03 15:29:33', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_adjustment` VALUES (3, 'SADJ/MSU/201710/0002', 'APPROVED', 1, NULL, '2010-06-01', 1, '2017-10-04', 1, 1, 1, NULL, '2017-10-04 03:18:01', '2017-10-04 03:47:15', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_adjustment_detail
-- ----------------------------
DROP TABLE IF EXISTS `stock_adjustment_detail`;
CREATE TABLE `stock_adjustment_detail` (
  `sa_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sa_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `db_quantity` double(25,10) DEFAULT NULL,
  `actual_quantity` double(25,10) DEFAULT NULL,
  `quantity_difference` double(25,10) DEFAULT NULL,
  `price` double(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sa_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_adjustment_detail
-- ----------------------------
BEGIN;
INSERT INTO `stock_adjustment_detail` VALUES (1, 2, 4, '', -15.0000000000, 0.0000000000, 15.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 15:29:33', '2017-10-03 15:29:33', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (2, 2, 2, '', -10.0000000000, 0.0000000000, 10.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 15:29:33', '2017-10-03 15:29:33', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (3, 2, 3, '', -60.0000000000, 0.0000000000, 60.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 15:29:34', '2017-10-03 15:29:34', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (4, 2, 1, '', -10.0000000000, 0.0000000000, 10.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-03 15:29:34', '2017-10-03 15:29:34', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (5, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:18:01', '2017-10-04 03:28:12', '2017-10-04 03:28:12');
INSERT INTO `stock_adjustment_detail` VALUES (6, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:18:01', '2017-10-04 03:28:12', '2017-10-04 03:28:12');
INSERT INTO `stock_adjustment_detail` VALUES (7, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:18:01', '2017-10-04 03:28:12', '2017-10-04 03:28:12');
INSERT INTO `stock_adjustment_detail` VALUES (8, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:18:01', '2017-10-04 03:28:12', '2017-10-04 03:28:12');
INSERT INTO `stock_adjustment_detail` VALUES (9, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:46:57', '2017-10-04 03:47:08', '2017-10-04 03:47:08');
INSERT INTO `stock_adjustment_detail` VALUES (10, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:46:57', '2017-10-04 03:47:08', '2017-10-04 03:47:08');
INSERT INTO `stock_adjustment_detail` VALUES (11, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:46:57', '2017-10-04 03:47:08', '2017-10-04 03:47:08');
INSERT INTO `stock_adjustment_detail` VALUES (12, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:46:57', '2017-10-04 03:47:08', '2017-10-04 03:47:08');
INSERT INTO `stock_adjustment_detail` VALUES (13, 3, 1, '', 0.0000000000, 2000.0000000000, 2000.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:47:08', '2017-10-04 03:47:08', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (14, 3, 2, '', 0.0000000000, -8.0000000000, -8.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:47:08', '2017-10-04 03:47:08', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (15, 3, 3, '', 5.0000000000, -12.0000000000, -17.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:47:08', '2017-10-04 03:47:08', NULL);
INSERT INTO `stock_adjustment_detail` VALUES (16, 3, 4, '', 0.0000000000, 0.0000000000, 0.0000000000, NULL, 1, 1, NULL, NULL, '2017-10-04 03:47:08', '2017-10-04 03:47:08', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_inventory
-- ----------------------------
DROP TABLE IF EXISTS `stock_inventory`;
CREATE TABLE `stock_inventory` (
  `si_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `last_price` decimal(25,10) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`si_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_inventory
-- ----------------------------
BEGIN;
INSERT INTO `stock_inventory` VALUES (1, NULL, 4, 2.0000000000, NULL, NULL, NULL, NULL, '2017-10-01 00:48:06', '2017-10-01 00:48:06', '2017-10-02 23:29:55');
INSERT INTO `stock_inventory` VALUES (2, 1, 2, 0.0000000000, NULL, NULL, NULL, NULL, '2017-10-01 03:31:32', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_inventory` VALUES (3, 1, 3, 5.0000000000, NULL, NULL, NULL, NULL, '2017-10-01 03:31:32', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_inventory` VALUES (4, 1, 4, 0.0000000000, NULL, NULL, NULL, NULL, '2017-10-02 16:30:50', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_inventory` VALUES (5, 1, 1, 2000.0000000000, 500.0000000000, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-07 16:46:45', NULL);
INSERT INTO `stock_inventory` VALUES (6, NULL, 3, 1.0000000000, NULL, NULL, NULL, NULL, '2017-10-07 18:08:20', '2017-10-07 18:08:20', NULL);
INSERT INTO `stock_inventory` VALUES (7, NULL, 1, 1.0000000000, NULL, NULL, NULL, NULL, '2017-10-07 23:57:18', '2017-10-07 23:57:18', NULL);
INSERT INTO `stock_inventory` VALUES (8, NULL, 1, 3.0000000000, NULL, NULL, NULL, NULL, '2017-10-28 02:55:37', '2017-10-28 02:55:37', NULL);
INSERT INTO `stock_inventory` VALUES (9, NULL, 1, 1.0000000000, NULL, NULL, NULL, NULL, '2017-10-28 02:56:03', '2017-10-28 02:56:03', NULL);
INSERT INTO `stock_inventory` VALUES (10, NULL, 1, 1.0000000000, NULL, NULL, NULL, NULL, '2017-10-28 03:00:36', '2017-10-28 03:00:36', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_movement
-- ----------------------------
DROP TABLE IF EXISTS `stock_movement`;
CREATE TABLE `stock_movement` (
  `sm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `si_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `quantity` decimal(25,10) DEFAULT NULL,
  `price` decimal(25,10) DEFAULT NULL,
  `total_price` decimal(25,10) DEFAULT NULL,
  `trigger_table` text COLLATE utf8_unicode_ci,
  `trigger_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_movement
-- ----------------------------
BEGIN;
INSERT INTO `stock_movement` VALUES (1, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `stock_movement` VALUES (2, NULL, 4, NULL, 2.0000000000, 50000.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-01 00:48:06', '2017-10-01 00:48:06', NULL);
INSERT INTO `stock_movement` VALUES (3, 2, 2, 1, 100.0000000000, NULL, NULL, 'production_detail', NULL, NULL, NULL, NULL, '2017-10-01 03:31:32', '2017-10-01 03:31:32', NULL);
INSERT INTO `stock_movement` VALUES (4, 3, 3, 1, 50.0000000000, NULL, NULL, 'production_detail', NULL, NULL, NULL, NULL, '2017-10-01 03:31:32', '2017-10-01 03:31:32', NULL);
INSERT INTO `stock_movement` VALUES (5, 2, 2, 1, 100.0000000000, 0.0000000000, 0.0000000000, 'production', NULL, NULL, NULL, NULL, '2017-10-01 03:32:37', '2017-10-01 03:32:37', NULL);
INSERT INTO `stock_movement` VALUES (6, 4, 4, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:30:50', '2017-10-02 16:30:50', NULL);
INSERT INTO `stock_movement` VALUES (7, 4, 4, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (8, 4, 4, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (9, 2, 2, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (10, 2, 2, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (11, 3, 3, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (12, 3, 3, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (13, 5, 1, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (14, 5, 1, 1, 5.0000000000, NULL, NULL, 'stock_mutation', 2, NULL, NULL, NULL, '2017-10-02 16:31:02', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_movement` VALUES (15, 4, 4, 1, 15.0000000000, NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 15:43:52', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_movement` VALUES (16, 2, 2, 1, 10.0000000000, NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 15:43:52', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_movement` VALUES (17, 3, 3, 1, 60.0000000000, NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 15:43:52', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_movement` VALUES (18, 5, 1, 1, 10.0000000000, NULL, NULL, 'stock_adjustment', 2, NULL, NULL, NULL, '2017-10-03 15:43:52', '2017-10-03 15:43:52', NULL);
INSERT INTO `stock_movement` VALUES (19, 5, 1, 1, 2000.0000000000, NULL, NULL, 'stock_adjustment', 3, NULL, NULL, NULL, '2017-10-04 03:47:15', '2017-10-04 03:47:15', NULL);
INSERT INTO `stock_movement` VALUES (20, 5, 1, 1, -500.0000000000, 500.0000000000, 500000.0000000000, 'delivery_order', NULL, NULL, NULL, NULL, '2017-10-07 16:46:45', '2017-10-07 16:46:45', NULL);
INSERT INTO `stock_movement` VALUES (21, NULL, 3, NULL, 1.0000000000, 50.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 18:08:20', '2017-10-07 18:08:20', NULL);
INSERT INTO `stock_movement` VALUES (22, NULL, 1, NULL, 1.0000000000, 50.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-07 23:57:18', '2017-10-07 23:57:18', NULL);
INSERT INTO `stock_movement` VALUES (23, NULL, 1, NULL, 3.0000000000, 200000.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 02:55:37', '2017-10-28 02:55:37', NULL);
INSERT INTO `stock_movement` VALUES (24, NULL, 1, NULL, 1.0000000000, 200000.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 02:56:03', '2017-10-28 02:56:03', NULL);
INSERT INTO `stock_movement` VALUES (25, NULL, 1, NULL, 1.0000000000, 200000.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-28 03:00:36', '2017-10-28 03:00:36', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_mutation
-- ----------------------------
DROP TABLE IF EXISTS `stock_mutation`;
CREATE TABLE `stock_mutation` (
  `sm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sm_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warehouse_id_from` int(11) DEFAULT NULL,
  `warehouse_id_to` int(11) DEFAULT NULL,
  `sm_date` date DEFAULT NULL,
  `sm_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_mutation
-- ----------------------------
BEGIN;
INSERT INTO `stock_mutation` VALUES (1, 'SMUT/MSU/201710/0001', 1, 1, '2017-10-18', 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-10-02 16:14:58', '2017-10-02 16:14:58', '2017-10-02 23:16:12');
INSERT INTO `stock_mutation` VALUES (2, 'SMUT/MSU/201710/0001', 1, 1, '2017-10-18', 'APPROVED', 1, '2017-10-02', 1, 1, NULL, NULL, '2017-10-02 16:16:14', '2017-10-02 16:31:02', NULL);
INSERT INTO `stock_mutation` VALUES (3, 'SMUT/MSU/201710/0002', 1, 2, '2017-10-10', 'REJECTED', NULL, NULL, 1, 1, NULL, NULL, '2017-10-03 04:53:54', '2017-10-03 04:54:03', NULL);
INSERT INTO `stock_mutation` VALUES (4, 'SMUT/MSU/201710/0003', 1, 2, '2017-10-06', 'DRAFT', NULL, NULL, 1, 1, NULL, NULL, '2017-10-04 03:12:06', '2017-10-04 03:12:06', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_mutation_detail
-- ----------------------------
DROP TABLE IF EXISTS `stock_mutation_detail`;
CREATE TABLE `stock_mutation_detail` (
  `sm_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sm_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `quantity` decimal(25,10) DEFAULT NULL,
  `quantity_from_before` decimal(25,10) DEFAULT NULL,
  `quantity_to_before` decimal(25,10) DEFAULT NULL,
  `quantity_from_after` decimal(25,10) DEFAULT NULL,
  `quantity_to_after` decimal(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sm_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_mutation_detail
-- ----------------------------
BEGIN;
INSERT INTO `stock_mutation_detail` VALUES (1, 2, 4, '', 5.0000000000, 0.0000000000, 0.0000000000, -5.0000000000, 5.0000000000, 1, 1, NULL, NULL, '2017-10-02 16:16:15', '2017-10-02 16:16:15', NULL);
INSERT INTO `stock_mutation_detail` VALUES (2, 2, 2, '', 5.0000000000, 0.0000000000, 0.0000000000, -5.0000000000, 5.0000000000, 1, 1, NULL, NULL, '2017-10-02 16:16:15', '2017-10-02 16:16:15', NULL);
INSERT INTO `stock_mutation_detail` VALUES (3, 2, 3, '', 5.0000000000, -50.0000000000, -50.0000000000, -55.0000000000, -45.0000000000, 1, 1, NULL, NULL, '2017-10-02 16:16:15', '2017-10-02 16:16:15', NULL);
INSERT INTO `stock_mutation_detail` VALUES (4, 2, 1, '', 5.0000000000, 0.0000000000, 0.0000000000, -5.0000000000, 5.0000000000, 1, 1, NULL, NULL, '2017-10-02 16:16:15', '2017-10-02 16:16:15', NULL);
INSERT INTO `stock_mutation_detail` VALUES (5, 3, 1, '', 0.0000000000, -10.0000000000, 0.0000000000, -10.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-10-03 04:53:54', '2017-10-03 04:53:54', NULL);
INSERT INTO `stock_mutation_detail` VALUES (6, 3, 2, '', 5.0000000000, -10.0000000000, 0.0000000000, -15.0000000000, 5.0000000000, 1, 1, NULL, NULL, '2017-10-03 04:53:54', '2017-10-03 04:53:54', NULL);
INSERT INTO `stock_mutation_detail` VALUES (7, 3, 3, '', 0.0000000000, -60.0000000000, 0.0000000000, -60.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-10-03 04:53:54', '2017-10-03 04:53:54', NULL);
INSERT INTO `stock_mutation_detail` VALUES (8, 3, 4, '', 10.0000000000, -15.0000000000, 0.0000000000, -25.0000000000, 10.0000000000, 1, 1, NULL, NULL, '2017-10-03 04:53:54', '2017-10-03 04:53:54', NULL);
INSERT INTO `stock_mutation_detail` VALUES (9, 4, 4, '', 10.0000000000, 0.0000000000, 0.0000000000, -10.0000000000, 10.0000000000, 1, 1, NULL, NULL, '2017-10-04 03:12:06', '2017-10-04 03:12:06', NULL);
INSERT INTO `stock_mutation_detail` VALUES (10, 4, 2, '', 10.0000000000, 0.0000000000, 0.0000000000, -10.0000000000, 10.0000000000, 1, 1, NULL, NULL, '2017-10-04 03:12:06', '2017-10-04 03:12:06', NULL);
INSERT INTO `stock_mutation_detail` VALUES (11, 4, 3, '', 10.0000000000, 5.0000000000, 0.0000000000, -5.0000000000, 10.0000000000, 1, 1, NULL, NULL, '2017-10-04 03:12:06', '2017-10-04 03:12:06', NULL);
INSERT INTO `stock_mutation_detail` VALUES (12, 4, 1, '', 10.0000000000, 0.0000000000, 0.0000000000, -10.0000000000, 10.0000000000, 1, 1, NULL, NULL, '2017-10-04 03:12:06', '2017-10-04 03:12:06', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_opname
-- ----------------------------
DROP TABLE IF EXISTS `stock_opname`;
CREATE TABLE `stock_opname` (
  `so_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `so_date` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `approved_at` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`so_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_opname
-- ----------------------------
BEGIN;
INSERT INTO `stock_opname` VALUES (1, 'STOPN/MSU/201710/0001', 'APPROVED', 1, '2017-10-26', 1, '2017-09-30', 1, 1, 1, NULL, '2017-09-30 18:23:16', '2017-09-30 19:16:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stock_opname_detail
-- ----------------------------
DROP TABLE IF EXISTS `stock_opname_detail`;
CREATE TABLE `stock_opname_detail` (
  `so_d_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `db_quantity` double(25,10) DEFAULT NULL,
  `actual_quantity` double(25,10) DEFAULT NULL,
  `quantity_difference` double(25,10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`so_d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_opname_detail
-- ----------------------------
BEGIN;
INSERT INTO `stock_opname_detail` VALUES (1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:23:17', '2017-09-30 18:48:30', '2017-09-30 18:48:30');
INSERT INTO `stock_opname_detail` VALUES (2, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:23:17', '2017-09-30 18:48:30', '2017-09-30 18:48:30');
INSERT INTO `stock_opname_detail` VALUES (3, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:23:17', '2017-09-30 18:48:30', '2017-09-30 18:48:30');
INSERT INTO `stock_opname_detail` VALUES (4, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:48:47', '2017-09-30 18:49:23', '2017-09-30 18:49:23');
INSERT INTO `stock_opname_detail` VALUES (5, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:48:47', '2017-09-30 18:49:23', '2017-09-30 18:49:23');
INSERT INTO `stock_opname_detail` VALUES (6, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2017-09-30 18:48:47', '2017-09-30 18:49:23', '2017-09-30 18:49:23');
INSERT INTO `stock_opname_detail` VALUES (7, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:50:08', '2017-09-30 18:58:33', '2017-09-30 18:58:33');
INSERT INTO `stock_opname_detail` VALUES (8, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:50:08', '2017-09-30 18:58:33', '2017-09-30 18:58:33');
INSERT INTO `stock_opname_detail` VALUES (9, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:50:08', '2017-09-30 18:58:33', '2017-09-30 18:58:33');
INSERT INTO `stock_opname_detail` VALUES (10, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:58:33', '2017-09-30 19:00:47', '2017-09-30 19:00:47');
INSERT INTO `stock_opname_detail` VALUES (11, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:58:33', '2017-09-30 19:00:47', '2017-09-30 19:00:47');
INSERT INTO `stock_opname_detail` VALUES (12, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 18:58:33', '2017-09-30 19:00:47', '2017-09-30 19:00:47');
INSERT INTO `stock_opname_detail` VALUES (13, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:00:47', '2017-09-30 19:02:13', '2017-09-30 19:02:13');
INSERT INTO `stock_opname_detail` VALUES (14, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:00:47', '2017-09-30 19:02:13', '2017-09-30 19:02:13');
INSERT INTO `stock_opname_detail` VALUES (15, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:00:47', '2017-09-30 19:02:13', '2017-09-30 19:02:13');
INSERT INTO `stock_opname_detail` VALUES (16, 1, 1, '', 2000.0000000000, 2000.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:02:13', '2017-09-30 19:02:13', NULL);
INSERT INTO `stock_opname_detail` VALUES (17, 1, 2, '', -8.0000000000, -8.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:02:13', '2017-09-30 19:02:13', NULL);
INSERT INTO `stock_opname_detail` VALUES (18, 1, 3, '', -12.0000000000, -12.0000000000, 0.0000000000, 1, 1, NULL, NULL, '2017-09-30 19:02:13', '2017-09-30 19:02:13', NULL);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `note` varchar(0) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `maxpoamount` double DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'admin', '$2y$10$7tSXd2W7XunXGV.CJq.WhO5SE/p0cLSx4Yz8WIIxI.uwPLQiLtsWS', 'Admin', 'admin@jafelmia.com', '0000000', 1, '2017-09-27', '2017-10-13', '', NULL, 200000, '0', 'ap0V57gvbVp9eJ9GGOKkMqbvXURKhvtTRFwojh0IuAwYFJQam6cV5dghGqRo', 0, 1, NULL, NULL, '2017-10-22 11:14:55', NULL);
INSERT INTO `user` VALUES (2, 'Sales', '$2y$10$NwRH9aKCFWgkE6VRrEkkw.JAsD5vOmveSVr.ThFv0vCA9zdT9mOiK', 'Sales 1', 'sales@sales.sales', '123456', 0, NULL, NULL, NULL, NULL, NULL, '0', NULL, 1, NULL, NULL, '2017-10-03 03:51:28', '2017-10-03 03:51:28', NULL);
INSERT INTO `user` VALUES (3, 'husni', '$2y$10$v4mJsTgXKgf.DTCrRUG4oeiSZLQo.DFZ5DXcj8Sxy1gn99HmCiC.K', 'husni mubarok', 'husni.mubarok@outlook.com', '08717161617', 1, NULL, NULL, NULL, NULL, 10000000, '0', NULL, 1, NULL, NULL, '2017-10-29 02:02:58', '2017-10-29 02:02:58', NULL);
COMMIT;

-- ----------------------------
-- Triggers structure for table ap_payment_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_app`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `insert_app` AFTER INSERT ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (NEW.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (NEW.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (NEW.ap_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table ap_payment_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `update_app`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `update_app` AFTER UPDATE ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (NEW.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (NEW.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (NEW.ap_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table ap_payment_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `delete_app`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `delete_app` AFTER DELETE ON `ap_payment_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM ap_invoice_detail WHERE id IN (OLD.ap_d_id)) THEN

UPDATE ap_invoice_detail SET cp_amount= (SELECT IFNULL(SUM(amount),0) FROM ap_payment_detail WHERE ap_d_id IN (OLD.ap_d_id)
AND ap_payment_detail.ap_d_id=ap_invoice_detail.id) WHERE ap_invoice_detail.id IN (OLD.ap_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table ar_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_arr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `insert_arr` AFTER INSERT ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (NEW.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (NEW.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (NEW.ar_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table ar_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `update_arr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `update_arr` AFTER UPDATE ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (NEW.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (NEW.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (NEW.ar_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table ar_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `delete_arr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `delete_arr` AFTER DELETE ON `ar_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT si_id FROM sales_invoice WHERE si_id IN (OLD.ar_id)) THEN

UPDATE sales_invoice SET rec_amount= (SELECT IFNULL(SUM(amount),0) FROM ar_receive_detail WHERE ar_id IN (OLD.ar_id)
AND ar_receive_detail.ar_id=sales_invoice.si_id) WHERE sales_invoice.si_id IN (OLD.ar_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_order_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_pr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `insert_pr` AFTER INSERT ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (NEW.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (NEW.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (NEW.pr_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_order_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `update_pr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `update_pr` AFTER UPDATE ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (NEW.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (NEW.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (NEW.pr_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_order_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `delete_pr`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `delete_pr` AFTER DELETE ON `purchase_order_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_request_detail WHERE id IN (OLD.pr_d_id)) THEN

UPDATE purchase_request_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_order_detail WHERE pr_d_id IN (OLD.pr_d_id)
AND purchase_order_detail.pr_d_id=purchase_request_detail.id) WHERE purchase_request_detail.id IN (OLD.pr_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_po`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `insert_po` AFTER INSERT ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (NEW.po_d_id)) THEN

UPDATE purchase_order_detail SET gr_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (NEW.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (NEW.po_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `update_po`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `update_po` AFTER UPDATE ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (NEW.po_d_id)) THEN

UPDATE purchase_order_detail SET gr_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (NEW.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (NEW.po_d_id);

END IF;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table purchase_receive_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `delete_po`;
delimiter ;;
CREATE DEFINER = `root`@`localhost` TRIGGER `delete_po` AFTER DELETE ON `purchase_receive_detail` FOR EACH ROW BEGIN

IF EXISTS (SELECT id FROM purchase_order_detail WHERE id IN (OLD.po_d_id)) THEN

UPDATE purchase_order_detail SET po_qty= (SELECT IFNULL(SUM(quantity),0) FROM purchase_receive_detail WHERE po_d_id IN (OLD.po_d_id)
AND purchase_receive_detail.po_d_id=purchase_order_detail.id) WHERE purchase_order_detail.id IN (OLD.po_d_id);

END IF;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
