<?php

namespace App\Http\Controllers\Editor;

use Auth; 
use Illuminate\Support\Facades\Input; 
use App\Http\Controllers\Controller; 

class CacheController extends Controller
{
    
    public function index()
    {
 
        return view ('editor.cache.index');
    } 

    public function clearcache()
    {

        // dd("asdasd");
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('view:clear');
        $exitCode = Artisan::call('config:clear');

    	return view ('editor.cache.index');
    } 
}
