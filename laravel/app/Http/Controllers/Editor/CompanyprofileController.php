<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyprofileRequest;
use App\Http\Controllers\Controller;
use App\Repository\CompanyprofileRepository;

class CompanyprofileController extends Controller
{
    protected $CompanyprofileRepository;

    public function __construct(CompanyprofileRepository $companyprofile_repository)
    {
    	$this->CompanyprofileRepository = $companyprofile_repository;
    }

    public function index()
    {
    	$companyprofiles = $this->CompanyprofileRepository->get_all();
    	return view ('editor.companyprofile.index', compact('companyprofiles'));
    }

    public function create()
    {
    	return view ('editor.companyprofile.form');
    }

    public function store(CompanyprofileRequest $request)
    {
    	$companyprofile = $this->CompanyprofileRepository->store($request->input());
    	if($request->image)
    	{
    		$this->CompanyprofileRepository->update_image($companyprofile->id, $request->image);
    	}
    	return redirect()->action('Editor\CompanyprofileController@index');
    }

    public function edit($id)
    {
    	$companyprofile = $this->CompanyprofileRepository->get_one($id);
    	return view ('editor.companyprofile.form', compact('companyprofile'));
    }

    public function update($id, CompanyprofileRequest $request)
    {
    	$companyprofile = $this->CompanyprofileRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->CompanyprofileRepository->update_image($companyprofile->id, $request->image);
    	}
    	return redirect()->action('Editor\CompanyprofileController@index');
    }

    public function delete($id)
    {
    	$this->CompanyprofileRepository->delete($id);
    	return redirect()->action('Editor\CompanyprofileController@index');
    }
}
