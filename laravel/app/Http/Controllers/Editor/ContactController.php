<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use App\Repository\ContactRepository;

class ContactController extends Controller
{
    protected $ContactRepository;

    public function __construct(ContactRepository $contact_repository)
    {
    	$this->ContactRepository = $contact_repository;
    }

    public function index()
    {
    	$contacts = $this->ContactRepository->get_all();
    	return view ('editor.contact.index', compact('contacts'));
    }

    public function create()
    {
    	return view ('editor.contact.form');
    }

    public function store(ContactRequest $request)
    {
    	$contact = $this->ContactRepository->store($request->input());
    	if($request->image)
    	{
    		$this->ContactRepository->update_image($contact->id, $request->image);
    	}
    	return redirect()->action('Editor\ContactController@index');
    }

    public function edit($id)
    {
    	$contact = $this->ContactRepository->get_one($id);
    	return view ('editor.contact.form', compact('contact'));
    }

    public function update($id, ContactRequest $request)
    {
    	$contact = $this->ContactRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->ContactRepository->update_image($contact->id, $request->image);
    	}
    	return redirect()->action('Editor\ContactController@index');
    }

    public function delete($id)
    {
    	$this->ContactRepository->delete($id);
    	return redirect()->action('Editor\ContactController@index');
    }
}
