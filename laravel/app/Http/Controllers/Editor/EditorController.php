<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Payroll;
use DB;

class EditorController extends Controller
{
    public function index()
    {
	 $birthday = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				employee.sex,
				department.departmentname,
				employee.image, 
				DATE_FORMAT(employee.datebirth, '%d %M %Y') AS datebirth
			FROM
				employee
			LEFT JOIN department ON employee.departmentid = department.id
			WHERE
				MONTH (datebirth) = MONTH (NOW())
			ORDER BY DATE_FORMAT(employee.datebirth, '%d') ASC
        "));

	 $new_hire = \DB::select(\DB::raw("
				           SELECT
					employee.employeename,
					employee.sex,
					department.departmentname,
					DATE_FORMAT(employee.termdate, '%d %M') AS termdate,
					DATE_FORMAT(
						employee.joindate,
						'%d %M %Y'
					) AS joindate,
					employee.image
				FROM
					employee
				LEFT JOIN department ON employee.departmentid = department.id
				ORDER BY
					employee.joindate DESC
				LIMIT 4"));

	 $leave = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				DATE_FORMAT(
					leaving.leavingfrom,
					'%d-%m-%Y'
				) AS fromdate,
				DATE_FORMAT(
					leaving.leavingto,
					'%d-%m-%Y'
				) AS todate,
				department.departmentname
			FROM
				leaving 
			LEFT JOIN employee ON leaving.employeeid = employee.id
			LEFT JOIN department ON employee.departmentid = employee.employeename
			LIMIT 3
        "));

	  $contract = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				employee.sex,
				department.departmentname,
				DATE_FORMAT(employee.termdate, '%d-%m-%Y') AS termdate,
				DATE_FORMAT(
					employee.joindate, '%d-%m-%Y'
				) AS joindate,
				employee.image,
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) AS days
			FROM
				employee
			LEFT JOIN department ON employee.departmentid = department.id
			WHERE
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) IS NOT NULL
			ORDER BY
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) ASC
			LIMIT 4
        "));

	  $news = \DB::select(\DB::raw("
            SELECT
				news.id,
				news.title,
				news.content,
				DATE_FORMAT(news.date, '%d-%m-%Y') AS date,
				news.time,
				news.attachment,
				news.`status`
			FROM
				news
			LIMIT 4
        "));

	  $userlog = \DB::select(\DB::raw("
	  	SELECT
			`user`.username,
			COUNT(userlog.id) AS id
		FROM
			`user`
		LEFT JOIN userlog ON `user`.username = userlog.username
		GROUP BY
			`user`.username
	  	"));


	  $late = \DB::select(\DB::raw("
	  	SELECT
			employee.employeename,
			department.departmentname,
			absence.actualin
		FROM
			absence
		INNER JOIN employee ON absence.employeeid = employee.id
		INNER JOIN department ON employee.departmentid = department.id
		WHERE
			absence.datein = '2017-10-04'
		AND absence.actualin IS NOT NULL
	  	"));

	  $sql = 'SELECT
				position.positionname,
				COUNT(employee.id) AS id
			FROM
				employee
			INNER JOIN position ON employee.positionid = position.id
			GROUP BY
				position.positionname';
     $employeebyposition = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

     $sql_popup = 'SELECT
				popup.id,
				popup.popup_name,
				popup.description,
				popup.date_popup, 
				DATE_FORMAT(NOW(), "%Y-%m-%d") AS date_now 
			FROM
				popup';
	 $popup = DB::table(DB::raw("($sql_popup) as rs_sql"))->first(); 
 
    	return view ('editor.index', compact('birthday', 'leave', 'new_hire', 'contract', 'news', 'userlog', 'late', 'employeebyposition', 'popup'));
    }
}
