<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee; 
use App\Model\Position;
use App\Model\Department;
use App\Model\City;
use App\Model\Sex;
use App\Model\Taxstatus;
use App\Model\Golongan;
use App\Model\Payrolltype;
use App\Model\Religion; 
use App\Model\Educationlevel;
use App\Model\Educationmajor;
use App\Model\Location;
use App\Model\Shiftgroup;
use Validator;
use Response;
use App\Post;
use View;
use Intervention\Image\Facades\Image;

class EmployeeController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'employeename' => 'required|min:2'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $employees = Employee::all();
      return view ('editor.employee.index', compact('employees'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 
        $userid = Auth::id();
        $sql = 'SELECT
                  employee.id,
                  employee.nik,
                  employee.identityno,
                  employee.employeename,
                  employee.nickname,
                  employee.placebirth,
                  employee.datebirth,
                  employee.address,
                  employee.positionid,
                  position.positionname,
                  employee.departmentid,
                  employee.image,
                  employee.`status`,
                  employee.thrtype,
                  department.departmentname,
                  city.cityname,
                  employee.npwp,
                  employee.taxstatus,
                  employee.educationlevelid,
                  educationlevel.educationlevelname,
                  DATE_FORMAT(
                    employee.joindate,
                    "%d-%m-%Y"
                  ) AS joindate,
                  DATE_FORMAT(
                    employee.termdate,
                    "%d-%m-%Y"
                  ) AS termdate,
                  payrolltype.payrolltypename,
                  employee.bankaccount,
                  employee.bankname,
                  employee.bankbranch,
                  employee.bankan,
                  employee.jamsostekmember,
                  employee.gol,
                  employee.sex,
                  employee.basic,
                  employee.mealtransall,
                  employee.overtimeall,
                  employee.transportall,
                  employee.insentive,
                  datediff(
                    employee.termdate,
                    curdate()
                  ) AS date_status
                FROM
                  employee
                LEFT JOIN department ON employee.departmentid = department.id
                LEFT JOIN position ON employee.positionid = position.id
                LEFT JOIN city ON employee.cityid = city.id
                LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
                LEFT JOIN payrolltype ON employee.paytypeid = payrolltype.id
                INNER JOIN userposition ON employee.positionid = userposition.positionid
                WHERE
                  employee.deleted_at IS NULL AND userposition.userid = '.$userid.'';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="employee/'.$itemdata->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->employeename."'".')"> Delete</a>';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('image', function ($itemdata) {
            if ($itemdata->image == null) {
              return '<a class="fancybox" rel="group" href="../uploads/employee/placeholder.jpg"><img src="../uploads/employee/thumbnail/placeholder.jpg" class="img-thumbnail img-responsive" /></a>';
            }else{
             return '<a class="fancybox" rel="group" href="../uploads/employee/'.$itemdata->image.'"><img src="../uploads/employee/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
           };
         })

        ->addColumn('mstatus', function ($itemdata) {
            if ($itemdata->status == 1) {
              return '<span class="label label-danger"> Not Active </span>';
            }elseif($itemdata->status == 0){
              return '<span class="label label-success"> Active </span>';
            }else{
              return '<span class="label label-warning"> Off </span>';
           };
          })

         ->addColumn('gendericon', function ($itemdata) {
          if ($itemdata->sex == 1) {
            return '<span class="label label-primary" style="background-color:#428bca !important"> <i class="fa fa-male"></i> M</span>';
          }else{
           return '<span class="label label-primary" style="background-color:#ef9292 !important"><i class="fa fa-female"></i> F</span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datalookup(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                employee.id,
                employee.nik,
                employee.identityno,
                employee.employeename,
                employee.nickname,
                employee.placebirth,
                employee.datebirth,
                employee.address,
                employee.positionid,
                position.positionname,
                employee.departmentid,
                employee.image,
                employee.`status`,
                department.departmentname,
                city.cityname,
                employee.npwp,
                employee.taxstatus,
                employee.educationlevelid,
                educationlevel.educationlevelname,
                DATE_FORMAT(employee.joindate, "%d-%m-%Y") AS joindate,
                DATE_FORMAT(employee.termdate, "%d-%m-%Y") AS termdate,
                payrolltype.payrolltypename,
                employee.bankaccount,
                employee.bankname,
                employee.bankbranch,
                employee.bankan,
                employee.jamsostekmember,
                employee.gol,
                employee.sex, 
                employee.basic,
                employee.mealtransall,
                employee.overtimeall,
                employee.transportall,
                employee.insentive
                FROM
                employee
                LEFT JOIN department ON employee.departmentid = department.id
                LEFT JOIN position ON employee.positionid = position.id
                LEFT JOIN city ON employee.cityid = city.id
                LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
                LEFT JOIN payrolltype ON employee.paytypeid = payrolltype.id
                WHERE
                employee.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="javascript:void(0)" id="btnstore" onclick="addValue(this, '.$itemdata->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-hand-up"></i> Choose</a>';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('image', function ($itemdata) {
          if ($itemdata->image == null) {
            return '<a class="fancybox" rel="group" href="uploads/inventory/placeholder.png"><img src="../uploads/inventory/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a>';
          }else{
           return '<a class="fancybox" rel="group" href="../uploads/inventory/'.$itemdata->image.'"><img src="../uploads/inventory/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
         };
       })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Active </span>';
          }else{
           return '<span class="label label-danger"> Not Active </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 

      $sql_position = 'SELECT
                          position.id,
                          position.positionname,
                          userposition.userid
                        FROM
                          position
                        INNER JOIN userposition ON position.id = userposition.positionid
                        WHERE
                          userposition.userid = '.Auth::id().'';

      $department_list = Department::all()->pluck('departmentname', 'id');
      $position_list = DB::table(DB::raw("($sql_position) as rs_sql"))->get()->pluck('positionname', 'id'); 
      $city_list = City::all()->pluck('cityname', 'id');
      $sex_list = Sex::all()->pluck('sexname', 'id');
      $taxstatus_list = Taxstatus::all()->pluck('taxstatus', 'id');
      $golongan_list = Golongan::all()->pluck('golonganname', 'id');
      $payrolltype_list = Payrolltype::all()->pluck('payrolltypename', 'id');
      $religion_list = Religion::all()->pluck('religionname', 'id');
      $city_list = City::all()->pluck('cityname', 'id');
      $educationlevel_list = Educationlevel::all()->pluck('educationlevelname', 'id');
      $educationmajor_list = Educationmajor::all()->pluck('educationmajorname', 'id');
      $location_list = Location::all()->pluck('locationname', 'id');
      $employeeshift_list = array('1' => 'Yes', '0' => 'No');
      $shiftgroup_list = Location::all()->pluck('shiftgroupname', 'id'); 

      return view ('editor.employee.form', compact('department_list', 'position_list', 'city_list', 'sex_list', 'taxstatus_list', 'golongan_list','payrolltype_list','religion_list','city_list', 'educationlevel_list', 'educationmajor_list', 'location_list','shiftgroup_list', 'employeeshift_list'));
    }


    public function store(Request $request)
    { 

      $employee = new Employee; 
      $employee->identityno = $request->input('identityno');
      $employee->nik = $request->input('nik');
      $employee->employeename = $request->input('employeename');
      $employee->nickname = $request->input('nickname'); 
      $employee->datebirth = $request->input('datebirth'); 
      $employee->joindate = $request->input('joindate'); 
      $employee->termdate = $request->input('termdate'); 
      $employee->insuranceno = $request->input('insuranceno'); 
      $employee->npwp = $request->input('npwp'); 
      $employee->address = $request->input('address'); 
      $employee->hp = $request->input('hp'); 
      $employee->telp1 = $request->input('telp1'); 
      $employee->telp2 = $request->input('telp2'); 
      $employee->bankan = $request->input('bankan'); 
      $employee->bankaccount = $request->input('bankaccount'); 
      $employee->bankname = $request->input('bankname'); 
      $employee->bankbranch = $request->input('bankbranch'); 
      $employee->cityid = $request->input('cityid'); 
      $employee->sex = $request->input('sex'); 
      $employee->taxstatus = $request->input('taxstatus'); 
      $employee->departmentid = $request->input('departmentid'); 
      $employee->positionid = $request->input('positionid'); 
      $employee->gol = $request->input('golongan'); 
      $employee->paytypeid = $request->input('paytypeid'); 
      $employee->religionid = $request->input('religionid'); 
      $employee->placebirth = $request->input('placebirth'); 
      $employee->educationlevelid = $request->input('educationlevelid'); 
      $employee->locationid = $request->input('locationid'); 
      $employee->status = $request->input('status');  
      $employee->thrtype = $request->input('thrtype'); 
      $employee->employeeshiftid = $request->input('employeeshiftid');  
      $employee->shiftgroupid = $request->input('shiftgroupid'); 

      //basic
      $employee->basic = str_replace(",","",$request->input('basic'));
      $employee->mealtransall = str_replace(",","",$request->input('mealtransall'));
      $employee->transportall = str_replace(",","",$request->input('transportall'));
      $employee->overtimeall = str_replace(",","",$request->input('overtimeall'));
      $employee->insentive = str_replace(",","",$request->input('insentive'));

      $employee->created_by = Auth::id();
      $employee->save();

      if($request->image)
      {
        $employee = Employee::FindOrFail($employee->id);

        $original_directory = "uploads/employee/";

        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          }

      //$file_extension = $request->image->getClientOriginalExtension();
          $employee->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
          $request->image->move($original_directory, $employee->image);

          $thumbnail_directory = $original_directory."thumbnail/";
          if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
           }
           $thumbnail = Image::make($original_directory.$employee->image);
           $thumbnail->fit(10,10)->save($thumbnail_directory.$employee->image);

           $employee->save(); 
         }

         return redirect('editor/employee'); 

       }

       public function edit($id)
       { 

        $sql = 'SELECT
        employee.id,
        employee.nik,
        employee.identityno,
        employee.employeename,
        employee.nickname,
        employee.placebirth,
        employee.datebirth,
        employee.address,
        employee.positionid,
        employee.departmentid,
        employee.employeeshiftid,
        employee.shiftgroupid,
        employee.image,
        employee.`status`,
        employee.npwp,
        employee.paytypeid,
        employee.taxstatus,
        employee.educationlevelid,
        employee.joindate AS joindate,
        employee.termdate AS termdate,
        employee.bankaccount,
        employee.bankname,
        employee.bankbranch,
        employee.bankan,
        employee.jamsostekmember,
        employee.gol,
        employee.sex, 
        employee.thrtype,
        FORMAT(employee.basic, 0) AS basic,
        FORMAT(employee.mealtransall, 0) AS mealtransall,
        FORMAT(employee.overtimeall, 0) AS overtimeall,
        FORMAT(employee.transportall, 0) AS transportall,
        FORMAT(employee.insentive, 0) AS insentive
        FROM
        employee 
        WHERE
        employee.deleted_at IS NULL AND employee.id = '.$id.'';

        $employee = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

        
        $sql_position = 'SELECT
                            position.id,
                            position.positionname,
                            userposition.userid
                          FROM
                            position
                          INNER JOIN userposition ON position.id = userposition.positionid
                          WHERE
                            userposition.userid = '.Auth::id().'';

        $department_list = Department::all()->pluck('departmentname', 'id');
        $position_list = DB::table(DB::raw("($sql_position) as rs_sql"))->get()->pluck('positionname', 'id'); 
        $city_list = City::all()->pluck('cityname', 'id');
        $sex_list = Sex::all()->pluck('sexname', 'id');
        $taxstatus_list = Taxstatus::all()->pluck('taxstatus', 'id');
        $golongan_list = Golongan::all()->pluck('golonganname', 'id');
        $payrolltype_list = Payrolltype::all()->pluck('payrolltypename', 'id');
        $religion_list = Religion::all()->pluck('religionname', 'id');
        $city_list = City::all()->pluck('cityname', 'id');
        $educationlevel_list = Educationlevel::all()->pluck('educationlevelname', 'id');
        $educationmajor_list = Educationmajor::all()->pluck('educationmajorname', 'id');
        $location_list = Location::all()->pluck('locationname', 'id');
        $shiftgroup_list = Shiftgroup::all()->pluck('shiftgroupname', 'id'); 
        $employeeshift_list = array('1' => 'Yes', '0' => 'No');

        return view ('editor.employee.form', compact('employee', 'department_list', 'position_list', 'city_list', 'sex_list', 'taxstatus_list', 'golongan_list','payrolltype_list','religion_list','city_list', 'educationlevel_list', 'educationmajor_list', 'location_list', 'shiftgroup_list', 'employeeshift_list'));
      }

      public function update($id, Request $request)
      { 

        $employee = Employee::Find($id);
        $employee->identityno = $request->input('identityno');
        $employee->nik = $request->input('nik');
        $employee->employeename = $request->input('employeename');
        $employee->nickname = $request->input('nickname'); 
        $employee->datebirth = $request->input('datebirth'); 
        $employee->joindate = $request->input('joindate'); 
        $employee->termdate = $request->input('termdate'); 
        $employee->insuranceno = $request->input('insuranceno'); 
        $employee->npwp = $request->input('npwp'); 
        $employee->address = $request->input('address'); 
        $employee->hp = $request->input('hp'); 
        $employee->telp1 = $request->input('telp1'); 
        $employee->telp2 = $request->input('telp2'); 
        $employee->bankan = $request->input('bankan'); 
        $employee->bankaccount = $request->input('bankaccount'); 
        $employee->bankname = $request->input('bankname'); 
        $employee->bankbranch = $request->input('bankbranch'); 
        $employee->cityid = $request->input('cityid'); 
        $employee->sex = $request->input('sex'); 
        $employee->taxstatus = $request->input('taxstatus'); 
        $employee->departmentid = $request->input('departmentid'); 
        $employee->positionid = $request->input('positionid'); 
        $employee->gol = $request->input('golongan'); 
        $employee->paytypeid = $request->input('paytypeid'); 
        $employee->religionid = $request->input('religionid'); 
        $employee->placebirth = $request->input('placebirth'); 
        $employee->educationlevelid = $request->input('educationlevelid'); 
        $employee->locationid = $request->input('locationid');  
        $employee->status = $request->input('status');  
        $employee->thrtype = $request->input('thrtype');  
        $employee->employeeshiftid = $request->input('employeeshiftid');  
        $employee->shiftgroupid = $request->input('shiftgroupid'); 

        //basic
        $employee->basic = str_replace(",","",$request->input('basic'));
        $employee->mealtransall = str_replace(",","",$request->input('mealtransall'));
        $employee->transportall = str_replace(",","",$request->input('transportall'));
        $employee->overtimeall = str_replace(",","",$request->input('overtimeall'));
        $employee->insentive = str_replace(",","",$request->input('insentive'));
        $employee->updated_by = Auth::id();
        $employee->save();

        if($request->image)
        {
          $employee = Employee::FindOrFail($employee->id);

          $original_directory = "uploads/employee/";

          if(!File::exists($original_directory))
            {
              File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

      // $file_extension = $request->image->getClientOriginalExtension();
            $employee->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $employee->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
              {
               File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
             }
             $thumbnail = Image::make($original_directory.$employee->image);
             $thumbnail->fit(10,10)->save($thumbnail_directory.$employee->image);

             $employee->save(); 
           } 

           return redirect('editor/employee'); 
         }  

         public function delete($id)
         {
    //dd($id);
          $post =  Employee::Find($id);
          $post->delete(); 

          return response()->json($post); 
        }

        public function deletebulk(Request $request)
        {
         $idkey = $request->idkey;   
         foreach($idkey as $key => $id)
         { 
          $post = Employee::Find($id["1"]);
          $post->delete(); 
        }

        echo json_encode(array("status" => TRUE));

      }
    }
