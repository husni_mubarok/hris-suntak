<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Http\Controllers\Controller; 
use Validator;
use Response;
use App\Post;
use View;
use Intervention\Image\Facades\Image;

class EmployeerequestController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'employeename' => 'required|min:2'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $employees = Employee::all();
      return view ('editor.employee.index', compact('employees'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  materialused.id,
                  materialused.notrans,
                  employee.nik,
                  employee.employeename,
                  "MATERIAL REQUEST" AS req_type,
                  materialused.datetrans,
                  "WAITING APPROVE" AS `status`
                FROM
                  materialused
                INNER JOIN employee ON materialused.id = employee.id';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="employee/'.$itemdata->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->employeename."'".')"> Delete</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Active </span>';
          }else{
           return '<span class="label label-danger"> Not Active </span>';
         };
          })

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    }
