<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\LoanRequest;
use App\Http\Controllers\Controller;
use App\Model\Loan; 
use App\Model\Loandet; 
use App\Model\Employee;  
use App\Model\Loantype;
use Validator;
use Response;
use App\Post;
use View;

class LoanController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'loanno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'loanname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $loans = Loan::all();
      return view ('editor.loan.index', compact('loans'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  loan.id,
                  loan.notrans,
                  loan.datetrans,
                  loan.loanapproved,
                  loan.loantypeid,
                  loantype.loantypename,
                  loan.employeeid,
                  employee.employeename,
                  loan.basic,
                  loan.basic AS basicamt,
                  loan.requestamount,
                  loan.approvedamount,
                  loan.paid,
                  loan.remain,
                  DATE_FORMAT(loan.startdeduction, "%d-%m-%Y") AS startdeduction,
                  loan.installment,
                  loan.attachment,
                  loan.remark,
                  loan.`status`
                FROM
                  loan
                LEFT JOIN loantype ON loan.loantypeid = loantype.id
                LEFT JOIN employee ON loan.employeeid = employee.id
                WHERE
                  loan.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->addColumn('details_url', function($itemdata) {
            return url('editor/loan/detaildata/' . $itemdata->id);
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="loan/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
        };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function detaildata($id)
    {
        // $loandet = Loandet::find($id)->posts(); 
        $posts = Loandet::where('transid', $id)->get();
        return Datatables::of($posts)

         ->addColumn('action', function ($posts) {
          return '<a href="#" class="btn btn-primary btn-xs" onclick="edit('."'".$posts->id."'".')"><i class="fa fa-pencil"></i></a>';
        })

        ->make(true);
    }

    public function create()
    {  
      $loantype_list = Loantype::all()->pluck('loantypename', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      return view ('editor.loan.form', compact('loantype_list', 'employee_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO loan (codetrans, notrans, datetrans)
                    SELECT 'LOAN',
                    IFNULL(CONCAT('LOAN','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(loan.notrans),3))+1001,3)), CONCAT('LOAN','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    loan
                    WHERE codetrans='LOAN'");

       $lastInsertedID = DB::table('loan')->max('id');  

       // dd($lastInsertedID);

       $loan = Loan::where('id', $lastInsertedID)->first(); 
       $loan->employeeid = $request->input('employeeid');
       $loan->loantypeid = $request->input('loantypeid'); 
       $loan->basic = str_replace(",","",$request->input('basic')); 
       $loan->requestamount = str_replace(",","",$request->input('requestamount')); 
       $loan->approvedamount = str_replace(",","",$request->input('approvedamount'));  
       $loan->startdeduction = $request->input('startdeduction');  
       $loan->installment = $request->input('installment');  
       // $loan->dateinstallment = $request->input('dateinstallment');  
       $loan->created_by = Auth::id();
       $loan->save();

       if($request->attachment)
       {
        $loan = Loan::FindOrFail($loan->id);
        $original_directory = "uploads/loan/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $loan->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $loan->attachment);
          $loan->save(); 
        } 

      $dateinstallment = $request->input('startdeduction');
      $approvedamount = str_replace(",","",$request->input('approvedamount')) / $request->input('installment');

      for ($i = 0; $i < $request->input('installment'); $i++)
      {
       DB::insert("INSERT INTO loandet (dateinstallment, amount, transid)
                    VALUES
                      (
                        DATE_ADD(
                          '".$dateinstallment."',
                          INTERVAL ".$i." + 1 MONTH
                        ), ".$approvedamount.", ".$lastInsertedID."
                      )"); 
      };
        return redirect('editor/loan/'.$lastInsertedID.'/edit'); 
    }

    public function edit($id)
    {
      $loantype_list = Loantype::all()->pluck('loantypename', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      // $loan = Loan::Where('id', $id)->first();   


      $sql = 'SELECT
                loan.id,
                loan.notrans,
                loan.datetrans,
                loan.loanapproved,
                loan.loantypeid,
                loan.employeeid,
                FORMAT(loan.basic, 0) AS basic,
                FORMAT(loan.basic, 0) AS basicamt,
                FORMAT(loan.requestamount, 0) AS requestamount,
                FORMAT(loan.approvedamount, 0) AS approvedamount,
                FORMAT(loan.paid, 0) AS paid,
                FORMAT(loan.remain, 0) AS remain,
                loan.startdeduction AS startdeduction,
                FORMAT(loan.installment, 0) AS installment,
                loan.attachment,
                loan.remark,
                loan.`status`
              FROM
                loan';
        $loan = DB::table(DB::raw("($sql) as rs_sql"))->Where('id', $id)->first(); 

      // dd($loan); 
      return view ('editor.loan.form', compact('loan', 'loantype_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
       $loan = Loan::Find($id);
       $loan->employeeid = $request->input('employeeid');
       $loan->loantypeid = $request->input('loantypeid'); 
       $loan->basic = str_replace(",","",$request->input('basic'));  
       $loan->requestamount = str_replace(",","",$request->input('requestamount')); 
       $loan->approvedamount = str_replace(",","",$request->input('approvedamount'));  
       $loan->startdeduction = $request->input('startdeduction');  
       $loan->installment = $request->input('installment');  
       $loan->status = 0;  
       // $loan->dateinstallment = $request->input('dateinstallment');  
       $loan->created_by = Auth::id();
       $loan->save();

       if($request->attachment)
       {
        $loan = Loan::FindOrFail($loan->id);
        $original_directory = "uploads/loan/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $loan->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $loan->attachment);
          $loan->save(); 
        } 

      $post =  Loandet::Where('transid', $id);
      $post->delete(); 

      $dateinstallment = $request->input('startdeduction');
      $approvedamount = str_replace(",","",$request->input('approvedamount')) / $request->input('installment');

      for ($i = 0; $i < $request->input('installment'); $i++)
      {
       DB::insert("INSERT INTO loandet (dateinstallment, amount, transid)
                    VALUES
                      (
                        DATE_ADD(
                          '".$dateinstallment."',
                          INTERVAL ".$i." + 1 MONTH
                        ), ".$approvedamount.", ".$id."
                      )"); 
      };
        return redirect('editor/loan/'.$id.'/edit'); 
      }

      public function editinstallment($id)
      {
        $sql = 'SELECT
                  loandet.id,
                  DATE_FORMAT(loandet.dateinstallment, "%d-%m-%Y") AS dateinstallment,
                  FORMAT(loandet.amount,0) AS amount
                FROM
                  loandet';
        $loandet = DB::table(DB::raw("($sql) as rs_sql"))->Where('id', $id)->first(); 
        echo json_encode($loandet); 
      }

      public function updateinstallment($id, Request $request)
      {
        // $validator = Validator::make(Input::all(), $this->rules);
        //     if ($validator->fails()) {
        //         return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        //     } else {
        $post = Loandet::Find($id); 
        $post->amount = str_replace(",","",$request->detailinstallment);
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      // }
      }

      public function cancel($id, Request $request)
      {
        $mutation = Loan::Find($id); 
        $mutation->status = 9; 
        $mutation->created_by = Auth::id();
        $mutation->save(); 
      
        return response()->json($mutation); 
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Document::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   
  
       foreach($idkey as $key => $id)
       {
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
