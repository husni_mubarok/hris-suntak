<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\MarketRequest;
use App\Http\Controllers\Controller;
use App\Repository\MarketRepository;

class MarketController extends Controller
{
    protected $MarketRepository;

    public function __construct(MarketRepository $market_repository)
    {
    	$this->MarketRepository = $market_repository;
    }

    public function index()
    {
    	$markets = $this->MarketRepository->get_all();
    	return view ('editor.market.index', compact('markets'));
    }

    public function create()
    {
    	return view ('editor.market.form');
    }

    public function store(MarketRequest $request)
    {
    	$market = $this->MarketRepository->store($request->input());
    	if($request->image)
    	{
    		$this->MarketRepository->update_image($market->id, $request->image);
    	}
    	return redirect()->action('Editor\MarketController@index');
    }

    public function edit($id)
    {
    	$market = $this->MarketRepository->get_one($id);
    	return view ('editor.market.form', compact('market'));
    }

    public function update($id, MarketRequest $request)
    {
    	$market = $this->MarketRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->MarketRepository->update_image($market->id, $request->image);
    	}
    	return redirect()->action('Editor\MarketController@index');
    }

    public function delete($id)
    {
    	$this->MarketRepository->delete($id);
    	return redirect()->action('Editor\MarketController@index');
    }
}
