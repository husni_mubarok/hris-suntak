<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ModuleRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Module;

class ModuleController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $modules = Module::paginate(15);
    	return view ('editor.module.index', compact('modules'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.module.form');
    }

    public function store(ModuleRequest $request)
    {
    	$module = new Module;
    	$module->name = $request->input('name');
    	$module->description = $request->input('description');
    	$module->save();

    	return redirect()->action('Editor\ModuleController@index');
    }

    public function edit($id)
    {
    	$module = Module::find($id);
    	return view ('editor.module.form', compact('module'));
    }

    public function update($id, Request $request)
    {
    	$module = Module::find($id);
    	$module->description = $request->input('description');
    	$module->save();
    	return redirect()->action('Editor\ModuleController@index');
    }

    public function delete($id)
    {
    	Module::find($id)->delete();
    	return redirect()->action('Editor\ModuleController@index');
    }
}
