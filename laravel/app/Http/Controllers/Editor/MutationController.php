<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MutationRequest;
use App\Http\Controllers\Controller;
use App\Model\Mutation; 
use App\Model\Employee; 
use App\Model\Location;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;

class MutationController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'mutationno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'mutationname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $mutations = Mutation::all();
      return view ('editor.mutation.index', compact('mutations'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  mutation.id,
                  mutation.notrans,
                  mutation.datetrans,
                  employee.employeename,
                  mutation.skno,
                  mutation.placementdate,
                  mutation.requestdate,
                  mutation.accepdate,
                  location.locationname AS curlocationname,
                  location_1.locationname AS nexlocationname,
                  mutation.requestby,
                  mutation.accepby,
                  mutation.attachment,
                  mutation.`status`,
                  mutation.created_by,
                  mutation.updated_by,
                  mutation.deleted_by,
                  mutation.created_at,
                  mutation.updated_at,
                  mutation.deleted_at 
                FROM
                  mutation
                  LEFT JOIN employee ON mutation.employeeid = employee.id
                  LEFT JOIN location ON mutation.currentlocationid = location.id
                  LEFT JOIN location AS location_1 ON mutation.nextlocationid = location_1.id
                WHERE
                  mutation.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/mutation/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="mutation/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
        };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }


     public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  mutation.id,
                  mutation.notrans,
                  mutation.datetrans,
                  employee.employeename,
                  mutation.skno,
                  mutation.placementdate,
                  mutation.requestdate,
                  mutation.accepdate,
                  location.locationname AS curlocationname,
                  location_1.locationname AS nexlocationname,
                  mutation.requestby
                FROM
                  mutation
                LEFT JOIN employee ON mutation.employeeid = employee.id
                LEFT JOIN location ON mutation.currentlocationid = location.id
                LEFT JOIN location AS location_1 ON mutation.nextlocationid = location_1.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND mutation.employeeid = user.employeeid
                AND
                  mutation.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 
      $department_list = Department::all()->pluck('departmentname', 'departmentcode');
      $location_list = Location::all()->pluck('locationname', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 

      return view ('editor.mutation.form', compact('department_list', 'location_list', 'employee_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO mutation (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('MUTA','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(mutation.notrans),3))+1001,3)), CONCAT('MUTA','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    mutation
                    WHERE codetrans='MUTA'");

       $lastInsertedID = DB::table('mutation')->max('id');  

       // dd($lastInsertedID);

       $mutation = Mutation::where('id', $lastInsertedID)->first(); 
       $mutation->employeeid = $request->input('employeeid');
       $mutation->datetrans = $request->input('datetrans');
       $mutation->skno = $request->input('skno');
       $mutation->placementdate = Carbon::now();
       $mutation->requestdate = Carbon::now();
       $mutation->accepdate = Carbon::now();
       $mutation->currentlocationid = $request->input('currentlocationid'); 
       $mutation->nextlocationid = $request->input('nextlocationid');  
       $mutation->requestby = $request->input('requestby'); 
       $mutation->accepby = $request->input('accepby'); 
       $mutation->knowby = $request->input('knowby'); 
       $mutation->created_by = Auth::id();
       $mutation->save();

       if($request->attachment)
       {
        $mutation = Mutation::FindOrFail($mutation->id);
        $original_directory = "uploads/mutation/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $mutation->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $mutation->attachment);
          $mutation->save(); 
        } 
        return redirect('editor/mutation'); 
     
    }

    public function edit($id)
    {
      $department_list = Department::all()->pluck('departmentname', 'departmentcode');
      $location_list = Location::all()->pluck('locationname', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $mutation = Mutation::Where('id', $id)->first();   

      // dd($mutation); 
      return view ('editor.mutation.form', compact('mutation','department_list', 'location_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      $mutation = Mutation::Find($id);
      $mutation->employeeid = $request->input('employeeid');
      $mutation->skno = $request->input('skno');
      $mutation->datetrans = $request->input('datetrans'); 
      $mutation->placementdate = Carbon::now();
      $mutation->requestdate = Carbon::now();
      $mutation->accepdate = Carbon::now();
      $mutation->currentlocationid = $request->input('currentlocationid'); 
      $mutation->nextlocationid = $request->input('nextlocationid');
      $mutation->requestby = $request->input('requestby'); 
      $mutation->accepby = $request->input('accepby'); 
      $mutation->knowby = $request->input('knowby'); 
      $mutation->status = 0; 
      $mutation->created_by = Auth::id();
      $mutation->save();

      if($request->attachment)
      {
        $mutation = Mutation::FindOrFail($mutation->id);
        $original_directory = "uploads/mutation/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $mutation->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $mutation->attachment);
          $mutation->save(); 
        } 
        return redirect('editor/mutation');  
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Mutation::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function cancel($id, Request $request)
      {
        $mutation = Mutation::Find($id); 
        $mutation->status = 9; 
        $mutation->created_by = Auth::id();
        $mutation->save(); 
      
        return response()->json($mutation); 

      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;    

       foreach($idkey as $key => $id)
       {
    // $post =  Mutation::where('id', $id["1"])->get();
        $post = Mutation::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }

    public function slip($id)
    {
        $sql = 'SELECT
                  mutation.id,
                  mutation.notrans,
                  mutation.datetrans,
                  employee.employeename,
                  mutation.skno,
                  mutation.placementdate,
                  mutation.requestdate,
                  mutation.accepdate,
                  location.locationname AS curlocationname,
                  location_1.locationname AS nexlocationname,
                  mutation.requestby,
                  mutation.accepby,
                  mutation.attachment,
                  mutation.`status`,
                  mutation.created_by,
                  mutation.updated_by,
                  mutation.deleted_by,
                  mutation.created_at,
                  mutation.updated_at,
                  mutation.deleted_at 
                FROM
                  mutation
                  LEFT JOIN employee ON mutation.employeeid = employee.id
                  LEFT JOIN location ON mutation.currentlocationid = location.id
                  LEFT JOIN location AS location_1 ON mutation.nextlocationid = location_1.id
                WHERE
                  mutation.id = '.$id.''; 
        $mutation = DB::table(DB::raw("($sql) as rs_sql"))->first();

      return view ('editor.mutation.slip', compact('mutation'));
    }
  }
