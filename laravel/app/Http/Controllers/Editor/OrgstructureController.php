<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\OrgstructureRequest;
use App\Http\Controllers\Controller;
use App\Model\Orgstructure; 
use App\Model\Employee; 
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;

class OrgstructureController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'policeno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'policename' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function edit()
    {   
      $orgstructure = Orgstructure::Where('id', 1)->first(); 
      return view ('editor.orgstructure.form', compact('orgstructure'));
    }

    public function update(Request $request)
    { 

      if($request->attachment)
      {
        $orgstructure = Orgstructure::Where('id', 1)->first();  
        $original_directory = "uploads/orgstructure/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $orgstructure->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $orgstructure->attachment);
          $orgstructure->save(); 
        } 
        return redirect('editor/orgstructure/edit');  
      } 
  }
