<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\OvertimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Overtime; 
use App\Model\Overtimedetail;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;

class OvertimeController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
  'overtimename' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $overtimes = Overtime::all();
      return view ('editor.overtime.index', compact('overtimes'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  overtimerequest.id,
                  overtimerequest.codetrans,
                  overtimerequest.notrans,
                  DATE_FORMAT(
                    overtimerequest.datetrans,
                    "%d-%m-%Y"
                  ) AS datetrans,
                  overtimerequest.departmentid,
                  overtimerequest.departmentcode,
                  DATE_FORMAT(
                    overtimerequest.datefrom,
                    "%d-%m-%Y"
                  ) AS datefrom,
                  DATE_FORMAT(
                    overtimerequest.dateto,
                    "%d-%m-%Y"
                  ) AS dateto,
                  overtimerequest.datefrom AS datefromformat, 
                  overtimerequest.timefrom,
                  overtimerequest.timeto,
                  overtimerequest.overtimein,
                  overtimerequest.overtimeout,
                  overtimerequest.periodid,
                  overtimerequest.remark,
                  overtimerequest.`status`,
                  overtimerequest.planwork,
                  overtimerequest.actualwork,
                  overtimerequest.location
                FROM
                  overtimerequest
                WHERE
                  overtimerequest.deleted_at IS NULL
                ORDER BY overtimerequest.datefrom DESC';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('notrans', 'DESC')->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="overtime/detail/'.$itemdata->id.'" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Open </span>';
          }else{
           return '<span class="label label-danger"> Cancel </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }
 
  public function detail($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 

      //dd($used_list);
      $overtime = Overtime::Find($id); 
      return view ('editor.overtime.form', compact('employee_list', 'overtime'));
    }

    public function slip($id)
    {
       $overtime = \DB::select(\DB::raw("
           SELECT
              overtimerequest.id,
              overtimerequest.codetrans,
              overtimerequest.notrans,
              overtimerequest.datetrans,
              overtimerequest.departmentid,
              overtimerequest.departmentcode,
              overtimerequest.datefrom,
              overtimerequest.dateto,
              employee.employeename,
              department.departmentname,
              overtimerequest.timefrom,
              overtimerequest.timeto,
              overtimerequest.overtimein,
              overtimerequest.overtimeout,
              overtimerequest.periodid,
              overtimerequest.remark,
              overtimerequest.planwork,
              overtimerequest.actualwork,
              overtimerequest.location
            FROM
              overtimerequest
            INNER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
            INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
            INNER JOIN department ON employee.departmentid = department.id WHERE overtimerequest.id = ".$id."
        "));


      return view ('editor.overtime.slip', compact('overtime'));
    }


    public function store(Request $request)
    { 

      $userid= Auth::id();
      $codetrans = $request->input('codetrans'); 

       DB::insert("INSERT INTO overtimerequest (codetrans, notrans, datetrans, created_by, created_at)
      SELECT '".$codetrans."',
      IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(overtimerequest.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
      FROM
      overtimerequest
      WHERE codetrans= '".$codetrans."'");

      $lastInsertedID = DB::table('overtimerequest')->max('id');
      //return redirect()->action('Editor\OvertimeController@edit', $lastInsertedID->id);
      return redirect('editor/overtime/detail/'.$lastInsertedID.''); 
    }

    public function saveheader($id, Request $request)
    { 
        $post = Overtime::Find($id); 
        $post->datefrom = $request->datefrom; 
        $post->dateto = $request->dateto; 
        $post->timefrom = $request->timefrom; 
        $post->timeto = $request->timeto; 
        $post->remark = $request->remark; 
        $post->planwork = $request->planwork; 
        $post->actualwork = $request->actualwork; 
        $post->location = $request->location; 
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      
    }

    public function update($id, Request $request)
    { 
        $post = Overtime::Find($id); 
        $post->datefrom = $request->input('datefrom'); 
        $post->dateto = $request->input('dateto'); 
        $post->timefrom = $request->input('timefrom'); 
        $post->timeto = $request->input('timeto'); 
        $post->remark = $request->input('remark'); 
        $post->planwork = $request->input('planwork'); 
        $post->actualwork = $request->input('actualwork'); 
        $post->location = $request->input('location'); 
        $post->updated_by = Auth::id();
        $post->save();

        return redirect('editor/overtime');   
      
    }


     public function savedetail($id, Request $request)
    { 
        $post = new Overtimedetail;
        $post->transid = $id;  
        $post->employeeid = $request->employeeid;   
        $post->save();

        return response()->json($post); 
      
    }

    public function deletedet($id)
    {
    //dd($id);
      $post =  Overtimedetail::Find($id);
      $post->delete(); 

      return response()->json($post); 
    }

    public function deletebulk(Request $request)
    {

     $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

     foreach($idkey as $key => $id)
     {
    // $post =  Overtime::where('id', $id["1"])->get();
      $post = Overtime::Find($id["1"]);
      $post->delete(); 
    }

    echo json_encode(array("status" => TRUE));

  }
  public function datadetail(Request $request, $id)
    {   
     
    if($request->ajax()){ 

        $sql = 'SELECT
                  overtimerequestdet.id,
                  overtimerequestdet.transid,
                  overtimerequestdet.employeeid,
                  employee.nik,
                  employee.employeename,
                  overtimerequestdet.otin,
                  overtimerequestdet.otout
                FROM
                  overtimerequestdet
                INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
                WHERE overtimerequestdet.transid = '.$id.' AND overtimerequestdet.deleted_at is null';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

       return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->employeename."'".')"><i class="fa fa-trash"></i></a>';
      })
   
      ->make(true);
    } else {
      exit("No data available");
    }
    }
}
