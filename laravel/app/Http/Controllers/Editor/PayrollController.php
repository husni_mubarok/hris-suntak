<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\User;
use Validator;
use Response;
use App\Post;
use View;

//Editor
use
    App\Editor,
    App\Editor\Field,
    App\Editor\Format,
    App\Editor\Mjoin,
    App\Editor\Options,
    App\Editor\Upload,
    App\Editor\Validate;

class PayrollController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'payrollname' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $payrolls = Payroll::all();
    // $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

    return view ('editor.payroll.index', compact('payrolls','payperiod_list','department_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`status`,
              payperiod.`month`,
              payperiod.`year`,
              payrolltype.payrolltypename
            FROM
              payperiod
            LEFT JOIN payrolltype ON payperiod.payrolltypeid = payrolltype.id
            WHERE payperiod.deleted_at IS NULL AND payperiod.status = 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="payroll/'.$itemdata->id.'/edit" title="Detail" class="btn btn-primary btn-xs btn-flat" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 


  public function absence()
  {
    return view ('editor.payroll.absence');
  }

  public function dataabsence(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                absence.id,
                absencetype.absencetypename,
                absence.holiday,
                date_format(
                    absence.datein,
                    "%d/%m/%Y"
                  ) AS datein,
                absence.dayin,
                absence.dayout,
                absence.actualin,
                absence.actualout,
                absence.permitein,
                absence.permiteout,
                absence.overtimein,
                absence.overtimeout,
                absence.overtimehour,
                absence.timeovertime,
                absence.overtimehouractual
              FROM
                absence
              LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
              INNER JOIN `user` ON absence.employeeid = `user`.employeeid
              AND absence.periodid = `user`.periodid
              WHERE user.id = '.Auth::id().'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function storeabsence(Request $request)
  { 
    
    $post = User::where('id', Auth::id())->first(); 
    $post->employeeid = $request->employeeid;
    $post->periodid = $request->periodid;
    $post->save();

    return response()->json($post); 
  }



  public function datatime(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                absence.id,
                absencetype.absencetypename,
                absence.holiday,
                absence.datein,
                absence.dayin,
                absence.dayout,
                absence.actualin,
                absence.actualout
              FROM
                absence
              LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->holiday == 1) {
          return '<span class="label label-success"> Yes </span>';
        }else{
         return '<span class="label label-danger"> No </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store($id)
  {  
    DB::insert("insert into payroll
                      (employeeid, periodid, departmentid)
                select     derivedtbl.employeeid, derivedtbl.periodid, derivedtbl.departmentid
                from         (select     employee.id AS employeeid, employee.status, user.periodid, employee.departmentid
                                       from          employee cross join
                                                              user
                                       where      employee.status =0 and user.id = 1) derivedtbl left outer join
                                      payroll on derivedtbl.employeeid = payroll.employeeid and derivedtbl.periodid = payroll.periodid
                where     (payroll.periodid is null)");

    //return response()->json($post); 
  }

  public function generate($id)
  {  

    /* Untuk meng Update Gaji, Tunjangan, Uang Makan & Tetap, Overtime & Tetap mengjadi 0 */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) AS u
                SET a.basic = 0,
                 a.mealtransall = 0,
                 a.transportall = 0,
                 a.overtimeall = 0,
                 a.insentive = 0 
                WHERE
                  a.employeeid = u.id
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");

    /* Untuk mengambil Gaji dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.basic = u.basic, a.mealtransall = u.mealtransall, a.transportall = u.transportall, a.overtimeall = u.overtimeall, a.insentive = u.insentive 
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");
   
   //get dayjob
   DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                absence.employeeid,
                absence.periodid,
                SUM(
                  CASE
                  WHEN absence.absencetypeid = 5 OR absence.absencetypeid = 3 OR absence.absencetypeid = 10 OR absence.absencetypeid = 4 THEN
                    0
                  ELSE
                    1
                  END
                ) AS dayjob, 
                SUM(
                  CASE
                  WHEN absence.holiday = 1 THEN
                    1
                  ELSE
                    0
                  END
                ) AS holiday, 
                SUM(absence.overtimehour) AS overtimehour,
                SUM(absence.overtimehouractual) AS overtimehouractual
              FROM
                absence
              GROUP BY
                absence.employeeid,
                absence.periodid) u
                SET a.dayjob = u.dayjob - u.holiday, a.overtimehour = u.overtimehour, a.overtimehouractual = u.overtimehouractual
                WHERE
                  a.employeeid = u.employeeid AND a.periodid = u.periodid 
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");


          DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                absence.employeeid,
                absence.periodid,
                COUNT(
                   absence.id
                ) AS dayjob 
              FROM
                absence
              WHERE (absence.holiday IS NULL OR absence.holiday = 0)
              AND
                (absence.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id().")
                )
              GROUP BY
                absence.employeeid,
                absence.periodid) u
                SET a.dayjobdef = u.dayjob
                WHERE
                  a.employeeid = u.employeeid 
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");

    /* Get llowance */

    // DB::update("UPDATE payroll a
    //             INNER JOIN (SELECT
    //               payroll.id,
    //               payroll.periodid,
    //               payroll.employeeid, 
    //               employee.basic / 173 * 8 * payroll.dayjob AS basic
    //             FROM
    //               payroll INNER JOIN employee ON payroll.employeeid = employee.id) u
    //             SET a.basic = u.basic
    //             WHERE
    //               a.id = u.id
    //             AND a.periodid = (
    //               SELECT
    //                 user.periodid
    //               FROM
    //                 `user`
    //               WHERE
    //                 user.id = ".Auth::id()."
    //             )");

    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  payroll.id,
                  payroll.periodid,
                  payroll.employeeid,
                  payroll.dayjob * payroll.mealtransall AS mealtransall,
                  payroll.dayjob * payroll.transportall AS transportall,
                  (payroll.overtimehour / 173) * payroll.basic AS overtimeall,
                  payroll.basic / 173 * (IFNULL(payroll.dayjobdef,0) - IFNULL(payroll.dayjob,0)) AS absence,
                  payroll.basic / 173 * 8 * payroll.dayjob AS basic,
                  payroll.basic * 0.02 AS jamsostek,
                  payroll.basic * 0.01 AS bpjs  
                FROM
                  payroll) u
                SET a.overtime = u.overtimeall
                WHERE
                  a.id = u.id
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                )");

    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  payroll.id,
                  payroll.periodid,
                  payroll.employeeid,
                  payroll.dayjob * payroll.mealtransall AS mealtransall,
                  payroll.dayjob * payroll.transportall AS transportall,
                  (payroll.overtimehour / 173) * payroll.basic AS overtimeall,
                  payroll.basic / 173 * (IFNULL(payroll.dayjobdef,0) - IFNULL(payroll.dayjob,0)) AS absence,
                  payroll.basic / 173 * 8 * payroll.dayjob AS basic,
                  employee.basic * 0.02 AS jamsostek,
                  employee.basic * 0.01 AS bpjs  
                FROM
                  payroll INNER JOIN employee ON payroll.employeeid = employee.id) u
                SET a.mealtrans = u.mealtransall, a.transport = u.transportall, a.overtime = u.overtimeall, a.jamsostek = u.jamsostek, a.bpjs = u.bpjs, a.absence = u.absence
                WHERE
                  a.id = u.id
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");

    //get aovertime amount for driver
   DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                absence.employeeid,
                absence.periodid, 
                SUM(absence.overimeamount) AS overimeamount 
              FROM
                absence WHERE absence.positionid = 69
              GROUP BY
                absence.employeeid,
                absence.periodid) u
                SET a.overtime = u.overimeamount
                WHERE
                  a.employeeid = u.employeeid 
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");

    /* Loan */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  loandet.dateinstallment,
                  loandet.amount,
                  loan.employeeid
                FROM
                  loan
                INNER JOIN loandet ON loan.id = loandet.transid
                WHERE
                  loandet.dateinstallment BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id = ".Auth::id()."
                  )
                AND (
                  SELECT
                    user.enddate
                  FROM
                    user
                  WHERE
                    user.id = ".Auth::id()."
                )) u
                SET a.totalloan = u.amount
                WHERE
                  a.employeeid = u.employeeid
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                ) AND (a.manual IS NULL OR a.manual = 0)");

    /* Get llowance */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  payroll.id,
                  payroll.periodid,
                  payroll.employeeid,
                  (IFNULL(payroll.basic,0) + IFNULL(payroll.mealtrans,0) + IFNULL(payroll.transport,0) + IFNULL(payroll.overtime,0) + IFNULL(payroll.insentive,0) + IFNULL(payroll.correction,0)) - (IFNULL(payroll.jamsostek,0) + IFNULL(payroll.bpjs,0) + IFNULL(payroll.totalloan,0) + IFNULL(payroll.absence,0) + IFNULL(payroll.pph21,0)) AS thp
                FROM
                  payroll
                WHERE 
                  payroll.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                )) u
                SET a.totalnetto = u.thp 
                WHERE
                  a.id = u.id
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = ".Auth::id()."
                )");

  }


public function edit($id)
  {
    $userid = Auth::id();

    $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`month`,
              payperiod.`year`
            FROM
              payperiod
            WHERE payperiod.id = '.$id.''; 
    $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first();
    
    $sql_detail = 'SELECT
                  payroll.id,
                  employee.nik,
                  employee.employeename,
                  department.departmentname,
                  payroll.manual,
                  payroll.employeeid,
                  payroll.periodid,
                  payroll.dayjob,
                  payroll.dayjobdef,
                  FORMAT(employee.basic,0) AS basicemployee,
                  FORMAT(payroll.basic,0) AS basic,
                  FORMAT(payroll.mealtransall,0) AS mealtransall,
                  FORMAT(payroll.mealtrans,0) AS mealtrans,
                  FORMAT(payroll.transportall,0) AS transportall,
                  FORMAT(payroll.transport,0) AS transport,
                  payroll.overtimehour,
                  payroll.overtimehouractual,
                  payroll.overtimehourholiday,
                  payroll.overtimehourholidayactual,
                  FORMAT(payroll.overtimeall,0) AS overtimeall,
                  payroll.overtime,
                  payroll.overtimeholiday,
                  FORMAT(payroll.insentive,0) AS insentive,
                  FORMAT(payroll.jamsostek,0) AS jamsostek,
                  FORMAT(payroll.absence,0) AS absence,
                  FORMAT(payroll.bpjs,0) AS bpjs,
                  FORMAT(payroll.totalloan,0) AS totalloan,
                  FORMAT(payroll.pph21,0) AS pph21,
                  FORMAT(payroll.totalbruto,0) AS totalbruto,
                  FORMAT(payroll.correction,0) AS correction,
                  payroll.totalnetto
                FROM
                  payroll
                LEFT JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN department ON employee.departmentid = department.id
                INNER JOIN userposition ON employee.positionid = userposition.positionid
                WHERE payroll.periodid = '.$id.' AND userposition.userid = '.$userid.' AND employee.status = 0 AND payroll.dayjob > 0'; 
    $payroll_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get(); 

    // dd($payroll_detail);

    return view ('editor.payroll.form', compact('payroll', 'payroll_detail'));
  }

  public function datadetail(Request $request, $id)
  {   
 
    // if($request->ajax()){ 
      $sql = 'SELECT
                  employee.employeename,
                  department.departmentname,
                  payroll.otherded,
                  payroll.othersall,
                  payroll.vehicleloan,
                  payroll.homeloan,
                  Payroll.id
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                INNER JOIN department ON payroll.departmentid = department.id
                WHERE payroll.periodid = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 
 
      ->make(true);
    // } else {
    //   exit("No data available");
    // }
  } 


  public function update($id, Request $request)
  {
     
    foreach($request->input('detail') as $key => $detail_data)
    { 

      if( isset($detail_data['manual'])){
        $manual = 1;
      }else{
        $manual = 0;
      };
 
      $payroll_detail = Payroll::Find($key); 
      $payroll_detail->manual = $manual; 
      $payroll_detail->dayjob = $detail_data['dayjob']; 
      $payroll_detail->basic = str_replace(",","",$detail_data['basic']);
      $payroll_detail->mealtransall = str_replace(",","",$detail_data['mealtransall']);
      $payroll_detail->mealtrans = str_replace(",","",$detail_data['mealtrans']);
      $payroll_detail->transportall = str_replace(",","",$detail_data['transportall']);
      $payroll_detail->transport = str_replace(",","",$detail_data['transport']);
      // $payroll_detail->overtimeall = $detail_data['overtimeall'];
      // $payroll_detail->overtime = $detail_data['overtime'];
      $payroll_detail->insentive = str_replace(",","",$detail_data['insentive']);
      $payroll_detail->jamsostek = str_replace(",","",$detail_data['jamsostek']);
      $payroll_detail->bpjs = str_replace(",","",$detail_data['bpjs']);
      $payroll_detail->totalloan = str_replace(",","",$detail_data['totalloan']);
      $payroll_detail->pph21 = str_replace(",","",$detail_data['pph21']);
      // $payroll_detail->totalnetto = str_replace(",","",$detail_data['totalnetto']);
      $payroll_detail->absence = str_replace(",","",$detail_data['absence']);
      $payroll_detail->correction = str_replace(",","",$detail_data['correction']);

      $payroll_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\PayrollController@index'); 
  }  


  public function slip($id)
    {
       

        $sql = 'SELECT
                  payroll.id,
                  payroll.periodid,
                  employee.employeename,
                  employee.nik,
                  payroll.basic,
                  FORMAT(employee.basic, 0) AS basicemployee,
                  date_format(payperiod.begindate, "%d") AS begindate,
                  date_format(
                    payperiod.enddate,
                    "%d/%m/%Y"
                  ) AS enddate,
                  payroll.dayjob,
                  payroll.jamsostek,
                  payroll.bpjs,
                  payroll.totalloan,
                  payroll.pph21all,
                  payroll.otherall,
                  payroll.overtimeall,
                  payroll.transportall,
                  payroll.mealtransall,
                  payroll.transport,
                  payroll.mealtrans,
                  payroll.totalnetto,
                  department.departmentname,
                  employee.npwp,
                  employee.bankaccount,
                  employee.bankname,
                  employee.bankbranch,
                  employee.bankan,
                  employee.bpjskesehatanno,
                  employee.bpjstkno,
                  otholiday.overtimehourholiday,
                  ot.overtimehour
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN payperiod ON payroll.periodid = payperiod.id
                INNER JOIN department ON employee.departmentid = department.id
                INNER JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    IFNULL(
                      SUM(absence.overtimehour),
                      0
                    ) AS overtimehourholiday
                  FROM
                    absence
                  WHERE
                    absence.holiday = 1
                  GROUP BY
                    absence.employeeid,
                    absence.periodid
                ) AS otholiday ON payroll.employeeid = otholiday.employeeid
                AND payroll.periodid = otholiday.periodid
                INNER JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    IFNULL(
                      SUM(absence.overtimehour),
                      0
                    ) AS overtimehour
                  FROM
                    absence
                  WHERE
                    absence.holiday = 1
                  GROUP BY
                    absence.employeeid,
                    absence.periodid
                ) AS ot ON payroll.employeeid = ot.employeeid
                AND payroll.periodid = ot.periodid WHERE payroll.id = '.$id.''; 
        $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first();



        $sqlsakit = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Sakit"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypesakit = DB::table(DB::raw("($sqlsakit) as rs_sql"))->first();


        $sqlcuti = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Cuti"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypecuti = DB::table(DB::raw("($sqlcuti) as rs_sql"))->first();


        $sqlijin = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Ijin"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypeijin = DB::table(DB::raw("($sqlijin) as rs_sql"))->first();

        $sqlalpha = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Alpha"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypealpha = DB::table(DB::raw("($sqlalpha) as rs_sql"))->first();

       // dd($payroll);

      return view ('editor.payroll.slip', compact('payroll', 'abstypesakit', 'abstypecuti', 'abstypealpha', 'abstypeijin'));
    }


    public function slipall($id)
    {
       

        $sql = 'SELECT
                  payroll.id,
                  payroll.periodid,
                  employee.employeename,
                  employee.nik,
                  payroll.basic,
                  FORMAT(employee.basic,0) AS basicemployee,
                  date_format(payperiod.begindate, "%d") AS begindate,
                  date_format(payperiod.enddate, "%d/%m/%Y") AS enddate,
                  payroll.dayjob,
                  payroll.jamsostek,
                  payroll.bpjs,
                  payroll.totalloan,
                  payroll.pph21all,
                  payroll.otherall,
                  payroll.overtimeall,
                  payroll.transportall,
                  payroll.mealtransall,
                  payroll.transport,
                  payroll.mealtrans,
                  payroll.totalnetto,
                  department.departmentname,
                  employee.npwp,
                  employee.bankaccount,
                  employee.bankname,
                  employee.bankbranch,
                  employee.bankan,
                  employee.bpjskesehatanno,
                  employee.bpjstkno
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN payperiod ON payroll.periodid = payperiod.id
                INNER JOIN department ON employee.departmentid = department.id WHERE employee.departmentid = '.$id.' AND employee.status = 0'; 
        $payrolls = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return view ('editor.payroll.slipall', compact('payrolls'));
    }
 
}
