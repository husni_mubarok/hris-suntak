<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\User;
use Validator;
use Response;
use App\Post;
use View;

//Editor
use
    App\Editor,
    App\Editor\Field,
    App\Editor\Format,
    App\Editor\Mjoin,
    App\Editor\Options,
    App\Editor\Upload,
    App\Editor\Validate;

class PayrollviewController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'basic' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $payrolls = Payroll::all();
    // $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

    return view ('editor.payrollview.index', compact('payrolls','payperiod_list','department_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`status`,
              payperiod.`month`,
              payperiod.`year`,
              payrolltype.payrolltypename
            FROM
              payperiod
            LEFT JOIN payrolltype ON payperiod.payrolltypeid = payrolltype.id
            WHERE payperiod.deleted_at IS NULL AND payperiod.status = 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="payrollview/'.$itemdata->id.'/edit" title="Detail" class="btn btn-primary btn-xs btn-flat" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 


  public function absence()
  {
    return view ('editor.payroll.absence');
  }

  public function dataabsence(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                absence.id,
                absencetype.absencetypename,
                absence.holiday,
                date_format(
                    absence.datein,
                    "%d/%m/%Y"
                  ) AS datein,
                absence.dayin,
                absence.dayout,
                absence.actualin,
                absence.actualout,
                absence.permitein,
                absence.permiteout,
                absence.overtimein,
                absence.overtimeout,
                absence.overtimehour,
                absence.timeovertime,
                absence.overtimehouractual
              FROM
                absence
              LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
              INNER JOIN `user` ON absence.employeeid = `user`.employeeid
              AND absence.periodid = `user`.periodid
              WHERE user.id = '.Auth::id().'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function storeabsence(Request $request)
  { 
    
    $post = User::where('id', Auth::id())->first(); 
    $post->employeeid = $request->employeeid;
    $post->periodid = $request->periodid;
    $post->save();

    return response()->json($post); 
  }



  public function datatime(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                absence.id,
                absencetype.absencetypename,
                absence.holiday,
                absence.datein,
                absence.dayin,
                absence.dayout,
                absence.actualin,
                absence.actualout
              FROM
                absence
              LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->holiday == 1) {
          return '<span class="label label-success"> Yes </span>';
        }else{
         return '<span class="label label-danger"> No </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store($id)
  {  
    DB::insert("insert into payroll
                      (employeeid, periodid, departmentid)
                select     derivedtbl.employeeid, derivedtbl.periodid, derivedtbl.departmentid
                from         (select     employee.id AS employeeid, employee.status, user.periodid, employee.departmentid
                                       from          employee cross join
                                                              user
                                       where      employee.status =0 and user.id = 1) derivedtbl left outer join
                                      payroll on derivedtbl.employeeid = payroll.employeeid and derivedtbl.periodid = payroll.periodid
                where     (payroll.periodid is null)");

    //return response()->json($post); 
  }

  public function edit($id)
  {
    $userid = Auth::id();

    $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`month`,
              payperiod.`year`
            FROM
              payperiod
            WHERE payperiod.id = '.$id.''; 
    $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first();
    
    $sql_detail = 'SELECT
                  payroll.id,
                  employee.nik,
                  employee.employeename,
                  department.departmentname,
                  payroll.manual,
                  payroll.employeeid,
                  payroll.periodid,
                  payroll.dayjob,
                  payroll.dayjobdef,
                  FORMAT(employee.basic,0) AS basicemployee,
                  FORMAT(payroll.basic,0) AS basic,
                  FORMAT(payroll.mealtransall,0) AS mealtransall,
                  FORMAT(payroll.mealtrans,0) AS mealtrans,
                  FORMAT(payroll.transportall,0) AS transportall,
                  FORMAT(payroll.transport,0) AS transport,
                  payroll.overtimehour,
                  payroll.overtimehouractual,
                  FORMAT(payroll.overtimeall,0) AS overtimeall,
                  payroll.overtime,
                  FORMAT(payroll.insentive,0) AS insentive,
                  FORMAT(payroll.jamsostek,0) AS jamsostek,
                  FORMAT(payroll.absence,0) AS absence,
                  FORMAT(payroll.bpjs,0) AS bpjs,
                  FORMAT(payroll.totalloan,0) AS totalloan,
                  FORMAT(payroll.pph21,0) AS pph21,
                  FORMAT(payroll.totalbruto,0) AS totalbruto,
                  FORMAT(payroll.correction,0) AS correction,
                  payroll.totalnetto
                FROM
                  payroll
                LEFT JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN department ON employee.departmentid = department.id
                INNER JOIN userposition ON employee.positionid = userposition.positionid
                WHERE payroll.periodid = '.$id.' AND userposition.userid = '.$userid.' AND employee.deleted_at IS NULL AND employee.status = 0'; 
    $payroll_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get(); 

    // dd($payroll_detail);

    return view ('editor.payrollview.form', compact('payroll', 'payroll_detail'));
  }

  public function datadetail(Request $request, $id)
  {   
      $userid = Auth::id();
    // if($request->ajax()){ 
      $sql = 'SELECT
                  payroll.id,
                  employee.nik,
                  employee.employeename,
                  department.departmentname,
                  payroll.manual,
                  payroll.employeeid,
                  payroll.periodid,
                  payroll.dayjob,
                  payroll.dayjobdef,
                  employee.basic AS basicemployee,
                  payroll.basic AS basic,
                  payroll.mealtransall AS mealtransall,
                  payroll.mealtrans AS mealtrans,
                  payroll.transportall AS transportall,
                  payroll.transport AS transport,
                  payroll.overtimehour,
                  payroll.overtimehouractual,
                  payroll.overtimehourholiday,
                  payroll.overtimehourholidayactual,
                  payroll.overtimeall AS overtimeall,
                  payroll.overtime,
                  payroll.overtimeholiday,
                  payroll.insentive AS insentive,
                  payroll.jamsostek AS jamsostek,
                  payroll.absence AS absence,
                  payroll.bpjs AS bpjs,
                  payroll.totalloan AS totalloan,
                  payroll.pph21 AS pph21,
                  payroll.totalbruto AS totalbruto,
                  payroll.correction AS correction,
                  payroll.totalnetto
                FROM
                  payroll
                LEFT JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN department ON employee.departmentid = department.id
                INNER JOIN userposition ON employee.positionid = userposition.positionid
                WHERE payroll.periodid = '.$id.' AND userposition.userid = '.$userid.' AND employee.deleted_at IS NULL AND employee.status = 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-pencil"></i> Edit</a>';
      })

      ->addColumn('slip', function ($itemdata) {
        return '<a href="#" onclick="showslip('.$itemdata->id.');"><i class="fa fa-print"></i> Print</a>';
      })

      ->addColumn('manual', function ($itemdata) {
        if($itemdata->manual == 1){
           return '<span class="label label-warning"> Yes </span>';
        }else{
           return '<span class="label label-success"> No </span>';
        };
      })
 
      ->make(true);
  } 


  public function update($id, Request $request)
  {
     
    foreach($request->input('detail') as $key => $detail_data)
    { 

       if( isset($detail_data['manual'])){
        $manual = 1;
      }else{
        $manual = 0;
      };
 
      $payroll_detail = Payroll::Find($key); 
      $payroll_detail->manual = $manual; 
      $payroll_detail->dayjob = $detail_data['dayjob']; 
      $payroll_detail->basic = str_replace(",","",$detail_data['basic']);
      $payroll_detail->mealtransall = str_replace(",","",$detail_data['mealtransall']);
      $payroll_detail->mealtrans = str_replace(",","",$detail_data['mealtrans']);
      $payroll_detail->transportall = str_replace(",","",$detail_data['transportall']);
      $payroll_detail->transport = str_replace(",","",$detail_data['transport']);
      // $payroll_detail->overtimeall = $detail_data['overtimeall'];
      // $payroll_detail->overtime = $detail_data['overtime'];
      $payroll_detail->insentive = str_replace(",","",$detail_data['insentive']);
      $payroll_detail->jamsostek = str_replace(",","",$detail_data['jamsostek']);
      $payroll_detail->bpjs = str_replace(",","",$detail_data['bpjs']);
      $payroll_detail->totalloan = str_replace(",","",$detail_data['totalloan']);
      $payroll_detail->pph21 = str_replace(",","",$detail_data['pph21']);
      // $payroll_detail->totalnetto = str_replace(",","",$detail_data['totalnetto']);
      $payroll_detail->absence = str_replace(",","",$detail_data['absence']);
      $payroll_detail->correction = str_replace(",","",$detail_data['correction']);

      $payroll_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\PayrollController@index'); 
  }  


  public function editdetail($id)
  {
    // $payroll = Payroll::Find($id);
     $sql = 'SELECT
                  payroll.id,
                  employee.nik,
                  employee.employeename,
                  department.departmentname,
                  payroll.manual,
                  payroll.employeeid,
                  payroll.periodid,
                  payroll.dayjob,
                  payroll.dayjobdef,
                  FORMAT(employee.basic,0) AS basicemployee,
                  FORMAT(payroll.basic,0) AS basic,
                  FORMAT(payroll.mealtransall,0) AS mealtransall,
                  FORMAT(payroll.mealtrans,0) AS mealtrans,
                  FORMAT(payroll.transportall,0) AS transportall,
                  FORMAT(payroll.transport,0) AS transport,
                  payroll.overtimehour,
                  payroll.overtimehouractual,
                  FORMAT(payroll.overtimeall,0) AS overtimeall,
                  FORMAT(payroll.overtime,0),
                  FORMAT(payroll.insentive,0) AS insentive,
                  FORMAT(payroll.jamsostek,0) AS jamsostek,
                  FORMAT(payroll.absence,0) AS absence,
                  FORMAT(payroll.bpjs,0) AS bpjs,
                  FORMAT(payroll.totalloan,0) AS totalloan,
                  FORMAT(payroll.pph21,0) AS pph21,
                  FORMAT(payroll.totalbruto,0) AS totalbruto,
                  FORMAT(payroll.correction,0) AS correction,
                  FORMAT(payroll.totalnetto,0) AS totalnetto
                FROM
                  payroll
                LEFT JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN department ON employee.departmentid = department.id 
                WHERE payroll.id = '.$id.'';
      $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first(); 
    echo json_encode($payroll); 
  }

  public function updatedetail($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $post = Payroll::Find($id); 
            $post->dayjob = str_replace(",", "", $request->dayjob);
            $post->basic = str_replace(",", "", $request->basic);
            $post->mealtransall = str_replace(",", "", $request->mealtransall);
            $post->manual = $request->manual;
            $post->mealtrans = str_replace(",", "", $request->mealtrans);
            $post->transportall = str_replace(",", "", $request->transportall);
            $post->overtimehour = str_replace(",", "", $request->overtimehour);
            $post->transport = str_replace(",", "", $request->transport);
            $post->insentive = str_replace(",", "", $request->insentive);
            $post->jamsostek = str_replace(",", "", $request->jamsostek);
            $post->bpjs = str_replace(",", "", $request->bpjs);
            $post->totalloan = str_replace(",", "", $request->totalloan);
            $post->pph21 = str_replace(",", "", $request->pph21);
            $post->absence = str_replace(",", "", $request->absence);
            $post->correction = str_replace(",", "", $request->correction);
            $post->updated_by = Auth::id();
            $post->save();
        return response()->json($post); 
      }
  }


  public function slip($id)
    {
       

        $sql = 'SELECT
                  payroll.id,
                  payroll.periodid,
                  employee.employeename,
                  employee.nik,
                  payroll.basic,
                  FORMAT(employee.basic, 0) AS basicemployee,
                  date_format(payperiod.begindate, "%d") AS begindate,
                  date_format(
                    payperiod.enddate,
                    "%d/%m/%Y"
                  ) AS enddate,
                  payroll.dayjob,
                  payroll.jamsostek,
                  payroll.bpjs,
                  payroll.totalloan,
                  payroll.pph21all,
                  payroll.otherall,
                  payroll.overtimeall,
                  payroll.transportall,
                  payroll.mealtransall,
                  payroll.totalnetto,
                  department.departmentname,
                  employee.npwp,
                  employee.bankaccount,
                  employee.bankname,
                  employee.bankbranch,
                  employee.bankan,
                  employee.bpjskesehatanno,
                  employee.bpjstkno,
                  otholiday.overtimehourholiday,
                  ot.overtimehour
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN payperiod ON payroll.periodid = payperiod.id
                INNER JOIN department ON employee.departmentid = department.id
                INNER JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    IFNULL(
                      SUM(absence.overtimehour),
                      0
                    ) AS overtimehourholiday
                  FROM
                    absence
                  WHERE
                    absence.holiday = 1
                  GROUP BY
                    absence.employeeid,
                    absence.periodid
                ) AS otholiday ON payroll.employeeid = otholiday.employeeid
                AND payroll.periodid = otholiday.periodid
                INNER JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    IFNULL(
                      SUM(absence.overtimehour),
                      0
                    ) AS overtimehour
                  FROM
                    absence
                  WHERE
                    absence.holiday = 1
                  GROUP BY
                    absence.employeeid,
                    absence.periodid
                ) AS ot ON payroll.employeeid = ot.employeeid
                AND payroll.periodid = ot.periodid WHERE payroll.id = '.$id.''; 
        $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first();



        $sqlsakit = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Sakit"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypesakit = DB::table(DB::raw("($sqlsakit) as rs_sql"))->first();


        $sqlcuti = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Cuti"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypecuti = DB::table(DB::raw("($sqlcuti) as rs_sql"))->first();


        $sqlijin = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Ijin"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypeijin = DB::table(DB::raw("($sqlijin) as rs_sql"))->first();

        $sqlalpha = 'SELECT
                  payroll.id,
                  abstype.absencetypename,
                  IFNULL(abstype.abstype,0) AS abstype
                FROM
                  payroll
                LEFT JOIN (
                  SELECT
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename,
                    COUNT(absence.id) AS abstype
                  FROM
                    absence
                  INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
                  GROUP BY
                    absence.employeeid,
                    absence.periodid,
                    absencetype.absencetypename
                ) AS abstype ON payroll.employeeid = abstype.employeeid
                AND abstype.absencetypename = "Alpha"
                AND payroll.periodid = abstype.periodid WHERE payroll.id = '.$id.''; 
        $abstypealpha = DB::table(DB::raw("($sqlalpha) as rs_sql"))->first();

       // dd($payroll);

      return view ('editor.payroll.slip', compact('payroll', 'abstypesakit', 'abstypecuti', 'abstypealpha', 'abstypeijin'));
    }


    public function slipall($id)
    {
       

        $sql = 'SELECT
                  payroll.id,
                  payroll.periodid,
                  employee.employeename,
                  employee.nik,
                  payroll.basic,
                  FORMAT(employee.basic,0) AS basicemployee,
                  date_format(payperiod.begindate, "%d") AS begindate,
                  date_format(payperiod.enddate, "%d/%m/%Y") AS enddate,
                  payroll.dayjob,
                  payroll.jamsostek,
                  payroll.bpjs,
                  payroll.totalloan,
                  payroll.pph21all,
                  payroll.otherall,
                  payroll.overtimeall,
                  payroll.transportall,
                  payroll.mealtransall,
                  payroll.totalnetto,
                  department.departmentname,
                  employee.npwp,
                  employee.bankaccount,
                  employee.bankname,
                  employee.bankbranch,
                  employee.bankan,
                  employee.bpjskesehatanno,
                  employee.bpjstkno
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN payperiod ON payroll.periodid = payperiod.id
                INNER JOIN department ON employee.departmentid = department.id WHERE employee.departmentid = '.$id.''; 
        $payrolls = DB::table(DB::raw("($sql) as rs_sql"))->get();


      return view ('editor.payroll.slipall', compact('payrolls'));
    }
 
}
