<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PoliceRequest;
use App\Http\Controllers\Controller;
use App\Model\Police; 
use App\Model\Employee; 
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;

class PoliceController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'policeno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'policename' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $polices = Police::all();
      return view ('editor.police.index', compact('polices'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  police.id, 
                  police.notrans,
                  police.datetrans,
                  police.policename_eng AS policename,
                  police.policename_eng,
                  police.policename_id,
                  police.about,
                  police.policeno,
                  DATE_FORMAT(
                    police.effectivedate,
                    "%d/%m/%Y"
                  ) AS effectivedate,
                  police.revision,
                  DATE_FORMAT(
                    police.revisiondate,
                    "%d/%m/%Y"
                  ) AS revisiondate,
                  police.content_eng,
                  police.content_id,
                  police.attachment,
                  police.`status`,
                  police.departmentid,
                  department.departmentname
                FROM
                  police
                LEFT JOIN department ON police.departmentid = department.id
                WHERE
                  police.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 


        ->addColumn('action', function ($itemdata) {
          return '<a href="police/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-warning"> Open </span>';
          }else if ($itemdata->status == 2){
           return '<span class="label label-danger"> Not Approve </span>';
        }else if ($itemdata->status == 1){
           return '<span class="label label-success"> Approve </span>';
         };

       })

         ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/reward/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
        })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $department_list = Department::all()->pluck('departmentname', 'id'); 

      return view ('editor.police.form', compact('department_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO police (codetrans, notrans, datetrans)
                    SELECT 'POLI',
                    IFNULL(CONCAT('POLI','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(police.notrans),3))+1001,3)), CONCAT('POLI','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    police
                    WHERE codetrans='POLI'");

       $lastInsertedID = DB::table('police')->max('id');  

       // dd($lastInsertedID);

       $police = Police::where('id', $lastInsertedID)->first(); 
       $police->policename_id = $request->input('policename_id'); 
       $police->policename_eng = $request->input('policename_eng');  
       $police->departmentid = $request->input('departmentid');  
       $police->about = $request->input('about');  
       $police->effectivedate = $request->input('effectivedate');  
       $police->policeno = $request->input('policeno');  
       $police->content_id = $request->input('content_id');  
       $police->content_eng = $request->input('content_eng');   
       $police->created_by = Auth::id();
       $police->save();

       if($request->attachment)
       {
        $police = Police::FindOrFail($police->id);
        $original_directory = "uploads/police/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $police->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $police->attachment);
          $police->save(); 
        } 
        return redirect('editor/police'); 
     
    }

    public function edit($id)
    {
      $department_list = Department::all()->pluck('departmentname', 'id');  
      $police = Police::Where('id', $id)->first();   

      return view ('editor.police.form', compact('police','department_list'));
    }

    public function update($id, Request $request)
    {
      
       $police = Police::FindOrFail($id); 
       $police->policename_id = $request->input('policename_id'); 
       $police->policename_eng = $request->input('policename_eng');  
       $police->departmentid = $request->input('departmentid');  
       $police->about = $request->input('about');  
       $police->effectivedate = $request->input('effectivedate');  
       $police->policeno = $request->input('policeno');  
       $police->content_id = $request->input('content_id');  
       $police->content_eng = $request->input('content_eng');   
       $police->created_by = Auth::id();
       $police->save();

      if($request->attachment)
      {
        $police = Police::FindOrFail($police->id);
        $original_directory = "uploads/police/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $police->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $police->attachment);
          $police->save(); 
        } 
        return redirect('editor/police');  
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Police::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
