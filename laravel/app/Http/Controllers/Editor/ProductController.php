<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductRepository;

class ProductController extends Controller
{
    protected $ProductRepository;

    public function __construct(ProductRepository $product_repository)
    {
    	$this->ProductRepository = $product_repository;
    }

    public function index()
    {
    	$products = $this->ProductRepository->get_all();
    	return view ('editor.product.index', compact('products'));
    }

    public function create()
    {
    	return view ('editor.product.form');
    }

    public function store(ProductRequest $request)
    {
    	$product = $this->ProductRepository->store($request->input());
    	if($request->image)
    	{
    		$this->ProductRepository->update_image($product->id, $request->image);
    	}
    	return redirect()->action('Editor\ProductController@index');
    }

    public function edit($id)
    {
    	$product = $this->ProductRepository->get_one($id);
    	return view ('editor.product.form', compact('product'));
    }

    public function update($id, ProductRequest $request)
    {
    	$product = $this->ProductRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->ProductRepository->update_image($product->id, $request->image);
    	}
    	return redirect()->action('Editor\ProductController@index');
    }

    public function delete($id)
    {
    	$this->ProductRepository->delete($id);
    	return redirect()->action('Editor\ProductController@index');
    }
}
