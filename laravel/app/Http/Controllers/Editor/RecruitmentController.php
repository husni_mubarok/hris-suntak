<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Model\Recruitment;
use App\Model\Recruitmentdet; 
use App\Model\Position;
use Validator;
use Response;
use App\Post;
use View;

class RecruitmentController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'datetrans' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $recruitments = Recruitmentdet::all();
    return view ('editor.recruitment.index', compact('recruitments'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                recruitment.id,
                recruitment.codetrans,
                recruitment.notrans,
                recruitment.datetrans,
                recruitment.recfrom,
                recruitment.recto,
                position.positionname,
                recruitment.requirements,
                recruitment.jobdesc,
                recruitment.remark,
                recruitment.`status`,
                recruitment.deleted_at
              FROM
                recruitment
              LEFT JOIN position ON recruitment.positionid = position.id
        WHERE
        recruitment.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="recruitment/detail/'.$itemdata->id.'" title="'.$itemdata->notrans.'"> '.$itemdata->notrans.'</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }
 
public function detail($id)
{  
  $recruitment = Recruitment::where('id', $id)->first(); 
  // dd($recruitment);
  $position_list = Position::all()->pluck('positionname', 'id'); 
  return view ('editor.recruitment.form', compact('recruitment', 'position_list'));
}

public function store(Request $request)
{ 

  $userid= Auth::id();
  $codetrans = $request->input('codetrans'); 

  DB::insert("INSERT INTO recruitment (notrans, datetrans, created_by, created_at)
    SELECT 
    IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(recruitment.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
    FROM
    recruitment");

  $lastInsertedID = DB::table('recruitment')->max('id'); 
  return redirect('recruitment/detail/'.$lastInsertedID.''); 
}

public function saveheader($id, Request $request)
{ 
  $post = PurchaseRequest::find($id); 
  $post->warehouse_id = $request->warehouse_id;  
  $post->remark = $request->remark; 
  $post->company_id = Auth::user()->company_id; 
  $post->updated_by = Auth::id();
  $post->save();

  if($request->attachment)
  {
    $recruitment = PurchaseRequest::FindOrFail($recruitment->id);

    $original_directory = "uploads/recruitment/";

    if(!File::exists($original_directory))
      {
        File::makeDirectory($original_directory, $mode = 0777, true, true);
      } 
      $recruitment->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
      $request->attachment->move($original_directory, $recruitment->attachment); 
      if(!File::exists($thumbnail_directory))
        {
         File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
       } 
       $recruitment->save(); 
     } 
     return response()->json($post);  
   }

   public function update($id, Request $request)
   {  
    $recruitment = PurchaseRequest::Find($id);
    $recruitment->datetrans = $request->input('datetrans'); 
    $recruitment->warehouse_id = $request->input('warehouse_id');  
    $recruitment->remark = $request->input('remark');  
    $recruitment->company_id = Auth::user()->company_id; 
    $recruitment->updated_by = Auth::id(); 
    $recruitment->save();

    if($request->attachment)
    {
      $recruitment = PurchaseRequest::FindOrFail($recruitment->id);

      $original_directory = "uploads/recruitment/";

      if(!File::exists($original_directory))
        {
          File::makeDirectory($original_directory, $mode = 0777, true, true);
        } 

        $recruitment->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
        $request->attachment->move($original_directory, $recruitment->attachment);


        $recruitment->save(); 
      } 

      return redirect('recruitment'); 
    }  

    public function cancel($id, Request $request)
    { 
      $post = PurchaseRequest::find($id);  
      if($post->status > 0)
      {
        $post->status = 0;
      }else
      { 
        $post->status = 9;  
      }; 
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post); 

    }

    public function close($id, Request $request)
    { 
      $post = PurchaseRequest::find($id);  
      $post->status = 1;   
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post);  
    }

    public function savedetail($id, Request $request)
    { 
      $post = new PurchaseRequestDetail;
      $post->pr_id = $id;  
      $post->item_id = $request->item_id; 
      $post->unit = $request->unit;  
      $post->quantity = $request->quantity;  
      $post->save();

      return response()->json($post);  
    }

    public function updatedetail($id, Request $request)
    { 
      $post = PurchaseRequestDetail::where('id', $request->pr_d_id)->first();  
      $post->item_id = $request->item_id; 
      $post->unit = $request->unit;  
      $post->quantity = $request->quantity;  
      $post->save();

      return response()->json($post); 

    }

    public function deletedet($id)
    { 
      //dd($id);
      $post =  PurchaseRequestDetail::where('id', $id)->delete(); 

      return response()->json($post); 
    }


    public function datadetail(Request $request, $id)
    {   

      if($request->ajax()){ 
        $sql = 'SELECT 
        recruitment_detail.id AS pr_d_id,
        recruitment_detail.pr_id,
        recruitment_detail.item_id,
        mst_item.item_name,
        mst_item.item_description,
        recruitment_detail.unit, 
        recruitment_detail.quantity 
        FROM
        recruitment_detail
        LEFT JOIN mst_item ON recruitment_detail.item_id = mst_item.item_id
        WHERE recruitment_detail.deleted_at IS NULL AND recruitment_detail.pr_id = '.$id.'';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-primary btn-xs" onclick="update_id(this, '."'".$itemdata->pr_d_id."', '".$itemdata->item_id."'".')"><i class="fa fa-pencil"></i></a> <a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->pr_d_id."', '".$itemdata->item_name."'".')"><i class="fa fa-trash"></i></a>';
        })

        ->make(true);
      } else {
        exit("No data available");
      }
    }
  }