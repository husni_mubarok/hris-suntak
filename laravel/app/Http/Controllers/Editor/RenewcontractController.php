<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\RenewcontractRequest;
use App\Http\Controllers\Controller;
use App\Model\Renewcontract; 
use App\Model\Employee; 
use App\Model\Location;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;

class RenewcontractController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'renewcontractno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'renewcontractname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $renewcontracts = Renewcontract::all();
      return view ('editor.renewcontract.index', compact('renewcontracts'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  renewcontract.id,
                  renewcontract.notrans,
                  renewcontract.datetrans,
                  employee.employeename,
                  renewcontract.skno,
                  renewcontract.contractdate,
                  renewcontract.requestdate,
                  renewcontract.accepdate, 
                  renewcontract.requestby,
                  renewcontract.accepby,
                  renewcontract.attachment,
                  renewcontract.`status`,
                  renewcontract.created_by,
                  renewcontract.updated_by,
                  renewcontract.deleted_by,
                  renewcontract.created_at,
                  renewcontract.updated_at,
                  renewcontract.deleted_at 
                FROM
                  renewcontract
                  LEFT JOIN employee ON renewcontract.employeeid = employee.id
                WHERE
                  renewcontract.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/renewcontract/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="renewcontract/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
        };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }


     public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  renewcontract.id,
                  renewcontract.notrans,
                  renewcontract.datetrans,
                  employee.employeename,
                  renewcontract.skno,
                  renewcontract.contractdate,
                  renewcontract.requestdate,
                  renewcontract.accepdate, 
                  renewcontract.requestby 
                FROM
                  renewcontract
                LEFT JOIN employee ON renewcontract.employeeid = employee.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND renewcontract.employeeid = user.employeeid
                AND
                  renewcontract.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 
      $department_list = Department::all()->pluck('departmentname', 'departmentcode');
      $location_list = Location::all()->pluck('locationname', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 

      return view ('editor.renewcontract.form', compact('department_list', 'location_list', 'employee_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO renewcontract (codetrans, notrans, datetrans)
                    SELECT 'RCNT',
                    IFNULL(CONCAT('RCNT','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(renewcontract.notrans),3))+1001,3)), CONCAT('RCNT','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    renewcontract
                    WHERE codetrans='RCNT'");

       $lastInsertedID = DB::table('renewcontract')->max('id');  

       // dd($lastInsertedID);

       $renewcontract = Renewcontract::where('id', $lastInsertedID)->first(); 
       $renewcontract->employeeid = $request->input('employeeid');
       $renewcontract->datetrans = $request->input('datetrans');
       $renewcontract->contractdate = $request->input('contractdate');
       $renewcontract->skno = $request->input('skno');
       $renewcontract->requestdate = Carbon::now();
       $renewcontract->accepdate = Carbon::now(); 
       $renewcontract->requestby = $request->input('requestby'); 
       $renewcontract->accepby = $request->input('accepby'); 
       $renewcontract->knowby = $request->input('knowby'); 
       $renewcontract->created_by = Auth::id();
       $renewcontract->save();

       if($request->attachment)
       {
        $renewcontract = Renewcontract::FindOrFail($renewcontract->id);
        $original_directory = "uploads/renewcontract/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $renewcontract->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $renewcontract->attachment);
          $renewcontract->save(); 
        } 

         DB::update("UPDATE employee SET termdate = ".$request->input('contractdate')." WHERE id= ".$request->input('employeeid')."");


        return redirect('editor/renewcontract'); 
     
    }

    public function edit($id)
    {
      $department_list = Department::all()->pluck('departmentname', 'departmentcode');
      $location_list = Location::all()->pluck('locationname', 'id'); 
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $renewcontract = Renewcontract::Where('id', $id)->first();   

      // dd($renewcontract); 
      return view ('editor.renewcontract.form', compact('renewcontract','department_list', 'location_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      $renewcontract = Renewcontract::Find($id);
      $renewcontract->employeeid = $request->input('employeeid');
      $renewcontract->skno = $request->input('skno');
      $renewcontract->datetrans = $request->input('datetrans'); 
      $renewcontract->contractdate = $request->input('contractdate'); 
      $renewcontract->requestdate = Carbon::now();
      $renewcontract->accepdate = Carbon::now(); 
      $renewcontract->requestby = $request->input('requestby'); 
      $renewcontract->accepby = $request->input('accepby'); 
      $renewcontract->knowby = $request->input('knowby'); 
      $renewcontract->status = 0; 
      $renewcontract->created_by = Auth::id();
      $renewcontract->save();

       DB::update("UPDATE employee SET termdate = '".$request->input('contractdate')."' WHERE id=".$request->input('employeeid')."");

      if($request->attachment)
      {
        $renewcontract = Renewcontract::FindOrFail($renewcontract->id);
        $original_directory = "uploads/renewcontract/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $renewcontract->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $renewcontract->attachment);
          $renewcontract->save(); 
        } 

       

        return redirect('editor/renewcontract');  
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Renewcontract::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function cancel($id, Request $request)
      {
        $renewcontract = Renewcontract::Find($id); 
        $renewcontract->status = 9; 
        $renewcontract->created_by = Auth::id();
        $renewcontract->save(); 
      
        return response()->json($renewcontract); 

      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;    

       foreach($idkey as $key => $id)
       {
    // $post =  Renewcontract::where('id', $id["1"])->get();
        $post = Renewcontract::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }

    public function slip($id)
    {
        $sql = 'SELECT
                  renewcontract.id,
                  renewcontract.notrans,
                  renewcontract.datetrans,
                  employee.employeename,
                  renewcontract.skno,
                  renewcontract.placementdate,
                  renewcontract.requestdate,
                  renewcontract.accepdate, 
                  renewcontract.requestby,
                  renewcontract.accepby,
                  renewcontract.attachment,
                  renewcontract.`status`,
                  renewcontract.created_by,
                  renewcontract.updated_by,
                  renewcontract.deleted_by,
                  renewcontract.created_at,
                  renewcontract.updated_at,
                  renewcontract.deleted_at 
                FROM
                  renewcontract
                  LEFT JOIN employee ON renewcontract.employeeid = employee.id 
                WHERE
                  renewcontract.id = '.$id.''; 
        $renewcontract = DB::table(DB::raw("($sql) as rs_sql"))->first();

      return view ('editor.renewcontract.slip', compact('renewcontract'));
    }
  }
