<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportincityallowanceController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportincityallowance.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                CONCAT(
                  mealtrandet.dic1,
                  " = ",
                  mealtrandet.dic1 * mealtrandet.ratemealincity
                ) AS dk1,
                CONCAT(
                  mealtrandet.dic2,
                  " = ",
                  mealtrandet.dic2 * mealtrandet.ratemealincity
                ) AS dk2,
                CONCAT(
                  mealtrandet.dic3,
                  " = ",
                  mealtrandet.dic3 * mealtrandet.ratemealincity
                ) AS dk3,
                CONCAT(
                  mealtrandet.dic4,
                  " = ",
                  mealtrandet.dic4 * mealtrandet.ratemealincity
                ) AS dk4,
                CONCAT(
                  mealtrandet.dic5,
                  " = ",
                  mealtrandet.dic5 * mealtrandet.ratemealincity
                ) AS dk5,
                mealtrandet.dayincity AS t_dk,
                mealtrandet.ratemealincity AS rate,
                mealtrandet.tundalamkota AS total
              FROM
                USER
              INNER JOIN mealtran ON ifnull(
                user.periodid,
                mealtran.periodid
              ) = mealtran.periodid
              AND ifnull(
                user.departmentid,
                mealtran.departmentid
              ) = mealtran.departmentid
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              AND ifnull(
                user.employeeid,
                mealtrandet.employeeid
              ) = mealtrandet.employeeid
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              INNER JOIN department ON mealtran.departmentid = department.id
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              WHERE
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
