<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportloanController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    return view ('editor.reportjamsostek.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                department.departmentname,
                employee.employeename,
                position.positionname AS jabatan,
                ifnull(payroll.insuranceloan, 0) AS insuranceloan,
                ifnull(payroll.employeeloan, 0) AS employeeloan,
                ifnull(payroll.vehicleloan, 0) AS vehicleloan,
                ifnull(payroll.homeloan, 0) AS pinjaman_rumah,
                payroll.totalloan AS total_pinjaman,
                ifnull(
                  payroll.remainemployeeloan,
                  0
                ) AS sisa_pinjaman_personal,
                ifnull(
                  payroll.remainvehicleloan,
                  0
                ) AS sisa_pinjaman_kendaraan,
                ifnull(
                  payroll.remaininsuranceloan,
                  0
                ) AS sisa_pinjaman_asuransi,
                ifnull(payroll.remainhomeloan, 0) AS sisa_pinjaman_rumah
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN USER ON payroll.periodid = USER .periodid
              AND payroll.departmentid = ifnull(
                USER .departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (USER .id = '.$userid.')
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
