<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportovertimeController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportovertime.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.nik,
                employee.employeename,
                overtimerequest.notrans,
                DATE_FORMAT(overtimerequest.datetrans, "%d-%m-%Y") AS datetrans, 
                DATE_FORMAT(overtimerequest.datefrom, "%d-%m-%Y") AS datefrom,
                DATE_FORMAT(overtimerequest.dateto, "%d-%m-%Y") AS dateto,
                overtimerequest.timefrom,
                overtimerequest.timeto,
                overtimerequest.overtimein,
                overtimerequest.overtimeout,
                overtimerequest.planwork,
                overtimerequest.actualwork,
                overtimerequest.location,
                absence.holiday,
                absence.overtimehouractual,
                absence.overtimehour
              FROM
                overtimerequest
              INNER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
              INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
              INNER JOIN absence ON overtimerequest.datefrom = absence.datein
              AND overtimerequestdet.employeeid = absence.employeeid, user
              WHERE (overtimerequest.datefrom BETWEEN user.begindate AND user.enddate) AND
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
