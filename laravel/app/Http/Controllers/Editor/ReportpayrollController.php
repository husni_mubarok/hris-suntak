<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportpayrollController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    // $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    return view ('editor.reportpayroll.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                  payroll.id,
                  employee.nik,
                  employee.employeename,
                  department.departmentname,
                  payroll.dayjob,
                  payroll.basic,
                  payroll.mealtransall,
                  payroll.mealtrans,
                  payroll.transportall,
                  payroll.transport,
                  payroll.overtimeall,
                  payroll.overtime,
                  payroll.insentive,
                  payroll.jamsostek,
                  payroll.bpjs,
                  payroll.totalloan,
                  payroll.pph21,
                  payroll.totalbruto,
                  payroll.totalnetto
                FROM
                  payroll
                LEFT JOIN employee ON payroll.employeeid = employee.id
                LEFT JOIN department ON employee.departmentid = department.id, user
              WHERE
                payroll.Periodid = user.Periodid AND
                (user.id = '.$userid.')
              AND (employee.status = 0)';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

       ->addColumn('printslip', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Print Slip"  onclick="showslip('."'".$itemdata->id."'".')"> <i class="fa fa-print"></i> Slip</a>';
      }) 

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
