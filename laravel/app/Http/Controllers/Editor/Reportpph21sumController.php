<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class Reportpph21sumController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    return view ('editor.reportpph21sum.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod. YEAR AS tahun_pajak,
                REPLACE (
                  REPLACE (
                    ifnull(
                      employee.npwp,
                      "0000000000000000"
                    ),
                    "-",
                    ""
                  ),
                  ".",
                  ""
                ) AS npwp,
                employee.employeename AS employee,
                sum(ifnull(payroll.bruto, 0)) AS jumlah_bruto,
                sum(
                  round(ifnull(payroll.pph21, 0), 0)
                ) AS jumlah_pph
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN USER ON payroll.departmentid = ifnull(
                USER .departmentid,
                payroll.departmentid
              )
              INNER JOIN payperiod ON payroll.periodid = payperiod.id
              AND ifnull(USER . YEAR, payperiod. YEAR) = payperiod. YEAR
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              WHERE
                (USER .id = 1)
              GROUP BY
                payperiod. YEAR,
                REPLACE (
                  REPLACE (
                    ifnull(
                      employee.npwp,
                      "0000000000000000"
                    ),
                    "-",
                    ""
                  ),
                  ".",
                  ""
                ),
                employee.employeename
              ORDER BY
                employee.employeename';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
