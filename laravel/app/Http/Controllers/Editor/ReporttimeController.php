<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReporttimeController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reporttime.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.nik,
                employee.employeename,
                absencetype.absencetypename,
                department.departmentname,
                position.positionname,
                absence.datein,
                absence.dayin,
                absence.dayout,
                absence.actualin,
                absence.actualout,
                absence.permitein,
                absence.permiteout,
                absence.overtimein,
                absence.overtimeout
              FROM
                employee
              INNER JOIN absence ON employee.id = absence.employeeid
              INNER JOIN absencetype ON absence.absencetypeid = absencetype.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN position ON position.id = employee.positionid, user
              WHERE (absence.datein BETWEEN user.begindate AND user.enddate) AND employee.id = IFNULL(user.employeeid, employee.id) AND
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
