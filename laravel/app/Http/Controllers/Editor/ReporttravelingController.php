<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReporttravelingController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportpromotion.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                travelling.notrans,
                travelling.datetrans,
                employee.nik,
                employee.employeename,
                travelling.travellingfrom,
                travelling.travellingto,
                travelingtype.travelingtypename,
                city.cityname,
                travelling.actualin,
                travelling.approveddate,
                travelling.used
              FROM
                travelling
              INNER JOIN employee ON travelling.employeeid = employee.id
              INNER JOIN travelingtype ON travelling.travellingtypeid = travelingtype.id
              INNER JOIN city ON travelling.cityid = city.id, user
              WHERE (promotion.datetrans BETWEEN user.begindate AND user.enddate) AND
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
