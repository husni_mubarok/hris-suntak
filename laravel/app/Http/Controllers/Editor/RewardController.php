<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\RewardRequest;
use App\Http\Controllers\Controller;
use App\Model\Reward; 
use App\Model\Employee; 
use App\Model\Sktype;
use Validator;
use Response;
use App\Post;
use View;

class RewardController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'rewardno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'rewardname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $rewards = Reward::all();
      return view ('editor.reward.index', compact('rewards'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  reward.id,
                  reward.notrans,
                  DATE_FORMAT( reward.datetrans, "%d/%m/%Y" ) AS datetrans,
                  reward.employeeid,
                  employee.employeename,
                  DATE_FORMAT( reward.datefrom, "%d/%m/%Y" ) AS datefrom,
                  DATE_FORMAT( reward.dateto, "%d/%m/%Y" ) AS dateto,
                  reward.sktypeid,
                  reward.status, 
                  reward.description,
                  reward.attachment 
                FROM
                  reward
                  LEFT JOIN employee ON reward.employeeid = employee.id
                WHERE
                  reward.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 


        ->addColumn('action', function ($itemdata) {
          return '<a href="reward/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

       ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
        }; 
       })

       ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/reward/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
        })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  reward.id,
                  reward.notrans,
                  reward.datetrans,
                  employee.employeename,
                  sktype.sktypename
                FROM
                  reward
                INNER JOIN employee ON reward.employeeid = employee.id
                INNER JOIN sktype ON reward.sktypeid = sktype.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND reward.employeeid = user.employeeid
                AND
                  reward.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $sktype_list = Sktype::all()->pluck('sktypename', 'id'); 

      return view ('editor.reward.form', compact('employee_list', 'sktype_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO reward (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(reward.notrans),3))+1001,3)), CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    reward
                    WHERE codetrans='PUNS'");

       $lastInsertedID = DB::table('reward')->max('id');  

       // dd($lastInsertedID);

       $reward = Reward::where('id', $lastInsertedID)->first(); 
       $reward->employeeid = $request->input('employeeid'); 
       $reward->sktypeid = $request->input('sktypeid');  
       $reward->created_by = Auth::id();
       $reward->save();

       if($request->attachment)
       {
        $reward = Reward::FindOrFail($reward->id);
        $original_directory = "uploads/reward/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $reward->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $reward->attachment);
          $reward->save(); 
        } 
        return redirect('editor/reward'); 
     
    }

    public function edit($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $sktype_list = Sktype::all()->pluck('sktypename', 'id'); 
      $reward = Reward::Where('id', $id)->first();   

      // dd($reward); 
      return view ('editor.reward.form', compact('reward','sktype_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      
       $reward = Reward::FindOrFail($id); 
       $reward->employeeid = $request->input('employeeid'); 
       $reward->sktypeid = $request->input('sktypeid');  
       $reward->created_by = Auth::id();
       $reward->save();

      if($request->attachment)
      {
        $reward = Reward::FindOrFail($reward->id);
        $original_directory = "uploads/reward/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $reward->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $reward->attachment);
          $reward->save(); 
        } 
        return redirect('editor/reward');  
      }

      public function cancel($id, Request $request)
      {
        $post = Reward::Find($id); 
        $post->status = 9; 
        $post->created_by = Auth::id();
        $post->save(); 
      
        return response()->json($post); 

      }

      public function delete($id)
      {
    //dd($id);
        $post =  Reward::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
