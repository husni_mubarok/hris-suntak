<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ShiftRequest;
use App\Http\Controllers\Controller;
use App\Model\Shift; 
use App\Model\Absencetype; 
use Validator;
use Response;
use App\Post;
use View;

class ShiftController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'shiftname' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $shifts = Shift::all();
    $absencetype_list = Absencetype::where('code', 'DW')->orWhere('code', 'OD')->get();

    return view ('editor.shift.index', compact('shifts', 'absencetype_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      // $itemdata = Shift::orderBy('shiftname', 'ASC')->get();

      $sql = 'SELECT
                shift.id,
                shift.shiftname,
                shift.starttime,
                shift.endtime,
                shift.startbreak,
                shift.endbreak,
                shift.absencetypeid,
                absencetype.absencetypename,
                CONCAT(shift.graceforlate, " Minute(s)") AS graceforlate,
                shift.remark,
                shift.status
              FROM
                shift
              LEFT JOIN absencetype ON shift.absencetypeid = absencetype.id
              WHERE shift.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->shiftname."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Shift(); 
    $post->shiftname = $request->shiftname; 
    $post->starttime = $request->starttime; 
    $post->endtime = $request->endtime; 
    $post->startbreak = $request->startbreak; 
    $post->endbreak = $request->endbreak; 
    $post->absencetypeid = $request->absencetypeid; 
    $post->graceforlate = $request->graceforlate; 
    $post->remark = $request->remark;  
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $shift = Shift::Find($id);
    echo json_encode($shift); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Shift::Find($id); 
    $post->shiftname = $request->shiftname;
    $post->starttime = $request->starttime; 
    $post->endtime = $request->endtime; 
    $post->startbreak = $request->startbreak; 
    $post->endbreak = $request->endbreak; 
    $post->absencetypeid = $request->absencetypeid; 
    $post->graceforlate = $request->graceforlate; 
    $post->remark = $request->remark; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Shift::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Shift::where('id', $id["1"])->get();
    $post = Shift::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
