<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ShiftgroupRequest;
use App\Http\Controllers\Controller;
use App\Model\Shiftgroup; 
use App\Model\Shiftgroupdetail; 
use App\Model\Shift; 
use App\Model\Payperiod;
use Validator;
use Response;
use App\Post;
use View;

class ShiftgroupController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'shiftgroupname' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $shiftgroups = Shiftgroup::all();
    $shift = Shift::all();
    $payperiod = Payperiod::all();
    return view ('editor.shiftgroup.index', compact('shiftgroups', 'shift', 'payperiod'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
              shiftgroup.id,
              shiftgroup.shiftgroupcode,
              shiftgroup.shiftgroupname,
              shiftgroup.dayshift,
              shiftgroup.startdate,
              shiftgroup.offfriday,
              shiftgroup.offsaturday,
              shiftgroup.offsunday,
              shiftgroup.firstperiodid,
              shiftgroup.status,
              shift.shiftname AS shift,
              payperiod.description AS period
            FROM
              shiftgroup
            LEFT JOIN shift ON shiftgroup.firstshiftid = shift.id
            LEFT JOIN payperiod ON shiftgroup.firstperiodid = payperiod.id
            WHERE shiftgroup.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->shiftgroupname."'".')"> Delete</a>';
      })

      ->addColumn('offday', function ($itemdata) {
        if($itemdata->offfriday == 1){
          $offfriday = '<span class="label label-danger"> Friday </span>&nbsp;';
        }else{
          $offfriday = '';
        };

        if($itemdata->offsaturday == 1){
          $offsaturday = '<span class="label label-danger"> Saturday </span>&nbsp;';
        }else{
          $offsaturday = '';
        };

        if($itemdata->offsunday == 1){
          $offsunday = '<span class="label label-danger"> Sunday </span>';
        }else{
          $offsunday = '';
        };

        return ''.$offfriday.''.$offsaturday.''.$offsunday.'';
         
      })

      ->addColumn('detail', function ($itemdata) {
        return '<a href="shiftgroup/'.$itemdata->id.'/edit" title="'."'".$itemdata->shiftgroupname."'".'" class="btn btn-default btn-xs""><i class="fa fa-clock-o"></i> Shift Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function datadetail(Request $request, $id)
    {   
     
      if($request->ajax()){ 

            $sql = 'SELECT
                      shiftgroupdetail.id,
                      shiftgroupdetail.day,
                      shiftgroupdetail.shiftgroupid,
                      shiftgroupdetail.shiftid,
                      shift.shiftname,
                      shift.starttime,
                      shift.endtime,
                      shift.startbreak,
                      shift.endbreak,
                      shift.graceforlate,
                      shift.absencetypeid,
                      absencetype.absencetypename
                    FROM
                      shiftgroupdetail
                    LEFT JOIN shift ON shiftgroupdetail.shiftid = shift.id
                    LEFT JOIN absencetype ON shift.absencetypeid = absencetype.id
                    WHERE shiftgroupdetail.shiftgroupid = '.$id.' AND shiftgroupdetail.deleted_at IS NULL
                    ORDER BY shiftgroupdetail.day ASC';
            $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('day', 'ASC')->get(); 

           return Datatables::of($itemdata) 

          ->addColumn('action', function ($itemdata) {
            return '<a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->shiftname."'".')"><i class="fa fa-trash"></i></a>';
          })

          ->addColumn('lbl_shift', function ($itemdata) {
            if($itemdata->shiftname == 'OFF'){
              return '<b style="color: red">'.$itemdata->shiftname.'</b>';
            }else{
              return ''.$itemdata->shiftname.'';  
            }
            
          })
       
          ->addColumn('description', function ($itemdata) {
            return '<li> Start - End: '.$itemdata->starttime.' - '.$itemdata->endtime.' </li> <li> Break: '.$itemdata->startbreak.' - '.$itemdata->endbreak.' </li> <li> Absence Type: '.$itemdata->absencetypename.' </li>  <li> Grace for late: '.$itemdata->graceforlate.' Minute(s) </li>';
          })

          ->make(true);
        } else {
          exit("No data available");
      }
    }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $post = new Shiftgroup(); 
            $post->shiftgroupcode = $request->shiftgroupcode; 
            $post->shiftgroupname = $request->shiftgroupname; 
            $post->dayshift = $request->dayshift;  
            $post->status = $request->status;
            $post->created_by = Auth::id();
            $post->save();

            return response()->json($post); 
        }
  }

  public function edit($id)
  {
    $shift_list = Shift::all();
    $period_list = Payperiod::all();
    // $shiftgroup = Shiftgroup::Find($id);
    $sql = 'SELECT
              shiftgroup.id,
              shiftgroup.shiftgroupcode,
              shiftgroup.shiftgroupname,
              shiftgroup.dayshift,
              shiftgroup.startdate,
              shiftgroup.offfriday,
              shiftgroup.offsaturday,
              shiftgroup.offsunday,
              shiftgroup.firstperiodid,
              payperiod.description AS period,
              shiftgroup.firstshiftid,
              shift.shiftname,
              shiftgroup.firstday,
              shiftgroup.dafaultholiday,
              shiftgroup.remark,
              shiftgroup.`status`
            FROM
              shiftgroup
            LEFT JOIN payperiod ON shiftgroup.firstperiodid = payperiod.id
            LEFT JOIN shift ON shiftgroup.firstshiftid = shift.id';
    $shiftgroup = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first();


    return view('editor.shiftgroup.form', compact('shiftgroup', 'shift_list', 'period_list'));
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = Shiftgroup::Find($id); 
      $post->shiftgroupcode = $request->shiftgroupcode;
      $post->shiftgroupname = $request->shiftgroupname;
      $post->dayshift = $request->dayshift;  
      $post->firstperiodid = $request->firstperiodid;  
      $post->firstshiftid = $request->firstshiftid;  
      $post->dafaultholiday = $request->input('defaultholiday');
      $post->firstday = $request->firstday;  
      $post->status = $request->status;
      $post->offsaturday = $request->has('offsaturday');
      $post->offfriday = $request->has('offfriday');
      $post->offsunday = $request->has('offsunday');
      $post->updated_by = Auth::id();
      $post->save();

      // return response()->json($post); 
      return redirect('editor/shiftgroup');   
    }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Shiftgroup::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function savedetail($id, Request $request)
  { 
      $post = new Shiftgroupdetail;
      $post->shiftgroupid = $id;  
      $post->shiftid = $request->shiftid;   
      $post->day = $request->day;   
      $post->save();

      return response()->json($post); 
  }

  public function deletedet($id)
  {
    $post =  Shiftgroupdetail::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Shiftgroup::where('id', $id["1"])->get();
    $post = Shiftgroup::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
