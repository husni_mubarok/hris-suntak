<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ShiftgroupmemberRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee; 
use App\Model\Shiftgroup; 
use App\Model\Shiftgroupmember;
use Validator;
use Response;
use App\Post;
use View;

class ShiftgroupmemberController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'shiftgroupid' => ''
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $shiftgroupmembers = Employee::all();
    $shiftgroup_list = Shiftgroup::all();

    return view ('editor.shiftgroupmember.index', compact('shiftgroupmembers', 'shiftgroup_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      // $itemdata = Shiftgroup::all(); 
       $sql = 'SELECT
                shiftgroup.id,
                shiftgroup.shiftgroupcode,
                shiftgroup.shiftgroupname,
                shiftgroup.dayshift,
                shiftgroup.`status`,
                shiftgroup.deleted_at
              FROM
                shiftgroup
              UNION ALL
                SELECT
                  0 id,
                  "NO SHIFT GROUP" AS shiftgroupcode,
                  "NO SHIFT GROUP" AS shiftgroupname,
                  0 AS dayshift,
                  "" AS `status`,
                  "" AS deleted_at';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata) 

      ->addColumn('details_url', function($itemdata) {
            return url('editor/shiftgroupmember/detaildata/' . $itemdata->id);
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

   public function detaildata($id)
    {
        // $posts = Loandet::where('transid', $id)->get();
        $sql = 'SELECT
                  employee.id,
                  employee.nik,
                  employee.identityno,
                  employee.employeename,
                  employee.nickname,
                  employee.placebirth,
                  employee.datebirth,
                  employee.address,
                  employee.positionid,
                  position.positionname,
                  employee.departmentid,
                  employee.image,
                  employee.`status`,
                  department.departmentname,
                  employee.shiftgroupid 
                FROM
                  employee
                LEFT JOIN department ON employee.departmentcode = department.departmentcode
                LEFT JOIN position ON employee.positionid = position.id
                LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
                LEFT JOIN shiftgroup ON employee.shiftgroupid = shiftgroup.id
                WHERE
                employee.deleted_at IS NULL';
        $posts = DB::table(DB::raw("($sql) as rs_sql"))->where('shiftgroupid', $id)->get(); 

        return Datatables::of($posts)

         ->addColumn('action', function ($posts) {
          return '<a href="#" class="btn btn-primary btn-xs" onclick="edit('."'".$posts->id."'".')"><i class="fa fa-pencil"></i> Change</a> <a href="#" class="btn btn-primary btn-xs" onclick="showhistory('."'".$posts->id."'".')"><i class="fa fa-sticky-note"></i> History</a>';
        })

        ->make(true);
    }

  public function history($id)
  {
    $shiftgroupmember = Shiftgroupmember::where('employeeid', $id);
    $getemployeeid = $id;
    return view ('editor.shiftgroupmember.history', compact('shiftgroupmember', 'getemployeeid'));
  }

  public function datahistory(Request $request, $id)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                shiftgroupmember.id,
                employee.employeename,
                groupfrom.shiftgroupname AS groupfrom,
                groupto.shiftgroupname AS groupto,
                DATE_FORMAT(shiftgroupmember.datechange, "%d-%m-%Y") AS datechange,
                DATE_FORMAT(shiftgroupmember.datechangeto, "%d-%m-%Y") AS datechangeto
              FROM
                shiftgroupmember
              LEFT JOIN employee ON shiftgroupmember.employeeid = employee.id
              LEFT JOIN shiftgroup AS groupfrom ON shiftgroupmember.shiftgroupidfrom = groupfrom.id
              LEFT JOIN shiftgroup AS groupto ON shiftgroupmember.shiftgroupidto = groupto.id
              WHERE shiftgroupmember.employeeid = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata) 
      
      ->make(true);
    } else {
      exit("No data available");
    }
  }


  public function edit($id)
  {
    $shiftgroupmember = Employee::Find($id);
    echo json_encode($shiftgroupmember); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
          $employee = Employee::where('id', $id)->first(); 
          DB::update("UPDATE shiftgroupmember SET status = 1, datechangeto =  '".$request->datechange."' WHERE shiftgroupmember.status = 0 AND employeeid = ".$id."");

          $shiftgroup = new Shiftgroupmember;
          $shiftgroup->shiftgroupidfrom = $employee->shiftgroupid;
          $shiftgroup->shiftgroupidto = $request->shiftgroupid;
          $shiftgroup->datechange = $request->datechange;
          $shiftgroup->datechangeto = $request->datechange;
          $shiftgroup->employeeid = $id;
          $shiftgroup->created_by = Auth::id();
          $shiftgroup->save();

          $post = Employee::Find($id); 
          $post->shiftgroupid = $request->shiftgroupid;
          $post->changeshiftdate = $request->datechange;
          $post->updated_by = Auth::id();
          $post->save();

          return response()->json($post); 
        }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Employee::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Employee::where('id', $id["1"])->get();
    $post = Employee::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
