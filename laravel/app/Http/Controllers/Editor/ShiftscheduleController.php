<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Time; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Absencetype;
use App\Model\Shift;
use App\Model\Shiftgroup;
use App\Model\Absence;
use App\Model\Employee;
use App\Model\Exchangeshift;
use Validator;
use Response;
use App\Post;
use View;

//Editor
use
    App\Editor,
    App\Editor\Field,
    App\Editor\Format,
    App\Editor\Mjoin,
    App\Editor\Options,
    App\Editor\Upload,
    App\Editor\Validate;

class ShiftscheduleController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'employeeid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $shiftschedules = Time::all();
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

    return view ('editor.shiftschedule.index', compact('shiftschedules','payperiod_list','department_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                employee.id,
                employee.nik,
                employee.employeename,
                employee.positionid,
                position.positionname,
                employee.departmentid,
                department.departmentname,
                payperiod.description,
                payperiod.id AS periodid
              FROM
                employee
              LEFT JOIN position ON employee.positionid = position.id
              LEFT JOIN department ON employee.departmentid = department.id
              INNER JOIN (
                SELECT
                  absence.employeeid,
                  absence.periodid
                FROM
                  absence
                WHERE absence.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )
                GROUP BY
                  absence.employeeid,
                  absence.periodid
              ) AS absence ON employee.id = absence.employeeid
              INNER JOIN payperiod ON absence.periodid = payperiod.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="shiftschedule/'.$itemdata->periodid.'/'.$itemdata->id.'/edit" title="Schedule" class="btn btn-default btn-xs" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-clock-o"></i> Schedule</a>';
      })

      ->make(true);
    } else {
      exit("No data available");
    }
  } 



  public function store($id, Request $request)
  {  

    //update user
    DB::insert("UPDATE user AS A INNER JOIN (SELECT
        payperiod.id,
        payperiod.begindate,
        payperiod.enddate
        FROM payperiod
        WHERE payperiod.id='".$request->periodid."') AS U SET A.begindate = U.begindate, A.enddate = U.enddate WHERE A.id='".Auth::id()."'");

    //update work calendar
    DB::insert("TRUNCATE TABLE workcalendar");
    DB::insert("set @i = -1");
    DB::insert("INSERT INTO workcalendar
        (`date`)
        SELECT DATE(ADDDATE(user.begindate, INTERVAL @i:=@i+1 DAY)) AS date FROM employee join user
        WHERE user.id='".Auth::id()."'
        HAVING 
        @i < DATEDIFF((SELECT enddate FROM user WHERE id='".Auth::id()."'), (SELECT begindate FROM user WHERE id='".Auth::id()."'))");

    //insert to shiftschedule
    DB::insert("INSERT INTO absence
        (employeeid, nik, datein, periodid)

        SELECT
          derivedtbl.id,
          derivedtbl.nik,
          derivedtbl.`date`,
          '".$request->periodid."'
        FROM
          (
            SELECT
              employee.id,
              employee.nik,
              workcalendar.`date`
            FROM
              employee
            INNER JOIN workcalendar ON employee.joindate <= workcalendar.`date`
            JOIN USER
            WHERE
              (
                workcalendar.`date` BETWEEN user.begindate
                AND user.enddate
              )
            AND user.id = '".Auth::id()."'
          ) derivedtbl
        LEFT JOIN holiday ON derivedtbl.`date` = holiday.dateholiday
        LEFT JOIN absence ON absence.employeeid = derivedtbl.id
        AND absence.datein = derivedtbl.`date`
        WHERE
          (
            derivedtbl.id,
            derivedtbl.`date`,
            '".$request->periodid."'
          ) NOT IN (
            SELECT
              absence.employeeid,
              absence.datein,
              absence.periodid
            FROM
              absence
          )
        GROUP BY
          derivedtbl.id,
          derivedtbl.nik,
          derivedtbl.`date`");

    //return response()->json($post); 
  }

  public function edit($id, $id2)
  {

    $shift_list = Shift::all();
    $shiftgroup_list = Shiftgroup::all();
    $absencetype_list = Absencetype::all();
    $employee_list = Employee::all();

    $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`month`,
              payperiod.`year`,
              employee.nik,
              employee.employeename,
              position.positionname,
              shiftgroup.shiftgroupname,
              payperiod.id AS periodid,
              employee.id AS employeeid
            FROM
              payperiod, employee
            LEFT JOIN position ON position.id = employee.positionid
            LEFT JOIN shiftgroup ON employee.shiftgroupid = shiftgroup.id
            WHERE payperiod.id = '.$id.' AND employee.id = '.$id2.''; 
    $shiftschedule = DB::table(DB::raw("($sql) as rs_sql"))->first();
    
    return view ('editor.shiftschedule.form', compact('shiftschedule', 'shift_list', 'shiftgroup_list', 'absencetype_list', 'employee_list'));
  }

  public function editdetail($id)
  {

     $sql = 'SELECT
              exchangeshift.id,
              DATE_FORMAT(exchangeshift.exchangedate, "%d-%m-%Y") AS exchangedate,
              employee.nik,
              employee.identityno,
              employee.employeename AS exchangeemployeename,
              DATE_FORMAT(absence.datein, "%d-%m-%Y") AS datein,
              absence.dateout,
              absence.absencetypeid,
              absencetype.absencetypename,
              shift.shiftname,
              absence.shiftid,
              absence.holiday,
              exchangeshift.employeeid,
              absence.id AS absenceid,
              absence.shiftgroupid,
              shiftgroup.shiftgroupname,
              shiftexchange.shiftname AS exchangeshiftname,
              exchangeshift.shiftgroupexchangeid,
              shiftgroupexchange.shiftgroupname AS exchangeshiftgroupname
            FROM
              absence
            LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
            LEFT JOIN shift ON absence.shiftid = shift.id
            LEFT JOIN shiftgroup ON absence.shiftgroupid = shiftgroup.id
            LEFT JOIN exchangeshift ON absence.id = exchangeshift.absenceid
            LEFT JOIN employee ON exchangeshift.employeeid = employee.id
            LEFT JOIN shift AS shiftexchange ON exchangeshift.shiftexchangeid = shiftexchange.id
            LEFT JOIN shiftgroup AS shiftgroupexchange ON exchangeshift.shiftgroupexchangeid = shiftgroupexchange.id
            WHERE absence.id = '.$id.'';
    $post = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    echo json_encode($post); 
  }

  public function updatedetail($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
        //exchnage shift
        $absence = Absence::where('id', $id)->first();

         $sql = 'SELECT
                  absence.id,
                  absence.employeeid,
                  absence.datein,
                  absence.shiftgroupid,
                  absence.shiftid
                FROM
                  exchangeshift 
                INNER JOIN
                  (SELECT * FROM absence WHERE id = '.$id.') AS absence
                ON exchangeshift.employeeid = absence.employeeid AND exchangeshift.exchangedate = absence.datein';
        $exchangeshift = DB::table(DB::raw("($sql) as rs_sql"))->first();

        //source
        $exchange_value = Absence::where('id', $id)->first();
        $get_exchange_value = Absence::where('employeeid', $request->employeeid)->where('datein', $request->exchangedate)->first();

        $sql1 = 'SELECT
                  exchangeshift.id,
                  exchangeshift.employeeid,
                  exchangeshift.exchangedate
                FROM
                  exchangeshift
                WHERE exchangeshift.employeeid = '.$request->employeeid.' AND exchangeshift.exchangedate = '.$request->exchangedate.' AND exchangeshift.absenceid = 0';
        $sourceshift = DB::table(DB::raw("($sql1) as rs_sql"))->first(); 


        //exchange
        if(isset($get_exchange_value)){
           if(isset($exchangeshift)){
            $post = Exchangeshift::where('absenceid', $id)->first(); 
            $post->fromemployeeid = $exchange_value->employeeid;
            $post->fromdate = $exchange_value->datein;
            $post->employeeid = $request->employeeid;
            $post->exchangedate = $request->exchangedate;
            $post->shiftgroupexchangeid = $get_exchange_value->shiftgroupid;
            $post->shiftexchangeid = $get_exchange_value->shiftid;
            $post->absenceid = $id;
            $post->updated_by = Auth::id();
            $post->save();   
          }else{
            $post = new Exchangeshift(); 
            $post->fromemployeeid = $exchange_value->employeeid;
            $post->fromdate = $exchange_value->datein;
            $post->employeeid = $request->employeeid;
            $post->exchangedate = $request->exchangedate;
            $post->shiftgroupexchangeid = $get_exchange_value->shiftgroupid;
            $post->shiftexchangeid = $get_exchange_value->shiftid;
            $post->absenceid = $id;
            $post->created_by = Auth::id();
            $post->save(); 
          };
        }else{
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        };

        // source
        if(isset($sourceshift)){
          $post = Exchangeshift::where('absenceid', $id)->first(); 
          $post->fromemployeeid = $request->employeeid;
          $post->fromdate = $request->exchangedate;
          $post->employeeid = $exchange_value->employeeid;
          $post->exchangedate = $exchange_value->datein;
          $post->absenceid = $get_exchange_value->id;
          $post->shiftgroupexchangeid = $absence->shiftgroupid;
          $post->shiftexchangeid = $absence->shiftid;
          $post->updated_by = Auth::id();
          $post->save();   
        }else{
          $post = new Exchangeshift(); 
          $post->fromemployeeid = $request->employeeid;
          $post->fromdate = $request->exchangedate;
          $post->employeeid = $exchange_value->employeeid;
          $post->exchangedate = $exchange_value->datein;
          $post->absenceid = $get_exchange_value->id;
          $post->shiftgroupexchangeid = $absence->shiftgroupid;
          $post->shiftexchangeid = $absence->shiftid;
          $post->created_by = Auth::id();
          $post->save();  
        };

        return response()->json($post); 
      }
  }

  public function deletedetail($id, Request $request)
  {

    // $post = Exchangeshift::where('absenceid', $id)->first();
    $sql = 'SELECT
              exchangeshift.id,
              exchangeshift.absenceid,
              exchangeshift.exchangedate,
              exchangeshift.employeeid
            FROM
              exchangeshift INNER JOIN (SELECT * FROM absence WHERE id ='.$id.') AS absence
            ON exchangeshift.exchangedate = absence.datein AND exchangeshift.employeeid = absence.employeeid';
    $getid = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    $post = Exchangeshift::where('id', $getid->id)->first();
    $post->delete();

    $post = Exchangeshift::where('absenceid', $id)->first();
    $post->delete();

    return response()->json($post); 
  }

  public function datadetail(Request $request, $id, $id2)
  {   
    // if($request->ajax()){ 
      $sql = 'SELECT
                exchangeshift.id,
                DATE_FORMAT(exchangeshift.exchangedate, "%d-%m-%Y") AS exchangedate,
                employee.nik,
                employee.identityno,
                employeeexchange.employeename AS exchangeemployeename,
                DATE_FORMAT(absence.datein, "%d-%m-%Y") AS datein,
                absence.dateout,
                absence.absencetypeid,
                CASE WHEN absence.holiday = 0 AND absence.absencetypeid IS NULL THEN "Dw" ELSE absencetype.absencetypename END AS absencetypename,
                shift.shiftname,
                absence.shiftid,
                absence.holiday,
                absence.id AS absenceid,
                absence.shiftgroupid,
                shiftgroup.shiftgroupname,
                shiftexchange.shiftname AS exchangeshiftname,
                exchangeshift.shiftgroupexchangeid,
                shiftgroupexchange.shiftgroupname AS exchangeshiftgroupname
              FROM
                absence
              INNER JOIN employee ON absence.employeeid = employee.id
              LEFT JOIN shiftgroup ON employee.shiftgroupid = shiftgroup.id
              LEFT JOIN exchangeshift ON absence.id = exchangeshift.absenceid
              LEFT JOIN employee AS employeeexchange ON exchangeshift.employeeid = employeeexchange.id
              LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
              LEFT JOIN shift ON absence.shiftid = shift.id
              LEFT JOIN shift AS shiftexchange ON exchangeshift.shiftexchangeid = shiftexchange.id
              LEFT JOIN shiftgroup AS shiftgroupexchange ON exchangeshift.shiftgroupexchangeid = shiftgroupexchange.id
              WHERE absence.periodid = '.$id.' AND absence.employeeid = '.$id2.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        if($itemdata->absencetypename == "Dw"){
            return '<a href="javascript:void(0)" title="Change Schedule"  onclick="edit('."'".$itemdata->absenceid."'".')" class="btn btn-default btn-xs"><i class="fa fa-calendar"></i> Change Schedule</a>';
        }else{
            return '';
        };
      })

      ->addColumn('action_delete', function ($itemdata) {
        if($itemdata->exchangeemployeename === null || $itemdata->holiday == 1){
          return '';
        }else {
          return '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->absenceid."', '".$itemdata->datein."'".')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
        };

      })

      ->addColumn('datein_lbl', function ($itemdata) {
        if($itemdata->holiday == 1){
          return '<p style="background-color: #FFC0CB">'.$itemdata->datein.'</p>';
        }else{
          return ''.$itemdata->datein.'';
        };

      })

      ->make(true); 
  } 


  public function update($id, Request $request)
  {
    foreach($request->input('detail') as $key => $detail_data)
    { 
 
      $shiftschedule_detail = Time::Find($key);  
      $shiftschedule_detail->actualin = $detail_data['actualin'];
      $shiftschedule_detail->actualout = $detail_data['actualout'];
      $shiftschedule_detail->overshiftschedulein = $detail_data['overshiftschedulein'];
      $shiftschedule_detail->overshiftscheduleout = $detail_data['overshiftscheduleout'];
      $shiftschedule_detail->shiftid = $detail_data['shiftid'];
      $shiftschedule_detail->shiftgroupid = $detail_data['shiftgroupid'];
      $shiftschedule_detail->remark = $detail_data['remark'];
      $shiftschedule_detail->save();
    } 

    return redirect()->action('Editor\TimeController@index'); 
  }  
 
}
