<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\SliderRequest;
use App\Http\Controllers\Controller;
use App\Repository\SliderRepository;

class SliderController extends Controller
{
    protected $SliderRepository;

    public function __construct(SliderRepository $slider_repository)
    {
    	$this->SliderRepository = $slider_repository;
    }

    public function index()
    {
    	$sliders = $this->SliderRepository->get_all();
    	return view ('editor.slider.index', compact('sliders'));
    }

    public function create()
    {
    	return view ('editor.slider.form');
    }

    public function store(SliderRequest $request)
    {
    	$slider = $this->SliderRepository->store($request->input());
    	if($request->image)
    	{
    		$this->SliderRepository->update_image($slider->id, $request->image);
    	}
    	return redirect()->action('Editor\SliderController@index');
    }

    public function edit($id)
    {
    	$slider = $this->SliderRepository->get_one($id);
    	return view ('editor.slider.form', compact('slider'));
    }

    public function update($id, SliderRequest $request)
    {
    	$slider = $this->SliderRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->SliderRepository->update_image($slider->id, $request->image);
    	}
    	return redirect()->action('Editor\SliderController@index');
    }

    public function delete($id)
    {
    	$this->SliderRepository->delete($id);
    	return redirect()->action('Editor\SliderController@index');
    }
}
