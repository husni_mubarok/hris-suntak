<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Time; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Absencetype;
use Validator;
use Response;
use App\Post;
use View;

//Editor
use
    App\Editor,
    App\Editor\Field,
    App\Editor\Format,
    App\Editor\Mjoin,
    App\Editor\Options,
    App\Editor\Upload,
    App\Editor\Validate;

class TimeController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'timename' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    // $times = Time::all();
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

    return view ('editor.time.index', compact('payperiod_list','department_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                employee.id,
                employee.nik,
                employee.employeename,
                employee.positionid,
                position.positionname,
                employee.departmentid,
                department.departmentname,
                payperiod.description,
                payperiod.id AS periodid
              FROM
                employee
              LEFT JOIN position ON employee.positionid = position.id
              LEFT JOIN department ON employee.departmentid = department.id
              INNER JOIN (
                SELECT
                  absence.employeeid,
                  absence.periodid
                FROM
                  absence
                WHERE absence.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = '.Auth::id().'
                )
                GROUP BY
                  absence.employeeid,
                  absence.periodid
              ) AS absence ON employee.id = absence.employeeid
              INNER JOIN payperiod ON absence.periodid = payperiod.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="time/'.$itemdata->periodid.'/'.$itemdata->id.'/edit" title="Detail" class="btn btn-primary btn-xs btn-flat" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store($id, Request $request)
  {  

    //update user
    DB::insert("UPDATE user AS A INNER JOIN (SELECT
        payperiod.id,
        payperiod.begindate,
        payperiod.enddate
        FROM payperiod
        WHERE payperiod.id='".$request->periodid."') AS U SET A.begindate = U.begindate, A.enddate = U.enddate WHERE A.id='".Auth::id()."'");

    //update work calendar
    DB::insert("TRUNCATE TABLE workcalendar");
    DB::insert("set @i = -1");
    DB::insert("INSERT INTO workcalendar
        (`date`)
        SELECT DATE(ADDDATE(user.begindate, INTERVAL @i:=@i+1 DAY)) AS date FROM employee join user
        WHERE user.id='".Auth::id()."'
        HAVING 
        @i < DATEDIFF((SELECT enddate FROM user WHERE id='".Auth::id()."'), (SELECT begindate FROM user WHERE id='".Auth::id()."'))");

    //insert to time
     DB::select("INSERT INTO absence
          (employeeid, nik, datein, periodid)
          SELECT
            derivedtbl.id,
            derivedtbl.nik,
            derivedtbl.`date`,
            ".$request->periodid."
          FROM
            (
              SELECT
                employee.id,
                employee.nik,
                workcalendar.`date`
              FROM
                employee
              INNER JOIN workcalendar ON employee.joindate <= workcalendar.`date`
              JOIN `user`            
              WHERE
                (
                  workcalendar.`date` BETWEEN user.begindate
                  AND user.enddate
                )
              AND user.id = '".Auth::id()."' AND employee.status = 0
            ) derivedtbl
          LEFT JOIN holiday ON derivedtbl.`date` = holiday.dateholiday
          LEFT JOIN absence ON absence.employeeid = derivedtbl.id
          AND absence.datein = derivedtbl.`date`
          WHERE
            (
              derivedtbl.id,
              derivedtbl.`date`,
              ".$request->periodid."
            ) NOT IN (
              SELECT
                absence.employeeid,
                absence.datein,
                absence.periodid
              FROM
                absence
            ) 
          GROUP BY
            derivedtbl.id,
            derivedtbl.nik,
            derivedtbl.`date`");

    //return response()->json($post); 
  }


  public function generateuplod($id)
  {   
     //dif date period
    $dateperiod = '(
                      SELECT
                        user.begindate
                      FROM
                        user
                      WHERE
                        user.id ='.Auth::id().'
                    )
                    AND (
                      SELECT
                        user.enddate
                      FROM
                        user
                      WHERE
                        user.id ='.Auth::id().')';

    $period = DB::table('payperiod')->where('id', $id)->first();
 
     /* Get time from upload */
    DB::update("UPDATE absence
                INNER JOIN (
                  SELECT
                    uploadtime.personnelid,
                    uploadtime.nik,
                    uploadtime.datetime,
                    uploadtime.actualin,
                    uploadtime.actualout,
                    uploadtime.inoutstatus
                  FROM
                    uploadtime 
                ) AS upload
                SET absence.actualin = DATE_FORMAT(
                  upload.actualin,
                  '%H:%i:%s'
                ), absence.actualout = DATE_FORMAT(
                  upload.actualout,
                  '%H:%i:%s'
                )
                WHERE
                  absence.datein = DATE_FORMAT(upload.datetime, '%Y-%m-%d')
                AND absence.nik = upload.nik AND (absence.manual IS NULL OR absence.manual = 0) AND 
                absence.periodid = ".$period->id."");
  } 


  public function generate($id)
  {  

    //dif date period
    $dateperiod = '(
                      SELECT
                        user.begindate
                      FROM
                        user
                      WHERE
                        user.id ='.Auth::id().'
                    )
                    AND (
                      SELECT
                        user.enddate
                      FROM
                        user
                      WHERE
                        user.id ='.Auth::id().')';

    $period = DB::table('payperiod')->where('id', $id)->first();


    // DB::update("UPDATE absence
    //         INNER JOIN (
    //           SELECT
    //             employee.nik,
    //             employee.id
    //           FROM
    //             employee 
    //         ) AS employee ON employee.id = absence.employeeid
    //         SET absence.nik =  employee.nik
    //         WHERE 
    //         absence.periodid = ".$period->id."");

     /* Get time from upload */
    DB::update("UPDATE absence
                INNER JOIN (
                  SELECT
                    uploadtime.personnelid,
                    uploadtime.nik,
                    uploadtime.datetime,
                    uploadtime.actualin,
                    uploadtime.actualout,
                    uploadtime.inoutstatus
                  FROM
                    uploadtime 
                ) AS upload ON absence.datein = DATE_FORMAT(upload.datetime, '%Y-%m-%d') AND absence.nik = upload.nik
                SET absence.actualin = DATE_FORMAT(
                  upload.actualin,
                  '%H:%i:%s'
                ), absence.actualout = DATE_FORMAT(
                  upload.actualout,
                  '%H:%i:%s'
                )
                WHERE absence.periodid = ".$period->id." AND (absence.manual IS NULL OR absence.manual = 0)");

     /* Untuk meng Update Gaji, Tunjangan, Uang Makan & Tetap, Overtime & Tetap mengjadi 0 */
    DB::update("UPDATE absence 
                SET absence.absencetypeid = NULL,
                 absence.workhour = 0,
                 absence.overtimein = NULL,
                 absence.overtimeout = NULL,
                 absence.dayin = NULL,
                 absence.dayout = NULL,
                 absence.overtimehour = NULL 
                WHERE absence.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = ".Auth::id()."
                ) AND (absence.manual IS NULL OR absence.manual = 0)");


      DB::update("UPDATE absence 
            SET absence.absencetypeid = NULL,
             absence.workhour = 0 
            WHERE absence.periodid = (
              SELECT
                `user`.periodid
              FROM
                `user`
              WHERE
                `user`.id = ".Auth::id()."
            )");


      //holiday
      DB::update("UPDATE absence
                  SET absence.holiday = 1
                  WHERE
                    absence.datein IN (
                      SELECT
                        a.datein
                      FROM
                        (
                          SELECT
                            holiday.dateholiday,
                            absence.datein
                          FROM
                            holiday
                          INNER JOIN absence ON holiday.dateholiday = absence.datein
                          UNION ALL
                          SELECT
                            absence.datein AS dateholiday,
                            absence.datein
                          FROM
                            absence
                          WHERE
                            weekday(absence.datein) = 6 OR weekday(absence.datein) = 5
                        ) AS a
                    )
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."')  AND (absence.manual IS NULL OR absence.manual = 0)");

      //payroll type
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      payrolltype.id,
                      payrolltype.payrolltypename,
                      payrolltype.dayin,
                      payrolltype.dayout,
                      payrolltype.dayinfriday,
                      payrolltype.dayoutfriday,
                      employee.id AS employeeid
                    FROM
                      payrolltype
                    INNER JOIN employee ON payrolltype.id = employee.paytypeid
                  ) AS payrolltype
                  SET absence.dayin = CASE WHEN DAYOFWEEK(absence.datein) = 6 THEN payrolltype.dayinfriday ELSE payrolltype.dayin END,
                   absence.dayout = CASE WHEN DAYOFWEEK(absence.datein) = 6 THEN payrolltype.dayoutfriday ELSE payrolltype.dayout END
                  WHERE
                    absence.employeeid = payrolltype.employeeid
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."')");

      //Work hour
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                  TIMESTAMPDIFF(
                      HOUR,
                      absence.actualin,
                      absence.actualout
                    ) AS workhour,
                  absence.id
                  FROM
                    absence 
                  ) AS workhour
                  SET absence.workhour = workhour.workhour
                  WHERE
                    absence.id = workhour.id
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."')");



      //ABSENCE TYPE
      //auto alpha
      DB::update("UPDATE absence 
                  SET absence.absencetypeid =  5
                  WHERE
                    (absence.actualin IS NULL OR absence.actualin = '00:00:00') AND holiday = 0
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."')"); 

       //travelling
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      travelling.id,
                      travelling.travellingfrom,
                      travelling.travellingto,
                      travelling.employeeid
                    FROM
                      travelling
                  ) AS travelling
                  SET absence.absencetypeid =  7 
                  WHERE
                    absence.employeeid = travelling.employeeid
                  AND absence.datein BETWEEN travelling.travellingfrom AND travelling.travellingto
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual IS NULL OR absence.manual = 0)");


      //Leave
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      leaving.id,
                      leaving.employeeid,
                      leaving.leavingfrom,
                      leaving.leavingto,
                      leaving.absencetypeid
                    FROM
                      leaving 
                  ) AS leaving
                  SET absence.absencetypeid =  leaving.absencetypeid 
                  WHERE
                    absence.employeeid = leaving.employeeid
                  AND absence.datein BETWEEN leaving.leavingfrom AND leaving.leavingto
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual IS NULL OR absence.manual = 0)");


      //Overtime
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      overtimerequest.id,
                      overtimerequest.datefrom,
                      overtimerequest.dateto,
                      overtimerequest.timefrom,
                      overtimerequest.timeto,
                      overtimerequestdet.employeeid,
                      TRIM(TRAILING '000' FROM TIMESTAMPDIFF(
                        MINUTE,
                        overtimerequest.timefrom,
                        overtimerequest.timeto
                      ) / 60) AS othour

                    FROM
                      overtimerequest
                    INNER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
                  ) AS overtime
                  SET absence.overtimein =  overtime.timefrom, absence.overtimeout =  overtime.timeto, absence.overtimehour = overtime.othour + (overtime.othour/2), absence.overtimehouractual = overtime.othour
                  WHERE
                    absence.employeeid = overtime.employeeid
                  AND absence.datein BETWEEN overtime.datefrom AND overtime.dateto
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual = 0 OR absence.manual IS NULL)  ");


       //Overtime
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      overtimerequest.id,
                      overtimerequest.datefrom,
                      overtimerequest.dateto,
                      overtimerequest.timefrom,
                      overtimerequest.timeto,
                      overtimerequestdet.employeeid,
                      TRIM(TRAILING '000' FROM TIMESTAMPDIFF(
                        MINUTE,
                        overtimerequest.timefrom,
                        overtimerequest.timeto
                      ) / 60) AS othour

                    FROM
                      overtimerequest
                    INNER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
                  ) AS overtime
                  SET absence.overtimehour = CASE WHEN absence.overtimehouractual > 1 THEN 1.5 + ((overtime.othour-2) + (overtime.othour)) ELSE overtime.othour + (overtime.othour/2) END
                  WHERE
                    absence.employeeid = overtime.employeeid
                  AND absence.datein BETWEEN overtime.datefrom AND overtime.dateto
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual = 0 OR absence.manual IS NULL)  ");


      DB::update("UPDATE absence
                  SET absence.overtimehouractual = TIMESTAMPDIFF(
                        HOUR,
                        absence.overtimein,
                        absence.overtimeout
                      ),
                      absence.overtimehour = (TIMESTAMPDIFF(
                        HOUR,
                        absence.overtimein,
                        absence.overtimeout
                      )) + (TIMESTAMPDIFF(
                        HOUR,
                        absence.overtimein,
                        absence.overtimeout
                      ) / 2)
                  WHERE absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual = 1)  ");

      // //Update overtime amount for driver
      DB::update("UPDATE absence 
                  SET absence.overimeamount =  CASE WHEN absence.holiday = 1 THEN absence.overtimehour * 30000 ELSE absence.overtimehour * 15000 END 
                  WHERE
                      absence.positionid = 69
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  ) 
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."')");


      //Overtime holiday
      DB::update("UPDATE absence
                  INNER JOIN (
                    SELECT
                      overtimerequest.id,
                      overtimerequest.datefrom,
                      overtimerequest.dateto,
                      overtimerequest.timefrom,
                      overtimerequest.timeto,
                      overtimerequestdet.employeeid,
                      TIMESTAMPDIFF(
                        HOUR,
                        overtimerequest.timefrom,
                        overtimerequest.timeto
                      ) AS othour

                    FROM
                      overtimerequest
                    INNER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
                  ) AS overtime
                  SET absence.overtimein =  overtime.timefrom, absence.overtimeout =  overtime.timeto, absence.overtimeholidayhour = overtime.othour + (overtime.othour/2), absence.overtimehouractual = overtime.othour
                  WHERE
                    absence.employeeid = overtime.employeeid AND absence.holiday = 1
                  AND absence.datein BETWEEN overtime.datefrom AND overtime.dateto
                  AND absence.datein BETWEEN (
                    SELECT
                      user.begindate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."'
                  )
                  AND (
                    SELECT
                      user.enddate
                    FROM
                      user
                    WHERE
                      user.id ='".Auth::id()."') AND (absence.manual = 0 OR absence.manual IS NULL) ");

    }


    public function edit($id, $id2)
      {
        $sql = 'SELECT
                  payperiod.id,
                  payperiod.description,
                  payperiod.dateperiod,
                  payperiod.begindate,
                  payperiod.enddate,
                  payperiod.paydate,
                  payperiod.`month`,
                  payperiod.`year`,
                  employee.nik,
                  employee.employeename
                FROM
                  payperiod, employee
                WHERE payperiod.id = '.$id.' AND employee.id = '.$id2.''; 
        $time = DB::table(DB::raw("($sql) as rs_sql"))->first();
        
        $sql_detail = 'SELECT
                        absence.id,
                        DATE_FORMAT(absence.datein, "%d-%m-%Y") AS datein,
                        absence.nik,
                        absence.manual,
                        absence.dateout,
                        absence.daytypeid,
                        absence.absencetypeid,
                        absence.holiday,
                        absence.dayin,
                        absence.dayout,
                        absence.actualin,
                        absence.actualout,
                        absence.permitein,
                        absence.permiteout,
                        absence.workhour,
                        absence.overtimein,
                        absence.overtimeout,
                        absence.overtimehour, 
                        absence.overtimehouractual,
                        CASE WHEN absencetype.absencetypename IS NULL AND absence.holiday = 0 THEN "In" ELSE absencetype.absencetypename END AS absencetypename
                    FROM
                      absence
                    LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
                    WHERE absence.periodid = '.$id.' AND absence.employeeid = '.$id2.''; 
        $time_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get(); 

        $absencetype_list = Absencetype::all()->pluck('absencetypename', 'id');
     
        return view ('editor.time.form', compact('time', 'time_detail', 'absencetype_list'));
  }

  public function datadetail(Request $request, $id)
  {   
    // if($request->ajax()){ 
      $sql = 'SELECT
                  employee.employeename,
                  department.departmentname,
                  time.otherded,
                  time.othersall,
                  time.vehicleloan,
                  time.homeloan,
                  time.id
                FROM
                  time
                INNER JOIN employee ON time.employeeid = employee.id
                INNER JOIN department ON time.departmentid = department.id
                WHERE time.periodid = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 
 
      ->make(true);
    // } else {
    //   exit("No data available");
    // }
  } 


  public function update($id, Request $request)
  {
     
    foreach($request->input('detail') as $key => $detail_data)
    { 

      if( isset($detail_data['manual'])){
        $manual = 1;
      }else{
        $manual = 0;
      };
 
      $time_detail = Time::Find($key); 
      $time_detail->actualin = $detail_data['actualin']; 
      $time_detail->actualout = $detail_data['actualout'];
      $time_detail->overtimein = $detail_data['overtimein']; 
      $time_detail->overtimeout = $detail_data['overtimeout']; 
      $time_detail->nik = $detail_data['nik']; 
      $time_detail->manual = $manual; 

      $time_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\TimeController@index'); 
  }  


 
}
