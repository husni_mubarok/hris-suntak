<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Time; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Absencetype;
use Validator;
use Response;
use App\Post;
use View;

//Editor
use
    App\Editor,
    App\Editor\Field,
    App\Editor\Format,
    App\Editor\Mjoin,
    App\Editor\Options,
    App\Editor\Upload,
    App\Editor\Validate;

class TimeviewController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'timename' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    // $times = Time::all();
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

    return view ('editor.timeview.index', compact('times','payperiod_list','department_list'));
  }

  public function data(Request $request)
  {   

    if($request->ajax()){ 
      $sql = 'SELECT
                employee.id,
                employee.nik,
                employee.employeename,
                employee.positionid,
                position.positionname,
                employee.departmentid,
                department.departmentname,
                payperiod.description,
                payperiod.id AS periodid,
                absence.dayjob
              FROM
                employee
              LEFT JOIN position ON employee.positionid = position.id
              LEFT JOIN department ON employee.departmentid = department.id
              INNER JOIN (
                SELECT
                  absence.employeeid,
                  absence.periodid,
                  payroll.dayjob
                FROM
                  absence
                LEFT JOIN payroll ON absence.employeeid = payroll.employeeid
                AND absence.periodid = payroll.periodid
                WHERE
                  absence.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = '.Auth::id().'
                  )
                GROUP BY
                  absence.employeeid,
                  absence.periodid
              ) AS absence ON employee.id = absence.employeeid
              INNER JOIN payperiod ON absence.periodid = payperiod.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="timeview/'.$itemdata->periodid.'/'.$itemdata->id.'/edit" title="Detail" class="btn btn-primary btn-xs btn-flat" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store($id, Request $request)
  {  

    //update user
    DB::insert("UPDATE user AS A INNER JOIN (SELECT
        payperiod.id,
        payperiod.begindate,
        payperiod.enddate
        FROM payperiod
        WHERE payperiod.id='".$request->periodid."') AS U SET A.begindate = U.begindate, A.enddate = U.enddate WHERE A.id='".Auth::id()."'");

    //update work calendar
    DB::insert("TRUNCATE TABLE workcalendar");
    DB::insert("set @i = -1");
    DB::insert("INSERT INTO workcalendar
        (`date`)
        SELECT DATE(ADDDATE(user.begindate, INTERVAL @i:=@i+1 DAY)) AS date FROM employee join user
        WHERE user.id='".Auth::id()."'
        HAVING 
        @i < DATEDIFF((SELECT enddate FROM user WHERE id='".Auth::id()."'), (SELECT begindate FROM user WHERE id='".Auth::id()."'))");

    //insert to time
     DB::select("INSERT INTO absence
          (employeeid, nik, datein, periodid)
          SELECT
            derivedtbl.id,
            derivedtbl.nik,
            derivedtbl.`date`,
            ".$request->periodid."
          FROM
            (
              SELECT
                employee.id,
                employee.nik,
                workcalendar.`date`
              FROM
                employee
              INNER JOIN workcalendar ON employee.joindate <= workcalendar.`date`
              JOIN `user`            
              WHERE
                (
                  workcalendar.`date` BETWEEN user.begindate
                  AND user.enddate
                )
              AND user.id = '".Auth::id()."'
            ) derivedtbl
          LEFT JOIN holiday ON derivedtbl.`date` = holiday.dateholiday
          LEFT JOIN absence ON absence.employeeid = derivedtbl.id
          AND absence.datein = derivedtbl.`date`
          WHERE
            (
              derivedtbl.id,
              derivedtbl.`date`,
              ".$request->periodid."
            ) NOT IN (
              SELECT
                absence.employeeid,
                absence.datein,
                absence.periodid
              FROM
                absence
            )
          GROUP BY
            derivedtbl.id,
            derivedtbl.nik,
            derivedtbl.`date`");

    //return response()->json($post); 
  }

    public function edit($id, $id2)
      {
        $sql = 'SELECT
                  payperiod.id,
                  payperiod.description,
                  payperiod.dateperiod,
                  payperiod.begindate,
                  payperiod.enddate,
                  payperiod.paydate,
                  payperiod.`month`,
                  payperiod.`year`,
                  employee.nik,
                  employee.employeename
                FROM
                  payperiod, employee
                WHERE payperiod.id = '.$id.' AND employee.id = '.$id2.''; 
        $time = DB::table(DB::raw("($sql) as rs_sql"))->first();
        
        $sql_detail = 'SELECT
                        absence.id,
                        DATE_FORMAT(absence.datein, "%d-%m-%Y") AS datein,
                        absence.manual,
                        absence.dateout,
                        absence.daytypeid,
                        absence.absencetypeid,
                        absence.holiday,
                        absence.dayin,
                        absence.dayout,
                        absence.actualin,
                        absence.actualout,
                        absence.permitein,
                        absence.permiteout,
                        absence.workhour,
                        absence.overtimein,
                        absence.overtimeout,
                        absence.overtimehour, 
                        absence.overtimehouractual,
                        CASE WHEN absencetype.absencetypename IS NULL AND absence.holiday = 0 THEN "In" ELSE absencetype.absencetypename END AS absencetypename
                    FROM
                      absence
                    LEFT JOIN absencetype ON absence.absencetypeid = absencetype.id
                    WHERE absence.periodid = '.$id.' AND absence.employeeid = '.$id2.''; 
        $time_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get(); 

        $absencetype_list = Absencetype::all()->pluck('absencetypename', 'id');
     
        return view ('editor.timeview.form', compact('time', 'time_detail', 'absencetype_list'));
  }

  public function datadetail(Request $request, $id)
  {   
    // if($request->ajax()){ 
      $sql = 'SELECT
                  employee.employeename,
                  department.departmentname,
                  time.otherded,
                  time.othersall,
                  time.vehicleloan,
                  time.homeloan,
                  time.id
                FROM
                  time
                INNER JOIN employee ON time.employeeid = employee.id
                INNER JOIN department ON time.departmentid = department.id
                WHERE time.periodid = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 
 
      ->make(true);
    // } else {
    //   exit("No data available");
    // }
  } 

  public function update($id, Request $request)
  {
     
    foreach($request->input('detail') as $key => $detail_data)
    { 

      if( isset($detail_data['manual'])){
        $manual = 1;
      }else{
        $manual = 0;
      };
 
      $time_detail = Time::Find($key); 
      $time_detail->actualin = $detail_data['actualin']; 
      $time_detail->actualout = $detail_data['actualout'];
      $time_detail->overtimein = $detail_data['overtimein']; 
      $time_detail->overtimeout = $detail_data['overtimeout']; 
      $time_detail->manual = $manual; 

      $time_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\TimeController@index'); 
  }  


 
}
