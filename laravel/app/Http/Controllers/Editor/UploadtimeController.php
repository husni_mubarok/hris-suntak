<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UploadtimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Uploadtime; 
use App\Model\Uploadtimedetail; 
use App\Model\Uploadtimelist;
use Carbon\Carbon; 
use Validator;
use Response;
use App\Post;
use View;
use Excel; 
use File;

class UploadtimeController extends Controller
{
   
  public function index()
  { 
    return view ('editor.uploadtime.index');
  } 

  public function storeimport(Request $request)
  { 
    Uploadtime::query()->truncate();


    if($request->hasFile('import_file')){
      $path = $request->file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();

      if(!empty($data) && $data->count()){
        foreach ($data->toArray() as $key => $value) {
          if(!empty($value)){
            foreach ($value as $v) {
              $insert[] = ['date' => $v[strtolower('Tanggal')], 'nik' => $v[strtolower('NIK')], 'employeename' => $v[strtolower('Nama')], 'actualin' => $v[strtolower(str_replace(" ", "", 'Scan Masuk'))], 'actualout' => $v[strtolower(str_replace(" ", "", 'Scan Pulang'))]];
            }
          }
        }

        $uploadtimelist = new Uploadtimelist();
        $original_directory = "uploads/uploadtimelist/";
        if(!File::exists($original_directory))
        {
          File::makeDirectory($original_directory, $mode = 0777, true, true);
        } 
        $uploadtimelist->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->import_file->getClientOriginalName();
        $request->import_file->move($original_directory, $uploadtimelist->attachment);
        $uploadtimelist->date = Carbon::now();
        $uploadtimelist->created_by = Auth::id();
        $uploadtimelist->save(); 
        

        if(!empty($insert)){
          Uploadtime::insert($insert);

          DB::update('UPDATE uploadtime
                      INNER JOIN (
                        SELECT
                          uploadtime.idabsence,
                          RIGHT (uploadtime.date, 4) AS fyear,
                          CASE
                        WHEN RIGHT (LEFT(uploadtime.date, 2), 1) = "/" THEN
                          CONCAT(
                            "0",
                            LEFT (uploadtime.date, 1)
                          )
                        ELSE
                          LEFT (uploadtime.date, 2)
                        END AS fmonth,
                        CASE
                      WHEN LEFT (RIGHT(uploadtime.date, 7), 1) = "/" THEN
                        CONCAT(
                          "0",
                          LEFT (RIGHT(uploadtime.date, 6), 1)
                        )
                      ELSE
                        LEFT (RIGHT(uploadtime.date, 7), 2)
                      END fday
                      FROM
                        uploadtime
                      ) AS derivedtbl
                      SET datetime = CONCAT(
                        derivedtbl.fyear,
                        "-",
                        derivedtbl.fmonth,
                        "-",
                        derivedtbl.fday
                      )
                      WHERE
                        uploadtime.idabsence = derivedtbl.idabsence');

          return back()->with('success','Insert Record successfully.');
        }
      }
    }
 
    return back()->with('error','Please Check your file, Something is wrong there.');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Uploadtime::orderBy('firstname', 'ASC')->get();

      return Datatables::of($itemdata)  
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }
}
