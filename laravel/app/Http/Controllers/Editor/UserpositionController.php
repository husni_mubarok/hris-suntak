<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests; 
use App\Http\Controllers\Controller;
use App\Model\Position;
use App\Model\Userposition;
use App\User;

class UserpositionController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        //$users = User::paginate(15);
        $users = DB::table('user') 
            ->select('user.id',
                'user.employee_id',
                'user.username',
                'user.email',
                'user.first_name',
                'user.last_name', 
                'user.created_at',
                'user.updated_at') 
            ->whereNull('user.deleted_at')
            ->get();

            //dd($users);

        return view ('editor.userposition.index', compact('users'))->with('number',$no);
    }

    public function create()
    {

    	return view ('editor.user.form');
    }

    public function show($id)
    {
    	$user = User::find($id);
    	return view ('editor.userposition.detail', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id); 
        $position_list = Position::pluck('positionname', 'id');
        $sql = 'SELECT
                    position.positionname,
                    userposition.id,
                    userposition.userid
                FROM
                    userposition
                INNER JOIN position ON userposition.positionid = position.id';
        $position_user = DB::table(DB::raw("($sql) as rs_sql"))->where('userid', $id)->get();

        return view ('editor.userposition.form', compact('user', 'position_list', 'position_user'));
    }

    public function update($id, Request $request)
    {
        $user = new Userposition;
        $user->positionid = $request->input('positionid'); 
        $user->userid = $id;
        $user->save();

        $user->save();

        return redirect()->action('Editor\UserpositionController@edit', $id);
    }

    public function delete($id)
    {
        Userposition::find($id)->delete();
        return redirect()->back();
        
    }
}
