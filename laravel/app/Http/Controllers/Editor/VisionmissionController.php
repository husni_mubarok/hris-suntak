<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\VisionmissionRequest;
use App\Http\Controllers\Controller;
use App\Repository\VisionmissionRepository;

class VisionmissionController extends Controller
{
    protected $VisionmissionRepository;

    public function __construct(VisionmissionRepository $visionmission_repository)
    {
    	$this->VisionmissionRepository = $visionmission_repository;
    }

    public function index()
    {
    	$visionmissions = $this->VisionmissionRepository->get_all();
    	return view ('editor.visionmission.index', compact('visionmissions'));
    }

    public function create()
    {
    	return view ('editor.visionmission.form');
    }

    public function store(VisionmissionRequest $request)
    {
    	$visionmission = $this->VisionmissionRepository->store($request->input());
    	if($request->image)
    	{
    		$this->VisionmissionRepository->update_image($visionmission->id, $request->image);
    	}
    	return redirect()->action('Editor\VisionmissionController@index');
    }

    public function edit($id)
    {
    	$visionmission = $this->VisionmissionRepository->get_one($id);
    	return view ('editor.visionmission.form', compact('visionmission'));
    }

    public function update($id, VisionmissionRequest $request)
    {
    	$visionmission = $this->VisionmissionRepository->update($id, $request->input());
    	if($request->image)
    	{
    		$this->VisionmissionRepository->update_image($visionmission->id, $request->image);
    	}
    	return redirect()->action('Editor\VisionmissionController@index');
    }

    public function delete($id)
    {
    	$this->VisionmissionRepository->delete($id);
    	return redirect()->action('Editor\VisionmissionController@index');
    }
}
