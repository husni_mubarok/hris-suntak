<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'required',
            'description' => '',
            // 'image' => 'image|between:0,5000'
        ];
    }

    public function messages()
    {
        return [
            // 'name.required' => 'Name is required',
            // 'image.image' => 'Image must be a valid file',
            // 'image.between' => 'Image size must be less than 5MB',
        ];
    }
}
