<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MutationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notrans' => 'required|unique:module,notrans',
        ];
    }

    public function messages()
    {
        return [
            'notrans.required' => 'Name is required',
            'notrans.unique' => 'Module is already created',
        ];
    }
}
