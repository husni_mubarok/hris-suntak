<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Absencetype extends Model
{
	use SoftDeletes;
	protected $table = 'absencetype';
	protected $dates = ['deleted_at'];  

}
