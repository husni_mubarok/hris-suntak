<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companyprofile extends Model
{
    protected $table = 'companyprofile';
    protected $dates = ['deleted_at'];
}
