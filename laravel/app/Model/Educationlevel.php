<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Educationlevel extends Model
{
	use SoftDeletes;
	protected $table = 'educationlevel';
	protected $dates = ['deleted_at'];  

}
