<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Educationmajor extends Model
{
	use SoftDeletes;
	protected $table = 'educationmajor';
	protected $dates = ['deleted_at'];  

}
