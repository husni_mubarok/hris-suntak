<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Educationtype extends Model
{
	use SoftDeletes;
	protected $table = 'educationtype';
	protected $dates = ['deleted_at'];  

}
