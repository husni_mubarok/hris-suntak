<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employeestatus extends Model
{
	use SoftDeletes;
	protected $table = 'employeestatus';
	protected $dates = ['deleted_at'];  

}
