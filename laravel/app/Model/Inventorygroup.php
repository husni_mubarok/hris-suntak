<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventorygroup extends Model
{
	use SoftDeletes;
	protected $table = 'inventorygroup';
	protected $dates = ['deleted_at'];  

}
