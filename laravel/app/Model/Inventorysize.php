<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventorysize extends Model
{
	use SoftDeletes;
	protected $table = 'inventorysize';
	protected $dates = ['deleted_at'];  

}
