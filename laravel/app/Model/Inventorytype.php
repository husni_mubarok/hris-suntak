<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventorytype extends Model
{
	use SoftDeletes;
	protected $table = 'inventorytype';
	protected $dates = ['deleted_at'];  

}
