<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Iuranpensiun extends Model
{
	use SoftDeletes;
	protected $table = 'iuranpensiun';
	protected $dates = ['deleted_at'];  

}
