<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Loantype extends Model
{
	use SoftDeletes;
	protected $table = 'loantype';
	protected $dates = ['deleted_at'];  

}
