<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Materialused extends Model
{
	use SoftDeletes;
	protected $table = 'materialused';
	protected $dates = ['deleted_at'];  

}
