<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Materialusedtype extends Model
{
	use SoftDeletes;
	protected $table = 'materialusedtype';
	protected $dates = ['deleted_at'];  

}
