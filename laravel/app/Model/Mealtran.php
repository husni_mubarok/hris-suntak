<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mealtran extends Model
{
	use SoftDeletes;
	protected $table = 'mealtran';
	protected $dates = ['deleted_at'];  

}
