<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mealtrandet extends Model
{
	use SoftDeletes;
	protected $table = 'mealtrandet';
	protected $dates = ['deleted_at'];  

}
