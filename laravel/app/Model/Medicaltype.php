<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicaltype extends Model
{
	use SoftDeletes;
	protected $table = 'medicaltype';
	protected $dates = ['deleted_at'];  

}
