<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Month extends Model
{
	use SoftDeletes;
	protected $table = 'month';
	protected $dates = ['deleted_at'];  

}
