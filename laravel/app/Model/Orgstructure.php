<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orgstructure extends Model
{
	use SoftDeletes;
	protected $table = 'orgstructure';
	protected $dates = ['deleted_at'];  

}
