<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payperiod extends Model
{
	use SoftDeletes;
	protected $table = 'payperiod';
	protected $dates = ['deleted_at'];  

}
