<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payrolltype extends Model
{
	use SoftDeletes;
	protected $table = 'payrolltype';
	protected $dates = ['deleted_at'];  

}
