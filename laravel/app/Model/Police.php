<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Police extends Model
{
	use SoftDeletes;
	protected $table = 'police';
	protected $dates = ['deleted_at'];  

}
