<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Renewcontract extends Model
{
	use SoftDeletes;
	protected $table = 'renewcontract';
	protected $dates = ['deleted_at'];  

}
