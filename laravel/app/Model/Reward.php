<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reward extends Model
{
	use SoftDeletes;
	protected $table = 'reward';
	protected $dates = ['deleted_at'];  

}
