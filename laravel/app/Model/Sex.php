<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sex extends Model
{
	use SoftDeletes;
	protected $table = 'sex';
	protected $dates = ['deleted_at'];  

}
