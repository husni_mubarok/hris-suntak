<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model
{
	use SoftDeletes;
	protected $table = 'shift';
	protected $dates = ['deleted_at'];  

}
