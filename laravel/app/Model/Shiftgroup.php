<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shiftgroup extends Model
{
	use SoftDeletes;
	protected $table = 'shiftgroup';
	protected $dates = ['deleted_at'];  

}
