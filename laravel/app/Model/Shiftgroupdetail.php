<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shiftgroupdetail extends Model
{
	use SoftDeletes;
	protected $table = 'shiftgroupdetail';
	protected $dates = ['deleted_at'];  

}
