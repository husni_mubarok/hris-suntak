<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sktype extends Model
{
	use SoftDeletes;
	protected $table = 'sktype';
	protected $dates = ['deleted_at'];  

}
