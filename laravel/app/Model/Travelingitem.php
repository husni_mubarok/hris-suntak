<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Travelingitem extends Model
{
	use SoftDeletes;
	protected $table = 'travelingitem';
	protected $dates = ['deleted_at'];  

}
