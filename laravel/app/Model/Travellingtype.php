<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Travellingtype extends Model
{
	use SoftDeletes;
	protected $table = 'travelingtype';
	protected $dates = ['deleted_at'];  

}
