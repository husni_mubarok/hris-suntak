<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userlog extends Model
{
	use SoftDeletes;
	protected $table = 'userlog';
	protected $dates = ['deleted_at'];  

}
