<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userposition extends Model
{
    protected $table = 'userposition';
    protected $dates = ['deleted_at'];

}
