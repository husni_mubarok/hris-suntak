<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visionmission extends Model
{
    protected $table = 'visionmission';
    protected $dates = ['deleted_at'];
}
