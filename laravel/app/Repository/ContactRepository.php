<?php

namespace App\Repository;
use File;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use App\Model\Contact;

class ContactRepository
{
    public function get_all()
    {
        return Contact::all();
    }

    public function get_one($id)
    {
        return Contact::FindOrFail($id);
    }

    public function store($data)
    {
        $slider = new Contact;
        $slider->description = $data['description'];
        $slider->map = $data['map'];

        $slider->save();
        return $slider;
    }

    public function update($id, $data)
    {
        $slider = Contact::FindOrFail($id);
        $slider->description = $data['description'];
        $slider->map = $data['map'];
 
        $slider->save();
        return $slider;
    }

    public function update_image($id, $image)
    {
        $slider = Contact::FindOrFail($id);

        $original_directory = "uploads/slider/".$slider->name."/";
        
        if(!File::exists($original_directory))
        {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
        }
        $file_extension = $image->getClientOriginalExtension();
        $slider->image = Carbon::now()->format("d-m-Y h-i-s").$image->getClientOriginalName();
        $image->move($original_directory, $slider->image);

        $thumbnail_directory = $original_directory."thumbnail/";
        if(!File::exists($thumbnail_directory))
        {
            File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
        }
        $thumbnail = Image::make($original_directory.$slider->image);
        $thumbnail->fit(15,15)->save($thumbnail_directory.$slider->image);

        $slider->save();
        return $slider;
    }

    public function delete_image($id)
    {
        $slider = Contact::FindOrFail($id);
        $slider->image = null;
        $slider->save();
    }

    public function delete($id)
    {
        Contact::FindOrFail($id)->delete();
    }
}