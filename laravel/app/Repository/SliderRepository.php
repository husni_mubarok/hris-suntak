<?php

namespace App\Repository;
use File;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use App\Model\Slider;

class SliderRepository
{
    public function get_all()
    {
        return Slider::all();
    }

    public function get_one($id)
    {
        return Slider::FindOrFail($id);
    }

    public function store($data)
    {
        $slider = new Slider;
        $slider->name = $data['name'];
        $slider->description = $data['description'];
        $slider->class = $data['class'];

        $slider->save();
        return $slider;
    }

    public function update($id, $data)
    {
        $slider = Slider::FindOrFail($id);
        $slider->name = $data['name'];
        $slider->description = $data['description'];
        $slider->class = $data['class'];
 
        $slider->save();
        return $slider;
    }

    public function update_image($id, $image)
    {
        $slider = Slider::FindOrFail($id);

        $original_directory = "uploads/slider/".$slider->name."/";
        
        if(!File::exists($original_directory))
        {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
        }
        $file_extension = $image->getClientOriginalExtension();
        $slider->image = Carbon::now()->format("d-m-Y h-i-s").$image->getClientOriginalName();
        $image->move($original_directory, $slider->image);

        $thumbnail_directory = $original_directory."thumbnail/";
        if(!File::exists($thumbnail_directory))
        {
            File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
        }
        $thumbnail = Image::make($original_directory.$slider->image);
        $thumbnail->fit(15,15)->save($thumbnail_directory.$slider->image);

        $slider->save();
        return $slider;
    }

    public function delete_image($id)
    {
        $slider = Slider::FindOrFail($id);
        $slider->image = null;
        $slider->save();
    }

    public function delete($id)
    {
        Slider::FindOrFail($id)->delete();
    }
}