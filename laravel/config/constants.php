<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => 'http://jafelmia.com/demo/hris-suntak/uploads',
        'bootstrap' => 'http://jafelmia.com/demo/hris-suntak/laravel/bootstrap',
        'css' => 'http://jafelmia.com/demo/hris-suntak/assets/css',
        'scss' => 'http://jafelmia.com/demo/hris-suntak/assets/lte_sass/build/scss',
        'img' => 'http://jafelmia.com/demo/hris-suntak/assets/img',
        'js' => 'http://jafelmia.com/demo/hris-suntak/assets/js',
        'plugin' => 'http://jafelmia.com/demo/hris-suntak/assets/plugins',
        'swf' => 'http://jafelmia.com/demo/hris-suntak/assets/swf',
        '404' => 'http://jafelmia.com/demo/hris-suntak/assets/404',
    ],

];