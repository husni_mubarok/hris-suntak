@extends('layouts.editor.template')
@section('content')
<section class="content">

<div class="register-box">
  <div class="register-box-body"> 
    <div class="register-logo">
      <a href="#"><i class="fa fa-paint-brush"></i> Clear Cache</a>
    </div>
        <form id="stdfrm" name="stdfrm"  action="report.php?mod=rptwpp_print" method="post" enctype="multipart/form-data" target="TheWindow">
           <center> <a href="{{ URL::route('editor.cache.index') }}" class="btn btn-success btn-flat btn-lg"><i class="fa fa-check"></i>  Clear Cache</a> </center>
        </form>
  </div><!-- /.form-box -->
</div><!-- /.register-box -->
</section>
@stop
