@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
  <h1>
    <i class="fa fa-bar-chart"></i> Chart Employee
    <small>Chart</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Chart</a></li>
    <li class="active">Chart Employee</li>
  </ol>
</section>


<section class="content">
  <div class="row">
    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <div id="chartContainer" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <div id="chartContainer2" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <div id="chartContainer3" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <div id="chartContainer4" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>

  </div> 
</section>   
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/canvasjs/canvasjs.min.js"></script>
  <script type="text/javascript">
    window.onload = function () {
      var chart1 = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        title:{
          text: "Employee by Position"              
        },
        exportEnabled: true,
        axisX:{
           valueFormatString: "#,##0.##",
         },
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($employeebyposition as $key => $employeebypositions)
            { label: "{{$employeebypositions->positionname}}",  y: {{$employeebypositions->id}}  }, 
          @endforeach
          ]
        }
        ]
      });

      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title:{
          text: "Employee by Department"              
        },
        exportEnabled: true,
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($employeebydept as $key => $employeebydepts)
            { label: "{{$employeebydepts->departmentname}}",  y: {{$employeebydepts->id}}  }, 
          @endforeach
          ]
        }
        ]
      });

      var chart2 = new CanvasJS.Chart("chartContainer3", {
        animationEnabled: true,
        title:{
          text: "Employee by Gender"              
        },
        exportEnabled: true,
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($employeebysex as $key => $employeebysexs)
            { label: "{{$employeebysexs->sex}}",  y: {{$employeebysexs->id}}  }, 
          @endforeach
          ]
        }
        ]
      });

      var chart3 = new CanvasJS.Chart("chartContainer4", {
        animationEnabled: true,
        title:{
          text: "Employee by Education Level"              
        },
        exportEnabled: true,
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($employeebyedu as $key => $employeebyedus)
            { label: "{{$employeebyedus->educationlevelname}}",  y: {{$employeebyedus->id}}  }, 
          @endforeach
          ]
        }
        ]
      });

      chart.render();
      chart1.render();
      chart2.render();
      chart3.render();
    }

  </script> 
@stop


