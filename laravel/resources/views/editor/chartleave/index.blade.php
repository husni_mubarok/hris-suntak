@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
  <h1>
    <i class="fa fa-bar-chart"></i> Chart Leave
    <small>Chart</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Chart</a></li>
    <li class="active">Chart Leave</li>
  </ol>
</section>


<section class="content">
  <div class="row">
    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
            <script src="{{Config::get('constants.path.plugin')}}/canvasjs/canvasjs.min.js"></script>
            <div id="chartContainer" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-6">
      <div class="box box-danger"> 
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body"> 
            <!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
            <script type="text/javascript">
              window.onload = function () {
                var chart1 = new CanvasJS.Chart("chartContainer2", {
                  animationEnabled: true,
                  title:{
                    text: "Leave by Date"              
                  },
                  data: [              
                  {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: [
                    @foreach($leavebydate as $key => $leavebydates)
                      { label: "{{$leavebydates->datetrans}}",  y: {{$leavebydates->id}}  }, 
                    @endforeach
                    ]
                  }
                  ]
                });

                var chart = new CanvasJS.Chart("chartContainer", {
                  animationEnabled: true,
                  title:{
                    text: "Leave by Leave Type"              
                  },
                  data: [              
                  {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: [
                    @foreach($leavebylesvetype as $key => $leavebylesvetypes)
                      { label: "{{$leavebylesvetypes->absencetypename}}",  y: {{$leavebylesvetypes->id}}  }, 
                    @endforeach
                    ]
                  }
                  ]
                });

                chart.render();
                chart1.render();
              }
            </script> 

            <div id="chartContainer2" style="height: 300px; width: 100%;"></div> 
          </div>
        </div>
      </div>
    </div>
  </div> 
</section>   
@stop
@section('scripts')
<script>
</script> 
@stop


