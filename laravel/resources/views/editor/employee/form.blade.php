 
@extends('layouts.editor.template')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-12">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($employee))
				{!! Form::model($employee, array('route' => ['editor.employee.update', $employee->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_employee'))!!}
				@else
				{!! Form::open(array('route' => 'editor.employee.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_employee'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							General Information
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('identityno', 'Identity No') }}
								{{ Form::text('identityno', old('identityno'), array('class' => 'form-control', 'placeholder' => 'Identity No*', 'required' => 'true', 'id' => 'identityno')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('nik', 'NIK') }}
								{{ Form::text('nik', old('nik'), array('class' => 'form-control', 'placeholder' => 'NIK*', 'required' => 'true', 'id' => 'nik')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('employeename', 'Employee Name') }}
								{{ Form::text('employeename', old('employeename'), array('class' => 'form-control', 'placeholder' => 'Employee Name*', 'required' => 'true', 'id' => 'employeename')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('nickname', 'Nick Name') }}
								{{ Form::text('nickname', old('nickname'), array('class' => 'form-control', 'placeholder' => 'Nick Name*', 'id' => 'nickname')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('identityno', 'Identity No') }}
								{{ Form::text('identityno', old('identityno'), array('class' => 'form-control', 'placeholder' => 'Identity No*', 'id' => 'identityno')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('datebirth', 'Date Birth') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('datebirth', old('datebirth'), array('class' => 'form-control', 'placeholder' => 'Date Birth*', 'id' => 'datebirth')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('joindate', 'Join Date') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('joindate', old('joindate'), array('class' => 'form-control', 'placeholder' => 'Join Date*', 'required' => 'true', 'id' => 'joindate')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('termdate', 'Term / Contract Starting Date') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('termdate', old('termdate'), array('class' => 'form-control', 'placeholder' => 'Term / Contract Starting Date*', 'required' => 'true', 'id' => 'termdate')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('insuranceno', 'Insurance No') }}
								{{ Form::text('insuranceno', old('insuranceno'), array('class' => 'form-control', 'placeholder' => 'Insurance No*', 'id' => 'insuranceno')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('npwp', 'NPWP') }}
								{{ Form::text('npwp', old('npwp'), array('class' => 'form-control', 'placeholder' => 'NPWP*', 'id' => 'npwp')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('address', 'Address') }}
								{{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'id' => 'address')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('hp', 'HP No') }}
								{{ Form::text('hp', old('hp'), array('class' => 'form-control', 'placeholder' => 'HP No*', 'id' => 'hp')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('telp1', 'Telp 1') }}
								{{ Form::text('telp1', old('telp1'), array('class' => 'form-control', 'placeholder' => 'Telp 1*', 'id' => 'telp1')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('telp2', 'Telp 2') }}
								{{ Form::text('telp2', old('telp2'), array('class' => 'form-control', 'placeholder' => 'Telp 2*', 'id' => 'telp2')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankan', 'Bank Account Name') }}
								{{ Form::text('bankan', old('bankan'), array('class' => 'form-control', 'placeholder' => 'Bank Account Name*', 'id' => 'bankan')) }}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankaccount', 'Bank Account') }}
								{{ Form::text('bankaccount', old('bankaccount'), array('class' => 'form-control', 'placeholder' => 'Bank Account*', 'id' => 'bankaccount')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankname', 'Bank Name') }}
								{{ Form::text('bankname', old('bankname'), array('class' => 'form-control', 'placeholder' => 'Bank Name*', 'id' => 'bankname')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankbranch', 'Bank Branch') }}
								{{ Form::text('bankbranch', old('bankbranch'), array('class' => 'form-control', 'placeholder' => 'Bank Branch*', 'id' => 'bankbranch')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('cityid', 'City') }}
								{{ Form::select('cityid', $city_list, old('cityid'), array('class' => 'form-control select2', 'placeholder' => 'Select City', 'id' => 'cityid')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('sex', 'Gender') }}
								{{ Form::select('sex', $sex_list, old('sex'), array('class' => 'form-control', 'placeholder' => 'Select Gender', 'id' => 'sex')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('taxstatus', 'Tax Status') }}
								{{ Form::select('taxstatus', $taxstatus_list, old('taxstatus'), array('class' => 'form-control', 'placeholder' => 'Select Tax Status', 'id' => 'taxstatus')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('departmentid', 'Department') }}
								{{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control select2', 'placeholder' => 'Select Department', 'id' => 'departmentid')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('positionid', 'Position') }}
								{{ Form::select('positionid', $position_list, old('positionid'), array('class' => 'form-control select2', 'placeholder' => 'Select Position', 'id' => 'positionid', 'required' => 'true')) }}
							</div>
						</div>
						 
						{{ Form::hidden('gol', $golongan_list, old('golongan'), array('class' => 'form-control', 'placeholder' => 'Select Golongan', 'id' => 'golongan')) }}  
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('religionid', 'Religion') }}
								{{ Form::select('religionid', $religion_list, old('religionid'), array('class' => 'form-control', 'placeholder' => 'Select Religion', 'id' => 'religionid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('placebirth', 'Place Birth') }}
								{{ Form::select('placebirth', $city_list, old('placebirth'), array('class' => 'form-control select2', 'placeholder' => 'Select Place Birth', 'id' => 'placebirth')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('educationlevelid', 'Education Level') }}
								{{ Form::select('educationlevelid', $educationlevel_list, old('educationlevelid'), array('class' => 'form-control select2', 'placeholder' => 'Select Education Level', 'id' => 'educationlevelid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('educationmajorid', 'Education Major') }}
								{{ Form::select('educationmajorid', $educationmajor_list, old('educationmajorid'), array('class' => 'form-control select2', 'placeholder' => 'Select Education Major', 'id' => 'educationmajorid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('locationid', 'Location') }}
								{{ Form::select('locationid', $location_list, old('locationid'), array('class' => 'form-control', 'placeholder' => 'Select Location', 'id' => 'locationid')) }}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('status', 'Status') }}
								<select class="form-control" name="status"  id="status">
								   @if(isset($employee))
					               <option value="{{ $employee->status }}"> @if($employee->status ==1) Not Active @elseif($employee->status ==0) Active @else Off @endif</option>
					               @endif
					               <option value="0">Active</option>
					               <option value="1">Not Active</option>
					               <option value="2">Off</option>
					             </select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('thrtype', 'THR Type') }}
								<select class="form-control" name="thrtype"  id="thrtype">
									@if(isset($employee))
					               <option value="{{ $employee->thrtype }}">{{ $employee->thrtype }}</option>
					               @endif
					               <option value="Lebaran">Lebaran</option>
					               <option value="Imlek">Imlek</option>
					             </select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('image', 'Photo') }} <br>
								<span class="btn btn-default"><input type="file" name="image" /></span><br/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Salary Information
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						@php
						$user = Auth::id();
						if($user != 35){
						@endphp
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('basic', 'Basic') }}
								{{ Form::text('basic', old('basic'), array('class' => 'form-control', 'placeholder' => 'Basic*', 'id' => 'basic', 'oninput' => 'cal_sparator();')) }}
							</div>
						</div>
						@php
						}
						@endphp
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('mealtransall', 'Meal') }}
								{{ Form::text('mealtransall', old('mealtransall'), array('class' => 'form-control', 'placeholder' => 'Meal*', 'id' => 'mealtransall')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('transportall', 'Transport') }}
								{{ Form::text('transportall', old('transportall'), array('class' => 'form-control', 'placeholder' => 'Transport*', 'id' => 'transportall')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('overtimeall', 'Overtime') }}
								{{ Form::text('overtimeall', old('overtimeall'), array('class' => 'form-control', 'placeholder' => 'Overtime*', 'id' => 'overtimeall')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('insentive', 'Insentive') }}
								{{ Form::text('insentive', old('insentive'), array('class' => 'form-control', 'placeholder' => 'Insentive*', 'id' => 'insentive')) }}
							</div>
						</div> 
					</div>
				</div>

				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Shift Information
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('employee_shift', 'Employee Shift') }}
								{{ Form::select('employeeshiftid', $employeeshift_list, old('employeeshiftid'), array('class' => 'form-control', 'placeholder' => 'Select Shift', 'id' => 'employeeshiftid', 'onchange' => 'employeeshift();')) }}
							</div>
						</div>
						@if(isset($employee))
							@if($employee->employeeshiftid == 1)
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('paytypeid', 'Reguler Shift') }}
									{{ Form::select('paytypeid', $payrolltype_list, old('paytypeid'), array('class' => 'form-control', 'placeholder' => 'NO', 'required' => 'true', 'id' => 'paytypeid', 'disabled')) }}
								</div>
							</div>
							@else
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('paytypeid', 'Reguler Shift') }}
									{{ Form::select('paytypeid', $payrolltype_list, old('paytypeid'), array('class' => 'form-control', 'placeholder' => 'NO', 'required' => 'true', 'id' => 'paytypeid')) }}
								</div>
							</div>
							@endif
						@else
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('paytypeid', 'Reguler Shift') }}
									{{ Form::select('paytypeid', $payrolltype_list, old('paytypeid'), array('class' => 'form-control', 'placeholder' => 'NO', 'required' => 'true', 'id' => 'paytypeid', 'disabled')) }}
								</div>
							</div>
						@endif
						
						@if(isset($employee)) 
							@if($employee->shiftgroupid == 1)
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('employee_shift', 'Shift Group') }}
									{{ Form::select('shiftgroupid', $shiftgroup_list, old('employee_shift'), array('class' => 'form-control', 'placeholder' => 'Select Shift Group', 'id' => 'shiftgroupid')) }}
								</div>
							</div>
							@else
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('employee_shift', 'Shift Group') }}
									{{ Form::select('shiftgroupid', $shiftgroup_list, old('employee_shift'), array('class' => 'form-control', 'placeholder' => 'Select Shift Group', 'id' => 'shiftgroupid', 'disabled')) }}
								</div>
							</div>
							@endif
						@else
							<div class="col-md-4">
								<div class="form-group">
									{{ Form::label('employee_shift', 'Shift Group') }}
									{{ Form::select('shiftgroupid', $shiftgroup_list, old('employee_shift'), array('class' => 'form-control', 'placeholder' => 'Select Shift Group', 'id' => 'shiftgroupid', 'disabled')) }}
								</div>
							</div>
						@endif

					</div>
				</div>
				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.employee.index') }}" class="btn btn-default btn-flat pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
		</div>
	</div>
</section>
@stop

@section('scripts') 
<script type="text/javascript">
	function cal_sparator() {  

	  var basic = document.getElementById('basic').value;
	  var result = document.getElementById('basic');
	  var rsamount = (basic);
	  result.value = rsamount.replace(/,/g, ""); 


	  var mealtransall = document.getElementById('mealtransall').value;
	  var result = document.getElementById('mealtransall');
	  var rsamount = (mealtransall);
	  result.value = rsamount.replace(/,/g, ""); 


	  var transportall = document.getElementById('transportall').value;
	  var result = document.getElementById('transportall');
	  var rsamount = (transportall);
	  result.value = rsamount.replace(/,/g, ""); 


	  var overtimeall = document.getElementById('overtimeall').value;
	  var result = document.getElementById('overtimeall');
	  var rsamount = (overtimeall);
	  result.value = rsamount.replace(/,/g, ""); 


	  var insentive = document.getElementById('insentive').value;
	  var result = document.getElementById('insentive');
	  var rsamount = (insentive);
	  result.value = rsamount.replace(/,/g, ""); 
	}

	window.onload= function(){ 
	  n2= document.getElementById('basic');

	  n2.onkeyup=n2.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='basic')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n2.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n2.value));
	    if(temp2)n2.value=addCommas(temp2.toFixed(0));
	  }


	  n3= document.getElementById('mealtransall');

	  n3.onkeyup=n3.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='mealtransall')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n3.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n3.value));
	    if(temp2)n3.value=addCommas(temp2.toFixed(0));
	  }

	  n4= document.getElementById('transportall');

	  n4.onkeyup=n4.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='transportall')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n4.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n4.value));
	    if(temp2)n4.value=addCommas(temp2.toFixed(0));
	  }

	  n5= document.getElementById('overtimeall');

	  n5.onkeyup=n5.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='overtimeall')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n5.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n5.value));
	    if(temp2)n5.value=addCommas(temp2.toFixed(0));
	  }

	  n6= document.getElementById('insentive');

	  n6.onkeyup=n6.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='insentive')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n6.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n6.value));
	    if(temp2)n6.value=addCommas(temp2.toFixed(0));
	  }
	} 

	function employeeshift() {
		var i = $("#employeeshiftid").val();
		// alert(i);
		if(i==1) { //3rd radiobutton
            $("#paytypeid").attr("disabled", "disabled"); 
            $("#shiftgroupid").attr("disabled", false); 
        }else if(i==0){
        	$("#shiftgroupid").attr("disabled", "disabled"); 
        	$("#paytypeid").attr("disabled", false); 
        }else{
        	$("#shiftgroupid").attr("disabled", "disabled"); 
        	$("#paytypeid").attr("disabled", "disabled"); 
        };
	}
</script>
@stop  

