@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>
<!-- Content Header (Page header) -->
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
  <h1>
    <i class="fa fa-user"></i> Employee Request
    <small>Employee Request</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Employee Request</a></li>
    <li class="active">Employee Request</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <a href="{{ URL::route('editor.employee.create') }}" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</a>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th width="3%">#</th> 
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label> 
                  </th>
                  <th>Request No</th>
                  <th>NIK</th>
                  <th>Employee Name</th> 
                  <th>Request Type</th>
                  <th>Request Date</th> 
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
          //editor 
         colReorder: true, 
         rowReorder: true,  

         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true, 
         "sScrollXInner": "230%", 
         "autoWidth": true,
         "rowReorder": true,
         fixedColumns:   {
          leftColumns: 5,
          rightColumns: 1
         },
          "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2, 3, 18 ] }
        ],
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/employee/data') }}",
         columns: [  
         { data: 'id', name: 'id', orderable: false, searchable: false, "width": "1%" },
         { data: 'check', name: 'check', orderable: false, searchable: false, "width": "1%" },
         { data: 'action', name: 'action', orderable: false, searchable: false, "width": "5%" }, 
         { data: 'nik', name: 'nik', "width": "5%" },
         { data: 'employeename', name: 'employeename', "width": "10%" }, 
         { data: 'departmentname', name: 'departmentname', "width": "5%" },
         { data: 'positionname', name: 'positionname', "width": "10%" },
         { data: 'taxstatus', name: 'taxstatus' },
         { data: 'educationlevelname', name: 'educationlevelname' },
         { data: 'gendericon', name: 'gendericon' },
         { data: 'joindate', name: 'joindate', "width": "5%" },
         { data: 'termdate', name: 'termdate', "width": "6%" },
         { data: 'payrolltypename', name: 'payrolltypename' },
         { data: 'npwp', name: 'npwp', "width": "7%" },
         { data: 'bankaccount', name: 'bankaccount', "width": "7%" },
         { data: 'bankan', name: 'bankan', "width": "8%" },  
         { data: 'jamsostekmember', name: 'jamsostekmember', render: function(data, type, row){return data == '1' ? '<span class="label label-success"><i class="fa fa-check"></i> </span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'} },   
         { class: 'text-block', data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
         { class: 'text-block', data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
         { class: 'text-block', data: 'transportall', name: 'transportall', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
         { class: 'text-block', data: 'overtimeall', name: 'overtimeall', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
         { class: 'text-block', data: 'insentive', name: 'insentive', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
         { data: 'image', name: 'image' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });

        //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function delete_id(id, employeename)
      {

        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + employeename + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'employee/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function bulk_delete()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-red',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "employee/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error deleting data!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
