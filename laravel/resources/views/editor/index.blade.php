@extends('layouts.editor.template')
@section('content')
 
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content"> 
      <div class="row">  
         <div class="col-md-4">
          <!-- USERS LIST -->
          <div class="box box-danger" style="min-height: 200px; max-height: 200px;overflow-y: scroll;">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-birthday-cake"></i> Employee Birthday</h3>
              <div class="box-tools pull-right">
                <span class="label label-danger"></span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
                @foreach($birthday as $key => $birthdays) 
                <li>
                  @if( $birthdays->image == '' )
                    @if( $birthdays->sex == 1 )
                      <img src="{{Config::get('constants.path.img')}}/male.png" alt="{{$birthdays->employeename}}">
                    @else
                      <img src="{{Config::get('constants.path.img')}}/female.png" alt="{{$birthdays->employeename}}">
                    @endif
                  @else
                    <img src="{{Config::get('constants.path.uploads')}}/employee/{{$birthdays->image}}" alt="{{$birthdays->employeename}}"> 
                  @endif
                  <a class="users-list-name" href="#">{{$birthdays->employeename}}</a>
                  <span class="users-list-date">{{$birthdays->datebirth}}</span>
                </li> 
                @endforeach
              </ul>
              <!-- /.users-list -->
            </div> 
            <!-- /.box-footer -->
          </div>
          <!--/.box -->
        </div>
        <!-- /.col -->

          <div class="col-md-4">
            <!-- USERS LIST -->
            <div class="box box-danger" style="min-height: 200px; max-height: 200px;overflow-y: scroll;">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-group"></i> New Hire</h3> 
                <div class="box-tools pull-right">
                  <span class="label label-danger">4 New Hires</span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <ul class="users-list clearfix">
                  @foreach($new_hire as $key => $new_hires) 
                  <li>
                    @if( $new_hires->image == '' )
                      @if( $new_hires->sex == 1 )
                        <img src="{{Config::get('constants.path.img')}}/male.png" alt="{{$new_hires->employeename}}">
                      @else
                        <img src="{{Config::get('constants.path.img')}}/female.png" alt="{{$new_hires->employeename}}">
                      @endif
                    @else
                      <img src="{{Config::get('constants.path.uploads')}}/employee/{{$new_hires->image}}" alt="{{$new_hires->employeename}}"> 
                    @endif
                    <a class="users-list-name" href="#">{{$new_hires->employeename}}</a>
                    <span class="users-list-date">{{$new_hires->joindate}}</span>
                  </li> 
                  @endforeach
                </ul>
                <!-- /.users-list -->
              </div> 
          <!-- /.box-footer -->
        </div>
        <!--/.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-danger" style="min-height: 200px; max-height: 200px;overflow-y: scroll;">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-newspaper-o"></i> News</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Event Date</th>
                    <th>Description</th> 
                    <th>Attachment</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($news AS $key => $newss)
                  <tr>
                    <td>{{$newss->date}}</td>
                    <td>{{$newss->title}}</td> 
                    @if($newss->attachment == '')
                    <td>-</td>
                    @else
                    <td><a href="{{Config::get('constants.path.uploads')}}/news/{{$newss->attachment}}" target="_blank"/><i class="fa fa-download"></i> Download</a></td>
                    @endif
                  </tr> 
                  @endforeach
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body --> 
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div>

      <div class="row">
        <div class="col-md-4">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-warning" style="min-height: 180px; max-height: 180px;overflow-y: scroll;">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-sign-out"></i> Employee Leave</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>From</th>
                    <th>To</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($leave as $key => $leaves)
                  <tr>
                    <td>{{$leaves->employeename}}</td>
                    <td>{{$leaves->departmentname}}</td>  
                    <td>{{$leaves->fromdate}}</td> 
                    <td>{{$leaves->todate}}</td> 
                  </tr> 
                  @endforeach
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body --> 
        </div><!-- /.box -->
      </div><!-- /.col -->

      <div class="col-md-4">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-warning" style="min-height: 180px; max-height: 180px;overflow-y: scroll;">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list"></i> Quick Menu</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
               <thead>
                  <tr>
                    <th>Menu Name</th>
                    <th>Module</th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="{{ URL::route('editor.employee.index') }}">Employee</a></td>
                    <td>Employee</td> 
                  </tr> 
                  <tr>
                    <td><a href="{{ URL::route('editor.leaving.index') }}">Leaving Request</a></td>
                    <td>Leaving</td> 
                  </tr> 
                  <tr>
                    <td><a href="{{ URL::route('editor.payroll.index') }}">Payroll</a></td>
                    <td>Time and Payroll</td> 
                  </tr> 
                  <tr>
                    <td><a href="{{ URL::route('editor.overtime.index') }}">Overtime</a></td>
                    <td>Time and Payroll</td> 
                  </tr> 
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->

      <div class="col-md-4">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-warning" style="min-height: 180px; max-height: 180px;overflow-y: scroll;">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-address-card-o"></i> Employee Contract</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Employee Name</th>
                    <th>Department</th> 
                    <th>Start Date</th> 
                    <th>End Date</th> 
                    <th>Days</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($contract as $key => $contracts)
                  <tr>
                    <td>{{$contracts->employeename}}</td>
                    <td>{{$contracts->departmentname}}</td>  
                    <td>{{$contracts->joindate}}</td> 
                    <td>{{$contracts->termdate}}</td> 
                    <td>{{$contracts->days}}</td> 
                  </tr> 
                  @endforeach
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row --> 
    @if($popup->date_popup == $popup->date_now)
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1 class="modal-title">{{$popup->popup_name}}</h1>
              </div>
              <div class="modal-body">
                {!!$popup->description!!}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
  </section>
  <!-- /.content -->
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/canvasjs/canvasjs.min.js"></script>
  <script type="text/javascript">
    window.onload = function () {
      var chart1 = new CanvasJS.Chart("chartContainer1", {
        animationEnabled: true,
        axisX:{
           valueFormatString: "#,##0.##",
         },
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($employeebyposition as $key => $employeebypositions)
            { label: "{{$employeebypositions->positionname}}",  y: {{$employeebypositions->id}}  }, 
          @endforeach
          ]
        }
        ]
      });


      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true, 
        data: [              
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "pie",
          dataPoints: [
          @foreach($userlog as $key => $userlogs)
            { label: "{{$userlogs->username}}",  y: {{$userlogs->id}}  }, 
          @endforeach
          ]
        }
        ]
      }); 

      chart.render();
      chart1.render();
    }
  </script> 

<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
@stop
