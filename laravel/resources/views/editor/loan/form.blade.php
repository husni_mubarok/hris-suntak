 
@extends('layouts.editor.template')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-exchange"></i> Loan
    <small>Time & Payroll</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Loan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- <div class="col-xs-2">
    </div> -->
    <div class="col-xs-8">
      <div class="box box-danger">
        @include('errors.error')
        @if(isset($loan))
        {!! Form::model($loan, array('route' => ['editor.loan.update', $loan->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_loan'))!!}
        @else
        {!! Form::open(array('route' => 'editor.loan.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_loan'))!!}
        @endif
        {{ csrf_field() }}
        <div class="box-header with-border">
          <section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
            <h4>
              @if(isset($loan))
              <i class="fa fa-pencil"></i> Edit
              @else
              <i class="fa fa-plus"></i> 
              @endif
              Loan
            </h4>
          </section>
        </div>
        <div class="box-header with-border">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('notrans', 'No Loan') }}
                {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Loan*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('datetrans', 'Date Loan') }}
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                    {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Loan*', 'required' => 'true', 'id' => 'datetrans')) }}
                </div><!-- /.input group --> 
              </div>
            </div>    
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('employeeid', 'Employee Name') }}
                {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Select Employee', 'id' => 'employeeid')) }}  
              </div>
            </div>  

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('basicamt', 'Basic Amount') }}
                {{ Form::text('basicamt', old('basic'), array('class' => 'form-control', 'placeholder' => 'Basic Amount*', 'required' => 'true', 'disabled' => 'disabled', 'id' => 'basicamt')) }}
                {{ Form::hidden('basic', old('basic'), array('id' => 'basic')) }}
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('requestamount', 'Request Amount') }}
                {{ Form::text('requestamount', old('requestamount'), array('class' => 'form-control', 'placeholder' => 'Request Amount*', 'required' => 'true', 'id' => 'requestamount')) }}
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('loantypeid', 'Loan Type') }}
                {{ Form::select('loantypeid', $loantype_list, old('loantypeid'), array('class' => 'form-control select2', 'placeholder' => 'Select Loan Type', 'id' => 'loantypeid')) }} 
              </div>
            </div>    
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('approvedamount', 'Approve Amount') }}
                {{ Form::text('approvedamount', old('approvedamount'), array('class' => 'form-control', 'placeholder' => 'Approve Amount*', 'required' => 'true', 'id' => 'approvedamount')) }}
              </div>
            </div> 

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('startdeduction', 'Start Deduction') }}
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                    {{ Form::text('startdeduction', old('startdeduction'), array('class' => 'form-control', 'placeholder' => 'Start Deduction*', 'required' => 'true', 'id' => 'startdeduction')) }}
                </div><!-- /.input group --> 
              </div>
            </div>  

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('installment', 'Installment') }}
                {{ Form::text('installment', old('installment'), array('class' => 'form-control', 'placeholder' => 'Installment*', 'required' => 'true', 'id' => 'installment')) }}
              </div>
            </div> 

            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('attachment', 'Attachment') }}<br>
                <span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
                <br/>
              </div>
            </div> 
          </div>
        </div>
        <div class="box-header with-border">
          <div class="row"> 
            <div class="col-md-12">
              <div class="form-group">
                @if(isset($loan)) 
                  @if($loan->status == 9)
                    <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save and Active</button>   
                    <script type="text/javascript"> 
                      $(document).ready(function(){
                         hidebtnactive();
                      });
                    </script>
                  @else
                    <a href="#" onclick="cancel();" class="btn btn-danger btn-flat"><i class="fa fa-minus-square"></i> Cancel</a>  
                  @endif
                @endif
                <button type="submit" id="btnsave" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save and Generate</button>
                <a href="{{ URL::route('editor.loan.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
              </div>
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

    <div class="col-xs-4">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important"> 
          <h4>Detail Instalment</h4> 
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Date Installment</th> 
                  <th>Amount</th> 
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
         <h3 class="modal-title">Installment Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">Date</label>
              <div class="col-md-8">
                <input name="datedetailinstallment" id="datedetailinstallment" class="form-control" type="text" disabled="disabled">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Installment</label>
              <div class="col-md-8">
                <input name="detailinstallment" id="detailinstallment" class="form-control" type="text">
                <small class="errorAsset detailinstallment hidden alert-danger"></small> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button> 
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@stop



@section('scripts')  
@if(isset($loan)) 
<script type="text/javascript"> 

  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
        //editor 
         colReorder: true,
         fixedHeader: true, 
         responsive: true,
         //rowReorder: true, 
         "rowReorder": {
            "update": false,
        },  
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/loan/detaildata') }}/" + {{$loan->id}},
         columns: [  
         { data: 'id', name: 'id' }, 
         { data: 'dateinstallment', name: 'dateinstallment' }, 
         { data: 'amount', name: 'amount', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'action', name: 'action' }
         ]
       });

        //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
 
      });

  
  function addCommas(nStr) {
      nStr += '';
      var x = nStr.split('.');
      var x1 = x[0];
      var x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
  }

  function cancel()
  {   
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this data?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () { 
         }
       },
       confirm: {
        text: 'CANCEL',
        btnClass: 'btn-red',
        action: function () {
         $.ajax({
          url : '../../loan/cancel/' + {{$loan->id}},
          type: "PUT", 
          data: {
            '_token': $('input[name=_token]').val() 
          }, 
          success: function(data) {  
            //var loc = 'ap_invoice';
            if ((data.errors)) { 
              alert("Cancel error!");
            } else{
              window.location.href = "{{ URL::route('editor.loan.index') }}";
            }
          }, 
        }); 
       }
     },
    }
  });
  }  
  function hidebtnactive() {
      $('#btnsave').hide(100); 
  }
</script> 
@endif

<script type="text/javascript"> 

  $("#employeeid").on('change', function()
  {
    // alert("asd");
    $.ajax({
      url : '{{ URL::route('get.employee') }}',
      data : {'employeeid':$("#employeeid").val()},
      type : 'GET',
      headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
      success : function(data, textStatus, jqXHR){

        $('#basic').empty();
        jQuery.each(data, function(i, val)
        {
          document.getElementById ("basic").value = data[0].basic;
          document.getElementById ("basicamt").value = addCommas(data[0].basic);
        }); 
      },
      error : function()
      {
        $('#basic').empty(); 
      },
    })
  });


  function cal_sparator() {  

    var requestamount = document.getElementById('requestamount').value;
    var result = document.getElementById('requestamount');
    var rsamount = (requestamount);
    result.value = rsamount.replace(/,/g, ""); 


    var approvedamount = document.getElementById('approvedamount').value;
    var result = document.getElementById('approvedamount');
    var rsamount = (approvedamount);
    result.value = rsamount.replace(/,/g, "");  


    var detailinstallment = document.getElementById('detailinstallment').value;
    var result = document.getElementById('detailinstallment');
    var rsamount = (detailinstallment);
    result.value = rsamount.replace(/,/g, "");  

  }

  window.onload= function(){ 

    n2= document.getElementById('requestamount');

    n2.onkeyup=n2.onchange= function(e){
      e=e|| window.event; 
      var who=e.target || e.srcElement,temp;
      if(who.id==='requestamount')  temp= validDigits(who.value,0); 
      else temp= validDigits(who.value);
      who.value= addCommas(temp);
    }   
    n2.onblur= function(){
      var 
      temp2=parseFloat(validDigits(n2.value));
      if(temp2)n2.value=addCommas(temp2.toFixed(0));
    }

    n3= document.getElementById('approvedamount');

    n3.onkeyup=n3.onchange= function(e){
      e=e|| window.event; 
      var who=e.target || e.srcElement,temp;
      if(who.id==='approvedamount')  temp= validDigits(who.value,0); 
      else temp= validDigits(who.value);
      who.value= addCommas(temp);
    }   
    n3.onblur= function(){
      var 
      temp2=parseFloat(validDigits(n3.value));
      if(temp2)n3.value=addCommas(temp2.toFixed(0));
    } 


    n4= document.getElementById('detailinstallment');

    n4.onkeyup=n4.onchange= function(e){
      e=e|| window.event; 
      var who=e.target || e.srcElement,temp;
      if(who.id==='detailinstallment')  temp= validDigits(who.value,0); 
      else temp= validDigits(who.value);
      who.value= addCommas(temp);
    }   
    n4.onblur= function(){
      var 
      temp2=parseFloat(validDigits(n4.value));
      if(temp2)n4.value=addCommas(temp2.toFixed(0));
    } 
  } 

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function edit(id)
  { 

  $('.errorAsset detailinstallment').addClass('hidden');

  $("#btnSave").attr("onclick","update("+id+")"); 

  save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
      url : '../../loan/editinstallment/' + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="id_key"]').val(data.id); 
        $('[name="detailinstallment"]').val(data.amount);
        $('[name="datedetailinstallment"]').val(data.dateinstallment);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Installment'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
  }


  function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : '../../loan/editinstallment/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'detailinstallment': $('#detailinstallment').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorAsset detailinstallment').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.detailinstallment) {
                $('.errorAsset detailinstallment').removeClass('hidden');
                $('.errorAsset detailinstallment').text(data.errors.detailinstallment);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };
</script>
@stop  
