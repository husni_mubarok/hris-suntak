 @extends('layouts.editor.template')
 @section('content')

 <style type="text/css">
 	.input-sm {
  height: 22px;
  padding: 1px 3px;
  font-size: 12px;
  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 0px;
  width: 90% !important;
}
 </style>
 <!-- Content Header (Page header) -->
 <section class="content-header">
 	<h4>
 		Mealtran
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">Mealtran</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-solid">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12"> 
 				<div class="col-md-12"> 
 					<div class="col-md-12">   
 						<div class="tab-content">

 							{!! Form::model($mealtran, array('route' => ['editor.mealtran.updatedetail', $mealtran->id, $mealtran->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_mealtran'))!!}
 							<div id="consummable" class="tab-pane fade in active"> 
 								<div class="box-body">
 									<div class="div_overflow"> 

 										<table class="table table-striped table-hover" id="mealtranTable">
 											<thead>
 												<tr>
 													<th>Employee</th>
 													<th>Location </th>
 													<th>Day 1</th> 
 													<th>Day 2</th>
 													<th>Day 3</th>
 													<th>Day 4</th>
 													<th>Day 5</th>
 													<th>T Day</th>
 													<th>Per Day</th>
 													<th>Add Meal</th>
 													<th>T POt Absence</th>
 													<th>Amount</th>
 													<th>Amount Pajak</th>
 												</tr>
 											</thead>
 											<tbody> 
 												@foreach($mealtran_detail as $key => $mealtran_details)
 												<tr>
 													<td class="col-sm-1 col-md-1">
 														{{$mealtran_details->employeename}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{$mealtran_details->locationname}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{ Form::number('detail['.$mealtran_details->id.'][day1]', old($mealtran_details->day1.'[day1]', $mealtran_details->day1), ['id' => 'day1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 1*', 'oninput' => 'tdayal()']) }} 
 													</td>  
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][day2]', old($mealtran_details->day2.'[day2]', $mealtran_details->day2), ['id' => 'day2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 2*', 'oninput' => 'tdayal()']) }} 
 													</td> 
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][day3]', old($mealtran_details->day3.'[day3]', $mealtran_details->day3), ['id' => 'day3'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 3*', 'oninput' => 'tdayal()']) }} 
 													</td> 
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][day4]', old($mealtran_details->id.'[day4]', $mealtran_details->day4), ['class' => 'form-control input-sm', 'placeholder' => 'Day 4*', 'id' => 'day4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); tdayal();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][day5]', old($mealtran_details->id.'[day5]', $mealtran_details->day5), ['class' => 'form-control input-sm', 'placeholder' => 'Day 5*', 'id' => 'day5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][tday]', old($mealtran_details->id.'[tday]', $mealtran_details->tday), ['class' => 'form-control input-sm', 'placeholder' => 'T Day*', 'id' => 'tday'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][perday]', old($mealtran_details->id.'[perday]', $mealtran_details->perday), ['class' => 'form-control input-sm', 'id' => 'perday'.$mealtran_details->id, 'placeholder' => 'Per Day*', 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][addmeal]', old($mealtran_details->id.'[addmeal]', $mealtran_details->addmeal), ['class' => 'form-control input-sm', 'placeholder' => 'Add Mealtran*', 'id' => 'addmeal'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][tpotabsence]', old($mealtran_details->id.'[tpotabsence]', $mealtran_details->tpotabsence), ['class' => 'form-control input-sm', 'placeholder' => 'Pot Absence*', 'id' => 'tpotabsence'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td>
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][amount]', old($mealtran_details->id.'[amount]', $mealtran_details->amount), ['class' => 'form-control input-sm', 'id' => 'amount'.$mealtran_details->id, 'placeholder' => 'Amount*', 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$mealtran_details->id.'][amountpajak]', old($mealtran_details->id.'[amountpajak]', $mealtran_details->amountpajak), ['class' => 'form-control input-sm', 'placeholder' => 'Amount Pajak*', 'id' => 'amountpajak'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$mealtran_details->id.'(); total();']) }} 
 													</td>  
 												</td>  
 											</tr>  
 											@endforeach

 										</tbody>
 									</table>
 								</div> 
 								<!-- /.box-body -->
 							</div>
 						</div> 
 					</div> 
 					<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
 					<a href="{{ URL::route('editor.mealtran.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
 				</div>
 				{!! Form::close() !!} 
 			</div>
 		</div>
 	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#mealtranTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		} 
		);
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_mealtran').submit(); 
					}
				},

			}
		});
	});
</script>
@stop

