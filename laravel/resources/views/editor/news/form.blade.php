 
@extends('layouts.editor.template')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-8">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($news))
				{!! Form::model($news, array('route' => ['editor.news.update', $news->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_news'))!!}
				@else
				{!! Form::open(array('route' => 'editor.news.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_news'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							News
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('title', 'Title') }}
								{{ Form::text('title', old('title'), array('class' => 'form-control', 'placeholder' => 'Title*', 'required' => 'true', 'id' => 'title')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group"> 
								{{ Form::label('date', 'Date') }} 
								 <div class="input-group">
	                  				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'true', 'id' => 'date')) }} 
								</div>
							</div>
						</div>    

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('attachment', 'Attachment') }}<br>
								<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
								<br/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.news.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
			{!! Form::close() !!}
		</div>
	</div>
</section>
@stop

 