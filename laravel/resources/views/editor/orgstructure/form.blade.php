 
@extends('layouts.editor.template')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-sitemap"></i> Organization Structure
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Organization Structure</li>
  </ol>
</section>


<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-8">
			<div class="box box-danger">
				@include('errors.error')
				{!! Form::model($orgstructure, array('route' => ['editor.orgstructure.update'], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_orgstructure'))!!} 
				{{ csrf_field() }} 
				
				<div class="box-header with-border">
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('attachment', 'File') }}<br>
							<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
							<br/>
						</div>
					</div>
					<div class="col-md-6">
						<br>
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button> 
						</div>
					</div>
				</div>

				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-12"> 
						 <img src="{{Config::get('constants.path.uploads')}}/orgstructure/{{$orgstructure->attachment}}" alt="" style="width: 90%" />
						</div>
					</div> 	
				</div>
			</section><!-- /.content -->
			{!! Form::close() !!}
		</div>
	</div>
</section>
@stop
 