 @extends('layouts.editor.template')
 @section('content')
 <style type="text/css">


  #detailModals .modal-dialog
  {
    width: 60%;
  }
  th { font-size: 13px; }
  td { font-size: 12px; }
</style>
<!-- Updated stylesheet url -->
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<!-- Updated JavaScript url -->
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>

<!-- Content Wrapper. Contains page content --> 
<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    <i class="fa fa-adjust"></i> Overtime
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Overtime</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        {!! Form::model($overtime, array('route' => ['editor.overtime.update', $overtime->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_employee'))!!}
        {{ csrf_field() }}
        <!--  Hidden element -->
        <input type="hidden" value="" id="idtrans" name="idtrans">
        <input type="hidden" value="" id="status" name="status">
        <div class="box-header with-border">
          <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-4">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">  
                  {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'datetrans', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Plan Work</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="planwork" name="planwork" onchange="saveheader();">{{$overtime->planwork}}</textarea>
              </div>
            </div>
          </div>
          <!-- Coloumn 2-->   
          <div class="col-md-4"> 
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Date From</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('datefrom', old('datefrom'), array('class' => 'form-control', 'placeholder' => 'Date From*', 'required' => 'true', 'id' => 'datefrom', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Date To</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('dateto', old('dateto'), array('class' => 'form-control', 'placeholder' => 'Date To*', 'required' => 'true', 'id' => 'dateto', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
          </div> 
          <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Actual Work</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="actualwork" name="actualwork" onchange="saveheader();">{{$overtime->actualwork}}</textarea>
              </div>
            </div>
        </div>
        <!-- Coloumn 3-->                                   
        <div class="col-md-4">  
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Time From</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>  
                  {{ Form::text('timefrom', old('timefrom'), array('class' => 'form-control', 'placeholder' => 'Time From*', 'required' => 'true', 'id' => 'timefrom', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
          </div> 

         <div class="bootstrap-timepicker"> 
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Time To</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>  
                  {{ Form::text('timeto', old('timeto'), array('class' => 'form-control timepicker', 'placeholder' => 'Time To*', 'required' => 'true', 'id' => 'timeto', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
          </div> 
        </div>

        <div class="form-group">
            <label for="location" class="col-sm-3 control-label">Location</label>
            <div class="col-sm-9">
              <select class="form-control" id="location" name="location">
                <option selected="selected" value="{{$overtime->location}}">{{$overtime->location}}</option>
                <option value="Luar Kantor">Luar Kantor</option>
                <option value="Produksi">Produksi</option>
                <option value="Lain-lain">Lain-lain</option>
              </select>
            </div>
        </div>
      </div>
    </div><!-- /.box-header -->

               
    <div class="box-body">
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>NIK</label>
          <div class="input-group">
            <input type="text" class="form-control pull-right" id="nik" name="nik" data-toggle="tooltip" data-placement="nik" value="">
            <span class="input-group-addon"><a href="#"  onclick="reload_table_detail();" data-toggle="modal" data-easein="swoopIn" data-target=".detailModals"><i class="fa fa-folder"></i></a></span>
          </div><!-- /.input group -->
        </div> 
      </div> 

      <div class="col-md-3" style="margin-right: 5px">
        <div class="form-group">
          <label>Employee Name</label>
          {{ Form::hidden('employeeid', old('employeeid'), array('id' => 'employeeid')) }}
          <input type="text" value="" class="form-control" id="employeename" name="employeename">
        </div> 
      </div>
  
      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
         <label>Action</label><br/>
         <a href="#" onclick="savedetail();" type="button" class="btn btn-success btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-plus"></i> Add</a>
       </div>   
     </div> 
   </div>  
   <div class="box-body">  
     <table id="dtTable" class="table table-bordered table-hover stripe">
      <thead>
       <tr>  
        <th>NIK</th> 
        <th>Employee Name</th>  
        <th>Action</th> 
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> 
</div>  
<hr style="margin-top: -10px">   
<div class="box-header with-border">
  <div class="col-md-6">
    <div class="form-group">
      <label for="real_name" class="col-sm-3 control-label">Remark</label>
      <div class="col-sm-9">
        <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();"> {{$overtime->remark}}</textarea>
      </div>
    </div>
  </div>

  <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>   
  <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveheader();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a>
  <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a>
  <a href="{{ URL::route('editor.overtime.index') }}" type="button" class="btn btn-default btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
  <iframe src='' height="0" width="0" frameborder='0' name="print_frame"></iframe>

</div>
{!! Form::close() !!}
</div>
</section><!-- /.content -->  

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid" value="{{$overtime->id}}">
          <option ondblclick="showslip()" value="overtime">SPK</option>
        </select>
      </div>
      <div class="modal-footer">
        <a href="#" onclick="showslip();" class="btn btn-primary btn-flat"> <i class="fa fa-search"></i> Preview</a> 
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Popup -->
<div class="modal fade detailModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="detailModals" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <h3> <i class="fa fa-user"></i> Employee Master</h3>
     </div>
     <div class="modal-body">
      <table id="detailTable" class="table table-bordered table-hover stripe">
        <thead>
         <tr>  
           <th>#</th>
           <th>NIK</th>
           <th>Employee Name</th>    
           <th>Position</th> 
           <th>Action</th>   
         </tr>
       </thead>
       <tbody>
       </tbody>
     </table> 
   </div>
   <div class="modal-footer">  
    <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
  </div>
</div>
</div>
</div>
@stop

@section('scripts')

<script type="text/javascript">

  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "120px",
          "rowReorder": true,
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
          ajax: "{{ url('editor/overtime/datadetail') }}/{{$overtime->id}}",
          
          columns: [   
          
          { data: 'nik', name: 'nik' },
          { data: 'employeename', name: 'employeename' }, 
          { data: 'action', name: 'action', orderable: false, searchable: false }
          ]
        });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        }); 
      });
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
       $("#btnSave").attr("onclick","save()");
       $("#btnSaveAdd").attr("onclick","saveadd()");

       $('.errorMaterial UsedName').addClass('hidden');

       save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Asset Request'); // Set Title to Bootstrap modal title
      } 

      function reload_table_detail()
      {
        table_detail.ajax.reload(null,false); //reload datatable ajax 
      }

      var table_detail;
      $(document).ready(function() {
        //datatables
        table_detail = $('#detailTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/employee/datalookup') }}",
         columns: [  
         { data: 'id', name: 'id', orderable: false, searchable: false },
         { data: 'nik', name: 'nik' },
         { data: 'employeename', name: 'employeename' }, 
         { data: 'positionname', name: 'positionname' }, 
         { data: 'action', name: 'action', orderable: false, searchable: false }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });

      function addValue(str, id){
        var empid = id;
        var employeeid = $(str).closest('tr').find('td:eq(0)').text();
        var nik = $(str).closest('tr').find('td:eq(1)').text(); 
        var employeename = $(str).closest('tr').find('td:eq(2)').text(); 

        $("#employeeid").val(employeeid);
        $("#nik").val(nik);
        $("#employeename").val(employeename); 

        console.log(id);
        $('#detailModals').modal('toggle');
      }


      function saveheader(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../overtime/saveheader/{{$overtime->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'employeeid': $('#employeeid').val(),
            'datefrom': $('#datefrom').val(),
            'dateto': $('#dateto').val(),
            'timefrom': $('#timefrom').val(),
            'timeto': $('#timeto').val(),
            'remark': $('#remark').val(),
            'planwork': $('#planwork').val(),
            'actualwork': $('#actualwork').val(),
            'location': $('#location').val()
          },
          success: function(data) {  
            if ((data.errors)) { 
              toastr.error('Data is required!', 'Error Validation', options);
            } 
          },
        })
      };

      function savedetail(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../overtime/savedetail/{{$overtime->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'employeeid': $('#employeeid').val()
          },
          success: function(data) {

           var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully add detail data!', 'Success Alert', options);

          if ((data.errors)) { 
            toastr.error('Data is required!', 'Error Validation', options);
          } 
          reload_table_detail();
          reload_table();

          $('#employeeid').val(''),
          $('#employeename').val(''),
          $('#nik').val('')

        },
      })
      };

      function delete_id(id, employeename)
      {
        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : '../../overtime/deletedet/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }
    </script>

    <script type="text/javascript">
      $(function() {
       $('#timefrom').timepicker({timeFormat: 'G:i', show2400: true});
       $('#timeto').timepicker({timeFormat: 'G:i', show2400: true}); 
     });
    </script>

    <script type="text/javascript">
      function showslip()
      {
         var slipid = $("#slipid").value;
         var url = "{{ URL::route('editor.overtime.slip', $overtime->id) }}";
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
    </script>
    @stop
