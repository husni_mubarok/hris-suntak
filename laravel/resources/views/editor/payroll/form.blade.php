 @extends('layouts.editor.template')
 @section('content')


 <style type="text/css">
  .toolbar {
      float: left;
  }
  .input-smform {
    height: 22px;
    padding: 1px 3px;
    font-size: 12px;
    line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
    border-radius: 0px;
    width: 95% !important;
  }

  th,td  {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
  }

  .dataTables_scroll
  {
      overflow:auto;
  }

  .input-group-addon {
    padding: 3px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: 1px solid #ccc;
    border-radius: 4px;
}
 </style>

	 
<section class="content-header">
  <h1>
    <i class="fa fa-dollar"></i> Payroll
    <small>Payroll</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Payroll</a></li>
    <li class="active">Payroll</li>
  </ol>
</section>

<section class="content"> 
    <div class="row">
        <!-- <div class="col-xs-1">
        </div> -->
        <div class="col-sm-12">
            <div class="box box-danger">  
              {!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_payroll'))!!}
             {{ csrf_field() }}
              
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_payroll" class="table table-bordered table-hover stripe" style="background-color: #fff"> 
                							<thead>
                							 <tr> 
                                  <th rowspan="2">NIK</th> 
                                  <th rowspan="2">Employee</th> 
                                  <th rowspan="2">Dept</th>
                                  <th rowspan="2">Manual</th>
                                  <th rowspan="2">Hari Kerja</th>
                                  <th rowspan="2">Hari Masuk</th>
                                  <th rowspan="2">Gaji/Bulan</th>
                                  <th rowspan="2">Gaji Pokok</th>
                                  <th colspan="4"><center>Tunjangan</center></th> 
                                  <th colspan="3"><center>Overtime</center></th>
                                  <th colspan="3"><center>Overtime Holiday</center></th>
                                  <th rowspan="2">Allowance</th> 
                                  <th colspan="5"><center>Deduction</center></th> 
                                  <th rowspan="2">Correction</th>
                                  <th rowspan="2">Total</th>
                                  <th rowspan="2">Print Slip</th>
                                </tr>
                                <tr>
                                  <th>T. Makan</th>
                                  <th>Makan (Rp)</th>
                                  <th>T. Transport</th>
                                  <th>Transport (Rp)</th>
                                  <th>OT Hour</th>
                                  <th>OT Hour Con</th>
                                  <th>OT (Rp)</th>
                                  <th>OT Hour</th>
                                  <th>OT Hour Con</th>
                                  <th>OT (Rp)</th>
                                  <th>Absen</th>
                                  <th>Jamsostek</th>
                                  <th>BPJS &nbsp; &nbsp;</th>
                                  <th>Pinjaman</th>
                                  <th>PPH 21</th>
                                </tr>
    							             </thead> 
                                <tbody> 
                                    @foreach($payroll_detail as $key => $payroll_details)
                                    <tr> 
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->nik}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->employeename}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->departmentname}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                          <input type="checkbox" id="manual" name="detail[{{$payroll_details->id}}][manual]" @if($payroll_details->manual == 1) checked @endif> 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->dayjobdef}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                          <div class="input-group">
                                           {{ Form::text('detail['.$payroll_details->id.'][dayjob]', old($payroll_details->dayjob.'[dayjob]', $payroll_details->dayjob), ['id' => 'dayjob'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_basic('.$payroll_details->id.')']) }} 
                                            
                                            <span class="input-group-addon"><a  href="javascript:void(0)" onclick="absence({{$payroll_details->employeeid}}, {{$payroll_details->periodid}})"><i class="fa fa-clock-o"></i></a></span>  
                                        </div>

                                        </td>  
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->basicemployee}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                          {{ Form::text('detail['.$payroll_details->id.'][basic]', old($payroll_details->basic.'[basic]', $payroll_details->basic), ['id' => 'basic'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_basic('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][mealtransall]', old($payroll_details->mealtransall.'[mealtransall]', $payroll_details->mealtransall), ['id' => 'mealtransall'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_mealtransall('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][mealtrans]', old($payroll_details->mealtrans.'[mealtrans]', $payroll_details->mealtrans), ['id' => 'mealtrans'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_mealtrans('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][transportall]', old($payroll_details->transportall.'[transportall]', $payroll_details->transportall), ['id' => 'transportall'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_transportall('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][transport]', old($payroll_details->transport.'[transport]', $payroll_details->transport), ['id' => 'transport'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_transport('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehouractual,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehour,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtime,0) }}  
                                        </td> 


                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehourholidayactual,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehourholiday,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimeholiday,0) }}  
                                        </td> 


                                         <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][insentive]', old($payroll_details->insentive.'[insentive]', $payroll_details->insentive), ['id' => 'insentive'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_insentive('.$payroll_details->id.')']) }} 
                                        </td>  

                                         <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][absence]', old($payroll_details->absence.'[absence]', $payroll_details->absence), ['id' => 'absence'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_absence('.$payroll_details->id.')']) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][jamsostek]', old($payroll_details->jamsostek.'[jamsostek]', $payroll_details->jamsostek), ['id' => 'jamsostek'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_jamsostek('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][bpjs]', old($payroll_details->bpjs.'[bpjs]', $payroll_details->bpjs), ['id' => 'bpjs'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_bpjs('.$payroll_details->id.')']) }} 
                                        </td> 

                                         <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][totalloan]', old($payroll_details->totalloan.'[totalloan]', $payroll_details->totalloan), ['id' => 'totalloan'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_totalloan('.$payroll_details->id.')']) }} 
                                        </td> 

                                         <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][pph21]', old($payroll_details->pph21.'[pph21]', $payroll_details->pph21), ['id' => 'pph21'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_pph21('.$payroll_details->id.')']) }} 
                                        </td>

                                         <td class="col-sm-1 col-md-1">
                                            {{ Form::text('detail['.$payroll_details->id.'][correction]', old($payroll_details->correction.'[correction]', $payroll_details->correction), ['id' => 'correction'.$payroll_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_correction('.$payroll_details->id.')']) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->totalnetto,0) }}  
                                        </td>  
                                        </td>   
                                    </td>  
                                    <td><a href="#" onclick="showslip({{$payroll_details->id}});"><i class="fa fa-print"></i> Print</a></td>
                                </tr>  
                                @endforeach

                            </tbody>
              					</table> 
              				    <!-- /.box-body -->
                         <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a> 
                        <a href="{{ URL::route('editor.payroll.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
					         </div>
                {!! Form::close() !!}     
          </div>
			</div> 
		</div>  
</section><!-- /.content -->
       
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_absence" role="dialog">
  <div class="modal-dialog" style="width:85% !important">
    <div class="modal-content">
       <div class="modal-body">
          <table id="dtAbsence" class="table table-bordered table-hover stripe">
            <thead>
              <tr> 
                <th>Date</th>
                <th>Type</th>
                <th>Actual In</th>
                <th>Actual Out</th>
                <th>Permite In</th>
                <th>Permite Out</th>
                <th>Overtime In</th>
                <th>Overtime Out</th>
                <th>Overtime Hour</th>
                <th>Overtime Conversion</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@stop 



@section('scripts')
 
<script>
  var table;
  $(document).ready(function() {
    table = $('#dtAbsence').DataTable({ 
      //editor 
     colReorder: true,
     fixedHeader: true, 
     responsive: true,
     //rowReorder: true, 
     "rowReorder": {
        "update": false,
     },
     processing: true,
     serverSide: true,
     "pageLength": 25,
     "scrollY": "360px",
     "rowReorder": true,
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax: "{{ url('editor/payroll/dataabsence') }}",
     columns: [  
     { data: 'datein', name: 'datein' }, 
     { data: 'absencetypename', name: 'absencetypename' }, 
     { data: 'actualin', name: 'actualin' }, 
     { data: 'actualout', name: 'actualout' }, 
     { data: 'permitein', name: 'permitein' }, 
     { data: 'permiteout', name: 'permiteout' }, 
     { data: 'overtimein', name: 'overtimein' }, 
     { data: 'overtimeout', name: 'overtimeout' }, 
     { data: 'overtimehouractual', name: 'overtimehouractual' }, 
     { data: 'overtimehour', name: 'overtimehour' }, 
     ]
    });
  });

 function absence(employeeid, periodid)
   { 
    console.log(employeeid);
    console.log(periodid);
      var url;
      url = "{{ URL::route('editor.payrollabsence.store') }}";
       $.ajax({
        type: 'POST',
        url: url,
        data: {
          '_token': $('input[name=_token]').val(), 
          'employeeid': employeeid, 
          'periodid': periodid
        },
      });
      table.ajax.reload(null,false); //reload datatable ajax 

      $('#modal_absence').modal('show'); // show bootstrap modal when complete loaded
    }

  $(document).ready(function () {
    $("#payrollTable").DataTable(
    {
      "language": {
        "emptyTable": "-"
      }
    } 
    );
  });
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_payroll').submit(); 
          }
        },

      }
    });
  });

    $(document).ready(function() { 
        $("#table_payroll").dataTable( {
            "sScrollX": true,
             "scrollY": "380px",
             "scrollX": true,
             "sScrollXInner": "200%",
             "bPaginate": false,
             // "sScrollXInner": "140%",
             "dom": '<"toolbar">frtip',
             // "ordering": false,
             fixedColumns:   {
              leftColumns: 3,
              rightColumns : 1
             },
        });
         $("div.toolbar").html('<b>Period : {{$payroll->description}}</b>');
    });
</script>

<script type="text/javascript">
      function showslip(id)
      {
        console.log(id);
       var url = '../../payroll/slip/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }

       function showabsence(id)
      {
        console.log(id);
       var url = '../../payroll/absence/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 

      function cal_sparator_insentive(id) {  
        //um 2
        var insentive = document.getElementById('insentive' + id).value;
        var result2 = document.getElementById('insentive' + id);
        var rsamount2 = (insentive);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('insentive' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='insentive' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_basic(id) {  
        //um 2
        var basic = document.getElementById('basic' + id).value;
        var result2 = document.getElementById('basic' + id);
        var rsamount2 = (basic);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('basic' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='basic' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_mealtransall(id) {  
        //um 2
        var mealtransall = document.getElementById('mealtransall' + id).value;
        var result2 = document.getElementById('mealtransall' + id);
        var rsamount2 = (mealtransall);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('mealtransall' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='mealtransall' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_mealtrans(id) {  
        //um 2
        var mealtrans = document.getElementById('mealtrans' + id).value;
        var result2 = document.getElementById('mealtrans' + id);
        var rsamount2 = (mealtrans);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('mealtrans' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='mealtrans' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_transportall(id) {  
        //um 2
        var transportall = document.getElementById('transportall' + id).value;
        var result2 = document.getElementById('transportall' + id);
        var rsamount2 = (transportall);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('transportall' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='transportall' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_transport(id) {  
        //um 2
        var transport = document.getElementById('transport' + id).value;
        var result2 = document.getElementById('transport' + id);
        var rsamount2 = (transport);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('transport' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='transport' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 

      function cal_sparator_insentive(id) {  
        //um 2
        var insentive = document.getElementById('insentive' + id).value;
        var result2 = document.getElementById('insentive' + id);
        var rsamount2 = (insentive);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('insentive' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='insentive' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_absence(id) {  
        //um 2
        var absence = document.getElementById('absence' + id).value;
        var result2 = document.getElementById('absence' + id);
        var rsamount2 = (absence);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('absence' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='absence' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }    


      function cal_sparator_jamsostek(id) {  
        //um 2
        var jamsostek = document.getElementById('jamsostek' + id).value;
        var result2 = document.getElementById('jamsostek' + id);
        var rsamount2 = (jamsostek);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('jamsostek' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='jamsostek' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }   


      function cal_sparator_bpjs(id) {  
        //um 2
        var bpjs = document.getElementById('bpjs' + id).value;
        var result2 = document.getElementById('bpjs' + id);
        var rsamount2 = (bpjs);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('bpjs' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='bpjs' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_totalloan(id) {  
        //um 2
        var totalloan = document.getElementById('totalloan' + id).value;
        var result2 = document.getElementById('totalloan' + id);
        var rsamount2 = (totalloan);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('totalloan' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='totalloan' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }  


      function cal_sparator_pph21(id) {  
        //um 2
        var pph21 = document.getElementById('pph21' + id).value;
        var result2 = document.getElementById('pph21' + id);
        var rsamount2 = (pph21);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('pph21' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='pph21' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }    


      function cal_sparator_correction(id) {  
        //um 2
        var correction = document.getElementById('correction' + id).value;
        var result2 = document.getElementById('correction' + id);
        var rsamount2 = (correction);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('correction' + id);

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='correction' + id)  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }     
  </script>
@stop

