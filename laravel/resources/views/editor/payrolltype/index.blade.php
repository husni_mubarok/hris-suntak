@extends('layouts.editor.template')

@section('content')

<!-- Updated stylesheet url -->
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<!-- Updated JavaScript url -->
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>

<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-tag"></i> Reguler Shift
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Reguler Shift</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th rowspan="2">#</th>
                  <th rowspan="2" style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label> 
                  </th>
                  <th rowspan="2">Action</th> 
                  <th rowspan="2">Reguler Shift</th> 
                  <th colspan="2"><center>Monday - Thursday</center></th>
                  <th colspan="2"><center>Friday</center></th>
                  <!-- <th>Day Out</th> -->
                  <!-- <th>Sign In</th>
                  <th>Sign Out</th> -->
                  <th rowspan="2">Status</th>
                </tr>
                <tr>
                  <th>Day In</th>
                  <th>Day Out</th>
                  <th>Day In</th>
                  <th>Day Out</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Reguler Shift Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
 
            <div class="form-group">
              <label class="control-label col-md-3">Reguler Shift</label>
              <div class="col-md-8">
                <input name="payrolltypename" id="payrolltypename" class="form-control" type="text">
                <small class="errorPayrolltypename hidden alert-danger"></small> 
              </div>
            </div> 

            <div class="col-md-12">
              <p>Monday - Thursday</p>
              <hr style="margin-top: -10px">
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Day In</label>
              <div class="col-md-8">
                <div class='input-group date'>
                    <input type='text' class="form-control timepicker" id='dayin' name='dayin'/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 
                <small class="errorDayin hidden alert-danger"></small> 
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Day Out</label>
              <div class="col-md-8"> 
                <div class='input-group date'>
                    <input type='text' class="form-control" id='dayout' name='dayout'/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 
                <small class="errorDayin hidden alert-danger"></small>  
              </div>
            </div> 

            <div class="col-md-12">
              <p>Friday</p>
              <hr style="margin-top: -10px">
            </div>


            <div class="form-group">
              <label class="control-label col-md-3">Day In</label>
              <div class="col-md-8">  
                <div class='input-group date'>
                    <input type='text' class="form-control" id='dayinfriday' name='dayinfriday'/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 
                <small class="errorDayin hidden alert-danger"></small>  
              </div> 
            </div>  

            <div class="form-group">
              <label class="control-label col-md-3">Day Out</label>
              <div class="col-md-8">  
                <div class='input-group date'>
                    <input type='text' class="form-control" id='dayoutfriday' name='dayoutfriday'/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>  
                <small class="errorSignout hidden alert-danger"></small> 
              </div>
            </div>  
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         //editor 
         colReorder: true,
         fixedHeader: true, 
         responsive: true,
         //rowReorder: true, 
         "rowReorder": {
            "update": false,
        },  
         //dttables
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/payrolltype/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'payrolltypename', name: 'payrolltypename' },
         { data: 'dayin', name: 'dayin' },
         { data: 'dayout', name: 'dayout' },

         { data: 'dayinfriday', name: 'dayinfriday' },
         { data: 'dayoutfriday', name: 'dayoutfriday' },
         // { data: 'dayinfriday', name: 'dayinfriday' }, 
         // { data: 'dayoutfriday', name: 'dayoutfriday' },  
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorPayrolltypename').addClass('hidden');
        $('.errorSignin').addClass('hidden');
        $('.errorSignout').addClass('hidden');
        $('.errorDayin').addClass('hidden');
        $('.errorDayout').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Reguler Shift'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.payrolltype.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'payrolltypename': $('#payrolltypename').val(), 
            'dayin': $('#dayin').val(), 
            'dayout': $('#dayout').val(), 
            'dayinfriday': $('#dayinfriday').val(),
            'dayoutfriday': $('#dayoutfriday').val(),
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorPayrolltypename').addClass('hidden');
            $('.errorSignin').addClass('hidden');
            $('.errorSignout').addClass('hidden');
            $('.errorDayin').addClass('hidden');
            $('.errorDayout').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.payrolltypename) {
                $('.errorPayrolltypename').removeClass('hidden');
                $('.errorPayrolltypename').text(data.errors.payrolltypename);
              }
              if (data.errors.dayin) {
                $('.errorDayin').removeClass('hidden');
                $('.errorDayin').text(data.errors.dayin);
              }
              if (data.errors.dayout) {
                $('.errorDayout').removeClass('hidden');
                $('.errorDayout').text(data.errors.dayout);
              }
              if (data.errors.dayinfriday) {
                $('.errorSignin').removeClass('hidden');
                $('.errorSignin').text(data.errors.dayinfriday);
              }
              if (data.errors.dayoutfriday) {
                $('.errorSignout').removeClass('hidden');
                $('.errorSignout').text(data.errors.dayoutfriday);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.payrolltype.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'payrolltypename': $('#payrolltypename').val(), 
          'dayin': $('#dayin').val(), 
          'dayout': $('#dayout').val(), 
          'dayinfriday': $('#dayinfriday').val(), 
          'dayoutfriday': $('#dayoutfriday').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorPayrolltypename').addClass('hidden'); 
            $('.errorSignin').addClass('hidden');
            $('.errorSignout').addClass('hidden');
            $('.errorDayin').addClass('hidden');
            $('.errorDayout').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.payrolltypename) {
                $('.errorPayrolltypename').removeClass('hidden');
                $('.errorPayrolltypename').text(data.errors.payrolltypename);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);

                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
     };

     function edit(id)
     { 

      $('.errorPayrolltypename').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'payrolltype/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="payrolltypename"]').val(data.payrolltypename);
            $('[name="dayin"]').val(data.dayin);
            $('[name="dayout"]').val(data.dayout);
            $('[name="dayinfriday"]').val(data.dayinfriday);
            $('[name="dayoutfriday"]').val(data.dayoutfriday);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Reguler Shift'); // Set title to Bootstrap modal title
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'payrolltype/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'payrolltypename': $('#payrolltypename').val(), 
            'dayin': $('#dayin').val(), 
            'dayout': $('#dayout').val(),  
            'dayinfriday': $('#dayinfriday').val(),  
            'dayoutfriday': $('#dayoutfriday').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorPayrolltypename').addClass('hidden'); 
            $('.errorSignin').addClass('hidden');
            $('.errorSignout').addClass('hidden');
            $('.errorDayin').addClass('hidden');
            $('.errorDayout').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.payrolltypename) {
                $('.errorPayrolltypename').removeClass('hidden');
                $('.errorPayrolltypename').text(data.errors.payrolltypename);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'payrolltype/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'payrolltypename': $('#payrolltypename').val(), 
            'dayin': $('#dayin').val(), 
            'dayout': $('#dayout').val(),  
            'dayinfriday': $('#dayinfriday').val(),  
            'dayoutfriday': $('#dayoutfriday').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                    $("#btnSave").attr("onclick","save()");
                    $("#btnSaveAdd").attr("onclick","saveadd()");
                  } 
                },
              })
      };

      function delete_id(id, payrolltypename)
      {

        //var varnamre= $('#payrolltypename').val();
        var payrolltypename = payrolltypename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + payrolltypename + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'payrolltype/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function bulk_delete()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-red',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "payrolltype/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error deleting data!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }

    $(".timepicker").timepicker({
      showInputs: false
    });
   </script> 

  
<script type="text/javascript">
   $(function() {
   $('#dayin').timepicker({timeFormat: 'G:i', show2400: true});
   $('#dayout').timepicker({timeFormat: 'G:i', show2400: true});
   $('#dayinfriday').timepicker({timeFormat: 'G:i', show2400: true});
   $('#dayoutfriday').timepicker({timeFormat: 'G:i', show2400: true});
 });
</script>
   @stop
