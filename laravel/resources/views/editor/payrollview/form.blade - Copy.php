 @extends('layouts.editor.template')
 @section('content')


 <style type="text/css">
 .input-sm {
      height: 22px;
      padding: 1px 3px;
      font-size: 12px;
      line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
      border-radius: 0px;
      /*width: 90% !important;*/
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>

	 
<section class="content-header">
  <h1>
    <i class="fa fa-dollar"></i> Payroll
    <small>Payroll</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Payroll</a></li>
    <li class="active">Payroll</li>
  </ol>
</section>

<section class="content">
   <!-- <div class="row"> 
      <div class="col-xs-4">
        <div class="box box-danger">   
            <div class="box-header with-border">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-3">PERIOD</label>
                  <div class="col-md-8">
                    : {{$payroll->description}}
                  </div>
                </div>
              </div>
            </div> 
        </div>
      </div>
    </div> -->
    <div class="row">
        <!-- <div class="col-xs-1">
        </div> -->
        <div class="col-sm-12">
            <div class="box box-danger">  
              {!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_payroll'))!!}
             {{ csrf_field() }}
              
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_payroll" class="table table-bordered table-hover stripe" style="background-color: #fff"> 
                							<thead>
                							  <tr> 
                                  <th rowspan="2">NIK</th> 
                                  <th rowspan="2">Employee</th> 
                                  <th rowspan="2">Dept</th>
                                  <th rowspan="2">Position</th>
                                  <th rowspan="2">Gol</th>
                                  <th rowspan="2">Day Job</th>
                                  <th rowspan="2">Basic</th>
                                  <th colspan="4"><center>Insentive</center></th> 
                                  <th colspan="3"><center>Overtime</center></th>
                                  <th rowspan="2">UM OT</th> 
                                  <th colspan="3"><center>Premi</center></th> 
                                  <th rowspan="2">Less</th> 
                                  <th rowspan="2">Total Bruto</th> 
                                  <th colspan="7"><center>Deduction</center></th> 
                                  <th rowspan="2">Total Ded</th>
                                  <th rowspan="2">Total</th>
                                  <th rowspan="2">Print Slip</th>
                                </tr>
                                <tr>
                                  <th>Meal Rate</th>
                                  <th>Meal Amt</th>
                                  <th>Trans Rate</th>
                                  <th>Trans Amt</th>
                                  <th>Reguler</th>
                                  <th>Day 6</th>
                                  <th>Holiday</th>
                                  <th>Night</th>
                                  <th>Sunday</th>
                                  <th>Production</th>
                                  <th>Absent</th>
                                  <th>BPJS &nbsp; &nbsp;</th>
                                  <th>PPH 21</th>
                                  <th>Persekot</th>
                                  <th>Koperasi</th>
                                  <th>Loan</th>
                                  <th>Serikat</th>
                                </tr>
    							             </thead> 
                                <tbody> 
                                    @foreach($payroll_detail as $key => $payroll_details)
                                    <tr>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->nik}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->employeename}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->departmentname}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->positionname}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$payroll_details->gol}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            <a  href="javascript:void(0)" onclick="absence({{$payroll_details->employeeid}}, {{$payroll_details->periodid}})">{{$payroll_details->dayjob}}</a>
                                        </td>  

                                        <td class="col-sm-1 col-md-1">
                                          {{ number_format($payroll_details->basic,0) }}
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->mealtransall,0) }}
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->mealtrans,0) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->transportall,0) }} 
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->transport,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehouractual,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtimehour,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->overtime,0) }}  
                                        </td> 


                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->insentive,0) }}
                                        </td>  

                                        <!-- premi -->
                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->preminight,0) }}  
                                        </td> 
                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->premisunday,0) }}  
                                        </td> 
                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->premiproduction,0) }}  
                                        </td> 


                                        <!-- kekurangan dan bruto -->
                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->lessamount,0) }}  
                                        </td> 
                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->basic + $payroll_details->mealtrans + $payroll_details->transport + $payroll_details->overtime + $payroll_details->insentive + $payroll_details->preminight + $payroll_details->premisunday + $payroll_details->premiproduction,0) }}  
                                        </td> 


                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->absence,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->bpjs,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->pph21,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->persekot,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->koperasi,0) }}  
                                        </td> 

                                         <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->totalloan,0) }}  
                                        </td>  

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->serikat,0) }}   
                                        </td>  

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format($payroll_details->absence + $payroll_details->bpjs + $payroll_details->pph21 + $payroll_details->persekot + $payroll_details->koperasi + $payroll_details->totalloan + $payroll_details->serikat,0) }}  
                                        </td> 

                                        <td class="col-sm-1 col-md-1">
                                            {{ number_format(
                                              ($payroll_details->basic + $payroll_details->mealtrans + $payroll_details->transport + $payroll_details->overtime + $payroll_details->insentive + $payroll_details->preminight + $payroll_details->premisunday + $payroll_details->premiproduction) 
                                              - 
                                              ($payroll_details->absence + $payroll_details->bpjs + $payroll_details->pph21 + $payroll_details->persekot + $payroll_details->koperasi + $payroll_details->totalloan + $payroll_details->serikat)
                                              ,0) }}  
                                        </td>  
                                        </td>   
                                    </td>  
                                    <td><a href="#" onclick="showslip({{$payroll_details->id}});"><i class="fa fa-print"></i> Print</a></td>
                                </tr>  
                                @endforeach

                            </tbody>
              					</table> 
              				    <!-- /.box-body -->
                        <a href="{{ URL::route('editor.payrollview.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
					         </div>
                {!! Form::close() !!}     
          </div>
			</div> 
		</div>  
</section><!-- /.content -->
       
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_absence" role="dialog">
  <div class="modal-dialog" style="width:85% !important">
    <div class="modal-content">
       <div class="modal-body">
          <table id="dtAbsence" class="table table-bordered table-hover stripe">
            <thead>
              <tr> 
                <th>Date</th>
                <th>Type</th>
                <th>Actual In</th>
                <th>Actual Out</th>
                <th>Permite In</th>
                <th>Permite Out</th>
                <th>Overtime In</th>
                <th>Overtime Out</th>
                <th>Overtime Hour</th>
                <th>Overtime Conversion</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@stop 



@section('scripts')
 
<script>
  var table;
  $(document).ready(function() {
    table = $('#dtAbsence').DataTable({ 
      //editor 
     colReorder: true,
     fixedHeader: true, 
     responsive: true,
     //rowReorder: true, 
     "rowReorder": {
        "update": false,
     },
     processing: true,
     serverSide: true,
     "pageLength": 25,
     "scrollY": "360px",
     "rowReorder": true,
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax: "{{ url('editor/payroll/dataabsence') }}",
     columns: [  
     { data: 'datein', name: 'datein' }, 
     { data: 'absencetypename', name: 'absencetypename' }, 
     { data: 'actualin', name: 'actualin' }, 
     { data: 'actualout', name: 'actualout' }, 
     { data: 'permitein', name: 'permitein' }, 
     { data: 'permiteout', name: 'permiteout' }, 
     { data: 'overtimein', name: 'overtimein' }, 
     { data: 'overtimeout', name: 'overtimeout' }, 
     { data: 'overtimehouractual', name: 'overtimehouractual' }, 
     { data: 'overtimehour', name: 'overtimehour' }, 
     ]
    });
  });

 function absence(employeeid, periodid)
   { 
    console.log(employeeid);
    console.log(periodid);
      var url;
      url = "{{ URL::route('editor.payrollabsence.store') }}";
       $.ajax({
        type: 'POST',
        url: url,
        data: {
          '_token': $('input[name=_token]').val(), 
          'employeeid': employeeid, 
          'periodid': periodid
        },
      });
      table.ajax.reload(null,false); //reload datatable ajax 

      $('#modal_absence').modal('show'); // show bootstrap modal when complete loaded
    }

  $(document).ready(function () {
    $("#payrollTable").DataTable(
    {
      "language": {
        "emptyTable": "-"
      }
    } 
    );
  });
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_payroll').submit(); 
          }
        },

      }
    });
  });

    $(document).ready(function() { 
        $("#table_payroll").dataTable( {
            "sScrollX": true,
             "scrollY": "380px",
             "scrollX": true,
             "sScrollXInner": "150%",
             "bPaginate": false,
             "sScrollXInner": "150%",
             "dom": '<"toolbar">frtip',
             fixedColumns:   {
              leftColumns: 3,
              rightColumns : 1
             },
        });
         $("div.toolbar").html('<b>Period : {{$payroll->description}}</b>');
    });
</script>

<script type="text/javascript">
      function showslip(id)
      {
        console.log(id);
       var url = '../../payroll/slip/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }

       function showabsence(id)
      {
        console.log(id);
       var url = '../../payroll/absence/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
 
    </script>
@stop

