 @extends('layouts.editor.template')
 @section('content')


 <style type="text/css">
 .input-sm {
      height: 22px;
      padding: 1px 3px;
      font-size: 12px;
      line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
      border-radius: 0px;
      /*width: 90% !important;*/
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>

	 
<section class="content-header">
  <h1>
    <i class="fa fa-dollar"></i> Payroll
    <small>Payroll</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Payroll</a></li>
    <li class="active">Payroll</li>
  </ol>
</section>

<section class="content">
    <div class="row">
        <!-- <div class="col-xs-1">
        </div> -->
        <div class="col-sm-12">
            <div class="box box-danger">  
              {!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_payroll'))!!}
             {{ csrf_field() }} 
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_payroll" class="table table-bordered table-hover stripe" style="background-color: #fff"> 
                							<thead>
                							  <th rowspan="2">NIK</th> 
                                  <th rowspan="2">Employee</th> 
                                  <th rowspan="2">Dept</th>
                                  <th rowspan="2">Manual</th>
                                  <th rowspan="2">Hari Kerja</th>
                                  <th rowspan="2">Hari Masuk</th>
                                  <th rowspan="2">Gaji/Bulan</th>
                                  <th rowspan="2">Gaji Pokok</th>
                                  <th colspan="4"><center>Tunjangan</center></th> 
                                  <th colspan="3"><center>Overtime</center></th>
                                  <th colspan="3"><center>Overtime Holiday</center></th>
                                  <th rowspan="2">Allowance</th> 
                                  <th colspan="5"><center>Deduction</center></th> 
                                  <th rowspan="2">Correction</th>
                                  <th rowspan="2">Total</th>
                                  <th rowspan="2">Action</th>
                                  <th rowspan="2">Print Slip</th>
                                </tr>
                                <tr>
                                  <th>T. Makan</th>
                                  <th>Makan (Rp)</th>
                                  <th>T. Transport</th>
                                  <th>Transport (Rp)</th>
                                  <th>OT Hour</th>
                                  <th>OT Hour Con</th>
                                  <th>OT (Rp)</th>
                                  <th>OT Hour</th>
                                  <th>OT Hour Con</th>
                                  <th>OT (Rp)</th>
                                  <th>Absen</th>
                                  <th>Jamsostek</th>
                                  <th>BPJS &nbsp; &nbsp;</th>
                                  <th>Pinjaman</th>
                                  <th>PPH 21</th>
                                </tr>
    							             </thead> 
                                <tbody> 
                                </tbody>
              					</table> 
              				    <!-- /.box-body -->
                        <a onClick="GenerateData()" class="btn btn-danger btn-flat pull-right" style="float:left; margin-right:2px"> <i class="fa fa-magic"></i> Generate</a>
                        <a onClick="RefreshData()" class="btn btn-success btn-flat pull-right" style="float:left; margin-right:2px"> <i class="fa fa-refresh"></i>  Refresh</a>
                        <a href="{{ URL::route('editor.payrollview.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
					         </div>
                {!! Form::close() !!}     
          </div>
			</div> 
		</div>  
</section><!-- /.content -->
       
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_absence" role="dialog">
  <div class="modal-dialog" style="width:85% !important">
    <div class="modal-content">
       <div class="modal-body">
          <table id="dtAbsence" class="table table-bordered table-hover stripe">
            <thead>
              <tr> 
                <th>Date</th>
                <th>Type</th>
                <th>Actual In</th>
                <th>Actual Out</th>
                <th>Permite In</th>
                <th>Permite Out</th>
                <th>Overtime In</th>
                <th>Overtime Out</th>
                <th>Overtime Hour</th>
                <th>Overtime Conversion</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
         <h3 class="modal-title">Correction Payroll</h3>
       </div>
       <div class="modal-body">
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">NIK</label>
              <div class="col-md-8">
                <input name="nik" id="nik" class="form-control" type="text" readonly>
                <small class="errorBasic hidden alert-danger"></small> 

              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Employee Name</label>
              <div class="col-md-8">
                <input name="employeename" id="employeename" class="form-control" type="text" readonly>
              </div>
            </div>  
            <div class="form-group">
              <label class="control-label col-md-3">Manual</label>
              <div class="col-md-8">
                <select class="form-control" name="manual"  id="manual">
                   <option value="0">No</option>
                   <option value="1">Yes</option>
                 </select>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Hari Kerja</label>
              <div class="col-md-8">
                <input name="dayjobdef" id="dayjobdef" class="form-control" type="text" readonly>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Hari Masuk</label>
              <div class="col-md-8">
                <input name="dayjob" id="dayjob" class="form-control" type="number">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Gaji/Bulan</label>
              <div class="col-md-8">
                <input name="basicemployee" id="basicemployee" class="form-control" type="text" readonly>
              </div>
            </div> 

             <div class="form-group">
              <label class="control-label col-md-3">Gaji Pokok</label>
              <div class="col-md-8">
                <input name="basic" id="basic" class="form-control" type="text" oninput="cal_sparator_basic();">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">T. Makan</label>
              <div class="col-md-8">
                <input name="mealtransall" id="mealtransall" class="form-control" type="text" oninput="cal_sparator_mealtransall();">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Makan (Rp)</label>
              <div class="col-md-8">
                <input name="mealtrans" id="mealtrans" class="form-control" type="text" oninput="cal_sparator_mealtrans();">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">T. Transport</label>
              <div class="col-md-8">
                <input name="transportall" id="transportall" class="form-control" type="text" oninput="cal_sparator_transportall();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">OT Hour Con.</label>
              <div class="col-md-8">
                <input name="overtimehour" id="overtimehour" class="form-control" type="number">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Transport (Rp)</label>
              <div class="col-md-8">
                <input name="transport" id="transport" class="form-control" type="text" oninput="cal_sparator_transport();">
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Allowance</label>
              <div class="col-md-8">
                <input name="insentive" id="insentive" class="form-control" type="text" oninput="cal_sparator_insentive()">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Deduction</label>
              <div class="col-md-8">
                <input name="absence" id="absence" class="form-control" type="text" oninput="cal_sparator_absence();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Jamsostek</label>
              <div class="col-md-8">
                <input name="jamsostek" id="jamsostek" class="form-control" type="text" oninput="cal_sparator_jamsostek();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">BPJS</label>
              <div class="col-md-8">
                <input name="bpjs" id="bpjs" class="form-control" type="text" oninput="cal_sparator_bpjs();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Pinjaman</label>
              <div class="col-md-8">
                <input name="totalloan" id="totalloan" class="form-control" type="text" oninput="cal_sparator_totalloan();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">PPh21</label>
              <div class="col-md-8">
                <input name="pph21" id="pph21" class="form-control" type="text" oninput="cal_sparator_pph21();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Koreksi</label>
              <div class="col-md-8">
                <input name="correction" id="correction" class="form-control" type="text" oninput="cal_sparator_correction();">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Total</label>
              <div class="col-md-8">
                <input name="totalnetto" id="totalnetto" class="form-control" type="text" readonly>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSave"  class="btn btn-primary-ghci btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@stop 
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
    table = $('#dtAbsence').DataTable({ 
      //editor 
     colReorder: true,
     fixedHeader: true, 
     responsive: true,
     //rowReorder: true, 
     "rowReorder": {
        "update": false,
     },
     processing: true,
     serverSide: true,
     "pageLength": 25,
     "scrollY": "360px",
     "rowReorder": true,
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax: "{{ url('editor/payroll/dataabsence') }}",
     columns: [  
     { data: 'datein', name: 'datein' }, 
     { data: 'absencetypename', name: 'absencetypename' }, 
     { data: 'actualin', name: 'actualin' }, 
     { data: 'actualout', name: 'actualout' }, 
     { data: 'permitein', name: 'permitein' }, 
     { data: 'permiteout', name: 'permiteout' }, 
     { data: 'overtimein', name: 'overtimein' }, 
     { data: 'overtimeout', name: 'overtimeout' }, 
     { data: 'overtimehouractual', name: 'overtimehouractual' }, 
     { data: 'overtimehour', name: 'overtimehour' }, 
     ]
    });
  });

  var table_payroll;
  $(document).ready(function() {
    var id = {{$payroll->id}}
    table_payroll = $('#table_payroll').DataTable({ 
     processing: true,
     serverSide: true,
     "sScrollX": true,
     "scrollY": "380px",
     "scrollX": true,
     "sScrollXInner": "150%",
     // "bPaginate": false,
     "sScrollXInner": "150%",
     "dom": '<"toolbar">frtip',
     fixedColumns:   {
      leftColumns: 4,
      rightColumns : 2
     },
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax: "{{ url('editor/payrollview/datadetail/') }}/" + id,
     columns: [  
     { data: 'nik', name: 'nik' }, 
     { data: 'employeename', name: 'employeename' }, 
     { data: 'departmentname', name: 'departmentname' }, 
     { data: 'manual', name: 'manual' }, 
     { data: 'dayjobdef', name: 'dayjobdef' }, 
     { data: 'dayjob', name: 'dayjob' }, 
     { data: 'basicemployee', name: 'basicemployee', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'mealtrans', name: 'mealtrans', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'transportall', name: 'transportall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'transport', name: 'transport', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'overtimehouractual', name: 'overtimehouractual' }, 
     { data: 'overtimehour', name: 'overtimehour' }, 
     { data: 'overtime', name: 'overtime', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'overtimehourholidayactual', name: 'overtimehourholidayactual' }, 
     { data: 'overtimehourholiday', name: 'overtimehourholiday' }, 
     { data: 'overtimeholiday', name: 'overtimeholiday', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'insentive', name: 'insentive', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'absence', name: 'absence', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'jamsostek', name: 'jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'bpjs', name: 'bpjs', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'totalloan', name: 'totalloan', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'pph21', name: 'pph21', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'correction', name: 'correction', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'totalnetto', name: 'totalnetto', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }, 
     { data: 'action', name: 'action' }, 
     { data: 'slip', name: 'slip' }, 
     ]
    });
  });

   function reload_table()
  {
    // table_payroll.ajax.reload(null,false); //reload datatable ajax 
    $('#table_payroll').DataTable().ajax.reload()
  }

 function absence(employeeid, periodid)
   { 
    console.log(employeeid);
    console.log(periodid);
      var url;
      url = "{{ URL::route('editor.payrollabsence.store') }}";
       $.ajax({
        type: 'POST',
        url: url,
        data: {
          '_token': $('input[name=_token]').val(), 
          'employeeid': employeeid, 
          'periodid': periodid
        },
      });
      table.ajax.reload(null,false); //reload datatable ajax 
      $('#modal_absence').modal('show'); // show bootstrap modal when complete loaded
    }

  $(document).ready(function () {
    $("#payrollTable").DataTable(
    {
      "language": {
        "emptyTable": "-"
      }
    } 
    );
  });
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_payroll').submit(); 
          }
        },
      }
    });
  });

    $(document).ready(function() { 
    $("#table_payroll1").dataTable( {
        "sScrollX": true,
         "scrollY": "380px",
         "scrollX": true,
         "sScrollXInner": "150%",
         "bPaginate": false,
         "sScrollXInner": "150%",
         "dom": '<"toolbar">frtip',
         fixedColumns:   {
          leftColumns: 3,
          rightColumns : 1
         },
    });
     $("div.toolbar").html('<b>Period : {{$payroll->description}}</b>');
    });


    function edit(id)
     { 
      $('.errorBasic').addClass('hidden');
      $("#btnSave").attr("onclick","update("+id+")");
      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : '../../payrollview/editdetail/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            $('[name="id_key"]').val(data.id); 
            $('[name="dayjob"]').val(data.dayjob);
            $('[name="dayjobdef"]').val(data.dayjobdef);
            $('[name="manual"]').val(data.manual);
            $('[name="employeename"]').val(data.employeename);
            $('[name="nik"]').val(data.nik);
            $('[name="basicemployee"]').val(data.basicemployee);
            $('[name="basic"]').val(data.basic);
            $('[name="mealtransall"]').val(data.mealtransall);
            $('[name="mealtrans"]').val(data.mealtrans);
            $('[name="transportall"]').val(data.transportall);
            $('[name="transport"]').val(data.transport);
            $('[name="overtimehour"]').val(data.overtimehour);
            $('[name="insentive"]').val(data.insentive);
            $('[name="absence"]').val(data.absence);
            $('[name="jamsostek"]').val(data.jamsostek);
            $('[name="bpjs"]').val(data.bpjs);
            $('[name="totalloan"]').val(data.totalloan);
            $('[name="pph21"]').val(data.pph21);
            $('[name="correction"]').val(data.correction); 
            $('[name="totalnetto"]').val(data.totalnetto); 
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Payroll Correction'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: '../../payrollview/editdetail/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'manual': $('#manual').val(), 
            'dayjob': $('#dayjob').val(), 
            'basic': $('#basic').val(), 
            'mealtransall': $('#mealtransall').val(), 
            'mealtrans': $('#mealtrans').val(), 
            'transportall': $('#transportall').val(), 
            'transport': $('#transport').val(), 
            'overtimehour': $('#overtimehour').val(), 
            'insentive': $('#insentive').val(), 
            'jamsostek': $('#jamsostek').val(), 
            'bpjs': $('#bpjs').val(), 
            'totalloan': $('#totalloan').val(), 
            'pph21': $('#pph21').val(), 
            'absence': $('#absence').val(), 
            'correction': $('#correction').val(),  
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorBasic').addClass('hidden');


            if ((data.errors)) {
              var options = { 
                "positiongroupClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.positiongroupname) {
                $('.errorBasic').removeClass('hidden');
                $('.errorBasic').text(data.errors.positiongroupname);
              }
            } else {
            var options = { 
              "positiongroupClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            $('#table_payroll').DataTable().ajax.reload()
            $('#table_payroll1').DataTable().ajax.reload()

            // alert('asdd');
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
            $('#form')[0].reset(); // reset form on modals
            reload_table();
            table_payroll.ajax.reload(null,false); //reload datatable ajax 
          } 
        },
      })
    };
</script>

<script type="text/javascript">
  function showslip(id)
  {
    console.log(id);
   var url = '../../payroll/slip/' + id;
     PopupCenter(url,'Popup_Window','700','650');
  }

   function showabsence(id)
  {
    console.log(id);
   var url = '../../payroll/absence/' + id;
     PopupCenter(url,'Popup_Window','700','650');
  }

  function PopupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
          
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
          
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
    
    // Puts focus on the newWindow  
    if (window.focus) {  
      newWindow.focus();  
    }  
  } 



  function cal_sparator_insentive() {  
        //um 2
        var insentive = document.getElementById('insentive').value;
        var result2 = document.getElementById('insentive');
        var rsamount2 = (insentive);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('insentive');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='insentive')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_basic() {  
        //um 2
        var basic = document.getElementById('basic').value;
        var result2 = document.getElementById('basic');
        var rsamount2 = (basic);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('basic');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='basic')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_mealtransall() {  
        //um 2
        var mealtransall = document.getElementById('mealtransall').value;
        var result2 = document.getElementById('mealtransall');
        var rsamount2 = (mealtransall);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('mealtransall');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='mealtransall')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_mealtrans() {  
        //um 2
        var mealtrans = document.getElementById('mealtrans').value;
        var result2 = document.getElementById('mealtrans');
        var rsamount2 = (mealtrans);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('mealtrans');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='mealtrans')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }


      function cal_sparator_transportall() {  
        //um 2
        var transportall = document.getElementById('transportall').value;
        var result2 = document.getElementById('transportall');
        var rsamount2 = (transportall);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('transportall');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='transportall')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_transport() {  
        //um 2
        var transport = document.getElementById('transport').value;
        var result2 = document.getElementById('transport');
        var rsamount2 = (transport);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('transport');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='transport')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 

      function cal_sparator_insentive() {  
        //um 2
        var insentive = document.getElementById('insentive').value;
        var result2 = document.getElementById('insentive');
        var rsamount2 = (insentive);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('insentive');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='insentive')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_absence() {  
        //um 2
        var absence = document.getElementById('absence').value;
        var result2 = document.getElementById('absence');
        var rsamount2 = (absence);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('absence');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='absence')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }    


      function cal_sparator_jamsostek() {  
        //um 2
        var jamsostek = document.getElementById('jamsostek').value;
        var result2 = document.getElementById('jamsostek');
        var rsamount2 = (jamsostek);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('jamsostek');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='jamsostek')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }   


      function cal_sparator_bpjs() {  
        //um 2
        var bpjs = document.getElementById('bpjs').value;
        var result2 = document.getElementById('bpjs');
        var rsamount2 = (bpjs);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('bpjs');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='bpjs')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 


      function cal_sparator_totalloan() {  
        //um 2
        var totalloan = document.getElementById('totalloan').value;
        var result2 = document.getElementById('totalloan');
        var rsamount2 = (totalloan);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('totalloan');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='totalloan')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }  


      function cal_sparator_pph21() {  
        //um 2
        var pph21 = document.getElementById('pph21').value;
        var result2 = document.getElementById('pph21');
        var rsamount2 = (pph21);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('pph21');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='pph21')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      }    


      function cal_sparator_correction() {  
        //um 2
        var correction = document.getElementById('correction').value;
        var result2 = document.getElementById('correction');
        var rsamount2 = (correction);
        result2.value = rsamount2.replace(/,/g, "");  

        n3= document.getElementById('correction');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp1;
          if(who.id==='correction')  temp1= validDigits(who.value,0); 
          else temp1= validDigits(who.value);
          who.value= addCommas(temp1);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }
      } 

       function RefreshData()
       {  
        table_payroll.ajax.reload(null,false); //reload datatable ajax 
      };    

      function GenerateData()
      {
 
        var periodid = {{$payroll->id}};

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
             waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
             $.ajax({
              url : '../../payroll/generate/' + periodid,
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                waitingDialog.hide();
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generate data!',
                });
              }
            });
           }
         },

       }
     });
      }

      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

       /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);

</script>
@stop

