 
@extends('layouts.editor.template')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-building"></i> Police
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Police</li>
  </ol>
</section>


<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-8">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($police))
				{!! Form::model($police, array('route' => ['editor.police.update', $police->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_police'))!!}
				@else
				{!! Form::open(array('route' => 'editor.police.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_police'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($police))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Police
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('notrans', 'Police Code') }}
								{{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
							</div>
						</div>  
					</div>
					<div class="row">  
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('employeeid', 'Police Name (ID)') }} 
								<div class="input-group">
				                    <span class="input-group-addon"><img src="{{Config::get('constants.path.img')}}/id.png" alt="" /></span>   
									{{ Form::text('policename_id', old('policename_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'policename_id')) }}
								</div>  
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('employeeid', 'Police Name (ENG)') }} 
								<div class="input-group">
				                    <span class="input-group-addon"><img src="{{Config::get('constants.path.img')}}/eng.png" alt="" /></span>   
									{{ Form::text('policename_eng', old('policename_eng'), array('class' => 'form-control', 'required' => 'true', 'id' => 'policename_eng')) }}
								</div>  
							</div>
						</div>
					</div>  
						  
					<div class="row"> 
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('departmentid', 'Department') }}
								{{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid')) }} 
							</div>
						</div>   

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('about', 'About') }}
								{{ Form::text('about', old('about'), array('class' => 'form-control', 'placeholder' => 'About*', 'required' => 'true', 'id' => 'about')) }}
							</div>
						</div>  

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('effectivedate', 'Effective Date') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('effectivedate', old('effectivedate'), array('class' => 'form-control', 'placeholder' => 'Effective Date*', 'required' => 'true', 'id' => 'effectivedate')) }}
			                    </div><!-- /.input group -->  
							</div>
						</div>  

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('policeno', 'Police No') }}
								{{ Form::text('policeno', old('policeno'), array('class' => 'form-control', 'placeholder' => 'Police No*', 'required' => 'true', 'id' => 'policeno')) }}
							</div>
						</div> 
					</div>

					<div class="row"> 
						<div class="col-md-12">
							<div class="form-group">
								<label>Policy Content (<img src="{{Config::get('constants.path.img')}}/id.png" alt="" />)</label>
								<div class="input-group">
			                         <textarea id="content_id" name="content_id"> 
			                    	 	@if(isset($police))
			                    	 		{{$police->content_id}}
			                    	 	@endif 
			                    	 </textarea>  
			                    </div><!-- /.input group -->  
							</div>
						</div>   
					</div> 

					<div class="row"> 
						<div class="col-md-12">
							<div class="form-group">
								<label>Policy Content (<img src="{{Config::get('constants.path.img')}}/eng.png" alt="" />)</label>
								<div class="input-group">
			                         <textarea id="content_eng" name="content_eng"> 
			                    	 	@if(isset($police))
			                    	 		{{$police->content_eng}}
			                    	 	@endif 
			                    	 </textarea>  
			                    </div><!-- /.input group -->  
							</div>
						</div>   
					</div> 

					<div class="row"> 
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('attachment', 'Attachment') }}<br>
								<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
								<br/>
							</div>
						</div>
					</div> 
				</div>
				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.police.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
			{!! Form::close() !!}
		</div>
	</div>
</section>
@stop

@section('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>  
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content_id');
    CKEDITOR.replace('content_eng');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
@stop