@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
  <h1>
    <i class="fa fa-graduation-cap"></i> Recruitment
    <small>Recruitment</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Recruitment</a></li>
    <li class="active">Recruitment</li>
  </ol>
</section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-danger"> 
          @include('errors.error') 
          {!! Form::model($recruitment, array('route' => ['editor.recruitment.update', $recruitment->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_recruitment'))!!} 
          {{ csrf_field() }}
          <!--  Hidden element -->
          <input type="hidden" value="" id="idtrans" name="idtrans">
          <input type="hidden" value="" id="status" name="status">
          <div class="box-header with-border">
            <div class="well">
              <div class="row">
                <!-- Coloumn 1-->
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="real_name" class="col-sm-3 control-label">Rec No</label>
                    <div class="col-sm-9">  
                      {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="real_name" class="col-sm-3 control-label">Rec Date</label>
                    <div class="col-sm-9">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                        {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'form_field_date', 'class' => 'form-control datepicker')) }}
                      </div><!-- /.input group --> 
                    </div>
                  </div> 
                </div>
                <!-- Coloumn 2-->   
                <div class="col-md-4">  
                  <div class="form-group">
                    <label for="real_name" class="col-sm-3 control-label">Position</label>
                    <div class="col-sm-9">  
                      {{ Form::select('positionid', $position_list, old('positionid'), array('class' => 'form-control', 'placeholder' => 'Select Position', 'id' => 'positionid')) }}
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="real_name" class="col-sm-3 control-label">Document</label>
                    <div class="col-sm-9"> 
                      {{ Form::file('attachment') }} 
                    </div>
                  </div>
                </div> 
                <!-- Coloumn 2-->   
                <div class="col-md-4">  
                  <div class="form-group">
                    <label for="real_name" class="col-sm-3 control-label">Requirement</label>
                    <div class="col-sm-9">  
                      <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();">{{ $recruitment->remark }}</textarea>
                    </div>
                  </div>  
                </div> 
              </div><!-- /.box-header -->
            </div>  
            <div class="box-body">  
              <table id="dtTable" class="table table-hover">
                <thead>
                  <tr>  
                    <th>Item Name</th> 
                    <th>Description</th> 
                    <th>Unit</th>
                    <th>Quantity</th> 
                    <th>Action</th> 
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table> 
            </div>  
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="real_name" class="col-sm-3 control-label">Remark</label>
                  <div class="col-sm-9">
                    <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();">{{ $recruitment->remark }}</textarea>
                  </div>
                </div>
              </div> 
              <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button> 
             <a href="{{ URL::route('editor.recruitment.index') }}" type="button" class="btn btn-default btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Exit</a>
             <iframe src='' height="0" width="0" frameborder='0' name="print_frame"></iframe>
           </div> 
           {!! Form::close() !!} 
         </div>
       </div>
      </div>
    </div>
  </section>
</div>


<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid">
          <option ondblclick="javascript:framePrint('print_frame');" value="recruitment">Purchase Order</option>
        </select>
      </div>
      <div class="modal-footer">
        <a href="javascript:framePrint('print_frame');" class="btn btn-primary btn-flat"> <i class="fa fa-search"></i> Preview</a> 
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div> 
@stop

@section('scripts') 
<script type="text/javascript"> 
  var table;
  $(document).ready(function() {
      //datatables
      table = $('#dtTable').DataTable({ 
        processing: true,
        serverSide: true,
        "pageLength": 10,
        // "scrollY": "170px",
        "rowReorder": true,
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
        ajax: "{{ url('recruitment/datadetail') }}/{{$recruitment->id}}", 
        columns: [   
        { data: 'item_name', name: 'item_name' },
        { data: 'item_description', name: 'item_description' },
        { data: 'unit', name: 'unit', class: 'text-center'  },
        { data: 'quantity', name: 'quantity', class: 'text-right'  },  
        { data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center text-block' }
        ]
      });
      //check all
      $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
      }); 
    });
  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function add()
    {
     $("#btnSave").attr("onclick","save()");
     $("#btnSaveAdd").attr("onclick","saveadd()");

     $('.errorMaterial UsedName').addClass('hidden');

     save_method = 'add'; 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Add Asset Request'); // Set Title to Bootstrap modal title
    } 

    function reload_table_detail()
    {
      table_detail.ajax.reload(null, false); //reload datatable ajax 
    }

    var table_detail;
    $(document).ready(function() {
      //datatables
      table_detail = $('#detailTable').DataTable({ 
       processing: true,
       serverSide: true,
       "pageLength": 10,
       // "scrollY": "360px",
       "rowReorder": true,
       "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
       ajax: "{{ url('item/datalookup') }}",
       columns: [   
       { data: 'item_id', name: 'item_id', orderable: false, searchable: false },
       { data: 'item_name', name: 'item_name' },
       { data: 'item_description', name: 'item_description' }, 
       { data: 'item_group', name: 'item_group' },
       { data: 'item_last_price', name: 'item_last_price' },
       { data: 'item_min_stock', name: 'item_min_stock' },
       { data: 'item_max_stock', name: 'item_max_stock' }, 
       { data: 'action', name: 'action', orderable: false, searchable: false }
       ]
     });
      //check all
      $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
      });
    });

    function addValue(str, id){
      var item_id = id;
      var item_id = $(str).closest('tr').find('td:eq(0)').text();
      var item_name = $(str).closest('tr').find('td:eq(1)').text(); 
      var item_description = $(str).closest('tr').find('td:eq(2)').text(); 

      $("#item_id").val(item_id);
      $("#item_name").val(item_name);
      $("#item_description").val(item_description); 
      $('#btn_add_detail').show(100);
      $('#btn_update_detail').hide(100);

      console.log(id);
      $('#detailModals').modal('toggle');
    }


    function saveheader(id)
    {
      save_method = 'update';  

      //Ajax Load data from ajax
      $.ajax({
        url: '../../recruitment/saveheader/{{$recruitment->id}}' ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'warehouse_id': $('#warehouse_id').val(),
          'datetrans': $('#datetrans').val(), 
          'remark': $('#remark').val(),  
          'attachment' : $('#attachment').val(),
        },
        success: function(data) {  
          if ((data.errors)) { 
            toastr.error('Data is required!', 'Error Validation', options);
          } 
        },
      })
    };

    function savedetail(id)
    {

     var item = $('#item_id').val();
     var unit =   $('#unit').val();
     var unitprice =   $('#unitprice').val(); 
     var quantity =   $('#quantity').val();   

     if(item == '' || unit == '' || quantity == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';  

      //Ajax Load data from ajax
      $.ajax({
        url: '../../recruitment/savedetail/{{$recruitment->id}}' ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'item_id': $('#item_id').val(),
          'unit': $('#unit').val(), 
          'quantity': $('#quantity').val()
        },
        success: function(data) { 
          reload_table_detail();
          reload_table();
          clear_detail(); 
        },
      })
    }
  };

  function updatedetail(id)
  {
    var pr_d_id = $('#pr_d_id').val();
    var item = $('#item_id').val();
    var unit =   $('#unit').val(); 
    var quantity =   $('#quantity').val();   

    if(item == '' || unit == '' || quantity == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';  

      //Ajax Load data from ajax
      $.ajax({
        url: '../../recruitment/updatedetail/' + pr_d_id ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(), 
          'pr_d_id': $('#pr_d_id').val(),
          'item_id': $('#item_id').val(),
          'unit': $('#unit').val(), 
          'quantity': $('#quantity').val()
        },
        success: function(data) {  
          reload_table_detail();
          reload_table();
          clear_detail(); 
        },
      })
    }
  };

  function update_id(str, pr_d_id, item_id)
  {
    clear_detail();
    console.log(pr_d_id);
    console.log(item_id); 

    var item_name = $(str).closest('tr').find('td:eq(0)').text(); 
    var item_description = $(str).closest('tr').find('td:eq(1)').text();  
    var quantity = $(str).closest('tr').find('td:eq(3)').text(); 
    var unit = $(str).closest('tr').find('td:eq(2)').text();  

    $("#pr_d_id").val(pr_d_id);
    $("#item_id").val(item_id);
    $("#item_name").val(item_name);
    $("#item_description").val(item_description); 
    $("#quantity").val(quantity); 
    $("#unit").val(unit);  
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100); 
  };

  function clear_detail()
  {
    $('#item_id').val(''),
    $('#unit').val(''),  
    $('#quantity').val(''), 
    $('#amount').val(''),
    $('#item_name').val(''),
    $('#item_description').val('')
  }

  function delete_id(po_d_id, item_name)
  {
      //console.log(id);
      var r = confirm("Are you sure you want to delete this data?");
      if (r == true) {   
       $.ajax({
        url : '../../recruitment/deletedet/' + po_d_id,
        type: "DELETE",
        data: {
          '_token': $('input[name=_token]').val() 
        },
        success: function(data)
        { 
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          }; 
          reload_table();
        }, 
        error: function (jqXHR, textStatus, errorThrown)
        { 
        },
      }) 
     }
   };

   function cancel(id)
   {
    var status = {{$recruitment->status}};
    if(status > 0)
    {
     var r = confirm("Open this transaction?");
   }else{
     var r = confirm("Cancel this transaction?");
   };

   if (r == true) { 
    save_method = 'post';  

      //Ajax Load data from ajax
      $.ajax({
       url: '../../recruitment/cancel/{{$recruitment->id}}' ,
       type: "POST",
       data: {
        '_token': $('input[name=_token]').val()
      },
      success: function(data) {  
          //var loc = 'recruitment';
          if ((data.errors)) { 
            alert("Cancel error!");
          } else{
            window.location.href = "{{ URL::route('editor.recruitment.index') }}";
          }
        },
      })
    }
  };

  function closetr(id)
  { 

   var r = confirm("Close this transaction?"); 
   if (r == true) { 
    save_method = 'post';  

      //Ajax Load data from ajax
      $.ajax({
       url: '../../recruitment/close/{{$recruitment->id}}' ,
       type: "POST",
       data: {
        '_token': $('input[name=_token]').val()
      },
      success: function(data) {  
          //var loc = 'recruitment';
          if ((data.errors)) { 
            alert("Cancel error!");
          } else{
            window.location.href = "{{ URL::route('editor.recruitment.index') }}";
          }
        },
      })
    }
  };


</script>

@stop
