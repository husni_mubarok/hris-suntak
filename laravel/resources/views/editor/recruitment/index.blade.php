@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
  <h1>
    <i class="fa fa-graduation-cap"></i> Recruitment
    <small>Recruitment</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Recruitment</a></li>
    <li class="active">Recruitment</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important"> 
          <a href="#" onclick="add();" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</a>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th width="3%">#</th> 
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label> 
                  </th> 
                  <th>No Trans</th> 
                  <th>Date Trans</th> 
                  <th>Rec From</th> 
                  <th>Rec To</th> 
                  <th>Position</th> 
                  <th>Requirement</th> 
                  <th>Job Desc</th> 
                  <th>Remark</th>  
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Modal Popup -->
<div class="modal fade modal_form"  id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
 <div class="modal-dialog" style="width:420px !important;">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Select the transaction code</h4>
  </div>
  {!! Form::open(array('route' => 'editor.recruitment.store', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_floor'))!!}
  {{ csrf_field() }}
  <div class="modal-body">
   <input type="hidden" class="form-control" id="idperiod" name="idperiod">
   <input type="hidden" class="form-control" id="type" name="type">
   <div class="form-group">
    <label for="real_name" class="col-sm-3 control-label">Transaction</label>
    <div class="col-sm-8">
      <select class="form-control"  style="width: 100%;" name="codetrans" id="codetrans"  placeholder="Transaction Code">
        <option value="RECR">Recruitment</option> 
      </select>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button class="btn btn-primary btn-flat"><i class="fa fa-check"></i>&nbsp;&nbsp;Submit</button>
  <input type="hidden" value="1" name="submit" />
  <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
</div> 
{!! Form::close() !!}
</form>
</div> 
</section>
</div>
</div>

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
          //editor 
         colReorder: true,
         fixedHeader: true, 
         responsive: true,
         //rowReorder: true, 
         "rowReorder": {
            "update": false,
         },
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/recruitment/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'datetrans', name: 'datetrans', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'recfrom', name: 'recfrom', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'recto', name: 'recto', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'positionname', name: 'positionname' },
         { data: 'requirements', name: 'requirements' },
         { data: 'jobdesc', name: 'jobdesc' },
         { data: 'remark', name: 'remark' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
          table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");

        $('.errorMaterial UsedName').addClass('hidden');

        save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Recruitment'); // Set Title to Bootstrap modal title
      } 
   </script> 
   @stop
