 @extends('layouts.editor.template')
 <style type="text/css">
    #print{
      position: absolute;
      right: 30px;
    }
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        /*background-color: #FAFAFA;*/
        font: 12pt arial;
      }
      * {
          box-sizing: border-box;
          -moz-box-sizing: border-box;
      }

      table{border-collapse: collapse;}
      table tr td, table tr th{vertical-align: top;font-size: 8pt;padding: 0mm;}

      .page {
          width: 210mm;
          height: 297mm;
          padding: 10mm;
          margin: 10mm auto;
          border: 1px #D3D3D3 solid;
          border-radius: 5px;
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          background: url('invoice_matrix_alt2.jpg');
          background-size: 210mm auto;
          font-size: 10pt;
          position: relative;
      }
        .page h2{margin: 10mm 0 0 0;}
        .page hr{
          border: 0;
          border-top: 1px solid #000;
          margin: 2mm 0;
        }
        .page .detail{
          border: 1px solid #707070;
          border-radius: 1mm;
          margin: 5mm 0 0 0mm;
          padding:3mm 3mm 0 3mm;
          background-color: #f3f3f3;
        }
          .page .detail table{
            margin-right: 2mm;
            display: inline-block;
            vertical-align: top;
          }
          .page .detail table tr td{
            padding: 1mm;
          }
        .page .item{
          /*outline: 1px solid blue;*/
          margin: 5mm 0 0 0mm;
          border: 1px solid #707070;
          overflow: hidden;
          border-radius: 1mm;
        }
          .page .item table{
            width: 100%;
            vertical-align: top;
          }
          .page .item table tr td{
            padding:2mm;
          }
          .page .item table thead tr th{
            background-color: #e1e1e1;
            padding:2mm;
          }
          .page .item table thead tr:first-child {
            border-radius: 1mm 0 0 0;
            -moz-border-radius: 1mm 0 0 0;
            -webkit-border-radius: 1mm 0 0 0;
            border-bottom: 1px solid #707070;
        }


        .page .item table tbody tr { 
            border-bottom: 1px solid #acacac;
        }
        .page .item table tbody tr:last-child { 
            border-bottom: 1px solid #707070;
        }
        .page .item table tfoot tr{
            border-bottom: 1px solid #707070;
        }
        .page .item table tfoot tr:last-child{
            border-bottom: 0px solid #707070;
        }
        .page .item table tfoot tr th{
            background-color: #f3f3f3;
            padding:2mm;
          }
        .page .item table tfoot tr:last-child th{
            background-color: #e1e1e1;
            padding:2mm;
          }
 
        .page .direktur{
          /*outline: 1px solid blue;*/
          margin: 30mm 0 0 142mm;
          font-weight:bold;
          text-align: center;
          height: 12mm;
          width: 40mm;
          font-size: 12px;
        }

        .page .direktur hr{margin:1mm 0;border: 0px;border-top:1px solid #000;}
 
        .text-center{text-align: center}
        .text-right{text-align: right}
        .text-left{text-align: left}
        .text-total{font-size: 12px;}

      @page {
          /*size: A4;*/
          margin: 0;
      }
      @media print {
          html, body {
              width: 210mm;
              height: 297mm;        
          }
          .page {
              margin: 0;
              border: initial;
              border-radius: initial;
              width: initial;
              min-height: initial;
              box-shadow: initial;
              background: initial;
              page-break-after: always;
              /*background: url('invoice_matrix_alt2.jpg');*/
            background-size: 100% auto;
          }
          #print{display: none;}
          .page .detail{
          background-color: #f3f3f3 !important;
        }
        .page .item table tfoot tr th{
            background-color: #f3f3f3 !important;
          }
        .page .item table tfoot tr:last-child th{
            background-color: #e1e1e1 !important;
          }
      }
  </style>

  <div class="book">
    <div class="page">

      <div id="print">
        <button class="btn btn-primary">
          <i class="fa fa-print"></i>&nbsp;&nbsp;PRINT
        </button>
      </div>

      <h2>PURCHASE REQUEST</h2>
      <hr>
      <div class="detail">
        <table border="0" style="width: 40mm">
          <tr>
            <td><b>No.</b></td>
            <td>:</td>
            <td>{{ $purchase_request->pr_reference }}</td>
          </tr>
          <tr>
            <td><b>Date</b></td>
            <td>:</td>
            <td>{{ $purchase_request->pr_date }}</td>
          </tr>
        </table>
        <table border="0" style="width: 60mm">
          <tr>
            <td><b>Warehouse</td>
            <td>:</td>
            <td>{{ $purchase_request->warehouse_name }}</td>
          </tr>
          <tr>
            <td><b>Remark</b></td>
            <td>:</td>
            <td>{{ $purchase_request->remark }}</td>
          </tr>
        </table> 
      </div>
       
      <div class="item">
        <table border="0">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-left">CODE</th>
              <th class="text-left">DESCRIPTION</th>
              <th class="text-left">UNIT</th>
              <th class="text-center">QUANTITY</th> 
            </tr>
          </thead>
          <tbody>
            @php
              $i = 1;
              $total_qty = 0; 
              @endphp
            @foreach($purchase_request_detail as $key => $purchase_request_details)
            <tr>
              <td class="text-center">{{$i++}}</td>
              <td class="text-left">{{ $purchase_request_details->item_name }}</td>
              <td class="text-left">{{ $purchase_request_details->item_description }}</td>
              <td class="text-left">{{ $purchase_request_details->unit }}</td>
              <td class="text-center">{{ $purchase_request_details->quantity }}</td> 
            </tr> 
            @php
            $total_qty += $purchase_request_details->quantity;
            @endphp
            @endforeach
          </tbody>
          <tfoot>
            <tr class="sub-total">
              <th class="text-right" colspan="4">Total</th>
              <th class="text-center"><b>{{$total_qty}}</b></th>
            </tr> 
          </tfoot>
        </table>
      </div>

      <div class="direktur"> 
        <hr>
        Request by
      </div>
    </div>
 
    @section('scripts') 
    <script type="text/javascript">
      $('#print').click(function(){
       window.print();
     });
   </script> 
   @stop
