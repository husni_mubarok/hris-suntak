@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    Report Employee Loan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Employee Loan</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Nama Karyawan</th> 
                  <th>Jabatan</th> 
                  <th>Pinjaman Insurance</th>
                  <th>Pinjaman Employee</th> 
                  <th>Pinjaman Kendaraan</th>
                  <th>Pinjaman Rumah</th>
                  <th>Total Pinjaman</th>
                  <th>Sisa Pinjaman Personal</th>
                  <th>Sisa Pinjaman kendaraan</th>
                  <th>Sisa Pinjaman Asuransi</th>
                  <th>Sisa Pinjaman Rumah</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "100%",
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reportloan/data') }}",
         columns: [   
         { data: 'departmentname', name: 'departmentname' }, 
         { data: 'employeename', name: 'employeename' },  
         { data: 'jabatan', name: 'jabatan' }, 
         { data: 'insuranceloan', name: 'insuranceloan' },  
         { data: 'employeeloan', name: 'employeeloan' },  
         { data: 'vehicleloan', name: 'vehicleloan' },  
         { data: 'pinjaman_rumah', name: 'pinjaman_rumah' },  
         { data: 'total_pinjaman', name: 'total_pinjaman' },  
         { data: 'sisa_pinjaman_personal', name: 'sisa_pinjaman_personal' },  
         { data: 'sisa_pinjaman_kendaraan', name: 'sisa_pinjaman_kendaraan' },  
         { data: 'sisa_pinjaman_asuransi', name: 'sisa_pinjaman_asuransi' },  
         { data: 'sisa_pinjaman_rumah', name: 'sisa_pinjaman_rumah' },    
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'departmentid': $('#departmentid').val(),    
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
    </script> 
    @stop
