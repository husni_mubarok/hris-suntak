@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    Report Payroll
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Payroll</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
    {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <a href="javascript:void(0)" title="Print Slip"  onclick="showslipall()" onClick="reload_table()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Print Slip</a>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe" style="background-color: #fff">
              <thead>
                <tr> 
                  <th rowspan="2">NIK</th> 
                  <th rowspan="2">Employee</th> 
                  <th rowspan="2">Dept</th>
                  <th rowspan="2">Hari Kerja</th>
                  <th colspan="4"><center>Tunjangan</center></th> 
                  <th colspan="2"><center>Lembur</center></th>
                  <th rowspan="2">Allowance/Insentive</th> 
                  <th colspan="5"><center>Deduction</center></th> 
                  <th rowspan="2">Total</th> 
                  <th rowspan="2">Action</th> 
                </tr>
                <tr>
                  <th>T. Makan</th>
                  <th>Makan (Rp)</th>
                  <th>T. Transport</th>
                  <th>Transport (Rp)</th>
                  <th>Total Jam Lembur</th>
                  <th>Lembur (Rp)</th>
                  <th>Absen</th>
                  <th>Jamsostek</th>
                  <th>BPJS</th>
                  <th>Pinjaman</th>
                  <th>PPH 21</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>   
@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "230%",
         fixedColumns:   {
          leftColumns: 3,
          rightColumns: 1
        },
        "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2 ] }
        ],
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], 
        ajax: "{{ url('editor/reportpayroll/data') }}",
        columns: [   
        { data: 'nik', name: 'nik' }, 
        { data: 'employeename', name: 'employeename' }, 
        { data: 'departmentname', name: 'departmentname' }, 
        { data: 'dayjob', name: 'dayjob', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'mealtrans', name: 'mealtrans', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'transportall', name: 'transportall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'transport', name: 'transport', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'overtimeall', name: 'overtimeall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'overtime', name: 'overtime', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'insentive', name: 'insentive', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'jamsostek', name: 'jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'bpjs', name: 'bpjs', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'totalloan', name: 'totalloan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'pph21', name: 'pph21', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'totalnetto', name: 'totalnetto', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
        { data: 'printslip', name: 'printslip' },
        ]
      });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
      { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'departmentid': $('#departmentid').val(),    
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
      });
    </script> 

    <script type="text/javascript">
      function showslip(id)
      {
        console.log(id);
       var url = 'payroll/slip/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }


      function showslipall()
      {
        // console.log(id);
       var dep = $("#departmentid").val();

       var url = 'payroll/slipall/' + dep;
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
    </script>
@stop
