@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    Report Promotion
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Promotion</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Expired From">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
              {{ Form::text('begindate', old('begindate'), array('class' => 'form-control', 'required' => 'true', 'id' => 'begindate', 'onchange' => 'RefreshData()')) }}
            </div>
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Expired To">
           <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
              {{ Form::text('enddate', old('enddate'), array('class' => 'form-control', 'required' => 'true', 'id' => 'enddate', 'onchange' => 'RefreshData()')) }}
            </div>
          </div>    
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>NIK</th> 
                  <th>Employee Name</th>
                  <th>SK No</th>
                  <th>Promotion No</th> 
                  <th>Promotion Date</th>  
                  <th>Current Position</th> 
                  <th>Next Position</th>  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "100%",
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reportpromotion/data') }}",
         columns: [   
         { data: 'nik', name: 'nik' }, 
         { data: 'employeename', name: 'employeename' },  
         { data: 'notrans', name: 'notrans' },  
         { data: 'datetrans', name: 'datetrans' },  
         { data: 'accepdate', name: 'accepdate' },  
         { data: 'curposition', name: 'curposition' }, 
         { data: 'nextposition', name: 'nextposition' },  
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'begindate': $('#begindate').val(),  
            'enddate': $('#enddate').val(),    
            'employeeid': $('#employeeid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
    </script> 
    @stop
