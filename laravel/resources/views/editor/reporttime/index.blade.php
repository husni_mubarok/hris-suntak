@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    Report Time
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Time</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Date From">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
              {{ Form::text('begindate', old('begindate'), array('class' => 'form-control', 'required' => 'true', 'id' => 'begindate', 'onchange' => 'RefreshData()')) }}
            </div>
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Date To">
           <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
              {{ Form::text('enddate', old('enddate'), array('class' => 'form-control', 'required' => 'true', 'id' => 'enddate', 'onchange' => 'RefreshData()')) }}
            </div>
          </div>   
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Employee">
            {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
          </div>   
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>NIK</th> 
                  <th>Employee Name</th>  
                  <th>Department</th> 
                  <th>Position</th>
                  <th>Date</th> 
                  <th>Time In</th> 
                  <th>Time Out</th> 
                  <th>Actual In</th> 
                  <th>Actual Out</th> 
                  <th>Permit In</th> 
                  <th>Permit Out</th>  
                  <th>Overtime In</th>
                  <th>Overtime Out</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "120%",
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reporttime/data') }}",
         columns: [   
         { data: 'nik', name: 'nik' }, 
         { data: 'employeename', name: 'employeename' },  
         { data: 'departmentname', name: 'departmentname' }, 
         { data: 'positionname', name: 'positionname' },  
         { data: 'datein', name: 'datein' },  
         { data: 'dayin', name: 'dayin' },  
         { data: 'dayout', name: 'dayout' },  
         { data: 'actualin', name: 'actualin' },  
         { data: 'actualout', name: 'actualout' },  
         { data: 'permitein', name: 'permitein' },    
         { data: 'permiteout', name: 'permiteout' },     
         { data: 'overtimein', name: 'overtimein' },     
         { data: 'overtimeout', name: 'overtimeout' },     
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'begindate': $('#begindate').val(),  
            'enddate': $('#enddate').val(),    
            'employeeid': $('#employeeid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
    </script> 
    @stop
