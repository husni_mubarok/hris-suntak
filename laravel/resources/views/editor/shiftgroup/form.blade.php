 @extends('layouts.editor.template')
 @section('content')
 <style type="text/css">


  #detailModals .modal-dialog
  {
    width: 60%;
  }
  th { font-size: 13px; }
  td { font-size: 12px; }
</style>
<!-- Content Wrapper. Contains page content --> 
<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    <i class="fa fa-adjust"></i> Shift Group
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Shift Group</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-xs-8">
      <div class="box box-danger">
        {!! Form::model($shiftgroup, array('route' => ['editor.shiftgroup.update', $shiftgroup->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_employee'))!!}
        {{ csrf_field() }}
        <!--  Hidden element -->
        <input type="hidden" value="" id="idtrans" name="idtrans">
        <input type="hidden" value="" id="status" name="status">
        <div class="box-header with-border">
          <div class="row"> 
          <!-- Coloumn 1-->   
          <div class="col-md-6">
           <div class="form-group">
            <label for="real_name" class="col-sm-4 control-label">Group Code</label>
              <div class="col-sm-8"> 
                  {{ Form::text('shiftgroupcode', old('shiftgroupcode'), array('class' => 'form-control', 'placeholder' => 'Group Code*', 'required' => 'true')) }}
              </div>
          </div>

           <div class="form-group">
            <label for="real_name" class="col-sm-4 control-label">Group Name</label>
              <div class="col-sm-8"> 
                  {{ Form::text('shiftgroupname', old('shiftgroupname'), array('class' => 'form-control', 'placeholder' => 'Group Name*', 'required' => 'true')) }}
              </div>
          </div> 
          <div class="form-group">
            <label for="real_name" class="col-sm-4 control-label">Day Shift</label>
              <div class="col-sm-8">
                  {{ Form::text('dayshift', old('dayshift'), array('class' => 'form-control', 'placeholder' => 'Day Shift*', 'required' => 'true')) }}
              </div>
          </div>  

          <div class="form-group">
            <label for="real_name" class="col-sm-4 control-label">Default Holiday</label>
              <div class="col-sm-8">
                  <select class="form-control" name="defaultholiday" id="defaultholiday" onchange="setholiday();">
                    @if(isset($shiftgroup->dafaultholiday))
                      <option value="{{ $shiftgroup->dafaultholiday }}"> 
                        @if($shiftgroup->dafaultholiday == 1)
                          Yes
                        @else
                          No
                        @endif
                      </option>
                    @endif
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
              </div>
          </div>  
          <div class="form-group" id="offday">
            <label for="real_name" class="col-sm-4 control-label">Off Day</label>
              <div class="col-sm-8">
                  <label class="checkbox-inline">
                    <input class="form-check-input" type="checkbox" id="offfriday" name="offfriday[]" value="1"  @if($shiftgroup->offfriday == 1) checked @endif> Friday
                  </label><br>
                  <label class="checkbox-inline">
                    <input class="form-check-input" type="checkbox" id="offsaturday" name="offsaturday[]" value="1"  @if($shiftgroup->offsaturday == 1) checked @endif> Saturday
                  </label><br>
                  <label class="checkbox-inline">
                    <input class="form-check-input" type="checkbox" id="offsunday" name="offsunday[]" value="1" @if($shiftgroup->offsunday == 1) checked @endif> Sunday
                  </label>
              </div>
          </div> 
        </div>
        <!-- Coloumn 2-->                                   
        <div class="col-md-6">  
          <h4><u>FIRST CONFIG </u></h4>
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">First Period</label>
              <div class="col-sm-9"> 
                <select name="firstperiodid" id="firstperiodid" class="form-control">
                  <option value="{{ $shiftgroup->firstperiodid }}"> {{ $shiftgroup->period }}</option>
                  @foreach($period_list AS $period_lists)
                    <option value="{{ $period_lists->id }}">{{ $period_lists->description }}</option>
                  @endforeach
                </select> 
              </div>
          </div> 
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">First Shift</label>
              <div class="col-sm-9"> 
                <select class="form-control" name="firstshiftid" id="firstshiftid">
                  <option value="{{ $shiftgroup->firstshiftid }}"> {{ $shiftgroup->shiftname }}</option>
                  @foreach($shift_list AS $shift_lists)
                    <option value="{{ $shift_lists->id }}">{{ $shift_lists->shiftname }}</option>
                  @endforeach            
                </select>
              </div>
          </div> 
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Day</label>
              <div class="col-sm-9"> 
                 {{ Form::text('firstday', old('firstday'), array('class' => 'form-control', 'placeholder' => 'Day*', 'required' => 'true')) }}
              </div>
          </div> 
        <hr> 
      </div>
    </div><!-- /.box-header -->
    <hr>
               
    <div class="box-body"> 
       <div class="col-md-4" style="margin-right: 5px">
        <div class="form-group">
          <label>Shift Code</label>
          <select class="form-control" name="shiftid" id="shiftid">
            @foreach($shift_list AS $shift_lists)
              <option value="{{ $shift_lists->id }}" @if($shift_lists->absencetypeid == 9) style="background-color: #e06b6b" @endif>{{ $shift_lists->shiftname }}</option>
            @endforeach            
          </select>
        </div> 
      </div>

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Day(s)</label>
          <input type="number" value="" class="form-control" id="day" name="day">
        </div> 
      </div>
  
      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
         <label>Action</label><br/>
         <a href="#" onclick="savedetail();" type="button" class="btn btn-primary btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-plus"></i> Add</a>
       </div>   
     </div> 
   </div>  
   <div class="box-body">  
     <table id="dtTable" class="table table-bordered table-hover stripe">
      <thead>
       <tr>  
        <th>Day(s)</th>
        <th>Shift Code</th> 
        <th>Shift Description</th>  
        <th>Action</th> 
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> 
</div>  
<hr style="margin-top: -10px">   
<div class="box-header with-border">
  <div class="col-md-6">
    <div class="form-group">
      <label for="real_name" class="col-sm-3 control-label">Remark</label>
      <div class="col-sm-9">
        <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();"> {{$shiftgroup->remark}}</textarea>
      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-primary-ghci pull-right btn-flat"><i class="fa fa-check"></i> Save</button>   
  <a href="{{ URL::route('editor.shiftgroup.index') }}" type="button" class="btn btn-primary-ghci btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
</div>
{!! Form::close() !!}
</div>
</section><!-- /.content -->  
@stop

@section('scripts')

<script type="text/javascript">
  var table;
  $(document).ready(function() {
        //datatables
        setholiday();
        table = $('#dtTable').DataTable({ 
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "500px",
          "rowReorder": true,
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
          ajax: "{{ url('editor/shiftgroup/datadetail') }}/{{$shiftgroup->id}}",
          "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData[1] == "1" )
            {
                $('td', nRow).css('background-color', 'red');
            }
            else if ( aData[0] == "4" )
            {
                $('td', nRow).css('background-color', 'Orange');
            }
          },
          columns: [   
          { data: 'day', name: 'day' }, 
          { data: 'lbl_shift', name: 'lbl_shift' },
          { data: 'description', name: 'description' }, 
          { data: 'action', name: 'action' }
          ]
        });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        }); 


      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
       $("#btnSave").attr("onclick","save()");
       $("#btnSaveAdd").attr("onclick","saveadd()");

       $('.errorMaterial UsedName').addClass('hidden');

       save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Asset Request'); // Set Title to Bootstrap modal title
      } 

      function reload_table_detail()
      {
        table_detail.ajax.reload(null,false); //reload datatable ajax 
      }

      var table_detail;
      $(document).ready(function() {
        //datatables
        table_detail = $('#detailTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/employee/datalookup') }}",
         columns: [  
         { data: 'id', name: 'id', orderable: false, searchable: false },
         { data: 'nik', name: 'nik' },
         { data: 'employeename', name: 'employeename' }, 
         { data: 'positionname', name: 'positionname' }, 
         { data: 'action', name: 'action', orderable: false, searchable: false }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });

      function addValue(str, id){
        var empid = id;
        var employeeid = $(str).closest('tr').find('td:eq(0)').text();
        var nik = $(str).closest('tr').find('td:eq(1)').text(); 
        var employeename = $(str).closest('tr').find('td:eq(2)').text(); 

        $("#employeeid").val(employeeid);
        $("#nik").val(nik);
        $("#employeename").val(employeename); 

        console.log(id);
        $('#detailModals').modal('toggle');
      }


      function saveheader(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../shiftgroup/saveheader/{{$shiftgroup->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'employeeid': $('#employeeid').val(),
            'datefrom': $('#datefrom').val(),
            'dateto': $('#dateto').val(),
            'timefrom': $('#timefrom').val(),
            'timeto': $('#timeto').val(),
            'remark': $('#remark').val(),
            'planwork': $('#planwork').val(),
            'actualwork': $('#actualwork').val(),
            'location': $('#location').val()
          },
          success: function(data) {  
            if ((data.errors)) { 
              toastr.error('Data is required!', 'Error Validation', options);
            } 
          },
        })
      };

      function savedetail(id)
      {
        var shiiftid = $("#shiiftid").val();
        save_method = 'update';  

        if(shiiftid == '')
        {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Shift data is required!', 'Error Validation', options);
        }else{
        //Ajax Load data from ajax
        $.ajax({
          url: '../../shiftgroup/savedetail/{{$shiftgroup->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'day': $('#day').val(),
            'shiftid': $('#shiftid').val()
          },
          success: function(data) {

          
           var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully add detail data!', 'Success Alert', options);

          if ((data.errors)) { 
            toastr.error('Data is required!', 'Error Validation', options);
          } 
          reload_table_detail();
          reload_table();

          $('#shiftid').val(''),
          $('#day').val('')

        },
      
      })
        }
      };

      function delete_id(id, employeename)
      {
        //alert("asdasd");
        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : '../../shiftgroup/deletedet/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

      function setholiday()
      {
        var defaultholiday = $("#defaultholiday").val();
        var offday = $("#shiiftid").val(0);
        if(defaultholiday == 1){
          $("#offday").show(100);
        }else{
          $("#offday").hide(200);
          $('#offsunday').prop('checked', false); // Unchecks it
          $('#offsaturday').prop('checked', false); // Unchecks it
        }
      };
    </script>
    @stop
