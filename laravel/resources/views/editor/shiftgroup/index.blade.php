@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<!-- <section class="content-header"> -->
<section class="content-header">
  <h4>
    <i class="fa fa-clock-o"></i> Shift Group
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time Management</a></li>
    <li class="active">Shift Group</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label>

                  </th>
                  <th>Action</th> 
                  <th>Group Code</th> 
                  <th>Group Name</th> 
                  <th>Day Shift</th>  
                  <th>Off Day</th>
                  <th>First Shift</th>
                  <th>First Period</th>
                  <th>Status</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Shift Group Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            
            <div class="form-group">
              <label class="control-label col-md-3">Group Code</label>
              <div class="col-md-8">
                <input name="shiftgroupcode" id="shiftgroupcode" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Group Name</label>
              <div class="col-md-8">
                <input name="shiftgroupname" id="shiftgroupname" class="form-control" type="text">
                <small class="errorShift GroupName hidden alert-danger"></small> 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Day Shift</label>
              <div class="col-md-8">
                <input name="dayshift" id="dayshift" class="form-control" type="number">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">First Period</label>
              <div class="col-md-8">
                <select name="firstperiodid" id="firstperiodid" class="form-control">
                  @foreach($payperiod AS $payperiods)
                    <option value="{{ $payperiods->id }}">{{ $payperiods->description }}</option>
                  @endforeach
                </select> 
              </div>
            </div>

             <div class="form-group">
              <label class="control-label col-md-3">First Shift</label>
              <div class="col-md-8">
                <select name="firstshiftid" id="firstshiftid" class="form-control">
                  @foreach($shift AS $shifts)
                    <option value="{{ $shifts->id }}">{{ $shifts->shiftname }}</option>
                  @endforeach
                </select> 
              </div>
            </div>

          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         // "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/shiftgroup/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'shiftgroupcode', name: 'shiftgroupcode' },
         { data: 'shiftgroupname', name: 'shiftgroupname' },
         { data: 'dayshift', name: 'dayshift' }, 
         { data: 'offday', name: 'offday' }, 
         { data: 'shift', name: 'shift' }, 
         { data: 'period', name: 'period' }, 
         { data: 'mstatus', name: 'mstatus' }, 
         { data: 'detail', name: 'detail' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorShift GroupName').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Shift Group'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.shiftgroup.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'shiftgroupcode': $('#shiftgroupcode').val(), 
            'shiftgroupname': $('#shiftgroupname').val(), 
            'dayshift': $('#dayshift').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorShift GroupName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.shiftgroupname) {
                $('.errorShift GroupName').removeClass('hidden');
                $('.errorShift GroupName').text(data.errors.shiftgroupname);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.shiftgroup.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'shiftgroupcode': $('#shiftgroupcode').val(), 
          'shiftgroupname': $('#shiftgroupname').val(), 
          'dayshift': $('#dayshift').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorShift GroupName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.shiftgroupname) {
                $('.errorShift GroupName').removeClass('hidden');
                $('.errorShift GroupName').text(data.errors.shiftgroupname);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorShift GroupName').addClass('hidden');

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'shiftgroup/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="shiftgroupname"]').val(data.shiftgroupname);
            $('[name="startshiftdaily"]').val(data.startshiftdaily);
            $('[name="shiftdetail"]').val(data.shiftdetail);
            $('[name="startdate"]').val(data.startdate);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Shift Group'); // Set title to Bootstrap modal title
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'shiftgroup/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'shiftgroupname': $('#shiftgroupname').val(), 
            'startshiftdaily': $('#startshiftdaily').val(), 
            'shiftdetail': $('#shiftdetail').val(), 
            'startdate': $('#startdate').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorShift GroupName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.shiftgroupname) {
                $('.errorShift GroupName').removeClass('hidden');
                $('.errorShift GroupName').text(data.errors.shiftgroupname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'shiftgroup/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'shiftgroupname': $('#shiftgroupname').val(), 
            'startshiftdaily': $('#startshiftdaily').val(), 
            'shiftdetail': $('#shiftdetail').val(), 
            'startdate': $('#startdate').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                    $("#btnSave").attr("onclick","save()");
                    $("#btnSaveAdd").attr("onclick","saveadd()");
                  } 
                },
              })
      };

      function delete_id(id, shiftgroupname)
      {

        //var varnamre= $('#shiftgroupname').val();
        var shiftgroupname = shiftgroupname.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + shiftgroupname + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'shiftgroup/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function bulk_delete()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-red',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "shiftgroup/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error deleting data!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script> 
   @stop
