@extends('layouts.editor.templatewinopenform')
@section('content')
<!-- Content Wrapper. Contains page content --> 
<section class="content" style="margin-top: -50px">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border"> 
        <h4>Change Group Member History</h4>
          <!-- <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button> -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Employee</th>
                  <th>Group From</th>
                  <th>Group To</th>
                  <th>Date Change</th> 
                  <th>Date Change To</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  


@stop
@section('scripts')
<script>
      var table;
      $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "paging":   false,
         "info":     false, 
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/shiftgroupmember/history/data')}}"+'/{{$getemployeeid}}',
         columns: [   
         { data: 'employeename', name: 'employeename' },
         { data: 'groupfrom', name: 'groupfrom' },
         { data: 'groupto', name: 'groupto' },
         { data: 'datechange', name: 'datechange' }, 
         { data: 'datechangeto', name: 'datechangeto' }, 
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

     
   </script> 
   @stop
