@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
 <style type="text/css">
  .toolbar {
      float: left;
  }
  th,td  {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
  }
</style>
<!-- Content Header (Page header) -->
<!-- <section class="content-header"> -->
<section class="content-header">
  <h4>
    Shift Group Member
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Shift Group Member</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button id="btn-show-all-children" type="button" class="btn btn-success btn-flat"><i class="fa fa-expand"></i> Expand All</button>
          <button id="btn-hide-all-children" type="button" class="btn btn-success btn-flat"><i class="fa fa-compress"></i> Collapse All</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe" cellspacing="0" width="98%">
              <thead>
                <tr> 
                  <th>Member</th>
                  <th>Group Code</th> 
                  <th>Group Name</th> 
                  <th>Day Shift</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
             <script id="details-template" type="text/x-handlebars-template">
                <!-- <div class="label label-info">Loan @{{notrans}} Posts</div><br/><br/> -->
                <table class="table details table-bordered" id="posts-@{{id}}">
                    <thead style="background-color: #ddd">
                    <tr>
                        <th>NIK</th>
                        <th>Employee Name</th>
                        <th>Department</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Shift Group Member Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">Employee Name</label>
              <div class="col-md-8">
                <input name="employeename" id="employeename" class="form-control" type="text">
                <small class="errorShift Group MemberName hidden alert-danger"></small> 
              </div>
            </div> 

          <div class="form-group">
              <label class="control-label col-md-3">Shift Group</label>
              <div class="col-md-8">
                  <select class="form-control" name="shiftgroupid" id="shiftgroupid">
                      @foreach($shiftgroup_list AS $shiftgroup_lists)
                          <option value="{{ $shiftgroup_lists->id }}">{{ $shiftgroup_lists->shiftgroupname }}</option>
                      @endforeach
                  </select>
              </div>
            </div> 
          </div>

          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Date Change</label>
            <div class="col-sm-8">
             <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
              {{ Form::text('datechange', old('datechange'), array('class' => 'form-control', 'placeholder' => 'Date Change*', 'required' => 'true', 'id' => 'datechange')) }}
            </div><!-- /.input group --> 
          </div>
        </div> 

        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <!-- <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-sticky-note"></i> History</button> -->
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  $(function() {
    var template = Handlebars.compile($("#details-template").html());
    var table;
      //datatables
      table = $('#dtTable').DataTable({ 
      // editor 
       colReorder: true,
       fixedHeader: true, 
       responsive: true,
       "scrollX": false,
       //rowReorder: true, 
      //  "rowReorder": {
      //     "update": false,
      // },  
       // dttables
       processing: true,
       serverSide: true,
       "pageLength": 25,   
       "scrollY": "360px", 
       "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],  
       ajax: "{{ url('editor/shiftgroupmember/data') }}",
       columns: [  
        {
          "className": 'details-control',
          "orderable": false,
          "searchable": false,
          "data": null,
          "defaultContent": '<a href="#" style="" class="btn btn-default btn-xs"><i class="fa fa-users"></i> Member</a>'
        },
       { data: 'shiftgroupcode', name: 'shiftgroupcode' }, 
       { data: 'shiftgroupname', name: 'shiftgroupname' }, 
       { data: 'dayshift', name: 'dayshift' }, 
       ],
        order: [[1, 'asc']]
     });

      // Add event listener for opening and closing details
      $('#dtTable tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row(tr);
          var tableId = 'posts-' + row.data().id;

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
              // Open this row
              row.child(template(row.data())).show();
              initTable(tableId, row.data());
              tr.addClass('shown');
              tr.next().find('td');
          }
      });

      function initTable(tableId, data) {
        // alert("sad");
          $('#' + tableId).DataTable({
              processing: true,
              serverSide: true,
              responsive: true,
              "scrollX": false,
              ajax: data.details_url,
              columns: [
                  { data: 'nik', name: 'nik', "width": "10%"  },
                  { data: 'employeename', name: 'employeename', "width": "10%"  },
                  { data: 'departmentname', name: 'departmentname', "width": "10%"  },
                  { data: 'positionname', name: 'positionname', "width": "10%"  },
                  { data: 'action', name: 'action', "width": "10%"  },
              ]
          })
      }

      // table.on( 'draw', function () {
      //     $.each( detailRows, function ( i, id ) {
      //         $('#'+id+' td.details-control').trigger( 'click' );
      //     } );
      // } );

      //check all
      // $("#check-all").click(function () {
      //   $(".data-check").prop('checked', $(this).prop('checked'));
      // });

      //auto number
      // table.on( 'order.dt search.dt', function () {
      //     table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      //         cell.innerHTML = i+1;
      //     } );
      // } ).draw();

      // Handle click on "Expand All" button
      $('#btn-show-all-children').on('click', function(){
          // Expand row details
          table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
      });

      // Handle click on "Collapse All" button
      $('#btn-hide-all-children').on('click', function(){
          // Collapse row details
          table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
      });

    });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorShift Group MemberName').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Shift Group Member'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.shiftgroupmember.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'shiftgroupmembername': $('#shiftgroupmembername').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
        $('.errorShift Group MemberName').addClass('hidden');

        if ((data.errors)) {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Data is required!', 'Error Validation', options);
          
          if (data.errors.shiftgroupmembername) {
            $('.errorShift Group MemberName').removeClass('hidden');
            $('.errorShift Group MemberName').text(data.errors.shiftgroupmembername);
          }
        } else {

          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);
          $('#modal_form').modal('hide');
                $('#form')[0].reset(); // reset form on modals
                reload_table(); 
              } 
            },
          })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.shiftgroupmember.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'shiftgroupmembername': $('#shiftgroupmembername').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorShift Group MemberName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.shiftgroupmembername) {
                $('.errorShift Group MemberName').removeClass('hidden');
                $('.errorShift Group MemberName').text(data.errors.shiftgroupmembername);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorShift Group MemberName').addClass('hidden');
      $("#btnSave").attr("onclick","update("+id+")");
      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");
      $("#employeename").removeAttr("disabled"); 

      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'shiftgroupmember/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            $('[name="id_key"]').val(data.id); 
            $('[name="edit"]').val(data.edit);
            $('[name="status"]').val(data.status);
            $('[name="employeename"]').val(data.employeename);
            $('[name="shiftgroupid"]').val(data.shiftgroupid);
            $("#employeename").attr("disabled", "disabled");
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Shift Group Member'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'shiftgroupmember/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'shiftgroupid': $('#shiftgroupid').val(), 
            'datechange': $('#datechange').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
          $('.errorShift Group MemberName').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Data is required!', 'Error Validation', options);
           
            if (data.errors.shiftgroupmembername) {
              $('.errorShift Group MemberName').removeClass('hidden');
              $('.errorShift Group MemberName').text(data.errors.shiftgroupmembername);
            }
          } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          // $('#table details table-bordered tbody').off('click', 'td.details-control');
          // openTableRows.push(rowIndex);
          $('#modal_form').modal('hide');
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
        } 
      },
    })
  };

  function showhistory(id)
  {
   var url = '../editor/shiftgroupmember/history/' + id;
     PopupCenter(url,'Popup_Window','700','400');
  }

  function PopupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
          
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
          
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
    
    // Puts focus on the newWindow  
    if (window.focus) {  
      newWindow.focus();  
    }  
  } 
      
   </script> 
   @stop
