 @extends('layouts.editor.template')
 @section('content')


 <style type="text/css">
 .input-sm {
      height: 22px;
      padding: 1px 3px;
      font-size: 12px;
      line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
      border-radius: 0px;
      /*width: 90% !important;*/
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>

   
<section class="content-header">
  <h4>
    <i class="fa fa-clock-o"></i> Shift Schedule
    <small>Shift Schedule</small>
  </h4>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Shift Schedule</a></li>
    <li class="active">Shift Schedule</li>
  </ol>
</section>

<section class="content">
  <div class="row">
      <div class="col-xs-1">
      </div>
      <div class="col-xs-9">
        <div class="box box-danger-ghci">   
            <div class="box-header with-border">
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label col-md-3">NIK</label>
                <div class="col-md-8">
                  : {{$shiftschedule->nik}}
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-8">
                  : {{$shiftschedule->employeename}}
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label col-md-3">Position</label>
                <div class="col-md-8">
                  : {{$shiftschedule->positionname}}
                </div>
              </div>
            </div>
            </div> 
        </div>
      </div>
  </div> 
    <div class="row">
        <div class="col-xs-1">
        </div>
        <div class="col-sm-9">
            <div class="box box-danger-ghci">   
                <div class="box-header with-border">
                  <div class="row">
                      <div class="box-body"> 
                          <table id="dtTable" class="table table-bordered table-hover stripe">
                            <thead>
                              <tr>
                                <th colspan="4"><center>Shift</center></th>
                                <th colspan="5"><center>Shift Exchange</center></th>
                                <th rowspan="2"><center>Schedule</center></th>
                              </tr>
                              <tr>
                                <th>Date In</th>
                                <th>Absence Type</th>
                                <th>Group</th>
                                <th>Shift</th>
                                <th>Employee</th>
                                <th>Date Exchange</th>
                                <th>Shift Group</th>
                                <th>Shift</th>
                                <th>Action</th>
                              </tr>
                             </thead> 
                              <tbody> 
                              </tbody>
                          </table> 
                      <a href="{{ URL::route('editor.time.index') }}" class="btn btn-primary-ghci pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
                 </div>
              </div>
          </div>
      </div> 
    </div>  
</section><!-- /.content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
         <h3 class="modal-title">Shift Schedule Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">Employee Name</label>
              <div class="col-md-8">
                  <select class="form-control select2" name="employeeid" id="employeeid" style="width: 100%">
                    @foreach($employee_list AS $employee_lists)
                      <option value="{{ $employee_lists->id }}">NIK: {{ $employee_lists->nik }} | Name: {{ $employee_lists->employeename }}</option>
                    @endforeach
                  </select>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Date Exchange</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('exchangedate', old('exchangedate'), array('class' => 'form-control', 'placeholder' => 'Date Change*', 'id' => 'exchangedate')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@stop 

@section('scripts')
<script>
  var table;
  $(document).ready(function() {
    //datatables
    table = $('#dtTable').DataTable({ 
     processing: true,
     serverSide: true,
     "pageLength": 50,
     "scrollY": "360px",
     "ordering": true,
     "rowReorder": true,
     "order": [[ 0, "asc" ]],
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax : '../../datadetail/' + {{$shiftschedule->periodid}} + '/'  + {{$shiftschedule->employeeid}},
     columns: [  
     { data: 'datein', name: 'datein' },
     { data: 'absencetypename', name: 'absencetypename' },
     { data: 'shiftgroupname', name: 'shiftgroupname' },
     { data: 'shiftname', name: 'shiftname' },
     { data: 'exchangeemployeename', name: 'exchangeemployeename' },
     { data: 'exchangedate', name: 'exchangedate' },
     { data: 'exchangeshiftgroupname', name: 'exchangeshiftgroupname' },
     { data: 'exchangeshiftname', name: 'exchangeshiftname' },
     { data: 'action_delete', name: 'action_delete' },
     { data: 'action', name: 'action' },
     ]
   }); 
  });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  $(document).ready(function () {
    $("#timeTable").DataTable(
    {
        "language": {
          "emptyTable": "-"
        }
      } 
    );
  });
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  function edit(id)
  { 
      $('.errorEmployeename').addClass('hidden');
      $("#btnSave").attr("onclick","update("+id+")");

      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url : '../../../shiftschedule/editdetail/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $('[name="id_key"]').val(data.id); 
          $('[name="employeeid"]').val(data.employeeid);
          $('[name="exchangedate"]').val(data.exchangedate);
          $('#employeeid').select2('data', {id: data.employeeid, a_key: data.exchangeemployeename});
          $('[name="status"]').val(data.status);
          $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Exchange Shift Schedule'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function update(id)
  {
      save_method = 'update'; 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url: '../../../shiftschedule/editdetail/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'employeeid': $('#employeeid').val(), 
          'exchangedate': $('#exchangedate').val()
        },
        success: function(data) {  
          $('.errorEmployeename').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Date not available or data is null!', 'Error', options);
           
            if (data.errors.cityname) {
              $('.errorEmployeename').removeClass('hidden');
              $('.errorEmployeename').text(data.errors.cityname);
            }
          } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#modal_form').modal('hide');
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
        } 
      },
    })
  };

  function delete_id(id)
  {

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to delete data?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () { 
         }
       },
      confirm: {
      text: 'DELETE',
      btnClass: 'btn-red',
      action: function () {
       $.ajax({
        url : '../../../shiftschedule/deletedetail/' + id,
        type: "DELETE",
        data: {
          '_token': $('input[name=_token]').val() 
        },
        success: function(data)
        { 
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully deleted data!', 'Success Alert', options);
          reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          $.alert({
            type: 'red',
            icon: 'fa fa-danger', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'Error deleteing data!',
          });
        }
      });
       }
     },
   }
 });
}
</script>  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_time').submit(); 
          }
        },

      }
    });
  });
</script>

@stop

