@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<style>
  /* Center the loader */
  #loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

  /* Add animation to "page content" */
  .animate-bottom {
    position: relative;
    -webkit-animation-name: animatebottom;
    -webkit-animation-duration: 1s;
    animation-name: animatebottom;
    animation-duration: 1s
  }

  @-webkit-keyframes animatebottom {
    from { bottom:-100px; opacity:0 } 
    to { bottom:0px; opacity:1 }
  }

  @keyframes animatebottom { 
    from{ bottom:-100px; opacity:0 } 
    to{ bottom:0; opacity:1 }
  }

  #myDiv {
    display: none;
    text-align: center;
  }
</style> 

<div id="loader"></div>
<!-- <section class="content-header"> -->
<section class="content-header">
  <h4>
    <i class="fa fa-clock-o"></i> Shift Schedule
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Shift Schedule</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
            <!-- <a onClick="CraeteData()" class="btn btn-primary-ghci btn-flat"  data-toggle="tooltip" data-placement="top" title="Create for get data from selected period"> <i class="fa fa-play"></i>  Create</a> -->
                <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
                  <div class="col-sm-12 pull-right">
                    <input type="hidden" id="type" name="type" value="generate"> 
                    <input type="hidden" id="userid" name="userid" value="">  
                      {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }}  
                </div>
              </div>
              <a onClick="RefreshData()" class="btn btn-primary btn-flat"> <i class="fa fa-refresh"></i>  Refresh</a>
 
          <div class="box-tools pull-right">
            <div class="tableTools-container">
              </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Action</th> 
                  <th>Period</th> 
                  <th>NIK</th> 
                  <th>Employee Name</th> 
                  <th>Position</th>
                  <th>Department</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Shift Schedule Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">Shift Schedule Name</label>
              <div class="col-md-8">
                <input name="payrollname" id="payrollname" class="form-control" type="text">
                <small class="errorShift ScheduleName hidden alert-danger"></small> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        $("#loader").hide();
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         // "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/shiftschedule/data') }}",
         columns: [  
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'description', name: 'description' }, 
         { data: 'nik', name: 'nik' }, 
         { data: 'employeename', name: 'employeename' },
         { data: 'positionname', name: 'positionname' },
         { data: 'departmentname', name: 'departmentname' } 
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
  
      function RefreshData()
       {  
        $("#loader").show();
        CraeteData();
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilteronly') }}",
          data: {
            '_token': $('input[name=_token]').val(),     
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully filtering data!', 'Success Alert', options);
            // reload_table();
            $("#loader").hide(100);
          }
        }) 
      }; 

      function CraeteData()
      { 
 
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();
         $.ajax({
          url : 'time/create/' + periodid,
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val(),
            'periodid': $('#periodid').val(),
          },
          success: function(data) { 
          }
        }) 
      }

      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });
   </script> 
   @stop
