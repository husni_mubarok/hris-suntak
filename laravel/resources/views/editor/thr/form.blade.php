 @extends('layouts.editor.template')
 @section('content')

 <style type="text/css">
 	.input-sm {
  height: 22px;
  padding: 1px 3px;
  font-size: 12px;
  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 0px;
  width: 90% !important;
}
 </style>
 <!-- Content Header (Page header) -->
 <section class="content-header">
 	<h4>
 		THR
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">THR</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-solid">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12"> 
 				<div class="col-md-12"> 
 					<div class="col-md-12">   
 						<div class="tab-content">

 							{!! Form::model($thr, array('route' => ['editor.thr.updatedetail', $thr->id, $thr->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_thr'))!!}
 							<div id="consummable" class="tab-pane fade in active"> 
 								<div class="box-body">
 									<div class="div_overflow"> 

 										<table class="table table-striped table-hover" id="thrTable">
 											<thead>
 												<tr>
 													<th>Employee</th>
 													<th>Join Date </th>
 													<th>NPWP</th> 
 													<th>Tax Status</th>
 													<th>Basic</th>
 													<th>Value</th>
 													<th>THR</th>
 													<th>Adj THR</th>
 													<th>Total THR</th>
 													<th>PPh 21</th>
 													<th>Netto</th> 
 												</tr>
 											</thead>
 											<tbody> 
 												@foreach($thr_detail as $key => $thr_details)
 												<tr>
 													<td class="col-sm-1 col-md-1">
 														{{$thr_details->employeename}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{$thr_details->joindate}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{$thr_details->npwp}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{$thr_details->taxstatus}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{number_format($thr_details->basic,0)}} 
 													</td>
 													<td class="col-sm-1 col-md-1">
 														{{ Form::number('detail['.$thr_details->id.'][value]', old($thr_details->value.'[value]', $thr_details->value), ['id' => 'value'.$thr_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 1*', 'oninput' => 'nettoal()']) }} 
 													</td>  
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$thr_details->id.'][thr]', old($thr_details->thr.'[thr]', $thr_details->thr), ['id' => 'thr'.$thr_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 2*', 'oninput' => 'tdayal()']) }} 
 													</td> 
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$thr_details->id.'][adjthr]', old($thr_details->adjthr.'[adjthr]', $thr_details->adjthr), ['id' => 'adjthr'.$thr_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Day 3*', 'oninput' => 'tdayal()']) }} 
 													</td> 
 													<td class="col-sm-1 col-md-1">  
 														{{ Form::number('detail['.$thr_details->id.'][tthr]', old($thr_details->id.'[tthr]', $thr_details->tthr), ['class' => 'form-control input-sm', 'placeholder' => 'Day 4*', 'id' => 'tthr'.$thr_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$thr_details->id.'(); tdayal();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$thr_details->id.'][pph21]', old($thr_details->id.'[pph21]', $thr_details->pph21), ['class' => 'form-control input-sm', 'placeholder' => 'Day 5*', 'id' => 'pph21'.$thr_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$thr_details->id.'(); total();']) }} 
 													</td> 
 													<td class="col-sm-1">  
 														{{ Form::number('detail['.$thr_details->id.'][netto]', old($thr_details->id.'[netto]', $thr_details->tthr + $thr_details->pph21), ['class' => 'form-control input-sm', 'placeholder' => 'T Day*', 'id' => 'netto'.$thr_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$thr_details->id.'(); total();']) }} 
 													</td>  
 												</td>  
 											</tr>  
 											@endforeach

 										</tbody>
 									</table>
 								</div> 
 								<!-- /.box-body -->
 							</div>
 						</div> 
 					</div> 
 					<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
 					<a href="{{ URL::route('editor.thr.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
 				</div>
 				{!! Form::close() !!} 
 			</div>
 		</div>
 	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#thrTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		} 
		);
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_thr').submit(); 
					}
				},

			}
		});
	});
</script>
@stop

