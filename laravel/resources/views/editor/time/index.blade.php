@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h4>
    <i class="fa fa-clock-o"></i> Time
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Time</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
            <!-- <a onClick="CraeteData()" class="btn btn-warning btn-flat" style="float:left; margin-right:5px"  data-toggle="tooltip" data-placement="top" title="Create for get data from selected period"> <i class="fa fa-play"></i>  Create</a> -->
                <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
                  <div class="col-sm-12 pull-right">
                    <input type="hidden" id="type" name="type" value="generate"> 
                    <input type="hidden" id="userid" name="userid" value="">  
                      @if(empty($period))
                        {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'CraeteData();')) }}  
                      @else
                        {{ Form::select('periodid', $payperiod_list, old('periodid', $period), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'CraeteData();')) }}  
                      @endif  
                </div>
              </div>
              <a onClick="GenerateData()" class="btn btn-danger btn-flat" style="float:left; margin-right:5px"  data-toggle="tooltip" data-placement="top" title="Calculate time from selected period"> <i class="fa fa-magic"></i> Generate <sup><span class="label label-warning">!</span></sup></a>
              <a onClick="RefreshData()" class="btn btn-success btn-flat" style="float:left; margin-left:5px"> <i class="fa fa-refresh"></i>  Refresh</a>
 
          <div class="box-tools pull-right">
            <div class="tableTools-container">
              </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Action</th> 
                  <th>Period</th> 
                  <th>NIK</th> 
                  <th>Employee Name</th> 
                  <th>Position</th>
                  <th>Department</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Time Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
 
            <div class="form-group">
              <label class="control-label col-md-3">Time Name</label>
              <div class="col-md-8">
                <input name="payrollname" id="payrollname" class="form-control" type="text">
                <small class="errorTimeName hidden alert-danger"></small> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         // "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/time/data') }}",
         columns: [  
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'description', name: 'description' }, 
         { data: 'nik', name: 'nik' }, 
         { data: 'employeename', name: 'employeename' },
         { data: 'positionname', name: 'positionname' },
         { data: 'departmentname', name: 'departmentname' } 
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 
      function CraeteData()
      {
 
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to create <b><u>' + perioddesc + '</u></b> data?',
          type: 'orange',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-orange',
            action: function () { 
             waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
             $.ajax({
              url : 'time/create/' + periodid,
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val(),
                'periodid': $('#periodid').val(),
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                waitingDialog.hide();
                toastr.success('Successfully created data!', 'Success Alert', options);
                RefreshData();
                // reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error creating data!',
                });
              }
            });
           }
         },

       }
     });
      }

      function GenerateData()
      {
        // alert("asd");
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate <b><u>' + perioddesc + '</u></b> data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
             waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
             $.ajax({
              url : 'time/generate/' + periodid,
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                waitingDialog.hide();
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generate data!',
                });
              }
            });
           }
         },

       }
     });
      }
      function RefreshData()
       {  
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilteronly') }}",
          data: {
            '_token': $('input[name=_token]').val(),     
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            // var options = { 
            //   "positionClass": "toast-bottom-right", 
            //   "timeOut": 1000, 
            // };
            // toastr.success('Successfully filtering data!', 'Success Alert', options);
            reload_table();
          }
        }) 
      }; 
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

       /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);
   </script> 
   @stop
