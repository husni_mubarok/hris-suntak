 @extends('layouts.editor.template')
 @section('content')

 <style type="text/css">
 .input-sm {
      height: 22px;
      padding: 1px 3px;
      font-size: 12px;
      line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
      border-radius: 0px;
      /*width: 90% !important;*/
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>

<section class="content-header">
  <h1>
    <i class="fa fa-clock-o"></i> Time
    <small>Time</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time</a></li>
    <li class="active">Time</li>
  </ol>
</section>

<section class="content">

  <div class="row">
      <!-- <div class="col-xs-2">
      </div> -->
      <div class="col-xs-4">
        <div class="box box-danger">   
            <div class="box-header with-border">

            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label col-md-3">NIK</label>
                <div class="col-md-8">
                  : {{$time->nik}}
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label col-md-3">NAME</label>
                <div class="col-md-8">
                  : {{$time->employeename}}
                </div>
              </div>
            </div>

            <!-- <div class="col-md-12">
              <div class="form-group">
                <label class="control-label col-md-3">PERIOD</label>
                <div class="col-md-8">
                  : {{$time->description}}
                </div>
              </div>
            </div> -->
  
            </div> 
        </div>
      </div>
  </div>

    <div class="row">
        <!-- <div class="col-xs-1">
        </div> -->
        <div class="col-sm-12">
            <div class="box box-danger">  
              {!! Form::model($time, array('route' => ['editor.time.update', $time->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_time'))!!}
              
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_time" class="table table-bordered table-hover stripe" style="background-color: #fff"> 
                							<thead>
                                <tr>
                                  <th>Manual</th>
                                  <th>Date In</th>
                                  <th>Type</th>
                                  <th>Holiday</th>
                                  <th>Day In</th>
                                  <th>Day Out</th>
                                  <th>Actual In</th>
                                  <th>Actual Out</th>
                                  <th>Work Hour</th>
                                  <th>OT In</th>
                                  <th>OT Out</th>
                                  <th>OT Hour</th>
                                  <th>OT Con</th>
                                </tr>
    							             </thead> 
                                <tbody> 
                                    @foreach($time_detail as $key => $time_details)
                                    <tr>
                                        <td class="col-sm-1 col-md-1">
                                          <input type="checkbox" id="manual" name="detail[{{$time_details->id}}][manual]" value="1" @if($time_details->manual == 1) checked @endif>
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$time_details->datein}} 
                                        </td> 
                                        <td class="col-sm-1 col-md-1"> 
                                            {{$time_details->absencetypename}}  
                                        </td>  
                                        <td class="col-sm-1 col-md-1">
                                            @if($time_details->holiday==0) 
                                              No
                                            @else
                                              <span class="label label-danger"> Yes </span>
                                            @endif 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$time_details->dayin}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                            {{$time_details->dayout}} 
                                        </td>
                                        <td class="col-sm-1 col-md-1">
                                           {{$time_details->actualin}} 
                                        </td> 
                                        <td class="col-sm-1 col-md-1">
                                          {{$time_details->actualout}}
                                        </td>  
                                        <td class="col-sm-1 col-md-1">
                                           {{$time_details->workhour}}   
                                        </td> 
                                        <td class="col-sm-1 col-md-1"> 
                                           {{$time_details->overtimein}}
                                        </td> 
                                        <td class="col-sm-1 col-md-1"> 
                                           {{$time_details->overtimeout}}
                                        </td> 
                                        <td class="col-sm-1 col-md-1">
                                           {{$time_details->overtimehouractual}}      
                                        </td> 
                                         <td class="col-sm-1 col-md-1">
                                           {{$time_details->overtimehour}}      
                                        </td> 
                                </tr>  
                                @endforeach

                            </tbody>
              					</table> 
              				    <!-- /.box-body -->
                         <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a> 
                        <a href="{{ URL::route('editor.time.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
					         </div>
                {!! Form::close() !!}     
          </div>
			</div> 
		</div>  
</section><!-- /.content -->
       

@stop 

@section('scripts')
 
<script>
  $(document).ready(function () {
    $("#timeTable").DataTable(
    {
      "language": {
        "emptyTable": "-"
      }
    } 
    );
  });
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green', 
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_time').submit(); 
          }
        },

      }
    });
  });

    $(document).ready(function() { 
        $("#table_time").dataTable( {
            // "sScrollX": false,
             "scrollY": "300px",
             "scrollX": true,
             // "scrollx": "300%",
             "bPaginate": false,
             "autowidth": true,
             "ordering": false,
             // fixedColumns:   {
             //  leftColumns: 3
             // },
        });
    });
</script>

@stop

