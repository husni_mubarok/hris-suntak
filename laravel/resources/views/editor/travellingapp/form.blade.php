 
@extends('layouts.editor.template')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-8">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($travelling))
				{!! Form::model($travelling, array('route' => ['editor.travelling.update', $travelling->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_travelling'))!!}
				@else
				{!! Form::open(array('route' => 'editor.travelling.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_travelling'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($travelling))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Travelling
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('notrans', 'No Trans') }}
								{{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('datetrans', 'Date Trans') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('employeeid', 'Employee Name') }}
								{{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Select Employee', 'id' => 'employeeid')) }} 
								  
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('educationtypeid', 'Travelling Type') }}
								{{ Form::select('educationtypeid', $travellingtype_list, old('educationtypeid'), array('class' => 'form-control', 'placeholder' => 'Select Travelling Type', 'id' => 'educationtypeid')) }} 
								  
							</div>
						</div>  

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('cityid', 'City') }}
								{{ Form::select('cityid', $city_list, old('cityid'), array('class' => 'form-control select2', 'placeholder' => 'Select City', 'id' => 'cityid')) }} 
								  
							</div>
						</div>  

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('travellingfrom', 'Travelling From') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('travellingfrom', old('travellingfrom'), array('class' => 'form-control', 'placeholder' => 'Travelling From*', 'required' => 'true', 'id' => 'travellingfrom', 'class' => 'form-control datepicker')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>   

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('travellingto', 'Travelling To') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('travellingto', old('travellingto'), array('class' => 'form-control', 'placeholder' => 'Travelling To*', 'required' => 'true', 'id' => 'travellingto')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>   

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('actualin', 'Actual In') }}
								{{ Form::text('actualin', old('actualin'), array('class' => 'form-control', 'placeholder' => 'Actual In*', 'required' => 'true', 'id' => 'actualin')) }}
							</div>
						</div> 

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('amount', 'Amount') }}
								{{ Form::text('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount*', 'required' => 'true', 'id' => 'amount')) }}
							</div>
						</div>  
						 
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('attachment', 'Attachment') }}<br>
								<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
								<br/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<div class="row">  
						<div class="col-md-12">
							<div class="form-group">
							@if(isset($travelling)) 
				                  @if($travelling->status == 9)
				                    <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save and Active</button>   
				                    <script type="text/javascript"> 
				                      $(document).ready(function(){
				                         hidebtnactive();
				                      });
				                    </script>
				                  @else
				                    <a href="#" onclick="cancel();" class="btn btn-danger btn-flat"><i class="fa fa-minus-square"></i> Cancel</a>  
				                  @endif
				                @endif
					            <button type="submit" id="btnsave" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.travelling.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
			{!! Form::close() !!}
		</div>
	</div>
</section>
@stop

@if(isset($travelling)) 
@section('scripts') 
<script type="text/javascript"> 
  function cancel()
  {   
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this data?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () { 
         }
       },
       confirm: {
        text: 'CANCEL',
        btnClass: 'btn-red',
        action: function () {
         $.ajax({
          url : '../../travelling/cancel/' + {{$travelling->id}},
          type: "PUT", 
          data: {
            '_token': $('input[name=_token]').val() 
          }, 
          success: function(data) {  
            //var loc = 'ap_invoice';
            if ((data.errors)) { 
              alert("Cancel error!");
            } else{
              window.location.href = "{{ URL::route('editor.travelling.index') }}";
            }
          }, 
        }); 
       }
     },
    }
  });
  }  
  function hidebtnactive() {
      $('#btnsave').hide(100); 
  }

  function cal_sparator() {  

	  var amount = document.getElementById('amount').value;
	  var result = document.getElementById('amount');
	  var rsamount = (amount);
	  result.value = rsamount.replace(/,/g, "");  
	}

	window.onload= function(){ 

	  n2= document.getElementById('amount');

	  n2.onkeyup=n2.onchange= function(e){
	    e=e|| window.event; 
	    var who=e.target || e.srcElement,temp;
	    if(who.id==='amount')  temp= validDigits(who.value,0); 
	    else temp= validDigits(who.value);
	    who.value= addCommas(temp);
	  }   
	  n2.onblur= function(){
	    var 
	    temp2=parseFloat(validDigits(n2.value));
	    if(temp2)n2.value=addCommas(temp2.toFixed(0));
	  } 
	}  
</script>
@stop  
@endif