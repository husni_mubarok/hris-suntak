 
@extends('layouts.editor.template')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-file-excel-o"></i> Upload Time
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Upload Time</li>
  </ol>
</section>


<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-4">
			<div class="box box-danger">
				{!! Form::open(array('route' => 'editor.uploadtime.storeimport', 'class'=>'create', 'id' => 'form_uploadtime', 'files' => 'true'))!!}  
				{{ csrf_field() }}	  
					<div class="box-header with-border">
						<div class="col-md-8">
							<div class="form-group">
								{{ Form::label('import_file', 'Import Item from Excel File (.ods)') }}<br>
								<span>Choose file</span><input type="file" name="import_file" />
								<br/>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-file-excel-o"></i> Import</button> 
							</div>
						</div>
					</div> 
				{!! Form::close() !!}
			</div>
		</div>
</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-danger">
        			<div class="box-header"> 
					 	<table id="dtTable" class="table table-bordered table-hover stripe">
				          <thead>
				            <tr> 
				              <th>NIK</th> 
				              <th>Employee Name</th> 
				              <th>Date</th>
				              <th>Actual In</th>
				              <th>Actual Out</th> 
				            </tr>
				          </thead>
				          <tbody>
				          </tbody>
				     </table>
				 </div>
		 		</div>
			</div>
		</div>
	</div>
</section>
@stop
 

@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/uploadtime/data') }}",
         columns: [  
         { data: 'nik', name: 'nik' },
         { data: 'employeename', name: 'employeename' }, 
         { data: 'date', name: 'date' },
         { data: 'actualin', name: 'actualin' },
         { data: 'actualout', name: 'actualout' }
         ]
       }); 
      });
  </script>
  @stop