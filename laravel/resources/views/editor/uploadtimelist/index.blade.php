 
@extends('layouts.editor.template')
@section('content')


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-file-excel-o"></i> Upload Time List
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Upload Time List</li>
  </ol>
</section>


<!-- Main content -->
<section class="content"> 
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-danger">
        			<div class="box-header"> 
					 	<table id="dtTable" class="table table-bordered table-hover stripe">
				          <thead>
				            <tr> 
				              <th>#</th>
				              <th>Date Upload</th> 
				              <th>File Name</th>  
				              <th>Uploaded By</th>  
				              <th>Download</th>
				            </tr>
				          </thead>
				          <tbody>
				          </tbody>
				     </table>
				 </div>
		 		</div>
			</div>
		</div>
	</div>
</section>
@stop
 

@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/uploadtimelist/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'dateupload', name: 'dateupload' },
         { data: 'attachmentname', name: 'attachmentname' }, 
         { data: 'username', name: 'username' },
         { data: 'attachment', name: 'attachment' },
         ]
       }); 
      });
  </script>
  @stop