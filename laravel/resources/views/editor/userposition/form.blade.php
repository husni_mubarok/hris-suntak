@extends('layouts.editor.template')
@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-user"></i> User Position
    <small>Auth</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Auth</a></li>
    <li class="active">User Position</li>
  </ol>
</section>
@actionStart('user', 'create|update')
<section class="content"> 
    <div class="row"> 
    	<div class="col-sm-6">
            <div class="box box-danger"> 
                	<div class="box-header with-border">
                    	<div class="row">
	                        <div class="box-body"> 
			                @include('errors.error') 
		                    {!! Form::model($user, array('route' => ['editor.userposition.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('username', 'Username') }}
		                    	@if(isset($user))
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control', 'disabled' => 'true']) }}
		                    	@else
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control']) }}
		                    	@endif
		                    	<br> 

		                    	{{ Form::label('position', 'Position') }}
		                    	{{ Form::select('positionid', $position_list, old('positionid'), array('class' => 'form-control', 'required' => 'true')) }}
		                    	<br> 

	                            <button type="submit" class="btn btn-success pull-right btn-flat btn-md"><i class="fa fa-check"></i> Save</button>
			                    <a href="{{ URL::route('editor.userposition.index') }}" class="btn btn-default pull-right btn-flat btn-md" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
	                    		<br>
	                    		<hr>
	                    		{!! Form::close() !!}
		                        <table id="dtTable" class="table table-bordered table-hover">
								  	<thead>
								  	  	<tr>
									      	<th>Position Name</th> 
									      	<th width="10%">Action</th>
								    	</tr>
								  	</thead>
								  	<tbody>
								     @foreach($position_user as $position_users)
								     	<tr>
								     		<td>{{$position_users->positionname}}</td>
								     		<td>
								      			{!! Form::open(array('route' => ['editor.userposition.delete', $position_users->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      				<button type="submit" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
								     		</td>
								     	</tr>
								     @endforeach
									</tbody>
								</table>
							 </div>
					        </div>
				        </div>
				    </div>
				</div>
		</section>
	</div>
</section>
@actionEnd
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#dtTable").DataTable();
    });
</script>
@stop