<footer class="main-footer">
	<div class="pull-right hidden-xs">
  		<b>Version</b> 1.0.2
	</div>
	<strong>Copyright &copy; 2017-2018 <a target="_blank" href="http://jafelmia.com">PT. Jafelmia Multi Artha</a></strong> All rights reserved.
</footer>