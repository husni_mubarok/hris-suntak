  <header class="main-header">
  <nav class="navbar navbar-static-top" role="navigation">
    
      <div class="navbar-header" style="margin-left: 10px; margin-right: 10px">
        <!-- <a href="dashboard" class="navbar-brand"><b>ERP</b>System</a> -->
        <a href="{{url('/')}}"><img src="{{Config::get('constants.path.img')}}/logo_index.png" alt="" /> </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            @actionStart('document', 'read', 'holiday', 'read', 'travelingitem', 'read', 'travelingtype', 'read', 'payrolltype', 'read', 'shift', 'read', 'shiftgroup', 'read', 'shiftschedule', 'read', 'shiftgroupmember', 'read', 'location', 'read', 'city', 'read', 'country', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-archive"></i> Master Data<span class="caret"></span></a>
            @actionEnd
            <ul class="dropdown-menu" role="menu"> 
              @actionStart('document', 'read')
              <li><a href="{{ URL::route('editor.document.index') }}"><i class="fa fa-book"></i> Document</a></li> 
              @actionEnd
              @actionStart('holiday', 'read')
              <li><a href="{{ URL::route('editor.holiday.index') }}"><i class="fa fa-calendar"></i> Holiday</a></li> 
              @actionEnd
              @actionStart('travelingitem', 'read')
              <li><a href="{{ URL::route('editor.travelingitem.index') }}"><i class="fa fa-car"></i>Traveling Item</a></li>
              @actionEnd
              @actionStart('travelingtype', 'read')
              <li><a href="{{ URL::route('editor.travelingtype.index') }}"><i class="fa fa-plane"></i> Traveling Type</a></li>
              @actionEnd
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-clock-o"></i> Time & Attandance</a>
                <ul class="dropdown-menu">
                  @actionStart('payrolltype', 'read')
                  <li><a href="{{ URL::route('editor.payrolltype.index') }}"> Reguler Shift</a></li>
                  @actionEnd
                  @actionStart('shift', 'read')
                  <li><a href="{{ URL::route('editor.shift.index') }}">Daily Shift</a></li> 
                  @actionEnd
                  @actionStart('shiftgroup', 'read')
                  <li><a href="{{ URL::route('editor.shiftgroup.index') }}">Shift Group</a></li> 
                  @actionEnd
                  @actionStart('shiftschedule', 'read')
                  <li><a href="{{ URL::route('editor.shiftschedule.index') }}">Shift Schedule</a></li>  
                  @actionEnd
                  @actionStart('shiftgroupmember', 'read')
                  <li><a href="{{ URL::route('editor.shiftgroupmember.index') }}">Shift Group Member</a></li> 
                  @actionEnd
                </ul>
              </li>      
              @actionStart('year', 'read')
              <li><a href="{{ URL::route('editor.year.index') }}"><i class="fa fa-calendar-o"></i> Year</a></li> 
              @actionEnd  
              <li class="divider"></li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-map"></i> Place</a>
                <ul class="dropdown-menu">
                  @actionStart('location', 'read')
                  <li><a href="{{ URL::route('editor.location.index') }}">Location</a></li>
                  @actionEnd
                  @actionStart('city', 'read')
                  <li><a href="{{ URL::route('editor.city.index') }}">City</a></li>
                  @actionEnd
                  @actionStart('country', 'read')
                  <li><a href="{{ URL::route('editor.country.index') }}">Country</a></li>
                  @actionEnd
                </ul>
              </li> 
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('employee', 'read', 'employeestatus', 'read', 'department', 'read', 'position', 'read', 'religion', 'read', 'golongan', 'read', 'taxstatus', 'read', 'educationlevel', 'read', 'educationmajor', 'read', 'educationtype', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Employee <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              @actionStart('employee', 'read')
              <li><a href="{{ URL::route('editor.employee.index') }}"><i class="fa fa-user"></i> Employee</a></li> 
              @actionEnd 
              @actionStart('employeestatus', 'read')
              <li><a href="{{ URL::route('editor.employeestatus.index') }}"><i class="fa fa-address-card"></i> Employee Status</a></li>
              @actionEnd 
              @actionStart('department', 'read')
              <li><a href="{{ URL::route('editor.department.index') }}"><i class="fa fa-vcard-o"></i> Department</a></li> 
              @actionEnd
              @actionStart('position', 'read')
              <li><a href="{{ URL::route('editor.position.index') }}"><i class="fa fa-briefcase"></i> Position</a></li>  
              @actionEnd
              @actionStart('religion', 'read')
              <li><a href="{{ URL::route('editor.religion.index') }}"><i class="fa fa-eye"></i> Religion</a></li> 
              @actionEnd
              @actionStart('golongan', 'read')
              <li><a href="{{ URL::route('editor.golongan.index') }}"><i class="fa fa-group"></i> Golongan</a></li> 
              @actionEnd
              <!-- <li><a href="{{ URL::route('editor.medicaltype.index') }}"><i class="fa fa-medkit"></i>Medical Type</a></li>  -->
              @actionStart('taxstatus', 'read')
              <li><a href="{{ URL::route('editor.taxstatus.index') }}"><i class="fa fa-money"></i>Tax Status</a></li> 
              @actionEnd
              <li class="divider"></li>  
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-graduation-cap"></i>Education</a>
                <ul class="dropdown-menu">
                  @actionStart('educationlevel', 'read')
                  <li><a href="{{ URL::route('editor.educationlevel.index') }}">Education Level</a></li>
                  @actionEnd
                  @actionStart('educationmajor', 'read')  
                  <li><a href="{{ URL::route('editor.educationmajor.index') }}">Education Major</a></li> 
                  @actionEnd
                  @actionStart('educationtype', 'read') 
                  <li><a href="{{ URL::route('editor.educationtype.index') }}">Education Type</a></li>
                  @actionEnd 
                </ul>
              </li>    
            </ul>
          </li>
          <li class="dropdown">
            @actionStart('renewcontract', 'read', 'historycontract', 'read', 'mutation', 'read', 'promotion', 'read', 'punishment', 'read', 'reward', 'read', 'training', 'read', 'trainingapp', 'read', 'travelling', 'read', 'travellingapp', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-id-card"></i> Personal Management <span class="caret"></span></a>
            @actionEnd
            <ul class="dropdown-menu" role="menu">
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-shopping-basket"></i>Employee Contract</a>
                <ul class="dropdown-menu">
                  @actionStart('renewcontract', 'read') 
                  <li><a href="{{ URL::route('editor.renewcontract.index') }}">Renew Contract</a></li>
                  @actionEnd 
                  @actionStart('historycontract', 'read') 
                  <li><a href="{{ URL::route('editor.historycontract.index') }}">Contract History</a></li> 
                  @actionEnd 
                </ul>
              </li> 
              @actionStart('mutation', 'read') 
              <li><a href="{{ URL::route('editor.mutation.index') }}"><i class="fa fa-exchange"></i>Mutation</a></li>
              @actionEnd 
              @actionStart('promotion', 'read') 
              <li><a href="{{ URL::route('editor.promotion.index') }}"><i class="fa fa-arrow-up"></i>Promotion</a></li>
              @actionEnd 
              @actionStart('punishment', 'read') 
              <li><a href="{{ URL::route('editor.punishment.index') }}"><i class="fa fa-minus-square"></i>Punishment</a></li>
              @actionEnd 
              @actionStart('reward', 'read') 
              <li><a href="{{ URL::route('editor.reward.index') }}"><i class="fa fa-check-square-o"></i>Reward</a></li> 
              @actionEnd 
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-vcard"></i>Training</a>
                <ul class="dropdown-menu">
                  @actionStart('training', 'read') 
                  <li><a href="{{ URL::route('editor.training.index') }}"> Training Request</a></li>
                  @actionEnd 
                  @actionStart('trainingapp', 'read') 
                  <li><a href="{{ URL::route('editor.trainingapp.index') }}"> Training Approval</a></li>
                  @actionEnd 
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-plane"></i>Traveling</a>
                <ul class="dropdown-menu">
                  @actionStart('travelling', 'read') 
                  <li><a href="{{ URL::route('editor.travelling.index') }}"> Traveling Request</a></li>
                  @actionEnd
                  @actionStart('travellingapp', 'read') 
                  <li><a href="{{ URL::route('editor.travellingapp.index') }}"> Traveling Approval</a></li>
                  @actionEnd
                </ul>
              </li>
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('leaving', 'read', 'leavingapp', 'read', 'leavingplafond', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sign-out"></i> Leave <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              @actionStart('leaving', 'read') 
              <li><a href="{{ URL::route('editor.leaving.index') }}"><i class="fa fa-share"></i>Leave Request</a></li>
              @actionEnd 
              @actionStart('leavingapp', 'read') 
              <li><a href="{{ URL::route('editor.leavingapp.index') }}"><i class="fa fa-check"></i>Leave Approval</a></li>
              @actionEnd 
              @actionStart('leavingplafond', 'read') 
              <li><a href="{{ URL::route('editor.leavingplafond.index') }}"><i class="fa fa-table"></i>Leave Plafond</a></li> 
              @actionEnd 
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('orgstructure', 'read', 'police', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sitemap"></i> Organization <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              @actionStart('orgstructure', 'read') 
              <li><a href="{{ URL::route('editor.orgstructure.edit') }}"><i class="fa fa-sitemap"></i>Organization Structure</a></li>
              @actionEnd 
              @actionStart('police', 'read') 
              <li><a href="{{ URL::route('editor.police.index') }}"><i class="fa fa-building"></i>Company Policy</a></li>
              @actionEnd 
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('payperiod', 'read', 'time', 'read', 'uploadtime', 'read', 'uploadtimelist', 'read', 'payroll', 'read', 'overtime', 'read', 'thr', 'read', 'loan', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-clock-o"></i> Time & Payroll <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              @actionStart('payperiod', 'read') 
              <li><a href="{{ URL::route('editor.payperiod.index') }}"><i class="fa fa-calendar"></i>Period</a></li>
              @actionEnd 
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-clock-o"></i> Time</a>
                <ul class="dropdown-menu">
                  @actionStart('time', 'read') 
                  <li><a href="{{ URL::route('editor.time.index') }}"><i class="fa fa-clock-o"></i> Attendance</a></li> 
                  @actionEnd 
                  @actionStart('timeview', 'read') 
                  <li><a href="{{ URL::route('editor.timeview.index') }}"><i class="fa fa-clock-o"></i> Attendance View</a></li> 
                  @actionEnd 
                  @actionStart('uploadtime', 'read') 
                  <li><a href="{{ URL::route('editor.uploadtime.index') }}"><i class="fa fa-file-excel-o"></i> Upload Attendance</a></li>
                  @actionEnd 
                  @actionStart('uploadtimelist', 'read') 
                  <li><a href="{{ URL::route('editor.uploadtimelist.index') }}"><i class="fa fa-file-excel-o"></i> Upload Attendance List</a></li> 
                  @actionEnd 
                </ul>
              </li> 
              @actionStart('payroll', 'read') 
              <!-- <li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-dollar"></i> Payroll</a></li>  -->
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-dollar"></i> Payroll</a>
                <ul class="dropdown-menu">
                  <li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-dollar"></i> Payroll</a></li> 
                  <li><a href="{{ URL::route('editor.payrollview.index') }}"><i class="fa fa-dollar"></i> Payroll View</a></li> 
                </ul>
              </li> 
              @actionEnd 
              @actionStart('overtime', 'read') 
              <li><a href="{{ URL::route('editor.overtime.index') }}"><i class="fa fa-adjust"></i>Overtime</a></li> 
              @actionEnd 
              @actionStart('thr', 'read') 
              <li><a href="{{ URL::route('editor.thr.index') }}"><i class="fa fa-diamond"></i>THR</a></li> 
              @actionEnd  
              @actionStart('loan', 'read') 
              <li><a href="{{ URL::route('editor.loan.index') }}"><i class="fa fa-bank"></i>Employee Loan</a></li>
              @actionEnd 
            </ul>
          </li> 
          <li class="dropdown">
            @actionStart('reportpayroll', 'read', 'reportovertime', 'read', 'reportdocument', 'read', 'reportpromotion', 'read', 'reporttraveling', 'read', 'reportleaving', 'read', 'reporttime', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> Report <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu"> 
              @actionStart('reportpayroll', 'read') 
              <li><a href="{{ URL::route('editor.reportpayroll.index') }}"><i class="fa fa-dollar"></i> Payroll Report</a></li> 
              @actionEnd 
              @actionStart('reportovertime', 'read') 
              <li><a href="{{ URL::route('editor.reportovertime.index') }}"><i class="fa fa-lightbulb-o"></i> Overtime Report</a></li> 
              @actionEnd 
              @actionStart('reportdocument', 'read')  
              <li><a href="{{ URL::route('editor.reportdocument.index') }}"><i class="fa fa-credit-card"></i> Document Expired</a></li>
              @actionEnd 
              @actionStart('reportpromotion', 'read')    
              <li><a href="{{ URL::route('editor.reportpromotion.index') }}"><i class="fa fa-arrow-up"></i> Promotion History</a></li> 
              @actionEnd 
              @actionStart('reporttraveling', 'read')   
              <li><a href="{{ URL::route('editor.reporttraveling.index') }}"><i class="fa fa-car"></i> Traveling History</a></li> 
              @actionEnd  
              @actionStart('reportleaving', 'read') 
              <li><a href="{{ URL::route('editor.reportleaving.index') }}"><i class="fa fa-share"></i> Leave History</a></li>   
              @actionEnd 
              @actionStart('reporttime', 'read') 
              <li><a href="{{ URL::route('editor.reporttime.index') }}"><i class="fa fa-clock-o"></i> Daily Time</a></li>  
              @actionEnd  
              @actionStart('reportdaywork', 'read') 
              <li><a href="{{ URL::route('editor.reportdaywork.index') }}"><i class="fa fa-clock-o"></i> Day Work</a></li>  
              @actionEnd  
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('chartleave', 'read', 'chartemployee', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> Chart <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              @actionStart('chartleave', 'read') 
              <li><a href="{{ URL::route('editor.chartleave.index') }}"><i class="fa fa-sign-out"></i> Employee Leave</a></li>
              @actionEnd 
              @actionStart('chartemployee', 'read') 
              <li><a href="{{ URL::route('editor.chartemployee.index') }}"><i class="fa fa-male"></i> Employee </a></li> 
              @actionEnd 
            </ul>
          </li>

          <li class="dropdown">
            @actionStart('user', 'read', 'action', 'read', 'privilege', 'read', 'module', 'read', 'userposition', 'read', 'news', 'read', 'popup', 'read', 'faq', 'read', 'cache', 'read') 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> <span class="caret"></span></a>
            @actionEnd 
            <ul class="dropdown-menu" role="menu">
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"><i class="fa fa-user-circle"></i> Role Management</a> 
                <ul class="dropdown-menu">
                  @actionStart('user', 'read') 
                  <li><a href="{{ URL::route('editor.user.index') }}"> User List</a></li> 
                  @actionEnd 
                  @actionStart('action', 'read') 
                  <li><a href="{{ URL::route('editor.action.index') }}">Action</a></li> 
                  @actionEnd 
                  @actionStart('privilege', 'read') 
                  <li><a href="{{ URL::route('editor.privilege.index') }}"> Previlage</a></li> 
                  @actionEnd 
                  @actionStart('module', 'read') 
                  <li><a href="{{ URL::route('editor.module.index') }}"> Module</a></li>
                  @actionEnd   
                  @actionStart('userposition', 'read') 
                  <li><a href="{{ URL::route('editor.userposition.index') }}"> User Positon</a></li> 
                  @actionEnd 
                </ul>
              </li> 
              @actionStart('news', 'read') 
              <li><a href="{{ URL::route('editor.news.index') }}"><i class="fa fa-newspaper-o"></i> News</a></li>  
              @actionEnd   
              @actionStart('popup', 'read') 
              <li><a href="{{ URL::route('editor.popup.index') }}"><i class="fa fa-bell"></i> Popup</a></li> 
              @actionEnd 
              @actionStart('faq', 'read') 
              <li><a href="{{ URL::route('editor.faq.index') }}"><i class="fa fa-question"></i> FAQ</a></li> 
              @actionEnd 
              @actionStart('cache', 'read') 
              <li><a href="{{ URL::route('editor.cache.index') }}"><i class="fa fa-bell"></i> Clear Cache</a></li> 
              @actionEnd 
            </ul>
          </li>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav"> 
            <!-- <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
            </li>  --> 
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                @if(Auth::user()->filename == null)
                <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="user-image" alt="User Image">
                {{ strtoupper(Auth::user()->username) }}
                @else
                <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="user-image" alt="User Image">
                
                {{ strtoupper(Auth::user()->username) }}
                @endif
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="label label-success"></span>
                
              </a>
              <ul class="dropdown-menu">
               <li class="user-header">
                @if(Auth::user()->filename == null)
                <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="img-circle" alt="User Image">
                @else
                <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="img-circle" alt="User Image">
                @endif

                <p>
                 {{Auth::user()->username}}
                 <small>Member since {{date("d-m-Y", strtotime(Auth::user()->created_at))}}</small>
               </p>
             </li>
             <!-- Menu Body -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
               <a href="{{ URL::route('editor.profile.show') }}" class="btn btn-default btn-flat">Profile</a>
             </div>
             <div class="pull-right">
              <form id="logout-form" action="{{ url('/logout') }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.navbar-custom-menu --> 
</nav>
</header>