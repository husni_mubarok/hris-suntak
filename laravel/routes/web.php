<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'Editor'], function()
{
Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
Route::get('/editor', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:clear');
    // dd($exitCode);
    return 'Cache has been clear!';
});

//User Management
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);

	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create 
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//edit  
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);

	//User
		//index
	Route::get('/userposition', ['middleware' => ['role:userposition|read'], 'as' => 'editor.userposition.index', 'uses' => 'UserpositionController@index']);
		//create 
	Route::get('/userposition/create', ['middleware' => ['role:userposition|create'], 'as' => 'editor.userposition.create', 'uses' => 'UserpositionController@create']);
	Route::post('/userposition/create', ['middleware' => ['role:userposition|create'], 'as' => 'editor.userposition.store', 'uses' => 'UserpositionController@store']);
		//edit  
	Route::get('/userposition/{id}/edit', ['middleware' => ['role:userposition|update'], 'as' => 'editor.userposition.edit', 'uses' => 'UserpositionController@edit']);
	Route::put('/userposition/{id}/edit', ['middleware' => ['role:userposition|update'], 'as' => 'editor.userposition.update', 'uses' => 'UserpositionController@update']);
		//delete
	Route::delete('/userposition/{id}/delete', ['middleware' => ['role:userposition|delete'], 'as' => 'editor.userposition.delete', 'uses' => 'UserpositionController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);

	//Document
		//index
	Route::get('/document', ['middleware' => ['role:document|read'], 'as' => 'editor.document.index', 'uses' => 'DocumentController@index']);
	Route::get('/document/data', ['as' => 'editor.document.data', 'uses' => 'DocumentController@data']);
	
		//create
	Route::get('/document/create', ['middleware' => ['role:document|create'], 'as' => 'editor.document.create', 'uses' => 'DocumentController@create']);
	Route::post('/document/create', ['middleware' => ['role:document|create'], 'as' => 'editor.document.store', 'uses' => 'DocumentController@store']);
		//edit
	Route::get('/document/{id}/edit', ['middleware' => ['role:document|update'], 'as' => 'editor.document.edit', 'uses' => 'DocumentController@edit']);
	Route::put('/document/{id}/edit', ['middleware' => ['role:document|update'], 'as' => 'editor.document.update', 'uses' => 'DocumentController@update']);
		//delete
	Route::delete('/document/delete/{id}', ['middleware' => ['role:document|delete'], 'as' => 'editor.document.delete', 'uses' => 'DocumentController@delete']);
	Route::post('/document/deletebulk', ['middleware' => ['role:document|delete'], 'as' => 'editor.document.deletebulk', 'uses' => 'DocumentController@deletebulk']);

	//News
		//index
	Route::get('/news', ['middleware' => ['role:news|read'], 'as' => 'editor.news.index', 'uses' => 'NewsController@index']);
	Route::get('/news/data', ['as' => 'editor.news.data', 'uses' => 'NewsController@data']);
	
		//create
	Route::get('/news/create', ['middleware' => ['role:news|create'], 'as' => 'editor.news.create', 'uses' => 'NewsController@create']);
	Route::post('/news/create', ['middleware' => ['role:news|create'], 'as' => 'editor.news.store', 'uses' => 'NewsController@store']);
		//edit
	Route::get('/news/{id}/edit', ['middleware' => ['role:news|update'], 'as' => 'editor.news.edit', 'uses' => 'NewsController@edit']);
	Route::put('/news/{id}/edit', ['middleware' => ['role:news|update'], 'as' => 'editor.news.update', 'uses' => 'NewsController@update']);
		//delete
	Route::delete('/news/delete/{id}', ['middleware' => ['role:news|delete'], 'as' => 'editor.news.delete', 'uses' => 'NewsController@delete']);
	Route::post('/news/deletebulk', ['middleware' => ['role:news|delete'], 'as' => 'editor.news.deletebulk', 'uses' => 'NewsController@deletebulk']);

	//City
		//index
	Route::get('/city', ['middleware' => ['role:city|read'], 'as' => 'editor.city.index', 'uses' => 'CityController@index']);
	Route::get('/city/data', ['as' => 'editor.city.data', 'uses' => 'CityController@data']);
	
		//create
	Route::get('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.create', 'uses' => 'CityController@create']);
	Route::post('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.store', 'uses' => 'CityController@store']);
		//edit
	Route::get('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.edit', 'uses' => 'CityController@edit']);
	Route::put('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.update', 'uses' => 'CityController@update']);
		//delete
	Route::delete('/city/delete/{id}', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.delete', 'uses' => 'CityController@delete']);
	Route::post('/city/deletebulk', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.deletebulk', 'uses' => 'CityController@deletebulk']);

	//Payperiod
		//index
	Route::get('/payperiod', ['middleware' => ['role:payperiod|read'], 'as' => 'editor.payperiod.index', 'uses' => 'PayperiodController@index']);
	Route::get('/payperiod/data', ['as' => 'editor.payperiod.data', 'uses' => 'PayperiodController@data']);
	
		//create
	Route::get('/payperiod/create', ['middleware' => ['role:payperiod|create'], 'as' => 'editor.payperiod.create', 'uses' => 'PayperiodController@create']);
	Route::post('/payperiod/create', ['middleware' => ['role:payperiod|create'], 'as' => 'editor.payperiod.store', 'uses' => 'PayperiodController@store']);
		//edit
	Route::get('/payperiod/edit/{id}', ['middleware' => ['role:payperiod|update'], 'as' => 'editor.payperiod.edit', 'uses' => 'PayperiodController@edit']);
	Route::put('/payperiod/edit/{id}', ['middleware' => ['role:payperiod|update'], 'as' => 'editor.payperiod.update', 'uses' => 'PayperiodController@update']);
		//delete
	Route::delete('/payperiod/delete/{id}', ['middleware' => ['role:payperiod|delete'], 'as' => 'editor.payperiod.delete', 'uses' => 'PayperiodController@delete']);
	Route::post('/payperiod/deletebulk', ['middleware' => ['role:payperiod|delete'], 'as' => 'editor.payperiod.deletebulk', 'uses' => 'PayperiodController@deletebulk']);

	//Taxstatus
		//index
	Route::get('/taxstatus', ['middleware' => ['role:taxstatus|read'], 'as' => 'editor.taxstatus.index', 'uses' => 'TaxstatusController@index']);
	Route::get('/taxstatus/data', ['as' => 'editor.taxstatus.data', 'uses' => 'TaxstatusController@data']);
	
		//create
	Route::get('/taxstatus/create', ['middleware' => ['role:taxstatus|create'], 'as' => 'editor.taxstatus.create', 'uses' => 'TaxstatusController@create']);
	Route::post('/taxstatus/create', ['middleware' => ['role:taxstatus|create'], 'as' => 'editor.taxstatus.store', 'uses' => 'TaxstatusController@store']);
		//edit
	Route::get('/taxstatus/edit/{id}', ['middleware' => ['role:taxstatus|update'], 'as' => 'editor.taxstatus.edit', 'uses' => 'TaxstatusController@edit']);
	Route::put('/taxstatus/edit/{id}', ['middleware' => ['role:taxstatus|update'], 'as' => 'editor.taxstatus.update', 'uses' => 'TaxstatusController@update']);
		//delete
	Route::delete('/taxstatus/delete/{id}', ['middleware' => ['role:taxstatus|delete'], 'as' => 'editor.taxstatus.delete', 'uses' => 'TaxstatusController@delete']);
	Route::post('/taxstatus/deletebulk', ['middleware' => ['role:taxstatus|delete'], 'as' => 'editor.taxstatus.deletebulk', 'uses' => 'TaxstatusController@deletebulk']);

	//Employeesalary
		//index
	Route::get('/employeesalary', ['middleware' => ['role:employeesalary|read'], 'as' => 'editor.employeesalary.index', 'uses' => 'EmployeesalaryController@index']);
	Route::get('/employeesalary/data', ['as' => 'editor.employeesalary.data', 'uses' => 'EmployeesalaryController@data']);
	
		//create
	Route::get('/employeesalary/create', ['middleware' => ['role:employeesalary|create'], 'as' => 'editor.employeesalary.create', 'uses' => 'EmployeesalaryController@create']);
	Route::post('/employeesalary/create', ['middleware' => ['role:employeesalary|create'], 'as' => 'editor.employeesalary.store', 'uses' => 'EmployeesalaryController@store']);
		//edit
	Route::get('/employeesalary/edit/{id}', ['middleware' => ['role:employeesalary|update'], 'as' => 'editor.employeesalary.edit', 'uses' => 'EmployeesalaryController@edit']);
	Route::put('/employeesalary/edit/{id}', ['middleware' => ['role:employeesalary|update'], 'as' => 'editor.employeesalary.update', 'uses' => 'EmployeesalaryController@update']);
		//delete
	Route::delete('/employeesalary/delete/{id}', ['middleware' => ['role:employeesalary|delete'], 'as' => 'editor.employeesalary.delete', 'uses' => 'EmployeesalaryController@delete']);
	Route::post('/employeesalary/deletebulk', ['middleware' => ['role:employeesalary|delete'], 'as' => 'editor.employeesalary.deletebulk', 'uses' => 'EmployeesalaryController@deletebulk']);

	//Medicaltype
		//index
	Route::get('/medicaltype', ['middleware' => ['role:medicaltype|read'], 'as' => 'editor.medicaltype.index', 'uses' => 'MedicaltypeController@index']);
	Route::get('/medicaltype/data', ['as' => 'editor.medicaltype.data', 'uses' => 'MedicaltypeController@data']);
	
		//create
	Route::get('/medicaltype/create', ['middleware' => ['role:medicaltype|create'], 'as' => 'editor.medicaltype.create', 'uses' => 'MedicaltypeController@create']);
	Route::post('/medicaltype/create', ['middleware' => ['role:medicaltype|create'], 'as' => 'editor.medicaltype.store', 'uses' => 'MedicaltypeController@store']);
		//edit
	Route::get('/medicaltype/edit/{id}', ['middleware' => ['role:medicaltype|update'], 'as' => 'editor.medicaltype.edit', 'uses' => 'MedicaltypeController@edit']);
	Route::put('/medicaltype/edit/{id}', ['middleware' => ['role:medicaltype|update'], 'as' => 'editor.medicaltype.update', 'uses' => 'MedicaltypeController@update']);
		//delete
	Route::delete('/medicaltype/delete/{id}', ['middleware' => ['role:medicaltype|delete'], 'as' => 'editor.medicaltype.delete', 'uses' => 'MedicaltypeController@delete']);
	Route::post('/medicaltype/deletebulk', ['middleware' => ['role:medicaltype|delete'], 'as' => 'editor.medicaltype.deletebulk', 'uses' => 'MedicaltypeController@deletebulk']);

	//Loantype
		//index
	Route::get('/loantype', ['middleware' => ['role:loantype|read'], 'as' => 'editor.loantype.index', 'uses' => 'LoantypeController@index']);
	Route::get('/loantype/data', ['as' => 'editor.loantype.data', 'uses' => 'LoantypeController@data']); 
		//create
	Route::get('/loantype/create', ['middleware' => ['role:loantype|create'], 'as' => 'editor.loantype.create', 'uses' => 'LoantypeController@create']);
	Route::post('/loantype/create', ['middleware' => ['role:loantype|create'], 'as' => 'editor.loantype.store', 'uses' => 'LoantypeController@store']);
		//edit
	Route::get('/loantype/edit/{id}', ['middleware' => ['role:loantype|update'], 'as' => 'editor.loantype.edit', 'uses' => 'LoantypeController@edit']);
	Route::put('/loantype/edit/{id}', ['middleware' => ['role:loantype|update'], 'as' => 'editor.loantype.update', 'uses' => 'LoantypeController@update']);
		//delete
	Route::delete('/loantype/delete/{id}', ['middleware' => ['role:loantype|delete'], 'as' => 'editor.loantype.delete', 'uses' => 'LoantypeController@delete']);
	Route::post('/loantype/deletebulk', ['middleware' => ['role:loantype|delete'], 'as' => 'editor.loantype.deletebulk', 'uses' => 'LoantypeController@deletebulk']);

	//Umr
		//index
	Route::get('/umr', ['middleware' => ['role:umr|read'], 'as' => 'editor.umr.index', 'uses' => 'UmrController@index']);
	Route::get('/umr/data', ['as' => 'editor.umr.data', 'uses' => 'UmrController@data']);
	
		//create
	Route::get('/umr/create', ['middleware' => ['role:umr|create'], 'as' => 'editor.umr.create', 'uses' => 'UmrController@create']);
	Route::post('/umr/create', ['middleware' => ['role:umr|create'], 'as' => 'editor.umr.store', 'uses' => 'UmrController@store']);
		//edit
	Route::get('/umr/edit/{id}', ['middleware' => ['role:umr|update'], 'as' => 'editor.umr.edit', 'uses' => 'UmrController@edit']);
	Route::put('/umr/edit/{id}', ['middleware' => ['role:umr|update'], 'as' => 'editor.umr.update', 'uses' => 'UmrController@update']);
		//delete
	Route::delete('/umr/delete/{id}', ['middleware' => ['role:umr|delete'], 'as' => 'editor.umr.delete', 'uses' => 'UmrController@delete']);
	Route::post('/umr/deletebulk', ['middleware' => ['role:umr|delete'], 'as' => 'editor.umr.deletebulk', 'uses' => 'UmrController@deletebulk']);

	//Tarif
		//index
	Route::get('/tarif', ['middleware' => ['role:tarif|read'], 'as' => 'editor.tarif.index', 'uses' => 'TarifController@index']);
	Route::get('/tarif/data', ['as' => 'editor.tarif.data', 'uses' => 'TarifController@data']);
	
		//create
	Route::get('/tarif/create', ['middleware' => ['role:tarif|create'], 'as' => 'editor.tarif.create', 'uses' => 'TarifController@create']);
	Route::post('/tarif/create', ['middleware' => ['role:tarif|create'], 'as' => 'editor.tarif.store', 'uses' => 'TarifController@store']);
		//edit
	Route::get('/tarif/edit/{id}', ['middleware' => ['role:tarif|update'], 'as' => 'editor.tarif.edit', 'uses' => 'TarifController@edit']);
	Route::put('/tarif/edit/{id}', ['middleware' => ['role:tarif|update'], 'as' => 'editor.tarif.update', 'uses' => 'TarifController@update']);
		//delete
	Route::delete('/tarif/delete/{id}', ['middleware' => ['role:tarif|delete'], 'as' => 'editor.tarif.delete', 'uses' => 'TarifController@delete']);
	Route::post('/tarif/deletebulk', ['middleware' => ['role:tarif|delete'], 'as' => 'editor.tarif.deletebulk', 'uses' => 'TarifController@deletebulk']);

	//Iuranpensiun
		//index
	Route::get('/iuranpensiun', ['middleware' => ['role:iuranpensiun|read'], 'as' => 'editor.iuranpensiun.index', 'uses' => 'IuranpensiunController@index']);
	Route::get('/iuranpensiun/data', ['as' => 'editor.iuranpensiun.data', 'uses' => 'IuranpensiunController@data']);
	
		//create
	Route::get('/iuranpensiun/create', ['middleware' => ['role:iuranpensiun|create'], 'as' => 'editor.iuranpensiun.create', 'uses' => 'IuranpensiunController@create']);
	Route::post('/iuranpensiun/create', ['middleware' => ['role:iuranpensiun|create'], 'as' => 'editor.iuranpensiun.store', 'uses' => 'IuranpensiunController@store']);
		//edit
	Route::get('/iuranpensiun/edit/{id}', ['middleware' => ['role:iuranpensiun|update'], 'as' => 'editor.iuranpensiun.edit', 'uses' => 'IuranpensiunController@edit']);
	Route::put('/iuranpensiun/edit/{id}', ['middleware' => ['role:iuranpensiun|update'], 'as' => 'editor.iuranpensiun.update', 'uses' => 'IuranpensiunController@update']);
		//delete
	Route::delete('/iuranpensiun/delete/{id}', ['middleware' => ['role:iuranpensiun|delete'], 'as' => 'editor.iuranpensiun.delete', 'uses' => 'IuranpensiunController@delete']);
	Route::post('/iuranpensiun/deletebulk', ['middleware' => ['role:iuranpensiun|delete'], 'as' => 'editor.iuranpensiun.deletebulk', 'uses' => 'IuranpensiunController@deletebulk']);

	//Religion
		//index
	Route::get('/religion', ['middleware' => ['role:religion|read'], 'as' => 'editor.religion.index', 'uses' => 'ReligionController@index']);
	Route::get('/religion/data', ['as' => 'editor.religion.data', 'uses' => 'ReligionController@data']);
	
		//create
	Route::get('/religion/create', ['middleware' => ['role:religion|create'], 'as' => 'editor.religion.create', 'uses' => 'ReligionController@create']);
	Route::post('/religion/create', ['middleware' => ['role:religion|create'], 'as' => 'editor.religion.store', 'uses' => 'ReligionController@store']);
		//edit
	Route::get('/religion/edit/{id}', ['middleware' => ['role:religion|update'], 'as' => 'editor.religion.edit', 'uses' => 'ReligionController@edit']);
	Route::put('/religion/edit/{id}', ['middleware' => ['role:religion|update'], 'as' => 'editor.religion.update', 'uses' => 'ReligionController@update']);
		//delete
	Route::delete('/religion/delete/{id}', ['middleware' => ['role:religion|delete'], 'as' => 'editor.religion.delete', 'uses' => 'ReligionController@delete']);
	Route::post('/religion/deletebulk', ['middleware' => ['role:religion|delete'], 'as' => 'editor.religion.deletebulk', 'uses' => 'ReligionController@deletebulk']);

	//Golongan
		//index
	Route::get('/golongan', ['middleware' => ['role:golongan|read'], 'as' => 'editor.golongan.index', 'uses' => 'GolonganController@index']);
	Route::get('/golongan/data', ['as' => 'editor.golongan.data', 'uses' => 'GolonganController@data']);
	
		//create
	Route::get('/golongan/create', ['middleware' => ['role:golongan|create'], 'as' => 'editor.golongan.create', 'uses' => 'GolonganController@create']);
	Route::post('/golongan/create', ['middleware' => ['role:golongan|create'], 'as' => 'editor.golongan.store', 'uses' => 'GolonganController@store']);
		//edit
	Route::get('/golongan/edit/{id}', ['middleware' => ['role:golongan|update'], 'as' => 'editor.golongan.edit', 'uses' => 'GolonganController@edit']);
	Route::put('/golongan/edit/{id}', ['middleware' => ['role:golongan|update'], 'as' => 'editor.golongan.update', 'uses' => 'GolonganController@update']);
		//delete
	Route::delete('/golongan/delete/{id}', ['middleware' => ['role:golongan|delete'], 'as' => 'editor.golongan.delete', 'uses' => 'GolonganController@delete']);
	Route::post('/golongan/deletebulk', ['middleware' => ['role:golongan|delete'], 'as' => 'editor.golongan.deletebulk', 'uses' => 'GolonganController@deletebulk']);

	//Educationlevel
		//index
	Route::get('/educationlevel', ['middleware' => ['role:educationlevel|read'], 'as' => 'editor.educationlevel.index', 'uses' => 'EducationlevelController@index']);
	Route::get('/educationlevel/data', ['as' => 'editor.educationlevel.data', 'uses' => 'EducationlevelController@data']);
	
		//create
	Route::get('/educationlevel/create', ['middleware' => ['role:educationlevel|create'], 'as' => 'editor.educationlevel.create', 'uses' => 'EducationlevelController@create']);
	Route::post('/educationlevel/create', ['middleware' => ['role:educationlevel|create'], 'as' => 'editor.educationlevel.store', 'uses' => 'EducationlevelController@store']);
		//edit
	Route::get('/educationlevel/edit/{id}', ['middleware' => ['role:educationlevel|update'], 'as' => 'editor.educationlevel.edit', 'uses' => 'EducationlevelController@edit']);
	Route::put('/educationlevel/edit/{id}', ['middleware' => ['role:educationlevel|update'], 'as' => 'editor.educationlevel.update', 'uses' => 'EducationlevelController@update']);
		//delete
	Route::delete('/educationlevel/delete/{id}', ['middleware' => ['role:educationlevel|delete'], 'as' => 'editor.educationlevel.delete', 'uses' => 'EducationlevelController@delete']);
	Route::post('/educationlevel/deletebulk', ['middleware' => ['role:educationlevel|delete'], 'as' => 'editor.educationlevel.deletebulk', 'uses' => 'EducationlevelController@deletebulk']);

	//Educationmajor
		//index
	Route::get('/educationmajor', ['middleware' => ['role:educationmajor|read'], 'as' => 'editor.educationmajor.index', 'uses' => 'EducationmajorController@index']);
	Route::get('/educationmajor/data', ['as' => 'editor.educationmajor.data', 'uses' => 'EducationmajorController@data']);
	
		//create
	Route::get('/educationmajor/create', ['middleware' => ['role:educationmajor|create'], 'as' => 'editor.educationmajor.create', 'uses' => 'EducationmajorController@create']);
	Route::post('/educationmajor/create', ['middleware' => ['role:educationmajor|create'], 'as' => 'editor.educationmajor.store', 'uses' => 'EducationmajorController@store']);
		//edit
	Route::get('/educationmajor/edit/{id}', ['middleware' => ['role:educationmajor|update'], 'as' => 'editor.educationmajor.edit', 'uses' => 'EducationmajorController@edit']);
	Route::put('/educationmajor/edit/{id}', ['middleware' => ['role:educationmajor|update'], 'as' => 'editor.educationmajor.update', 'uses' => 'EducationmajorController@update']);
		//delete
	Route::delete('/educationmajor/delete/{id}', ['middleware' => ['role:educationmajor|delete'], 'as' => 'editor.educationmajor.delete', 'uses' => 'EducationmajorController@delete']);
	Route::post('/educationmajor/deletebulk', ['middleware' => ['role:educationmajor|delete'], 'as' => 'editor.educationmajor.deletebulk', 'uses' => 'EducationmajorController@deletebulk']);


	//Educationtype
		//index
	Route::get('/educationtype', ['middleware' => ['role:educationtype|read'], 'as' => 'editor.educationtype.index', 'uses' => 'EducationtypeController@index']);
	Route::get('/educationtype/data', ['as' => 'editor.educationtype.data', 'uses' => 'EducationtypeController@data']);
	
		//create
	Route::get('/educationtype/create', ['middleware' => ['role:educationtype|create'], 'as' => 'editor.educationtype.create', 'uses' => 'EducationtypeController@create']);
	Route::post('/educationtype/create', ['middleware' => ['role:educationtype|create'], 'as' => 'editor.educationtype.store', 'uses' => 'EducationtypeController@store']);
		//edit
	Route::get('/educationtype/edit/{id}', ['middleware' => ['role:educationtype|update'], 'as' => 'editor.educationtype.edit', 'uses' => 'EducationtypeController@edit']);
	Route::put('/educationtype/edit/{id}', ['middleware' => ['role:educationtype|update'], 'as' => 'editor.educationtype.update', 'uses' => 'EducationtypeController@update']);
		//delete
	Route::delete('/educationtype/delete/{id}', ['middleware' => ['role:educationtype|delete'], 'as' => 'editor.educationtype.delete', 'uses' => 'EducationtypeController@delete']);
	Route::post('/educationtype/deletebulk', ['middleware' => ['role:educationtype|delete'], 'as' => 'editor.educationtype.deletebulk', 'uses' => 'EducationtypeController@deletebulk']);

	//Travelingtype
		//index
	Route::get('/travelingtype', ['middleware' => ['role:travelingtype|read'], 'as' => 'editor.travelingtype.index', 'uses' => 'TravelingtypeController@index']);
	Route::get('/travelingtype/data', ['as' => 'editor.travelingtype.data', 'uses' => 'TravelingtypeController@data']); 
		//create
	Route::get('/travelingtype/create', ['middleware' => ['role:travelingtype|create'], 'as' => 'editor.travelingtype.create', 'uses' => 'TravelingtypeController@create']);
	Route::post('/travelingtype/create', ['middleware' => ['role:travelingtype|create'], 'as' => 'editor.travelingtype.store', 'uses' => 'TravelingtypeController@store']);
		//edit
	Route::get('/travelingtype/edit/{id}', ['middleware' => ['role:travelingtype|update'], 'as' => 'editor.travelingtype.edit', 'uses' => 'TravelingtypeController@edit']);
	Route::put('/travelingtype/edit/{id}', ['middleware' => ['role:travelingtype|update'], 'as' => 'editor.travelingtype.update', 'uses' => 'TravelingtypeController@update']);
		//delete
	Route::delete('/travelingtype/delete/{id}', ['middleware' => ['role:travelingtype|delete'], 'as' => 'editor.travelingtype.delete', 'uses' => 'TravelingtypeController@delete']);
	Route::post('/travelingtype/deletebulk', ['middleware' => ['role:travelingtype|delete'], 'as' => 'editor.travelingtype.deletebulk', 'uses' => 'TravelingtypeController@deletebulk']);

	//Employeestatus
		//index
	Route::get('/employeestatus', ['middleware' => ['role:employeestatus|read'], 'as' => 'editor.employeestatus.index', 'uses' => 'EmployeestatusController@index']);
	Route::get('/employeestatus/data', ['as' => 'editor.employeestatus.data', 'uses' => 'EmployeestatusController@data']);
	
		//create
	Route::get('/employeestatus/create', ['middleware' => ['role:employeestatus|create'], 'as' => 'editor.employeestatus.create', 'uses' => 'EmployeestatusController@create']);
	Route::post('/employeestatus/create', ['middleware' => ['role:employeestatus|create'], 'as' => 'editor.employeestatus.store', 'uses' => 'EmployeestatusController@store']);
		//edit
	Route::get('/employeestatus/edit/{id}', ['middleware' => ['role:employeestatus|update'], 'as' => 'editor.employeestatus.edit', 'uses' => 'EmployeestatusController@edit']);
	Route::put('/employeestatus/edit/{id}', ['middleware' => ['role:employeestatus|update'], 'as' => 'editor.employeestatus.update', 'uses' => 'EmployeestatusController@update']);
		//delete
	Route::delete('/employeestatus/delete/{id}', ['middleware' => ['role:employeestatus|delete'], 'as' => 'editor.employeestatus.delete', 'uses' => 'EmployeestatusController@delete']);
	Route::post('/employeestatus/deletebulk', ['middleware' => ['role:employeestatus|delete'], 'as' => 'editor.employeestatus.deletebulk', 'uses' => 'EmployeestatusController@deletebulk']);

	//Employeerequest
		//index
	Route::get('/employeerequest', ['middleware' => ['role:employeerequest|read'], 'as' => 'editor.employeerequest.index', 'uses' => 'EmployeerequestController@index']);
	Route::get('/employeerequest/data', ['as' => 'editor.employeerequest.data', 'uses' => 'EmployeerequestController@data']);

	//Travelingitem
		//index
	Route::get('/travelingitem', ['middleware' => ['role:travelingitem|read'], 'as' => 'editor.travelingitem.index', 'uses' => 'TravelingitemController@index']);
	Route::get('/travelingitem/data', ['as' => 'editor.travelingitem.data', 'uses' => 'TravelingitemController@data']); 
		//create
	Route::get('/travelingitem/create', ['middleware' => ['role:travelingitem|create'], 'as' => 'editor.travelingitem.create', 'uses' => 'TravelingitemController@create']);
	Route::post('/travelingitem/create', ['middleware' => ['role:travelingitem|create'], 'as' => 'editor.travelingitem.store', 'uses' => 'TravelingitemController@store']);
		//edit
	Route::get('/travelingitem/edit/{id}', ['middleware' => ['role:travelingitem|update'], 'as' => 'editor.travelingitem.edit', 'uses' => 'TravelingitemController@edit']);
	Route::put('/travelingitem/edit/{id}', ['middleware' => ['role:travelingitem|update'], 'as' => 'editor.travelingitem.update', 'uses' => 'TravelingitemController@update']);
		//delete
	Route::delete('/travelingitem/delete/{id}', ['middleware' => ['role:travelingitem|delete'], 'as' => 'editor.travelingitem.delete', 'uses' => 'TravelingitemController@delete']);
	Route::post('/travelingitem/deletebulk', ['middleware' => ['role:travelingitem|delete'], 'as' => 'editor.travelingitem.deletebulk', 'uses' => 'TravelingitemController@deletebulk']);

	//Year
		//index
	Route::get('/year', ['middleware' => ['role:year|read'], 'as' => 'editor.year.index', 'uses' => 'YearController@index']);
	Route::get('/year/data', ['as' => 'editor.year.data', 'uses' => 'YearController@data']); 
		//create
	Route::get('/year/create', ['middleware' => ['role:year|create'], 'as' => 'editor.year.create', 'uses' => 'YearController@create']);
	Route::post('/year/create', ['middleware' => ['role:year|create'], 'as' => 'editor.year.store', 'uses' => 'YearController@store']);
		//edit
	Route::get('/year/edit/{id}', ['middleware' => ['role:year|update'], 'as' => 'editor.year.edit', 'uses' => 'YearController@edit']);
	Route::put('/year/edit/{id}', ['middleware' => ['role:year|update'], 'as' => 'editor.year.update', 'uses' => 'YearController@update']);
		//delete
	Route::delete('/year/delete/{id}', ['middleware' => ['role:year|delete'], 'as' => 'editor.year.delete', 'uses' => 'YearController@delete']);
	Route::post('/year/deletebulk', ['middleware' => ['role:year|delete'], 'as' => 'editor.year.deletebulk', 'uses' => 'YearController@deletebulk']);

	//Country
		//index
	Route::get('/country', ['middleware' => ['role:country|read'], 'as' => 'editor.country.index', 'uses' => 'CountryController@index']);
	Route::get('/country/data', ['as' => 'editor.country.data', 'uses' => 'CountryController@data']);
	
		//create
	Route::get('/country/create', ['middleware' => ['role:country|create'], 'as' => 'editor.country.create', 'uses' => 'CountryController@create']);
	Route::post('/country/create', ['middleware' => ['role:country|create'], 'as' => 'editor.country.store', 'uses' => 'CountryController@store']);
		//edit
	Route::get('/country/edit/{id}', ['middleware' => ['role:country|update'], 'as' => 'editor.country.edit', 'uses' => 'CountryController@edit']);
	Route::put('/country/edit/{id}', ['middleware' => ['role:country|update'], 'as' => 'editor.country.update', 'uses' => 'CountryController@update']);
		//delete
	Route::delete('/country/delete/{id}', ['middleware' => ['role:country|delete'], 'as' => 'editor.country.delete', 'uses' => 'CountryController@delete']);
	Route::post('/country/deletebulk', ['middleware' => ['role:country|delete'], 'as' => 'editor.country.deletebulk', 'uses' => 'CountryController@deletebulk']);

	//Position
		//index
	Route::get('/position', ['middleware' => ['role:position|read'], 'as' => 'editor.position.index', 'uses' => 'PositionController@index']);
	Route::get('/position/data', ['as' => 'editor.position.data', 'uses' => 'PositionController@data']);
	
		//create
	Route::get('/position/create', ['middleware' => ['role:position|create'], 'as' => 'editor.position.create', 'uses' => 'PositionController@create']);
	Route::post('/position/create', ['middleware' => ['role:position|create'], 'as' => 'editor.position.store', 'uses' => 'PositionController@store']);
		//edit
	Route::get('/position/edit/{id}', ['middleware' => ['role:position|update'], 'as' => 'editor.position.edit', 'uses' => 'PositionController@edit']);
	Route::put('/position/edit/{id}', ['middleware' => ['role:position|update'], 'as' => 'editor.position.update', 'uses' => 'PositionController@update']);
		//delete
	Route::delete('/position/delete/{id}', ['middleware' => ['role:position|delete'], 'as' => 'editor.position.delete', 'uses' => 'PositionController@delete']);
	Route::post('/position/deletebulk', ['middleware' => ['role:position|delete'], 'as' => 'editor.position.deletebulk', 'uses' => 'PositionController@deletebulk']);


	//Unit
		//index
	Route::get('/unit', ['middleware' => ['role:unit|read'], 'as' => 'editor.unit.index', 'uses' => 'UnitController@index']);
	Route::get('/unit/data', ['as' => 'editor.unit.data', 'uses' => 'UnitController@data']);
	
		//create
	Route::get('/unit/create', ['middleware' => ['role:unit|create'], 'as' => 'editor.unit.create', 'uses' => 'UnitController@create']);
	Route::post('/unit/create', ['middleware' => ['role:unit|create'], 'as' => 'editor.unit.store', 'uses' => 'UnitController@store']);
		//edit
	Route::get('/unit/edit/{id}', ['middleware' => ['role:unit|update'], 'as' => 'editor.unit.edit', 'uses' => 'UnitController@edit']);
	Route::put('/unit/edit/{id}', ['middleware' => ['role:unit|update'], 'as' => 'editor.unit.update', 'uses' => 'UnitController@update']);
		//delete
	Route::delete('/unit/delete/{id}', ['middleware' => ['role:unit|delete'], 'as' => 'editor.unit.delete', 'uses' => 'UnitController@delete']);
	Route::post('/unit/deletebulk', ['middleware' => ['role:unit|delete'], 'as' => 'editor.unit.deletebulk', 'uses' => 'UnitController@deletebulk']);

	//Location
		//index
	Route::get('/location', ['middleware' => ['role:location|read'], 'as' => 'editor.location.index', 'uses' => 'LocationController@index']);
	Route::get('/location/data', ['as' => 'editor.location.data', 'uses' => 'LocationController@data']);
	
		//create
	Route::get('/location/create', ['middleware' => ['role:location|create'], 'as' => 'editor.location.create', 'uses' => 'LocationController@create']);
	Route::post('/location/create', ['middleware' => ['role:location|create'], 'as' => 'editor.location.store', 'uses' => 'LocationController@store']);
		//edit
	Route::get('/location/edit/{id}', ['middleware' => ['role:location|update'], 'as' => 'editor.location.edit', 'uses' => 'LocationController@edit']);
	Route::put('/location/edit/{id}', ['middleware' => ['role:location|update'], 'as' => 'editor.location.update', 'uses' => 'LocationController@update']);
		//delete
	Route::delete('/location/delete/{id}', ['middleware' => ['role:location|delete'], 'as' => 'editor.location.delete', 'uses' => 'LocationController@delete']);
	Route::post('/location/deletebulk', ['middleware' => ['role:location|delete'], 'as' => 'editor.location.deletebulk', 'uses' => 'LocationController@deletebulk']);

	//Holiday
		//index
	Route::get('/holiday', ['middleware' => ['role:holiday|read'], 'as' => 'editor.holiday.index', 'uses' => 'HolidayController@index']);
	Route::get('/holiday/data', ['as' => 'editor.holiday.data', 'uses' => 'HolidayController@data']);
	
		//create
	Route::get('/holiday/create', ['middleware' => ['role:holiday|create'], 'as' => 'editor.holiday.create', 'uses' => 'HolidayController@create']);
	Route::post('/holiday/create', ['middleware' => ['role:holiday|create'], 'as' => 'editor.holiday.store', 'uses' => 'HolidayController@store']);
		//edit
	Route::get('/holiday/edit/{id}', ['middleware' => ['role:holiday|update'], 'as' => 'editor.holiday.edit', 'uses' => 'HolidayController@edit']);
	Route::put('/holiday/edit/{id}', ['middleware' => ['role:holiday|update'], 'as' => 'editor.holiday.update', 'uses' => 'HolidayController@update']);
		//delete
	Route::delete('/holiday/delete/{id}', ['middleware' => ['role:holiday|delete'], 'as' => 'editor.holiday.delete', 'uses' => 'HolidayController@delete']);
	Route::post('/holiday/deletebulk', ['middleware' => ['role:holiday|delete'], 'as' => 'editor.holiday.deletebulk', 'uses' => 'HolidayController@deletebulk']);

	//Payrolltype
		//index
	Route::get('/payrolltype', ['middleware' => ['role:payrolltype|read'], 'as' => 'editor.payrolltype.index', 'uses' => 'PayrolltypeController@index']);
	Route::get('/payrolltype/data', ['as' => 'editor.payrolltype.data', 'uses' => 'PayrolltypeController@data']);
	
		//create
	Route::get('/payrolltype/create', ['middleware' => ['role:payrolltype|create'], 'as' => 'editor.payrolltype.create', 'uses' => 'PayrolltypeController@create']);
	Route::post('/payrolltype/create', ['middleware' => ['role:payrolltype|create'], 'as' => 'editor.payrolltype.store', 'uses' => 'PayrolltypeController@store']);
		//edit
	Route::get('/payrolltype/edit/{id}', ['middleware' => ['role:payrolltype|update'], 'as' => 'editor.payrolltype.edit', 'uses' => 'PayrolltypeController@edit']);
	Route::put('/payrolltype/edit/{id}', ['middleware' => ['role:payrolltype|update'], 'as' => 'editor.payrolltype.update', 'uses' => 'PayrolltypeController@update']);
		//delete
	Route::delete('/payrolltype/delete/{id}', ['middleware' => ['role:payrolltype|delete'], 'as' => 'editor.payrolltype.delete', 'uses' => 'PayrolltypeController@delete']);
	Route::post('/payrolltype/deletebulk', ['middleware' => ['role:payrolltype|delete'], 'as' => 'editor.payrolltype.deletebulk', 'uses' => 'PayrolltypeController@deletebulk']);

	//Shift Group Member
		//index
	Route::get('/shiftgroupmember', ['middleware' => ['role:shiftgroupmember|read'], 'as' => 'editor.shiftgroupmember.index', 'uses' => 'ShiftgroupmemberController@index']);
	Route::get('/shiftgroupmember/data', ['as' => 'editor.shiftgroupmember.data', 'uses' => 'ShiftgroupmemberController@data']);
	Route::get('/shiftgroupmember/detaildata/{id}', ['as' => 'editor.shiftgroupmember.detaildata', 'uses' => 'ShiftgroupmemberController@detaildata']);
		//create
	Route::get('/shiftgroupmember/create', ['middleware' => ['role:shiftgroupmember|create'], 'as' => 'editor.shiftgroupmember.create', 'uses' => 'ShiftgroupmemberController@create']);
	Route::post('/shiftgroupmember/create', ['middleware' => ['role:shiftgroupmember|create'], 'as' => 'editor.shiftgroupmember.store', 'uses' => 'ShiftgroupmemberController@store']);
		//edit
	Route::get('/shiftgroupmember/edit/{id}', ['middleware' => ['role:shiftgroupmember|update'], 'as' => 'editor.shiftgroupmember.edit', 'uses' => 'ShiftgroupmemberController@edit']);
	Route::put('/shiftgroupmember/edit/{id}', ['middleware' => ['role:shiftgroupmember|update'], 'as' => 'editor.shiftgroupmember.update', 'uses' => 'ShiftgroupmemberController@update']);
		//delete
	Route::delete('/shiftgroupmember/delete/{id}', ['middleware' => ['role:shiftgroupmember|delete'], 'as' => 'editor.shiftgroupmember.delete', 'uses' => 'ShiftgroupmemberController@delete']);
	Route::post('/shiftgroupmember/deletebulk', ['middleware' => ['role:shiftgroupmember|delete'], 'as' => 'editor.shiftgroupmember.deletebulk', 'uses' => 'ShiftgroupmemberController@deletebulk']);

		//history
	Route::get('/shiftgroupmember/history/{id}', ['middleware' => ['role:shiftgroupmember|read'], 'as' => 'editor.shiftgroupmember.history', 'uses' => 'ShiftgroupmemberController@history']);
	Route::get('/shiftgroupmember/history/data/{id}', ['as' => 'editor.shiftgroupmember.data', 'uses' => 'ShiftgroupmemberController@datahistory']);

		//Shiftgroup
		//index
	Route::get('/shiftgroup', ['middleware' => ['role:shiftgroup|read'], 'as' => 'editor.shiftgroup.index', 'uses' => 'ShiftgroupController@index']);
	Route::get('/shiftgroup/data', ['as' => 'editor.shiftgroup.data', 'uses' => 'ShiftgroupController@data']);
	
		//create
	Route::get('/shiftgroup/create', ['middleware' => ['role:shiftgroup|create'], 'as' => 'editor.shiftgroup.create', 'uses' => 'ShiftgroupController@create']);
	Route::post('/shiftgroup/create', ['middleware' => ['role:shiftgroup|create'], 'as' => 'editor.shiftgroup.store', 'uses' => 'ShiftgroupController@store']);
		//edit
	Route::get('/shiftgroup/{id}/edit', ['middleware' => ['role:shiftgroup|update'], 'as' => 'editor.shiftgroup.edit', 'uses' => 'ShiftgroupController@edit']);
	Route::put('/shiftgroup/{id}/edit', ['middleware' => ['role:shiftgroup|update'], 'as' => 'editor.shiftgroup.update', 'uses' => 'ShiftgroupController@update']);
		//delete
	Route::delete('/shiftgroup/delete/{id}', ['middleware' => ['role:shiftgroup|delete'], 'as' => 'editor.shiftgroup.delete', 'uses' => 'ShiftgroupController@delete']);
	Route::post('/shiftgroup/deletebulk', ['middleware' => ['role:shiftgroup|delete'], 'as' => 'editor.shiftgroup.deletebulk', 'uses' => 'ShiftgroupController@deletebulk']);
	// detailitem 
	Route::get('/shiftgroup/datadetail/{id}', ['as' => 'editor.shiftgroup.datadetail', 'uses' => 'ShiftgroupController@datadetail']);
	Route::put('/shiftgroup/savedetail/{id}', ['middleware' => ['role:shiftgroup|update'], 'as' => 'editor.shiftgroup.savedetail', 'uses' => 'ShiftgroupController@savedetail']);
	Route::delete('/shiftgroup/deletedet/{id}', ['middleware' => ['role:shiftgroup|delete'], 'as' => 'editor.shiftgroup.deletedet', 'uses' => 'ShiftgroupController@deletedet']);

	//Finger log
	Route::get('/fingerlog', ['middleware' => ['role:fingerlog|read'], 'as' => 'editor.fingerlog.index', 'uses' => 'FingerlogController@index']);
	Route::get('/fingerlog/data', ['middleware' => ['role:fingerlog|read'], 'as' => 'editor.fingerlog.data', 'uses' => 'FingerlogController@data']);


	//Shift
		//index
	Route::get('/shift', ['middleware' => ['role:shift|read'], 'as' => 'editor.shift.index', 'uses' => 'ShiftController@index']);
	Route::get('/shift/data', ['as' => 'editor.shift.data', 'uses' => 'ShiftController@data']);
	
		//create
	Route::get('/shift/create', ['middleware' => ['role:shift|create'], 'as' => 'editor.shift.create', 'uses' => 'ShiftController@create']);
	Route::post('/shift/create', ['middleware' => ['role:shift|create'], 'as' => 'editor.shift.store', 'uses' => 'ShiftController@store']);
		//edit
	Route::get('/shift/edit/{id}', ['middleware' => ['role:shift|update'], 'as' => 'editor.shift.edit', 'uses' => 'ShiftController@edit']);
	Route::put('/shift/edit/{id}', ['middleware' => ['role:shift|update'], 'as' => 'editor.shift.update', 'uses' => 'ShiftController@update']);
		//delete
	Route::delete('/shift/delete/{id}', ['middleware' => ['role:shift|delete'], 'as' => 'editor.shift.delete', 'uses' => 'ShiftController@delete']);
	Route::post('/shift/deletebulk', ['middleware' => ['role:shift|delete'], 'as' => 'editor.shift.deletebulk', 'uses' => 'ShiftController@deletebulk']);


	//Shift Schedule
	//index
	Route::get('/shiftschedule', ['middleware' => ['role:shiftschedule|read'], 'as' => 'editor.shiftschedule.index', 'uses' => 'ShiftscheduleController@index']);
	Route::get('/shiftschedule/data', ['as' => 'editor.shiftschedule.data', 'uses' => 'ShiftscheduleController@data']);
	Route::get('/shiftschedule/datadetail/{id}/{id2}', ['as' => 'editor.shiftschedule.datadetail', 'uses' => 'ShiftscheduleController@datadetail']);
	
	//edit
	Route::get('/shiftschedule/{id}/{id2}/edit', ['middleware' => ['role:shiftschedule|update'], 'as' => 'editor.shiftschedule.edit', 'uses' => 'ShiftscheduleController@edit']);
	Route::put('/shiftschedule/{id}/edit', ['middleware' => ['role:shiftschedule|update'], 'as' => 'editor.shiftschedule.update', 'uses' => 'ShiftscheduleController@update']);

	//edit detail
	Route::get('/shiftschedule/editdetail/{id}', ['middleware' => ['role:shiftschedule|update'], 'as' => 'editor.shiftschedule.editdetail', 'uses' => 'ShiftscheduleController@editdetail']);
	Route::put('/shiftschedule/editdetail/{id}', ['middleware' => ['role:shiftschedule|update'], 'as' => 'editor.shiftschedule.update', 'uses' => 'ShiftscheduleController@updatedetail']);
	Route::delete('/shiftschedule/deletedetail/{id}', ['middleware' => ['role:shiftschedule|delete'], 'as' => 'editor.shiftschedule.delete', 'uses' => 'ShiftscheduleController@deletedetail']);

	//data detail
	Route::get('/shiftschedule/datadetail', ['as' => 'editor.shiftschedule.datadetail', 'uses' => 'ShiftscheduleController@datadetail']); 

	//Exchangeshift
		//index
	Route::get('/exchangeshift', ['middleware' => ['role:exchangeshift|read'], 'as' => 'editor.exchangeshift.index', 'uses' => 'ExchangeshiftController@index']);
	Route::get('/exchangeshift/data', ['as' => 'editor.exchangeshift.data', 'uses' => 'ExchangeshiftController@data']);
	
	//create
	Route::get('/exchangeshift/create', ['middleware' => ['role:exchangeshift|create'], 'as' => 'editor.exchangeshift.create', 'uses' => 'ExchangeshiftController@create']);
	Route::post('/exchangeshift/create', ['middleware' => ['role:exchangeshift|create'], 'as' => 'editor.exchangeshift.store', 'uses' => 'ExchangeshiftController@store']);
		
		//detail
	Route::get('/exchangeshift/detail/{id}', ['middleware' => ['role:exchangeshift|update'], 'as' => 'editor.exchangeshift.detail', 'uses' => 'ExchangeshiftController@detail']);
	Route::get('/exchangeshift/slip/{id}', ['middleware' => ['role:exchangeshift|update'], 'as' => 'editor.exchangeshift.slip', 'uses' => 'ExchangeshiftController@slip']);
	Route::put('/exchangeshift/saveheader/{id}', ['middleware' => ['role:exchangeshift|update'], 'as' => 'editor.exchangeshift.saveheader', 'uses' => 'ExchangeshiftController@saveheader']);
	Route::put('/exchangeshift/savedetail/{id}', ['middleware' => ['role:exchangeshift|update'], 'as' => 'editor.exchangeshift.savedetail', 'uses' => 'ExchangeshiftController@savedetail']);

	Route::put('/exchangeshift/detail/{id}', ['middleware' => ['role:exchangeshift|update'], 'as' => 'editor.exchangeshift.update', 'uses' => 'ExchangeshiftController@update']);
		//delete
	Route::delete('/exchangeshift/deletedet/{id}', ['middleware' => ['role:exchangeshift|delete'], 'as' => 'editor.exchangeshift.deletedet', 'uses' => 'ExchangeshiftController@deletedet']);
	// detailitem 
	Route::get('/exchangeshift/datadetail/{id}', ['as' => 'editor.exchangeshift.datadetail', 'uses' => 'ExchangeshiftController@datadetail']);
	 
	//generate
	Route::get('/overtime/generate/{id}', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.generate', 'uses' => 'ExchangeshiftController@generate']);
	Route::post('/overtime/generate/{id}', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.generate', 'uses' => 'ExchangeshiftController@generate']);

	//Employee
		//index
	Route::get('/employee', ['middleware' => ['role:employee|read'], 'as' => 'editor.employee.index', 'uses' => 'EmployeeController@index']);
	Route::get('/employee/data', ['as' => 'editor.employee.data', 'uses' => 'EmployeeController@data']);
	
	//create
	Route::get('/employee/create', ['middleware' => ['role:employee|create'], 'as' => 'editor.employee.create', 'uses' => 'EmployeeController@create']);
	Route::post('/employee/create', ['middleware' => ['role:employee|create'], 'as' => 'editor.employee.store', 'uses' => 'EmployeeController@store']);
		
		//edit
	Route::get('/employee/{id}/edit', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.edit', 'uses' => 'EmployeeController@edit']);
	Route::put('/employee/{id}/edit', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.update', 'uses' => 'EmployeeController@update']);

	Route::put('/employee/edit/{id}', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.update', 'uses' => 'EmployeeController@update']);
		//delete
	Route::delete('/employee/delete/{id}', ['middleware' => ['role:employee|delete'], 'as' => 'editor.employee.delete', 'uses' => 'EmployeeController@delete']);
	Route::post('/employee/deletebulk', ['middleware' => ['role:employee|delete'], 'as' => 'editor.employee.deletebulk', 'uses' => 'EmployeeController@deletebulk']);
	Route::get('/employee/datalookup', ['as' => 'editor.employee.datalookup', 'uses' => 'EmployeeController@datalookup']);

	//Supplier
		//index
	Route::get('/supplier', ['middleware' => ['role:supplier|read'], 'as' => 'editor.supplier.index', 'uses' => 'SupplierController@index']);
	Route::get('/supplier/data', ['as' => 'editor.supplier.data', 'uses' => 'SupplierController@data']);
	
	//create
	Route::get('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.create', 'uses' => 'SupplierController@create']);
	Route::post('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.store', 'uses' => 'SupplierController@store']);
		
		//edit
	Route::get('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.edit', 'uses' => 'SupplierController@edit']);
	Route::put('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update', 'uses' => 'SupplierController@update']);

	Route::put('/supplier/edit/{id}', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update', 'uses' => 'SupplierController@update']);
		//delete
	Route::delete('/supplier/delete/{id}', ['middleware' => ['role:supplier|delete'], 'as' => 'editor.supplier.delete', 'uses' => 'SupplierController@delete']);
	Route::post('/supplier/deletebulk', ['middleware' => ['role:supplier|delete'], 'as' => 'editor.supplier.deletebulk', 'uses' => 'SupplierController@deletebulk']);

	//Inventory
		//index
	Route::get('/inventory', ['middleware' => ['role:inventory|read'], 'as' => 'editor.inventory.index', 'uses' => 'InventoryController@index']);
	Route::get('/inventory/data', ['as' => 'editor.inventory.data', 'uses' => 'InventoryController@data']);
	Route::get('/inventory/datalookup', ['as' => 'editor.inventory.datalookup', 'uses' => 'InventoryController@datalookup']);
	
	//create
	Route::get('/inventory/create', ['middleware' => ['role:inventory|create'], 'as' => 'editor.inventory.create', 'uses' => 'InventoryController@create']);
	Route::post('/inventory/create', ['middleware' => ['role:inventory|create'], 'as' => 'editor.inventory.store', 'uses' => 'InventoryController@store']);
		
		//edit
	Route::get('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.edit', 'uses' => 'InventoryController@edit']);
	Route::put('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.update', 'uses' => 'InventoryController@update']);

	Route::put('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.update', 'uses' => 'InventoryController@update']);
		//delete
	Route::delete('/inventory/delete/{id}', ['middleware' => ['role:inventory|delete'], 'as' => 'editor.inventory.delete', 'uses' => 'InventoryController@delete']);
	Route::post('/inventory/deletebulk', ['middleware' => ['role:inventory|delete'], 'as' => 'editor.inventory.deletebulk', 'uses' => 'InventoryController@deletebulk']);

	//Inventorybrand
		//index
	Route::get('/inventorybrand', ['middleware' => ['role:inventorybrand|read'], 'as' => 'editor.inventorybrand.index', 'uses' => 'InventorybrandController@index']);
	Route::get('/inventorybrand/data', ['as' => 'editor.inventorybrand.data', 'uses' => 'InventorybrandController@data']);

	
		//create
	Route::get('/inventorybrand/create', ['middleware' => ['role:inventorybrand|create'], 'as' => 'editor.inventorybrand.create', 'uses' => 'InventorybrandController@create']);
	Route::post('/inventorybrand/create', ['middleware' => ['role:inventorybrand|create'], 'as' => 'editor.inventorybrand.store', 'uses' => 'InventorybrandController@store']);
		//edit
	Route::get('/inventorybrand/edit/{id}', ['middleware' => ['role:inventorybrand|update'], 'as' => 'editor.inventorybrand.edit', 'uses' => 'InventorybrandController@edit']);
	Route::put('/inventorybrand/edit/{id}', ['middleware' => ['role:inventorybrand|update'], 'as' => 'editor.inventorybrand.update', 'uses' => 'InventorybrandController@update']);
		//delete
	Route::delete('/inventorybrand/delete/{id}', ['middleware' => ['role:inventorybrand|delete'], 'as' => 'editor.inventorybrand.delete', 'uses' => 'InventorybrandController@delete']);
	Route::post('/inventorybrand/deletebulk', ['middleware' => ['role:inventorybrand|delete'], 'as' => 'editor.inventorybrand.deletebulk', 'uses' => 'InventorybrandController@deletebulk']);

	//Inventorytype
		//index
	Route::get('/inventorytype', ['middleware' => ['role:inventorytype|read'], 'as' => 'editor.inventorytype.index', 'uses' => 'InventorytypeController@index']);
	Route::get('/inventorytype/data', ['as' => 'editor.inventorytype.data', 'uses' => 'InventorytypeController@data']);
	
		//create
	Route::get('/inventorytype/create', ['middleware' => ['role:inventorytype|create'], 'as' => 'editor.inventorytype.create', 'uses' => 'InventorytypeController@create']);
	Route::post('/inventorytype/create', ['middleware' => ['role:inventorytype|create'], 'as' => 'editor.inventorytype.store', 'uses' => 'InventorytypeController@store']);
		//edit
	Route::get('/inventorytype/edit/{id}', ['middleware' => ['role:inventorytype|update'], 'as' => 'editor.inventorytype.edit', 'uses' => 'InventorytypeController@edit']);
	Route::put('/inventorytype/edit/{id}', ['middleware' => ['role:inventorytype|update'], 'as' => 'editor.inventorytype.update', 'uses' => 'InventorytypeController@update']);
		//delete
	Route::delete('/inventorytype/delete/{id}', ['middleware' => ['role:inventorytype|delete'], 'as' => 'editor.inventorytype.delete', 'uses' => 'InventorytypeController@delete']);
	Route::post('/inventorytype/deletebulk', ['middleware' => ['role:inventorytype|delete'], 'as' => 'editor.inventorytype.deletebulk', 'uses' => 'InventorytypeController@deletebulk']);

	//Inventorygroup
		//index
	Route::get('/inventorygroup', ['middleware' => ['role:inventorygroup|read'], 'as' => 'editor.inventorygroup.index', 'uses' => 'InventorygroupController@index']);
	Route::get('/inventorygroup/data', ['as' => 'editor.inventorygroup.data', 'uses' => 'InventorygroupController@data']);
	
		//create
	Route::get('/inventorygroup/create', ['middleware' => ['role:inventorygroup|create'], 'as' => 'editor.inventorygroup.create', 'uses' => 'InventorygroupController@create']);
	Route::post('/inventorygroup/create', ['middleware' => ['role:inventorygroup|create'], 'as' => 'editor.inventorygroup.store', 'uses' => 'InventorygroupController@store']);
		//edit
	Route::get('/inventorygroup/edit/{id}', ['middleware' => ['role:inventorygroup|update'], 'as' => 'editor.inventorygroup.edit', 'uses' => 'InventorygroupController@edit']);
	Route::put('/inventorygroup/edit/{id}', ['middleware' => ['role:inventorygroup|update'], 'as' => 'editor.inventorygroup.update', 'uses' => 'InventorygroupController@update']);
		//delete
	Route::delete('/inventorygroup/delete/{id}', ['middleware' => ['role:inventorygroup|delete'], 'as' => 'editor.inventorygroup.delete', 'uses' => 'InventorygroupController@delete']);
	Route::post('/inventorygroup/deletebulk', ['middleware' => ['role:inventorygroup|delete'], 'as' => 'editor.inventorygroup.deletebulk', 'uses' => 'InventorygroupController@deletebulk']);

	//Inventorysize
		//index
	Route::get('/inventorysize', ['middleware' => ['role:inventorysize|read'], 'as' => 'editor.inventorysize.index', 'uses' => 'InventorysizeController@index']);
	Route::get('/inventorysize/data', ['as' => 'editor.inventorysize.data', 'uses' => 'InventorysizeController@data']);
	
		//create
	Route::get('/inventorysize/create', ['middleware' => ['role:inventorysize|create'], 'as' => 'editor.inventorysize.create', 'uses' => 'InventorysizeController@create']);
	Route::post('/inventorysize/create', ['middleware' => ['role:inventorysize|create'], 'as' => 'editor.inventorysize.store', 'uses' => 'InventorysizeController@store']);
		//edit
	Route::get('/inventorysize/edit/{id}', ['middleware' => ['role:inventorysize|update'], 'as' => 'editor.inventorysize.edit', 'uses' => 'InventorysizeController@edit']);
	Route::put('/inventorysize/edit/{id}', ['middleware' => ['role:inventorysize|update'], 'as' => 'editor.inventorysize.update', 'uses' => 'InventorysizeController@update']);
		//delete
	Route::delete('/inventorysize/delete/{id}', ['middleware' => ['role:inventorysize|delete'], 'as' => 'editor.inventorysize.delete', 'uses' => 'InventorysizeController@delete']);
	Route::post('/inventorysize/deletebulk', ['middleware' => ['role:inventorysize|delete'], 'as' => 'editor.inventorysize.deletebulk', 'uses' => 'InventorysizeController@deletebulk']);

	//Inventorycolor
		//index
	Route::get('/inventorycolor', ['middleware' => ['role:inventorycolor|read'], 'as' => 'editor.inventorycolor.index', 'uses' => 'InventorycolorController@index']);
	Route::get('/inventorycolor/data', ['as' => 'editor.inventorycolor.data', 'uses' => 'InventorycolorController@data']);
	
		//create
	Route::get('/inventorycolor/create', ['middleware' => ['role:inventorycolor|create'], 'as' => 'editor.inventorycolor.create', 'uses' => 'InventorycolorController@create']);
	Route::post('/inventorycolor/create', ['middleware' => ['role:inventorycolor|create'], 'as' => 'editor.inventorycolor.store', 'uses' => 'InventorycolorController@store']);
		//edit
	Route::get('/inventorycolor/edit/{id}', ['middleware' => ['role:inventorycolor|update'], 'as' => 'editor.inventorycolor.edit', 'uses' => 'InventorycolorController@edit']);
	Route::put('/inventorycolor/edit/{id}', ['middleware' => ['role:inventorycolor|update'], 'as' => 'editor.inventorycolor.update', 'uses' => 'InventorycolorController@update']);
		//delete
	Route::delete('/inventorycolor/delete/{id}', ['middleware' => ['role:inventorycolor|delete'], 'as' => 'editor.inventorycolor.delete', 'uses' => 'InventorycolorController@delete']);
	Route::post('/inventorycolor/deletebulk', ['middleware' => ['role:inventorycolor|delete'], 'as' => 'editor.inventorycolor.deletebulk', 'uses' => 'InventorycolorController@deletebulk']);

	//Materialusedtype
		//index
	Route::get('/materialusedtype', ['middleware' => ['role:materialusedtype|read'], 'as' => 'editor.materialusedtype.index', 'uses' => 'MaterialusedtypeController@index']);
	Route::get('/materialusedtype/data', ['as' => 'editor.materialusedtype.data', 'uses' => 'MaterialusedtypeController@data']); 
	//create
	Route::get('/materialusedtype/create', ['middleware' => ['role:materialusedtype|create'], 'as' => 'editor.materialusedtype.create', 'uses' => 'MaterialusedtypeController@create']);
	Route::post('/materialusedtype/create', ['middleware' => ['role:materialusedtype|create'], 'as' => 'editor.materialusedtype.store', 'uses' => 'MaterialusedtypeController@store']); 
		//edit
	Route::get('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.edit', 'uses' => 'MaterialusedtypeController@edit']);
	Route::put('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.update', 'uses' => 'MaterialusedtypeController@update']);

	Route::put('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.update', 'uses' => 'MaterialusedtypeController@update']);
		//delete
	Route::delete('/materialusedtype/delete/{id}', ['middleware' => ['role:materialusedtype|delete'], 'as' => 'editor.materialusedtype.delete', 'uses' => 'MaterialusedtypeController@delete']);
	Route::post('/materialusedtype/deletebulk', ['middleware' => ['role:materialusedtype|delete'], 'as' => 'editor.materialusedtype.deletebulk', 'uses' => 'MaterialusedtypeController@deletebulk']);

	//Department
		//index
	Route::get('/department', ['middleware' => ['role:department|read'], 'as' => 'editor.department.index', 'uses' => 'DepartmentController@index']);
	Route::get('/department/data', ['as' => 'editor.department.data', 'uses' => 'DepartmentController@data']);
	
		//create
	Route::get('/department/create', ['middleware' => ['role:department|create'], 'as' => 'editor.department.create', 'uses' => 'DepartmentController@create']);
	Route::post('/department/create', ['middleware' => ['role:department|create'], 'as' => 'editor.department.store', 'uses' => 'DepartmentController@store']);
		//edit
	Route::get('/department/edit/{id}', ['middleware' => ['role:department|update'], 'as' => 'editor.department.edit', 'uses' => 'DepartmentController@edit']);
	Route::put('/department/edit/{id}', ['middleware' => ['role:department|update'], 'as' => 'editor.department.update', 'uses' => 'DepartmentController@update']);
		//delete
	Route::delete('/department/delete/{id}', ['middleware' => ['role:department|delete'], 'as' => 'editor.department.delete', 'uses' => 'DepartmentController@delete']);
	Route::post('/department/deletebulk', ['middleware' => ['role:department|delete'], 'as' => 'editor.department.deletebulk', 'uses' => 'DepartmentController@deletebulk']);

	//Materialused
		//index
	Route::get('/materialused', ['middleware' => ['role:materialused|read'], 'as' => 'editor.materialused.index', 'uses' => 'MaterialusedController@index']);
	Route::get('/materialused/data', ['as' => 'editor.materialused.data', 'uses' => 'MaterialusedController@data']);
	
	//create
	Route::get('/materialused/create', ['middleware' => ['role:materialused|create'], 'as' => 'editor.materialused.create', 'uses' => 'MaterialusedController@create']);
	Route::post('/materialused/create', ['middleware' => ['role:materialused|create'], 'as' => 'editor.materialused.store', 'uses' => 'MaterialusedController@store']);
		
		//detail
	Route::get('/materialused/detail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.detail', 'uses' => 'MaterialusedController@detail']);
	Route::put('/materialused/saveheader/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.saveheader', 'uses' => 'MaterialusedController@saveheader']);
	Route::put('/materialused/savedetail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.savedetail', 'uses' => 'MaterialusedController@savedetail']);

	Route::put('/materialused/detail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.update', 'uses' => 'MaterialusedController@update']);
		//delete
	Route::delete('/materialused/deletedet/{id}', ['middleware' => ['role:materialused|delete'], 'as' => 'editor.materialused.deletedet', 'uses' => 'MaterialusedController@deletedet']);
	Route::post('/materialused/deletebulk', ['middleware' => ['role:materialused|delete'], 'as' => 'editor.materialused.deletebulk', 'uses' => 'MaterialusedController@deletebulk']);
	// detailitem 
	Route::get('/materialused/datadetail/{id}', ['as' => 'editor.materialused.datadetail', 'uses' => 'MaterialusedController@datadetail']);

	//Materialused App
		//index
	Route::get('/materialusedapp', ['middleware' => ['role:materialusedapp|read'], 'as' => 'editor.materialusedapp.index', 'uses' => 'MaterialusedappController@index']);
	Route::get('/materialusedapp/detail/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.detail', 'uses' => 'MaterialusedappController@detail']);
	Route::get('/materialusedapp/data', ['as' => 'editor.materialusedapp.data', 'uses' => 'MaterialusedappController@data']);
	// detailitem 
	Route::get('/materialusedapp/datadetail/{id}', ['as' => 'editor.materialusedapp.datadetail', 'uses' => 'MaterialusedappController@datadetail']);
	Route::put('/materialusedapp/approve/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.approve', 'uses' => 'MaterialusedappController@approve']);
	Route::put('/materialusedapp/notapprove/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.notapprove', 'uses' => 'MaterialusedappController@notapprove']);

	//Recruitment
	//index
	Route::get('/recruitment', ['middleware' => ['role:recruitment|read'], 'as' => 'editor.recruitment.index', 'uses' => 'RecruitmentController@index']);
	Route::get('/recruitment/data', ['as' => 'editor.recruitment.data', 'uses' => 'RecruitmentController@data']);
	//create
	Route::get('/recruitment/create', ['middleware' => ['role:recruitment|create'], 'as' => 'editor.recruitment.create', 'uses' => 'RecruitmentController@create']);
	Route::post('/recruitment/create', ['middleware' => ['role:recruitment|create'], 'as' => 'editor.recruitment.store', 'uses' => 'RecruitmentController@store']);
	//cancel
	Route::post('/recruitment/cancel/{id}', ['middleware' => ['role:recruitment|delete'], 'as' => 'editor.recruitment.cancel', 'uses' => 'RecruitmentController@cancel']);  
	//detail
	Route::get('/recruitment/detail/{id}', ['middleware' => ['role:recruitment|update'], 'as' => 'editor.recruitment.detail', 'uses' => 'RecruitmentController@detail']);
	Route::put('/recruitment/saveheader/{id}', ['middleware' => ['role:recruitment|update'], 'as' => 'editor.recruitment.saveheader', 'uses' => 'RecruitmentController@saveheader']);
	Route::put('/recruitment/savedetail/{id}', ['middleware' => ['role:recruitment|update'], 'as' => 'editor.recruitment.savedetail', 'uses' => 'RecruitmentController@savedetail']);
	Route::post('/recruitment/updatedetail/{id}', ['middleware' => ['role:recruitment|update'], 'as' => 'editor.recruitment.updatedetail', 'uses' => 'RecruitmentController@updatedetail']);
	Route::put('/recruitment/detail/{id}', ['middleware' => ['role:recruitment|update'], 'as' => 'editor.recruitment.update', 'uses' => 'RecruitmentController@update']);
	//delete
	Route::delete('/recruitment/deletedet/{id}', ['middleware' => ['role:recruitment|delete'], 'as' => 'editor.recruitment.deletedet', 'uses' => 'RecruitmentController@deletedet']); 
	// detailitem 
	Route::get('/recruitment/datadetail/{id}', ['as' => 'editor.recruitment.datadetail', 'uses' => 'RecruitmentController@datadetail']);
	//slip
	Route::get('/recruitment/slip/{id}', ['middleware' => ['role:recruitment|read'], 'as' => 'editor.recruitment.slip', 'uses' => 'RecruitmentController@slip']);


	//Personal Management
	//Renewcontract
		//index
	Route::get('/renewcontract', ['middleware' => ['role:renewcontract|read'], 'as' => 'editor.renewcontract.index', 'uses' => 'RenewcontractController@index']);
	Route::get('/renewcontract/data', ['as' => 'editor.renewcontract.data', 'uses' => 'RenewcontractController@data']);
	Route::get('/renewcontract/datahistory', ['as' => 'editor.renewcontract.datahistory', 'uses' => 'RenewcontractController@datahistory']);
	
		//create
	Route::get('/renewcontract/create', ['middleware' => ['role:renewcontract|create'], 'as' => 'editor.renewcontract.create', 'uses' => 'RenewcontractController@create']);
	Route::post('/renewcontract/create', ['middleware' => ['role:renewcontract|create'], 'as' => 'editor.renewcontract.store', 'uses' => 'RenewcontractController@store']);
		//edit
	Route::get('/renewcontract/{id}/edit', ['middleware' => ['role:renewcontract|update'], 'as' => 'editor.renewcontract.edit', 'uses' => 'RenewcontractController@edit']);
	Route::put('/renewcontract/{id}/edit', ['middleware' => ['role:renewcontract|update'], 'as' => 'editor.renewcontract.update', 'uses' => 'RenewcontractController@update']);
		//delete
	Route::delete('/renewcontract/delete/{id}', ['middleware' => ['role:renewcontract|delete'], 'as' => 'editor.renewcontract.delete', 'uses' => 'RenewcontractController@delete']);
	Route::post('/renewcontract/deletebulk', ['middleware' => ['role:renewcontract|delete'], 'as' => 'editor.renewcontract.deletebulk', 'uses' => 'RenewcontractController@deletebulk']);
	Route::put('/renewcontract/cancel/{id}', ['middleware' => ['role:renewcontract|delete'], 'as' => 'editor.renewcontract.cancel', 'uses' => 'RenewcontractController@cancel']);
		//slip
	Route::get('/renewcontract/slip/{id}', ['as' => 'slipor.renewcontract.slip', 'uses' => 'RenewcontractController@slip']);

	//history contract
	Route::get('/historycontract', ['middleware' => ['role:historycontract|read'], 'as' => 'editor.historycontract.index', 'uses' => 'HistorycontractController@index']);
	Route::get('/historycontract/data', ['middleware' => ['role:historycontract|read'], 'as' => 'editor.historycontract.data', 'uses' => 'HistorycontractController@data']);


	//Mutation
		//index
	Route::get('/mutation', ['middleware' => ['role:mutation|read'], 'as' => 'editor.mutation.index', 'uses' => 'MutationController@index']);
	Route::get('/mutation/data', ['as' => 'editor.mutation.data', 'uses' => 'MutationController@data']);
	Route::get('/mutation/datahistory', ['as' => 'editor.mutation.datahistory', 'uses' => 'MutationController@datahistory']);
	
		//create
	Route::get('/mutation/create', ['middleware' => ['role:mutation|create'], 'as' => 'editor.mutation.create', 'uses' => 'MutationController@create']);
	Route::post('/mutation/create', ['middleware' => ['role:mutation|create'], 'as' => 'editor.mutation.store', 'uses' => 'MutationController@store']);
		//edit
	Route::get('/mutation/{id}/edit', ['middleware' => ['role:mutation|update'], 'as' => 'editor.mutation.edit', 'uses' => 'MutationController@edit']);
	Route::put('/mutation/{id}/edit', ['middleware' => ['role:mutation|update'], 'as' => 'editor.mutation.update', 'uses' => 'MutationController@update']);
		//delete
	Route::delete('/mutation/delete/{id}', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.delete', 'uses' => 'MutationController@delete']);
	Route::post('/mutation/deletebulk', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.deletebulk', 'uses' => 'MutationController@deletebulk']);
	Route::put('/mutation/cancel/{id}', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.cancel', 'uses' => 'MutationController@cancel']);
		//slip
	Route::get('/mutation/slip/{id}', ['as' => 'slipor.mutation.slip', 'uses' => 'MutationController@slip']);

	//Promotion
		//index
	Route::get('/promotion', ['middleware' => ['role:promotion|read'], 'as' => 'editor.promotion.index', 'uses' => 'PromotionController@index']);
	Route::get('/promotion/data', ['as' => 'editor.promotion.data', 'uses' => 'PromotionController@data']);
	Route::get('/promotion/datahistory', ['as' => 'editor.promotion.datahistory', 'uses' => 'PromotionController@datahistory']);
	
		//create
	Route::get('/promotion/create', ['middleware' => ['role:promotion|create'], 'as' => 'editor.promotion.create', 'uses' => 'PromotionController@create']);
	Route::post('/promotion/create', ['middleware' => ['role:promotion|create'], 'as' => 'editor.promotion.store', 'uses' => 'PromotionController@store']);
		//edit
	Route::get('/promotion/{id}/edit', ['middleware' => ['role:promotion|update'], 'as' => 'editor.promotion.edit', 'uses' => 'PromotionController@edit']);
	Route::put('/promotion/{id}/edit', ['middleware' => ['role:promotion|update'], 'as' => 'editor.promotion.update', 'uses' => 'PromotionController@update']);
		//delete
	Route::delete('/promotion/delete/{id}', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.delete', 'uses' => 'PromotionController@delete']);
	Route::post('/promotion/deletebulk', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.deletebulk', 'uses' => 'PromotionController@deletebulk']);
	Route::put('/promotion/cancel/{id}', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.cancel', 'uses' => 'PromotionController@cancel']);
	//slip
	Route::get('/promotion/slip/{id}', ['as' => 'slipor.promotion.slip', 'uses' => 'PromotionController@slip']);

	//Punishment
		//index
	Route::get('/punishment', ['middleware' => ['role:punishment|read'], 'as' => 'editor.punishment.index', 'uses' => 'PunishmentController@index']);
	Route::get('/punishment/data', ['as' => 'editor.punishment.data', 'uses' => 'PunishmentController@data']);
	Route::get('/punishment/datahistory', ['as' => 'editor.punishment.datahistory', 'uses' => 'PunishmentController@datahistory']);

		//create
	Route::get('/punishment/create', ['middleware' => ['role:punishment|create'], 'as' => 'editor.punishment.create', 'uses' => 'PunishmentController@create']);
	Route::post('/punishment/create', ['middleware' => ['role:punishment|create'], 'as' => 'editor.punishment.store', 'uses' => 'PunishmentController@store']);
		//edit
	Route::get('/punishment/{id}/edit', ['middleware' => ['role:punishment|update'], 'as' => 'editor.punishment.edit', 'uses' => 'PunishmentController@edit']);
	Route::put('/punishment/{id}/edit', ['middleware' => ['role:punishment|update'], 'as' => 'editor.punishment.update', 'uses' => 'PunishmentController@update']);
		//delete
	Route::delete('/punishment/delete/{id}', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.delete', 'uses' => 'PunishmentController@delete']);
	Route::post('/punishment/deletebulk', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.deletebulk', 'uses' => 'PunishmentController@deletebulk']);
	Route::put('/punishment/cancel/{id}', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.cancel', 'uses' => 'PunishmentController@cancel']);

	//Reward
		//index
	Route::get('/reward', ['middleware' => ['role:reward|read'], 'as' => 'editor.reward.index', 'uses' => 'RewardController@index']);
	Route::get('/reward/data', ['as' => 'editor.reward.data', 'uses' => 'RewardController@data']);
	Route::get('/reward/datahistory', ['as' => 'editor.reward.datahistory', 'uses' => 'RewardController@datahistory']);

		//create
	Route::get('/reward/create', ['middleware' => ['role:reward|create'], 'as' => 'editor.reward.create', 'uses' => 'RewardController@create']);
	Route::post('/reward/create', ['middleware' => ['role:reward|create'], 'as' => 'editor.reward.store', 'uses' => 'RewardController@store']);
		//edit
	Route::get('/reward/{id}/edit', ['middleware' => ['role:reward|update'], 'as' => 'editor.reward.edit', 'uses' => 'RewardController@edit']);
	Route::put('/reward/{id}/edit', ['middleware' => ['role:reward|update'], 'as' => 'editor.reward.update', 'uses' => 'RewardController@update']);
		//delete
	Route::delete('/reward/delete/{id}', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.delete', 'uses' => 'RewardController@delete']);
	Route::post('/reward/deletebulk', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.deletebulk', 'uses' => 'RewardController@deletebulk']);
	Route::put('/reward/cancel/{id}', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.cancel', 'uses' => 'RewardController@cancel']);


	//Training
		//index
	Route::get('/training', ['middleware' => ['role:training|read'], 'as' => 'editor.training.index', 'uses' => 'TrainingController@index']);
	Route::get('/training/data', ['as' => 'editor.training.data', 'uses' => 'TrainingController@data']);
	Route::get('/training/datahistory', ['as' => 'editor.training.datahistory', 'uses' => 'TrainingController@datahistory']);

		//create
	Route::get('/training/create', ['middleware' => ['role:training|create'], 'as' => 'editor.training.create', 'uses' => 'TrainingController@create']);
	Route::post('/training/create', ['middleware' => ['role:training|create'], 'as' => 'editor.training.store', 'uses' => 'TrainingController@store']);
		//edit
	Route::get('/training/{id}/edit', ['middleware' => ['role:training|update'], 'as' => 'editor.training.edit', 'uses' => 'TrainingController@edit']);
	Route::put('/training/{id}/edit', ['middleware' => ['role:training|update'], 'as' => 'editor.training.update', 'uses' => 'TrainingController@update']);
		//delete
	Route::delete('/training/delete/{id}', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.delete', 'uses' => 'TrainingController@delete']);
	Route::post('/training/deletebulk', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.deletebulk', 'uses' => 'TrainingController@deletebulk']);
	Route::put('/training/cancel/{id}', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.cancel', 'uses' => 'TrainingController@cancel']);

	//Training App
		//index
	Route::get('/trainingapp', ['middleware' => ['role:trainingapp|read'], 'as' => 'editor.trainingapp.index', 'uses' => 'TrainingappController@index']);
	Route::get('/trainingapp/detail/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.detail', 'uses' => 'TrainingappController@detail']);
	Route::get('/trainingapp/data', ['as' => 'editor.trainingapp.data', 'uses' => 'TrainingappController@data']);
	// detailitem 
	Route::get('/trainingapp/datadetail/{id}', ['as' => 'editor.trainingapp.datadetail', 'uses' => 'TrainingappController@datadetail']);
	Route::put('/trainingapp/approve/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.approve', 'uses' => 'TrainingappController@approve']);
	Route::put('/trainingapp/notapprove/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.notapprove', 'uses' => 'TrainingappController@notapprove']);

	//Travelling
		//index
	Route::get('/travelling', ['middleware' => ['role:travelling|read'], 'as' => 'editor.travelling.index', 'uses' => 'TravellingController@index']);
	Route::get('/travelling/data', ['as' => 'editor.travelling.data', 'uses' => 'TravellingController@data']);
	Route::get('/travelling/datahistory', ['as' => 'editor.travelling.datahistory', 'uses' => 'TravellingController@datahistory']);
	
		//create
	Route::get('/travelling/create', ['middleware' => ['role:travelling|create'], 'as' => 'editor.travelling.create', 'uses' => 'TravellingController@create']);
	Route::post('/travelling/create', ['middleware' => ['role:travelling|create'], 'as' => 'editor.travelling.store', 'uses' => 'TravellingController@store']);
		//edit
	Route::get('/travelling/{id}/edit', ['middleware' => ['role:travelling|update'], 'as' => 'editor.travelling.edit', 'uses' => 'TravellingController@edit']);
	Route::put('/travelling/{id}/edit', ['middleware' => ['role:travelling|update'], 'as' => 'editor.travelling.update', 'uses' => 'TravellingController@update']);
		//delete
	Route::delete('/travelling/delete/{id}', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.delete', 'uses' => 'TravellingController@delete']);
	Route::post('/travelling/deletebulk', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.deletebulk', 'uses' => 'TravellingController@deletebulk']);
	Route::put('/travelling/cancel/{id}', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.cancel', 'uses' => 'TravellingController@cancel']);


	//Travelling App
		//index
	Route::get('/travellingapp', ['middleware' => ['role:travellingapp|read'], 'as' => 'editor.travellingapp.index', 'uses' => 'TravellingappController@index']);
	Route::get('/travellingapp/detail/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.detail', 'uses' => 'TravellingappController@detail']);
	Route::get('/travellingapp/data', ['as' => 'editor.travellingapp.data', 'uses' => 'TravellingappController@data']);
	// detailitem 
	Route::get('/travellingapp/datadetail/{id}', ['as' => 'editor.travellingapp.datadetail', 'uses' => 'TravellingappController@datadetail']);
	Route::put('/travellingapp/approve/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.approve', 'uses' => 'TravellingappController@approve']);
	Route::put('/travellingapp/notapprove/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.notapprove', 'uses' => 'TravellingappController@notapprove']);

	//Leaving
		//index
	Route::get('/leaving', ['middleware' => ['role:leaving|read'], 'as' => 'editor.leaving.index', 'uses' => 'LeavingController@index']);
	Route::get('/leaving/data', ['as' => 'editor.leaving.data', 'uses' => 'LeavingController@data']);
	Route::get('/leaving/datahistory', ['as' => 'editor.leaving.datahistory', 'uses' => 'LeavingController@datahistory']);

	
		//create
	Route::get('/leaving/create', ['middleware' => ['role:leaving|create'], 'as' => 'editor.leaving.create', 'uses' => 'LeavingController@create']);
	Route::post('/leaving/create', ['middleware' => ['role:leaving|create'], 'as' => 'editor.leaving.store', 'uses' => 'LeavingController@store']);
		//edit
	Route::get('/leaving/{id}/edit', ['middleware' => ['role:leaving|update'], 'as' => 'editor.leaving.edit', 'uses' => 'LeavingController@edit']);
	Route::put('/leaving/{id}/edit', ['middleware' => ['role:leaving|update'], 'as' => 'editor.leaving.update', 'uses' => 'LeavingController@update']);
		//delete
	Route::delete('/leaving/delete/{id}', ['middleware' => ['role:leaving|delete'], 'as' => 'editor.leaving.delete', 'uses' => 'LeavingController@delete']);
	Route::post('/leaving/deletebulk', ['middleware' => ['role:leaving|delete'], 'as' => 'editor.leaving.deletebulk', 'uses' => 'LeavingController@deletebulk']);

	Route::put('/leaving/cancel/{id}', ['middleware' => ['role:leaving|delete'], 'as' => 'editor.leaving.cancel', 'uses' => 'LeavingController@cancel']);  

	//Leaving App
		//index
	Route::get('/leavingapp', ['middleware' => ['role:leavingapp|read'], 'as' => 'editor.leavingapp.index', 'uses' => 'LeavingappController@index']);
	Route::get('/leavingapp/detail/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.detail', 'uses' => 'LeavingappController@detail']);
	Route::get('/leavingapp/data', ['as' => 'editor.leavingapp.data', 'uses' => 'LeavingappController@data']);
	// detailitem 
	Route::get('/leavingapp/datadetail/{id}', ['as' => 'editor.leavingapp.datadetail', 'uses' => 'LeavingappController@datadetail']);
	Route::put('/leavingapp/approve/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.approve', 'uses' => 'LeavingappController@approve']);
	Route::put('/leavingapp/notapprove/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.notapprove', 'uses' => 'LeavingappController@notapprove']);

	//Leaving App
		//index
	Route::get('/leavingplafond', ['middleware' => ['role:leavingplafond|read'], 'as' => 'editor.leavingplafond.index', 'uses' => 'LeavingplafondController@index']);
	Route::get('/leavingplafond/data', ['as' => 'editor.leavingplafond.data', 'uses' => 'LeavingplafondController@data']);

	//Organization Structure

	//Orgstructure 
		//edit
	Route::get('/orgstructure/edit', ['middleware' => ['role:orgstructure|update'], 'as' => 'editor.orgstructure.edit', 'uses' => 'OrgstructureController@edit']);
	Route::put('/orgstructure/edit', ['middleware' => ['role:orgstructure|update'], 'as' => 'editor.orgstructure.update', 'uses' => 'OrgstructureController@update']); 


	//Police
		//index
	Route::get('/police', ['middleware' => ['role:police|read'], 'as' => 'editor.police.index', 'uses' => 'PoliceController@index']);
	Route::get('/police/data', ['as' => 'editor.police.data', 'uses' => 'PoliceController@data']);
	
		//create
	Route::get('/police/create', ['middleware' => ['role:police|create'], 'as' => 'editor.police.create', 'uses' => 'PoliceController@create']);
	Route::post('/police/create', ['middleware' => ['role:police|create'], 'as' => 'editor.police.store', 'uses' => 'PoliceController@store']);
		//edit
	Route::get('/police/{id}/edit', ['middleware' => ['role:police|update'], 'as' => 'editor.police.edit', 'uses' => 'PoliceController@edit']);
	Route::put('/police/{id}/edit', ['middleware' => ['role:police|update'], 'as' => 'editor.police.update', 'uses' => 'PoliceController@update']);
		//delete
	Route::delete('/police/delete/{id}', ['middleware' => ['role:police|delete'], 'as' => 'editor.police.delete', 'uses' => 'PoliceController@delete']);
	Route::post('/police/deletebulk', ['middleware' => ['role:police|delete'], 'as' => 'editor.police.deletebulk', 'uses' => 'PoliceController@deletebulk']);

	//Payroll
		//index
	Route::get('/payroll', ['middleware' => ['role:payroll|read'], 'as' => 'editor.payroll.index', 'uses' => 'PayrollController@index']);
	Route::get('/payroll/data', ['as' => 'editor.payroll.data', 'uses' => 'PayrollController@data']);
	Route::get('/payroll/datatime', ['as' => 'editor.payroll.datatime', 'uses' => 'PayrollController@datatime']);
	Route::get('/payroll/datadetail/{id}', ['as' => 'editor.payroll.datadetail', 'uses' => 'PayrollController@datadetail']);
	
		//create
	Route::get('/payroll/create/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.create', 'uses' => 'PayrollController@create']);
	Route::post('/payroll/create/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.store', 'uses' => 'PayrollController@store']);

	//edit
	Route::get('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.edit', 'uses' => 'PayrollController@edit']);
	Route::put('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.update', 'uses' => 'PayrollController@update']);
	Route::get('/payroll/datadetail', ['as' => 'editor.payroll.datadetail', 'uses' => 'PayrollController@datadetail']); 

	//generate
	Route::get('/payroll/generate/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.generate', 'uses' => 'PayrollController@generate']);
	Route::post('/payroll/generate/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.generate', 'uses' => 'PayrollController@generate']);
	//slip
	Route::get('/payroll/slip/{id}', ['middleware' => ['role:payroll|update'], 'as' => 'slipor.payroll.slip', 'uses' => 'PayrollController@slip']);
	Route::get('/payroll/slipall/{id}', ['middleware' => ['role:payroll|update'], 'as' => 'slipor.payroll.slip', 'uses' => 'PayrollController@slipall']);
	//history
	Route::get('/payroll/dataabsence', ['as' => 'editor.payroll.dataabsence', 'uses' => 'PayrollController@dataabsence']);
	Route::post('/payroll/storeabsence', ['as' => 'editor.payrollabsence.store', 'uses' => 'PayrollController@storeabsence']);

	//Payroll view
		//index
	Route::get('/payrollview', ['middleware' => ['role:payrollview|read'], 'as' => 'editor.payrollview.index', 'uses' => 'PayrollviewController@index']);
	Route::get('/payrollview/data', ['as' => 'editor.payrollview.data', 'uses' => 'PayrollviewController@data']);
	Route::get('/payrollview/datadetail/{id}', ['as' => 'editor.payrollview.datadetail', 'uses' => 'PayrollviewController@datadetail']);
	
	//edit
	Route::get('/payrollview/{id}/edit', ['middleware' => ['role:payrollview|update'], 'as' => 'editor.payrollview.edit', 'uses' => 'PayrollviewController@edit']);

	//edit
	Route::get('/payrollview/editdetail/{id}', ['middleware' => ['role:payrollview|update'], 'as' => 'editor.payrollview.edit', 'uses' => 'PayrollviewController@editdetail']);
	Route::put('/payrollview/editdetail/{id}', ['middleware' => ['role:payrollview|update'], 'as' => 'editor.payrollview.edit', 'uses' => 'PayrollviewController@updatedetail']);


	//Upload Time
		//index
	Route::get('/uploadtime', ['middleware' => ['role:uploadtime|read'], 'as' => 'editor.uploadtime.index', 'uses' => 'UploadtimeController@index']); 
	Route::get('/uploadtime/data', ['as' => 'editor.uploadtime.data', 'uses' => 'UploadtimeController@data']);
	  
	Route::post('/uploadtime/storeimport', ['middleware' => ['role:uploadtime|update'], 'as' => 'editor.uploadtime.storeimport', 'uses' => 'UploadtimeController@storeimport']); 

	//Upload Time List
		//index
	Route::get('/uploadtimelist', ['middleware' => ['role:uploadtimelist|read'], 'as' => 'editor.uploadtimelist.index', 'uses' => 'UploadtimelistController@index']); 
	Route::get('/uploadtimelist/data', ['as' => 'editor.uploadtimelist.data', 'uses' => 'UploadtimelistController@data']); 

	//Time
		//index
	Route::get('/time', ['as' => 'editor.time.index', 'uses' => 'TimeController@index']);
	Route::get('/time/data', ['as' => 'editor.time.data', 'uses' => 'TimeController@data']);
	Route::get('/time/datadetail/{id}', ['as' => 'editor.time.datadetail', 'uses' => 'TimeController@datadetail']);
	
		//create
	Route::get('/time/create/{id}', ['as' => 'editor.time.create', 'uses' => 'TimeController@create']);
	Route::post('/time/create/{id}', ['as' => 'editor.time.store', 'uses' => 'TimeController@store']);

	//edit
	Route::get('/time/{id}/{id2}/edit', ['middleware' => ['role:time|update'], 'as' => 'editor.time.edit', 'uses' => 'TimeController@edit']);
	Route::put('/time/{id}/edit', ['middleware' => ['role:time|update'], 'as' => 'editor.time.update', 'uses' => 'TimeController@update']);
	Route::get('/time/datadetail', ['as' => 'editor.time.datadetail', 'uses' => 'TimeController@datadetail']); 

	//generate
	Route::get('/time/generate/{id}', ['as' => 'editor.time.generate', 'uses' => 'TimeController@generate']);
	Route::post('/time/generate/{id}', ['as' => 'editor.time.generate', 'uses' => 'TimeController@generate']);

	Route::post('/time/generateupload/{id}', ['as' => 'editor.time.generateupload', 'uses' => 'TimeController@generateupload']);

	//Time View
	//index
	Route::get('/timeview', ['middleware' => ['role:timeview|read'], 'as' => 'editor.timeview.index', 'uses' => 'TimeviewController@index']);
	Route::get('/timeview/data', ['as' => 'editor.timeview.data', 'uses' => 'TimeviewController@data']);
	Route::get('/timeview/datadetail/{id}', ['as' => 'editor.timeview.datadetail', 'uses' => 'TimeviewController@datadetail']);


	//edit
	Route::get('/timeview/{id}/{id2}/edit', ['middleware' => ['role:timeview|update'], 'as' => 'editor.timeview.edit', 'uses' => 'TimeviewController@edit']);
	Route::put('/timeview/{id}/edit', ['middleware' => ['role:timeview|update'], 'as' => 'editor.timeview.update', 'uses' => 'TimeController@update']);
	Route::get('/timeview/datadetail', ['as' => 'editor.timeview.datadetail', 'uses' => 'TimeviewController@datadetail']); 


	//Loan
		//index
	Route::get('/loan', ['middleware' => ['role:loan|read'], 'as' => 'editor.loan.index', 'uses' => 'LoanController@index']);
	Route::get('/loan/data', ['as' => 'editor.loan.data', 'uses' => 'LoanController@data']);
	Route::get('/loan/detaildata/{id}', ['as' => 'editor.loan.detaildata', 'uses' => 'LoanController@detaildata']);
	
		//create
	Route::get('/loan/create', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.create', 'uses' => 'LoanController@create']);
	Route::post('/loan/create', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.store', 'uses' => 'LoanController@store']);
		//edit
	Route::get('/loan/{id}/edit', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.edit', 'uses' => 'LoanController@edit']);
	Route::put('/loan/{id}/edit', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.update', 'uses' => 'LoanController@update']);
		//delete
	Route::delete('/loan/delete/{id}', ['middleware' => ['role:loan|delete'], 'as' => 'editor.loan.delete', 'uses' => 'LoanController@delete']);
	Route::post('/loan/deletebulk', ['middleware' => ['role:loan|delete'], 'as' => 'editor.loan.deletebulk', 'uses' => 'LoanController@deletebulk']);
	Route::put('/loan/cancel/{id}', ['middleware' => ['role:loan|delete'], 'as' => 'editor.loan.cancel', 'uses' => 'LoanController@cancel']);
	//edit
	Route::get('/loan/editinstallment/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.editinstallment', 'uses' => 'LoanController@editinstallment']);
	Route::put('/loan/editinstallment/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.updateinstallment', 'uses' => 'LoanController@updateinstallment']);


	//Overtime
		//index
	Route::get('/overtime', ['middleware' => ['role:overtime|read'], 'as' => 'editor.overtime.index', 'uses' => 'OvertimeController@index']);
	Route::get('/overtime/data', ['as' => 'editor.overtime.data', 'uses' => 'OvertimeController@data']);
	
	//create
	Route::get('/overtime/create', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.create', 'uses' => 'OvertimeController@create']);
	Route::post('/overtime/create', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.store', 'uses' => 'OvertimeController@store']);
		
		//detail
	Route::get('/overtime/detail/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.detail', 'uses' => 'OvertimeController@detail']);
	Route::get('/overtime/slip/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.slip', 'uses' => 'OvertimeController@slip']);
	Route::put('/overtime/saveheader/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.saveheader', 'uses' => 'OvertimeController@saveheader']);
	Route::put('/overtime/savedetail/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.savedetail', 'uses' => 'OvertimeController@savedetail']);

	Route::put('/overtime/detail/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.update', 'uses' => 'OvertimeController@update']);
		//delete
	Route::delete('/overtime/deletedet/{id}', ['middleware' => ['role:overtime|delete'], 'as' => 'editor.overtime.deletedet', 'uses' => 'OvertimeController@deletedet']);
	Route::post('/overtime/deletebulk', ['middleware' => ['role:overtime|delete'], 'as' => 'editor.overtime.deletebulk', 'uses' => 'OvertimeController@deletebulk']);
	// detailitem 
	Route::get('/overtime/datadetail/{id}', ['as' => 'editor.overtime.datadetail', 'uses' => 'OvertimeController@datadetail']);

		//Mealtran
		//index
	Route::get('/mealtran', ['middleware' => ['role:mealtran|read'], 'as' => 'editor.mealtran.index', 'uses' => 'MealtranController@index']);
	Route::get('/mealtran/data', ['as' => 'editor.mealtran.data', 'uses' => 'MealtranController@data']);
	
		//create
	Route::get('/mealtran/create', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.create', 'uses' => 'MealtranController@create']);
	Route::post('/mealtran/create', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.store', 'uses' => 'MealtranController@store']);
		//edit
	Route::get('/mealtran/edit/{id}', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.edit', 'uses' => 'MealtranController@edit']);
	Route::put('/mealtran/edit/{id}', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.update', 'uses' => 'MealtranController@update']);
		//edit detail
	Route::get('/mealtran/{id}/{id2}/editdetail', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.editdetail', 'uses' => 'MealtranController@editdetail']);
	Route::put('/mealtran/{id}/{id2}/editdetail', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.updatedetail', 'uses' => 'MealtranController@updatedetail']);
		//delete
	Route::delete('/mealtran/delete/{id}', ['middleware' => ['role:mealtran|delete'], 'as' => 'editor.mealtran.delete', 'uses' => 'MealtranController@delete']);
	Route::post('/mealtran/deletebulk', ['middleware' => ['role:mealtran|delete'], 'as' => 'editor.mealtran.deletebulk', 'uses' => 'MealtranController@deletebulk']);
	 
	//generate
	Route::get('/mealtran/generate/{id}', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.generate', 'uses' => 'MealtranController@generate']);
	Route::post('/mealtran/generate/{id}', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.generate', 'uses' => 'MealtranController@generate']);

	//Thr
		//index
	Route::get('/thr', ['middleware' => ['role:thr|read'], 'as' => 'editor.thr.index', 'uses' => 'ThrController@index']);
	Route::get('/thr/data', ['as' => 'editor.thr.data', 'uses' => 'ThrController@data']);
	
		//create
	Route::get('/thr/create', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.create', 'uses' => 'ThrController@create']);
	Route::post('/thr/create', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.store', 'uses' => 'ThrController@store']);
		//edit
	Route::get('/thr/edit/{id}', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.edit', 'uses' => 'ThrController@edit']);
	Route::put('/thr/edit/{id}', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.update', 'uses' => 'ThrController@update']);
		//edit detail
	Route::get('/thr/{id}/editdetail', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.editdetail', 'uses' => 'ThrController@editdetail']);
	Route::put('/thr/{id}/editdetail', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.updatedetail', 'uses' => 'ThrController@updatedetail']);
		//delete
	Route::delete('/thr/delete/{id}', ['middleware' => ['role:thr|delete'], 'as' => 'editor.thr.delete', 'uses' => 'ThrController@delete']);
	Route::post('/thr/deletebulk', ['middleware' => ['role:thr|delete'], 'as' => 'editor.thr.deletebulk', 'uses' => 'ThrController@deletebulk']);
	 
	//generate
	Route::get('/thr/generate/{id}', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.generate', 'uses' => 'ThrController@generate']);
	Route::post('/thr/generate/{id}', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.generate', 'uses' => 'ThrController@generate']);

	//FILTER
	Route::post('/datefilter', ['as' => 'editor.datefilter', 'uses' => 'UserController@datefilter']);
	Route::post('/employeefilter', ['as' => 'editor.employeefilter', 'uses' => 'UserController@employeefilter']);
	Route::post('/periodfilter', ['as' => 'editor.periodfilter', 'uses' => 'UserController@periodfilter']);
	Route::post('/periodfilterthr', ['as' => 'editor.periodfilterthr', 'uses' => 'UserController@periodfilterthr']);
	Route::post('/periodfilteronly', ['as' => 'editor.periodfilteronly', 'uses' => 'UserController@periodfilteronly']);
	Route::post('/periodfilteremp', ['as' => 'editor.periodfilteremp', 'uses' => 'UserController@periodfilteremp']);
	
	//REPORT
	//report payroll
	Route::get('/reportpayroll', ['middleware' => ['role:reportpayroll|read'], 'as' => 'editor.reportpayroll.index', 'uses' => 'ReportpayrollController@index']);
	Route::get('/reportpayroll/data', ['middleware' => ['role:reportpayroll|read'], 'as' => 'editor.reportpayroll.data', 'uses' => 'ReportpayrollController@data']);

	//report jamsostek
	Route::get('/reportjamsostek', ['middleware' => ['role:reportjamsostek|read'], 'as' => 'editor.reportjamsostek.index', 'uses' => 'ReportjamsostekController@index']);
	Route::get('/reportjamsostek/data', ['middleware' => ['role:reportjamsostek|read'], 'as' => 'editor.reportjamsostek.data', 'uses' => 'ReportjamsostekController@data']);

	//report loan
	Route::get('/reportloan', ['middleware' => ['role:reportloan|read'], 'as' => 'editor.reportloan.index', 'uses' => 'ReportloanController@index']);
	Route::get('/reportloan/data', ['middleware' => ['role:reportloan|read'], 'as' => 'editor.reportloan.data', 'uses' => 'ReportloanController@data']);

	//report pph21
	Route::get('/reportpph21', ['middleware' => ['role:reportpph21|read'], 'as' => 'editor.reportpph21.index', 'uses' => 'Reportpph21Controller@index']);
	Route::get('/reportpph21/data', ['middleware' => ['role:reportpph21|read'], 'as' => 'editor.reportpph21.data', 'uses' => 'Reportpph21Controller@data']);

	//report transferbca
	Route::get('/reporttransferbca', ['middleware' => ['role:reporttransferbca|read'], 'as' => 'editor.reporttransferbca.index', 'uses' => 'ReporttransferbcaController@index']);
	Route::get('/reporttransferbca/data', ['middleware' => ['role:reporttransferbca|read'], 'as' => 'editor.reporttransferbca.data', 'uses' => 'ReporttransferbcaController@data']);

	//report payrollslip
	Route::get('/reportpayrollslip', ['middleware' => ['role:reportpayrollslip|read'], 'as' => 'editor.reportpayrollslip.index', 'uses' => 'ReportpayrollslipController@index']);
	Route::get('/reportpayrollslip/data', ['middleware' => ['role:reportpayrollslip|read'], 'as' => 'editor.reportpayrollslip.data', 'uses' => 'ReportpayrollslipController@data']);

	//report overtime
	Route::get('/reportovertime', ['middleware' => ['role:reportovertime|read'], 'as' => 'editor.reportovertime.index', 'uses' => 'ReportovertimeController@index']);
	Route::get('/reportovertime/data', ['middleware' => ['role:reportovertime|read'], 'as' => 'editor.reportovertime.data', 'uses' => 'ReportovertimeController@data']);

	//report meal
	Route::get('/reportmeal', ['middleware' => ['role:reportmeal|read'], 'as' => 'editor.reportmeal.index', 'uses' => 'ReportmealController@index']);
	Route::get('/reportmeal/data', ['middleware' => ['role:reportmeal|read'], 'as' => 'editor.reportmeal.data', 'uses' => 'ReportmealController@data']);

	//report nightallowance
	Route::get('/reportnightallowance', ['middleware' => ['role:reportnightallowance|read'], 'as' => 'editor.reportnightallowance.index', 'uses' => 'ReportnightallowanceController@index']);
	Route::get('/reportnightallowance/data', ['middleware' => ['role:reportnightallowance|read'], 'as' => 'editor.reportnightallowance.data', 'uses' => 'ReportnightallowanceController@data']);

	//report reportincityllowance
	Route::get('/reportincityallowance', ['middleware' => ['role:reportincityallowance|read'], 'as' => 'editor.reportincityallowance.index', 'uses' => 'ReportincityallowanceController@index']);
	Route::get('/reportincityallowance/data', ['middleware' => ['role:reportincityallowance|read'], 'as' => 'editor.reportincityallowance.data', 'uses' => 'ReportincityallowanceController@data']);

	//report reportoutcityllowance
	Route::get('/reportoutcityallowance', ['middleware' => ['role:reportoutcityallowance|read'], 'as' => 'editor.reportoutcityallowance.index', 'uses' => 'ReportoutcityallowanceController@index']);
	Route::get('/reportoutcityallowance/data', ['middleware' => ['role:reportoutcityallowance|read'], 'as' => 'editor.reportoutcityallowance.data', 'uses' => 'ReportoutcityallowanceController@data']);

	//report absencededuction
	Route::get('/reportabsencededuction', ['middleware' => ['role:reportabsencededuction|read'], 'as' => 'editor.reportabsencededuction.index', 'uses' => 'ReportabsencedeductionController@index']);
	Route::get('/reportabsencededuction/data', ['middleware' => ['role:reportabsencededuction|read'], 'as' => 'editor.reportabsencededuction.data', 'uses' => 'ReportabsencedeductionController@data']);

	//report insentive
	Route::get('/reportinsentive', ['middleware' => ['role:reportinsentive|read'], 'as' => 'editor.reportinsentive.index', 'uses' => 'ReportinsentiveController@index']);
	Route::get('/reportinsentive/data', ['middleware' => ['role:reportinsentive|read'], 'as' => 'editor.reportinsentive.data', 'uses' => 'ReportinsentiveController@data']);

	//report reportmedicalreim
	Route::get('/reportreportmedicalreim', ['middleware' => ['role:reportreportmedicalreim|read'], 'as' => 'editor.reportreportmedicalreim.index', 'uses' => 'ReportreportmedicalreimController@index']);
	Route::get('/reportreportmedicalreim/data', ['middleware' => ['role:reportreportmedicalreim|read'], 'as' => 'editor.reportreportmedicalreim.data', 'uses' => 'ReportreportmedicalreimController@data']);

	//report pph21sum
	Route::get('/reportpph21sum', ['middleware' => ['role:reportpph21sum|read'], 'as' => 'editor.reportpph21sum.index', 'uses' => 'Reportpph21sumController@index']);
	Route::get('/reportpph21sum/data', ['middleware' => ['role:reportpph21sum|read'], 'as' => 'editor.reportpph21sum.data', 'uses' => 'Reportpph21sumController@data']);

	//report exportpph21monthly
	Route::get('/exportpph21monthly', ['middleware' => ['role:exportpph21monthly|read'], 'as' => 'editor.exportpph21monthly.index', 'uses' => 'Exportpph21monthlyController@index']);
	Route::get('/exportpph21monthly/data', ['middleware' => ['role:exportpph21monthly|read'], 'as' => 'editor.exportpph21monthly.data', 'uses' => 'Exportpph21monthlyController@data']);

	//report exportpph21yearly
	Route::get('/exportpph21yearly', ['middleware' => ['role:exportpph21yearly|read'], 'as' => 'editor.exportpph21yearly.index', 'uses' => 'Exportpph21yearlyController@index']);
	Route::get('/exportpph21yearly/data', ['middleware' => ['role:exportpph21yearly|read'], 'as' => 'editor.exportpph21yearly.data', 'uses' => 'Exportpph21yearlyController@data']);

	//report repordocument
	Route::get('/reporttime', ['middleware' => ['role:reporttime|read'], 'as' => 'editor.reporttime.index', 'uses' => 'ReporttimeController@index']);
	Route::get('/reporttime/data', ['middleware' => ['role:reporttime|read'], 'as' => 'editor.reporttime.data', 'uses' => 'ReporttimeController@data']);

	//report repordocument
	Route::get('/reportdocument', ['middleware' => ['role:reportdocument|read'], 'as' => 'editor.reportdocument.index', 'uses' => 'ReportdocumentController@index']);
	Route::get('/reportdocument/data', ['middleware' => ['role:reportdocument|read'], 'as' => 'editor.reportdocument.data', 'uses' => 'ReportdocumentController@data']);

	//report reporpromotion
	Route::get('/reportpromotion', ['middleware' => ['role:reportpromotion|read'], 'as' => 'editor.reportpromotion.index', 'uses' => 'ReportpromotionController@index']);
	Route::get('/reportpromotion/data', ['middleware' => ['role:reportpromotion|read'], 'as' => 'editor.reportpromotion.data', 'uses' => 'ReportpromotionController@data']);

	//report reportraveling
	Route::get('/reporttraveling', ['middleware' => ['role:reporttraveling|read'], 'as' => 'editor.reporttraveling.index', 'uses' => 'ReporttravelingController@index']);
	Route::get('/reporttraveling/data', ['middleware' => ['role:reporttraveling|read'], 'as' => 'editor.reporttraveling.data', 'uses' => 'ReporttravelingController@data']);

	//report reporleaving
	Route::get('/reportleaving', ['middleware' => ['role:reportleaving|read'], 'as' => 'editor.reportleaving.index', 'uses' => 'ReportleavingController@index']);
	Route::get('/reportleaving/data', ['middleware' => ['role:reportleaving|read'], 'as' => 'editor.reportleaving.data', 'uses' => 'ReportleavingController@data']);

	//CHART
	//chart leave
	Route::get('/chartleave', ['middleware' => ['role:chartleave|read'], 'as' => 'editor.chartleave.index', 'uses' => 'ChartleaveController@index']); 
	//chart employee
	Route::get('/chartemployee', ['middleware' => ['role:chartemployee|read'], 'as' => 'editor.chartemployee.index', 'uses' => 'ChartemployeeController@index']); 

	//Cache Clear
	Route::get('/cache', ['as' => 'editor.cache.index', 'uses' => 'CacheController@index']);
	Route::get('/cacheclear', ['as' => 'editor.cache.indexclear', 'uses' => 'CacheController@indexclear']);

	
	//Popup
		//index
	Route::get('/popup', ['middleware' => ['role:popup|read'], 'as' => 'editor.popup.index', 'uses' => 'PopupController@index']); 
		//edit
	Route::get('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.edit', 'uses' => 'PopupController@edit']);
	Route::put('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.update', 'uses' => 'PopupController@update']); 

	//Faq
		//index
	Route::get('/faq', ['middleware' => ['role:faq|read'], 'as' => 'editor.faq.index', 'uses' => 'FaqController@index']);
	Route::get('/faq/data', ['as' => 'editor.faq.data', 'uses' => 'FaqController@data']);
		//create
	Route::get('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'editor.faq.create', 'uses' => 'FaqController@create']);
	Route::post('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'editor.faq.store', 'uses' => 'FaqController@store']);
		//edit
	Route::get('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'editor.faq.edit', 'uses' => 'FaqController@edit']);
	Route::put('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'editor.faq.update', 'uses' => 'FaqController@update']);
		//delete
	Route::delete('/faq/delete/{id}', ['middleware' => ['role:faq|delete'], 'as' => 'editor.faq.delete', 'uses' => 'FaqController@delete']);
	Route::post('/faq/deletebulk', ['middleware' => ['role:faq|delete'], 'as' => 'editor.faq.deletebulk', 'uses' => 'FaqController@deletebulk']);


});
 	

 	//XHR
	Route::get('/getemployee', ['as' => 'get.employee', 'uses' => 'XHRController@get_employee']);
