set @date = '';
set @num  = 1;

SELECT
	id,
	trans_code,
	class, date_time,
@num := if(@date = DATE(date_time), @num + 1, 1) as row_number,
@date := DATE(date_time) as dummy
FROM
	(
		SELECT
			*, "no_label active" AS class
		FROM
			`temp_tally`
		WHERE
			`id` = 31
		UNION ALL
			SELECT
				*, "no_label" AS class
			FROM
				`temp_tally`
			WHERE
				`id` <> 31
			AND `deheading_status` IS NULL
			OR `deheading_status` <> "collect"
			UNION ALL
				SELECT
					*, "no_label complete" AS class
				FROM
					`temp_tally`
				WHERE
					`id` <> 31
				AND `deheading_status` = "collect"
	) AS DERIVEDTBL
ORDER BY trans_code ASC